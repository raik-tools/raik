.. raik_web documentation master file, created by
   sphinx-quickstart on Mon Mar  5 16:50:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
Welcome to raik_web's documentation!
====================================

.. toctree::
    :maxdepth: 1
    :caption: Site Contents:
    :glob:

    team/raik_objective
    enhancements/index
    ideas_capture
    raik/raik_communications_local
    raik_web/dev/*
    interfaces/*
    scratch_pad/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation Status
====================

* I still need to resolve terminology for different layers and features
* Several TODO items in existing documents, needing review


raik_web in context
===================

The RAIK control system is a privately developed system intended to produce an industrial grade
control system using consumer and commodity hardware.  More details exist elsewhere (TODO).

At the same time, I am interested in providing back to the community and am interested in developing
a business model so that RAIK can produce an income to further the development of RAIK.

At the moment, you will see mention of "Pro Features" within this documentation. This is where features
intended for a paying users may be added as part of a fairly common business model.  In most cases
they don't yet exist but are a reminder of future plans and added value.

raik_web forms part of the RAIK control system.  It's role is to provide a miniature but fully featured
web-based HMI that can run on a touch screen device or desktop device.

Features of raik_web include:

TODO: Link to product requirements somewhere and/or user stories.

* Online configurable displays
* Provide an online configuration of the local RAIK controller
* Monitoring and operation of the local RAIK controller


Welcome to Sphinx
=================

This is my first attempt at using Sphinx, so there will be a learning curve.  In the meantime, here are some links
that have been supportive in understanding it:

* https://matplotlib.org/sampledoc/cheatsheet.html#cheatsheet-literal
* http://www.sphinx-doc.org/en/stable/tutorial.html
* http://rest-sphinx-memo.readthedocs.io/en/latest/intro.html
* https://raw.githubusercontent.com/python/devguide/master/documenting.rst

This page contains a fair bit of content that is out of context for the source code, but I see it as being
relevant from the perspective in that it gives information relevant to participation and access to the design and
systems involved with developing RAIK.

Why Sphinx?

* Django uses it
* Everyone uses it in Python land (almost)
* PlantUML

Why not Sphinx?

* Confluence is very easy and quick to use
* See How to Speed up Sphinx below

Features of Interest
--------------------

* http://www.sphinx-doc.org/en/stable/ext/extlinks.html
* http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#field-lists


Plugins of Interest
-------------------

* https://github.com/ignatenkobrain/sphinxcontrib-issuetracker
* Graphviz


A Picture Tells a Thousand Words
--------------------------------

Graphs will be utilised where appropriate.  They are not a replacement for legible and clear code but only serve to
capture complex relationships and interactions.  If the markup for the diagrams is too difficult to maintain or
interpret, then consider avoiding a markup approach and use DrawIO or similar instead.

* Activity Diagrams - These are similar to SFC
* Graphical interface diagrams
* Archimate Diagrams - https://en.wikipedia.org/wiki/ArchiMate
* Sequence Diagram - For communication handshakes / protocols or sequence interactions
* State Diagram - Life cycle and state of entities
* Timing Diagram - Time sensitive interactions
* Use Case Diagram - Describe user interactions (but not to be overused)
* Object Diagram - Uncertain if we will work on these, it's currently supported by PlantUML, will keep an eye on them
* Deployment Diagram - This could be handy for architectural presentation
* Component Diagram
* Class Diagram

How to Speed up Sphinx
======================

I'm trialling Sphinx as an alternative to Confluence (which I'm used to from work).  There would be a couple
of things that would really make this easier:

* Some kind of Auto complete (perhaps in PyCharms) (_Conversation Starter: https://intellij-support.jetbrains.com/hc/en-us/community/posts/360000140844-Sphinx-Completion?page=1#community_comment_360000084390)
* Something to allow me to be a little lazy with headings (and ensure underlines match title length)
* Some kind of table tools.  Buttons or a script that can clean up tables to make sure spacing is consistent and
  presentation is clean.

How to make PyCharms Integration Better
=======================================

Since I use Pycharms Professional as my preferred IDE, here are some notes on how PyCharms could help speed up
Sphinx usage.

* Indentation for directives (like toctree)
* Auto Complete on references and internal links (source code references included)
* Permalink / Anchor recognition
* Auto line wrapping in Rest Documents (Nano does it)
* Auto heading underlines


Project Management
==================

TODO:  Review and update

RAIK Issue tracking was performed in Redmine (by default, it was already available).  However, I believe it's
outgrowing our internal issue tracker so here are some notes (to be removed later) covering different trackers.

In a commercial environment, I have a lot of experience with Confluence / Jira.  The integration of the two make them
stand out significantly.  A review of issue trackers is underway.  From open source, Trac and Taiga are quite
interesting.

This is still a moving target.  The most stable place to keep documentation will likely be in Sphinx.


Project Management Workflow
---------------------------

I'm used to different terminology for time and task management, but am making use of synonymous terminology from the
agile community.  If this doesn't work for management purposes, then it may change.  To keep things pure, since we're
not following Scrum, I've been referencing information from the `Agile Alliance`_, specifically the `Agile Map`_ and
`Agile Glossary`_.


Management parts (bottom up with definitions):

* Tasks:  These are action Items:  Task, Bug, Enhancement.  Typically from 5 minutes to 1 day, something that can be
          realistically completed before the end of the day (so longer task assume you'll be in an environment to
          complete these).
* User Stories:  These are the Outcomes being achieved by a group of tasks.  Key components include:
    * Action Plan (Action Items)
    * Outcome Description (Summary)
    * User Story
    * Purpose Description
    * In addition, they are intended to be based on discussions with Product Owners
* Epics:  These are small projects on their own, typically from 1 to 4 weeks.  These contain user stories and form
          a large story on their own.
* Internal Component - Unfortunately ambiguous, but refers to components within a RAIK System Component
    * It's not really a module, perhaps an Area or sub-component
* RAIK Module - This is a stand alone piece of software comprising of Epics and with a particular vision.
    * Maybe this is best called a module (since modules are interchangeable according to definition)
    * Component is more broader in definition but also includes modules
* RAIK Themes:  TODO, these are areas of continuous improvement across the system.

To Organise:

* Headline Features - I think this is a label given to key user stories or epics.  These are the things we want to
  advertise.
* Product Requirements - Captured in the description of a component and the headline features
* Interface Specifications - This is a document to describe a particular interaction between software layers that is
  expected to be extensible or used in a number of places.  Sits between components or sub components (maybe modules?)
* Points:  Work with points being approximately 15 minutes chunk.  This is the typical allotment of time available to
  progress the software.
* Module description: 4W, Vision, Problem statemnet, purpose
* Product Description:  Overarching / Grand version of Module description
* Headline Features / Enhancement Proposals - Not sure what ot call these.  This starts linking to the task system.
    I'll call them enhancements for now as this seems to tie in closer to other projects.
* Themes (Categories of Improvement)
* Task Query notes:  Look for Action items without "Parent" assignment from Epic, Story or Task.  For Themes, consider
  filtering by theme first and then filter out the remaining (hard query) that don't have a parent within the set


Work Flow

* Tasks, stories and Epics can be entered into the backlog.  User stories are the preferred level of managemnet with
  all task relating to a story.  If not relating to a story, they should still be grouped into a similar outcomes.
* Large User Stories should be turned into Epics
* System Components
* Back log review
    * Action items without parent assignment


Entry Points:

* TODO




.. _Agile Manifesto: http://agilemanifesto.org/
.. _Agile Alliance: https://www.agilealliance.org
.. _Agile Map:  https://www.agilealliance.org/agile101/subway-map-to-agile-practices/
.. _Agile Glossary:  https://www.agilealliance.org/agile101/agile-glossary/


Trac Workflow
-------------

The workflow has the following states:

* Backlog - Tasks that are not on the radar
* Accepted - Task that are on the radar for being performed.  This is a simple buffer.
* In Progress - Tasks that are in progress of being developed and tested
* Closed - Task that are closed and will not be reviewed again

.. uml ::

    [*] --> Backlog : Create
    Backlog --> Closed : Close
    Backlog --> Accepted : Put on radar

    Accepted --> InProgress : Start
    Accepted --> Closed : Close
    Accepted --> Backlog : Take off radar

    InProgress --> Backlog : Stop
    InProgress --> Accepted : Hold
    InProgress --> Closed : Close


    Closed --> [*]
    Closed --> Backlog : Reopen

    note right of Closed
        On close resolution is set to default resolutions
    end note



Tying together documentation and tasks
--------------------------------------

There is an intent to link this to an issue tracker.  Based on research, here is a rough structure on how to organise
everything here:

* Product Requirements
* Major Features / Headline Features?

