# This file abstracts the communication interface from the director to nodes.
# We must be able to change the backend down the road.
from raik.communications import NODE_HB_TIMEOUT, timestamp_seconds, connect_system_key_store


def get_node_status(n_id):
    """
    Retrieves detailed stataus information for a node
    :param n_id:  uuid of the node
    :return: Just the status for the node, per #217, or None if no entry exists
    """
    ks = connect_system_key_store()
    d = ks.get('status:up:' + n_id)
    if d is None:
        return None
    now = timestamp_seconds()
    node_access_time = d['at']
    node_online = (now - node_access_time) < NODE_HB_TIMEOUT
    if node_online:
        return 'Online'
    else:
        return 'Offline'


def get_node_status_detail(n_id):
    """
    Retrieves detailed status information for a node.  This is expected to be called a number of
    times and each call will continue the request for data from the node.  If not called for a
    significant period, the node will stop reporting health metrics.
    :param n_id:  uuid of the node
    :return: Dictionary of system data as per #217, or None if no entry exists
    """
    ks = connect_system_key_store()
    d_up = ks.get('status:up:' + n_id)
    if d_up is None:
        return None
    now = timestamp_seconds()
    node_access_time = d_up['at']
    node_online = (now - node_access_time) < NODE_HB_TIMEOUT
    status = 'Offline'
    metrics = None
    if node_online:
        status_down_id = 'status:down:' + n_id
        d_down = ks.get(status_down_id)
        last_request = d_down and d_down['rt'] or None
        if last_request is None or (now - last_request) >= NODE_HB_TIMEOUT:
            # No valid request exists
            # Update the request on the server.
            status = 'Requested'
            if not d_down:
                d_down = {'rt': now}
            else:
                d_down['rt'] = now
            ks.set(status_down_id, d_down)
        else:
            # A valid request exists, see if there is data
            last_request_read = d_up['rt']
            pay_load = d_up['p']
            if pay_load:
                if now - pay_load['rt'] > NODE_HB_TIMEOUT:
                    # Pay load is out of date
                    pay_load = None
            if not pay_load:
                if last_request_read and last_request_read >= last_request:
                    status = 'Request Read'
                else:
                    # No confirmation that the request has been read yet
                    status = 'Requested'
            else:
                status = 'Online'
                # There is a pay load and it is expected to up to date
                metrics = {
                    'cpu': pay_load.get('cpu'),
                    'up': pay_load.get('up'),
                    't_cpu': pay_load.get('t_cpu'),
                    'net': pay_load.get('net'),
                    'mem': pay_load.get('mem'),
                    'time': pay_load.get('time'),
                    'round_trip': node_access_time - last_request_read
                }
            # Update the request if needed
            if node_access_time >= last_request:
                if not d_down:
                    d_down = {'rt': now}
                else:
                    d_down['rt'] = now
                ks.set(status_down_id, d_down)

    return {'status': status, 'metrics': metrics}
