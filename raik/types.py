from typing import TypeVar, Union, NewType
import decimal

Number = TypeVar('Number', int, float, decimal.Decimal)
