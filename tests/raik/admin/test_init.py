"""
Test the init command
"""
import pathlib

import raik


# noinspection SpellCheckingInspection
def test_create_structure(tmpdir: pathlib.Path):
    """Run the init command, with a given folder name and test the structure created"""
    args = raik.admin.init.InitArgs(base_dir=tmpdir)

    raik.admin.init.main(args)

    assert (tmpdir / "certs").exists()
    assert (tmpdir / "project_lib").exists()
    assert (tmpdir / "project_lib" / "__init__.py").exists()
    assert (tmpdir / "control").exists()
    assert (tmpdir / "control" / "__init__.py").exists()
    assert (tmpdir / "conf.py").exists()
