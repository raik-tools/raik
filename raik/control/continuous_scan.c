#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "pifacedigital.h"
#include "hiredis.h"


int main( int argc, char *argv[] )
{
    unsigned int i, j;          /**< Loop iterator */
    uint8_t inputs;         /**< Input bits (pins 0-7) */
    int hw_addr = 0;        /**< PiFaceDigital hardware address  */
    int interrupts_enabled=0; /**< Whether or not interrupts are enabled  */
    clock_t begin, end;
    double time_spent;

    uint32_t freq = 1000;
    // uint8_t resolution = 100; resolution is usec in this example
    float duty = 0.5;
    int cycle_time, duration, pwm_ticks, remaining;

    /**
     * Read command line value for which PiFace to connect to
     */
    printf("Optional args of duty and then freq");
    if (argc > 1) {
        duty = (float)atof(argv[1]);
        if (argc > 2) {
            freq = atoi(argv[2]);
        }
        // hw_addr = atoi(argv[1]);
    }


    /**
     * Calculate PWM timing in usec
     */
    pwm_ticks = 1000000;
    cycle_time = pwm_ticks / freq;
    duration = (int) (cycle_time * duty);
    remaining = (int) (cycle_time - duration);


    /**
     * Open piface digital SPI connection(s)
     */
    printf("Opening piface digital connection at location %d\n", hw_addr);
    pifacedigital_open(hw_addr);

    printf("Starting PWM for 10 s\n");
    //sleep(1);
    pifacedigital_digital_write(5, 0);
    pifacedigital_digital_write(7, 1);
    j = 10 * pwm_ticks / cycle_time;
    for (i = 0; i < j; i++) {
        // This is not entirely accurate, not taking into account time for commands to be sent.
        // Start a pulse
        pifacedigital_digital_write(6, 1);
        usleep(duration);
        pifacedigital_digital_write(6, 0);
        usleep(remaining);
       // End the pulse
    }
    pifacedigital_digital_write(7, 0);
    printf("Time for 100:  %fs\n", time_spent);

    /**
     * Close the connection to the PiFace Digital
     */
    pifacedigital_close(hw_addr);
}