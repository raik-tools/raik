import raik.enable_testing
import logging
import unittest
import raik.control.functions
import raik.communications.local
# import multiprocessing
# import time
import os
import os.path
import sys
import shutil
# from .utilities import enable_multiprocess_coverage
logging.basicConfig(level=logging.DEBUG)


class TestFunctionScan(unittest.TestCase):
    """
        Tests the function execution
    """
    def setUp(self):
        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_values()
        raik.control.retry_time_medium = 1.0
        raik.control.retry_time_long = 1.0
        # Set up 'standard' config library:
        root_path = '/tmp/control_test'
        if os.path.exists(root_path):
            shutil.rmtree(root_path)
        os.mkdir(root_path)
        os.mkdir(os.path.join(root_path, 'test_lib'))
        with open(os.path.join(root_path, 'test_lib/__init__.py'), 'w') as f:
            f.write('\n')
        with open(os.path.join(root_path, 'test_lib/test_add_two.py'), 'w') as f:
            f.write('def test_add_two(a, b):\n')
            f.write('    x = a + b\n')
            f.write('    return {"x": x}\n# EOF\n')

        # Set up site library
        config_path = os.path.join(root_path, 'raik_control_config')
        os.mkdir(config_path)
        with open(os.path.join(root_path, 'raik_control_config/test_add_three.py'), 'w') as f:
            f.write('def test_add_three(a, b, c):\n')
            f.write('    x = a + b + c\n')
            f.write('    return {"x": x}\n# EOF\n')

        # Make libs accessible
        sys.path.append(root_path)
        self.root_path = root_path
        self.config_path = config_path
        raik.control.functions.set_paths(
            root_path,
            config_path
        )
        self.process = None

    def tearDown(self):
        raik.control.functions.set_paths()
        if self.process is not None:
            self.process.terminate()

    def test_execute_simple_function(self):
        input_map = {'a': 'test_1',
                     'b': 'test_2'}
        output_map = {'x': 'test_3'}
        # Set the input values
        raik.communications.local.set_value('test_1', 7)
        raik.communications.local.set_value('test_2', 35)

        # Compile the function
        function_name = 'add_two'
        input_names = ('a', 'b',)
        output_names = ('x',)
        function_body = 'x = a + b'
        fx_text = raik.control.functions.create_function(function_name, function_body, input_names, output_names)
        fx = raik.control.functions.compile_function(function_name, fx_text)

        # Execute the function
        raik.control.functions.execute_function(input_map, fx, output_map)

        # Read the output values
        x = raik.communications.local.get_value('test_3')
        self.assertEqual(x, 42)

        raik.communications.local.set_value('test_1', 8)
        raik.control.functions.execute_function(input_map, fx, output_map)
        x = raik.communications.local.get_value('test_3')
        self.assertEqual(x, 43)


class TestFunctionCreate(unittest.TestCase):
    """
        The control module needs to be able to dynamically create functions that can be executed with different inputs.

        Test that we can create a function definition script and each test will check that it can execute
        if expected to do so.
    """
    def test_create_add_two(self):
        function_name = 'add_two'
        input_names = ('a', 'b',)
        output_names = ('x',)
        function_body = 'x = a + b'
        end_result = (
            "def add_two(a, b):\n"
            "    x = a + b\n"
            "    return {'x': x}"
        )

        result = raik.control.functions.create_function(function_name, function_body, input_names, output_names)
        self.assertEqual(end_result, result)
        l = {}
        g = {'__builtins__': {}}
        exec(result, g, l)
        self.assertEqual(l['add_two'](2, 4)['x'], 6)

    def test_create_add_and_sub_two(self):
        function_name = 'add_and_sub_two'
        input_names = ('a', 'b',)
        output_names = ('x', 'y',)
        function_body = 'x = a + b\ny = a - b\n'
        end_result = (
            "def add_and_sub_two(a, b):\n"
            "    x = a + b\n"
            "    y = a - b\n"
            "    return {'x': x, 'y': y}"
        )

        result = raik.control.functions.create_function(function_name, function_body, input_names, output_names)
        self.assertEqual(end_result, result)
        l = {}
        g = {'__builtins__': {}}
        exec(result, g, l)
        self.assertEqual(l['add_and_sub_two'](2, 4)['x'], 6)
        self.assertEqual(l['add_and_sub_two'](2, 4)['y'], -2)

    def test_seg_fault_it_generate(self):
        function_name = 'break_it'
        input_names = ('a', 'b',)
        output_names = ('x', 'y',)
        function_body = """
(lambda fc=(
    lambda n: [
        c for c in
            ().__class__.__bases__[0].__subclasses__()
            if c.__name__ == n
        ][0]
    ):
    fc("function")(
        fc("code")(
            0,0,0,0,0,b"KABOOM",(),(),(),"","",0,b""
        ),{}
    )()
)()
"""
        self.assertRaises(
                SyntaxError,
                raik.control.functions.create_function, function_name, function_body, input_names, output_names
        )

    def test_import_in_function(self):
        function_name = 'break_it'
        input_names = ('a', 'b',)
        output_names = ('x', 'y',)
        function_body = 'import sys\nsys.getcwd()\n'

        self.assertRaises(
                SyntaxError,
                raik.control.functions.create_function, function_name, function_body, input_names, output_names
        )

    def test_compile_function(self):
        function_name = 'add_two'
        input_names = ('a', 'b',)
        output_names = ('x',)
        function_body = 'x = a + b'
        end_result = (
            "def add_two(a, b):\n"
            "    x = a + b\n"
            "    return {'x': x}"
        )

        result = raik.control.functions.create_function(function_name, function_body, input_names, output_names)
        self.assertEqual(end_result, result)
        fx = raik.control.functions.compile_function(function_name, result)
        self.assertEqual(fx(2, 4)['x'], 6)
