"""
A Flask template / maker that can be deployed using server and client
certificates.

The plan is to use any one of the following servers:

* uWSGI (Not yet supported): https://uwsgi-docs.readthedocs.io/en/latest/
* Gunicorn

This will eventually be a parent function block template to allow
quick and simple creation of an API server.

Usage:

class CustomServer(APIServer):

    def handle_fetch_data(self):
        "This will route to /fetch/data"
        return a dictionary which will be converted to JSON.

"""
import json


class SimpleApiServer:

    def __init__(self, key_path, cert_path, chain_path, bind_address="127.0.0.1", listen_port=8443,
                 timeout=30):
        from flask import Flask, request

        self.listen_port = listen_port
        self.bind_address = bind_address
        self.chain_path = chain_path
        self.cert_path = cert_path
        self.key_path = key_path
        self.timeout = timeout

        self.app = Flask(self.__class__.__name__)
        for attribute_name in dir(self):
            if attribute_name.startswith("handle"):
                attribute = getattr(self, attribute_name)
                if callable(attribute):
                    route = "/" + "/".join(attribute_name[7:].split("__"))

                    def _view_wrapper():
                        if request.method == "POST":
                            return attribute(form=request.form)
                        else:
                            return attribute()

                    self.app.add_url_rule(route, view_func=_view_wrapper, methods=['GET', 'POST'])

    def debug_run(self):
        self.app.run(debug=True)

    # noinspection PyAbstractClass
    def gunicorn_run(self):
        """
        Blocking Run Server Command using gunicorn
        :return:
        """
        import gunicorn.app.base
        parent_self = self

        class StandaloneApplication(gunicorn.app.base.BaseApplication):

            def __init__(self, app):
                import ssl
                # noinspection SpellCheckingInspection
                self.options = {
                    'bind': '%s:%i' % (parent_self.bind_address, parent_self.listen_port),
                    'workers': 1,
                    'timeout': parent_self.timeout,
                    'keyfile': str(parent_self.key_path),
                    "certfile": str(parent_self.cert_path),
                    "cert_reqs": ssl.CERT_REQUIRED,
                    "ca_certs": str(parent_self.chain_path),
                    "ssl_version": ssl.PROTOCOL_TLSv1_2,
                    "ciphers": (
                        "TLS_AES_256_GCM_SHA384:"
                        "TLS_CHACHA20_POLY1305_SHA256:"
                        "TLS_AES_128_GCM_SHA256:"
                        "TLS_AES_128_CCM_SHA256:"
                        "ECDHE-ECDSA-AES256-GCM-SHA384:"
                        "ECDHE-RSA-AES256-GCM-SHA384:"
                        "ECDHE-ECDSA-CHACHA20-POLY1305:"
                        "ECDHE-RSA-CHACHA20-POLY1305:"
                        "ECDHE-ECDSA-AES256-CCM:"
                        "ECDHE-ECDSA-AES128-GCM-SHA256:"
                        "ECDHE-RSA-AES128-GCM-SHA256:"
                        "ECDHE-ECDSA-AES128-CCM:"
                        "ECDHE-ECDSA-AES128-SHA256:"
                        "ECDHE-RSA-AES128-SHA256:"
                        "ECDHE-ECDSA-AES256-SHA:"
                        "ECDHE-RSA-AES256-SHA:"
                        "ECDHE-ECDSA-AES128-SHA:"
                        "ECDHE-RSA-AES128-SHA:"
                        "AES256-GCM-SHA384:"
                        "AES256-CCM:"
                        "AES128-GCM-SHA256:"
                        "AES128-CCM:"
                        "AES256-SHA256:"
                        "AES128-SHA256:"
                        "AES256-SHA:"
                        "AES128-SHA:"
                        "DHE-RSA-AES256-GCM-SHA384:"
                        "DHE-RSA-CHACHA20-POLY1305:"
                        "DHE-RSA-AES256-CCM:"
                        "DHE-RSA-AES128-GCM-SHA256:"
                        "DHE-RSA-AES128-CCM:"
                        "DHE-RSA-AES256-SHA256:"
                        "DHE-RSA-AES128-SHA256:"
                        "DHE-RSA-AES256-SHA:"
                        "DHE-RSA-AES128-SHA:"
                        "PSK-AES256-GCM-SHA384:"
                        "PSK-CHACHA20-POLY1305:"
                        "PSK-AES256-CCM:"
                        "PSK-AES128-GCM-SHA256:"
                        "PSK-AES128-CCM:"
                        "PSK-AES256-CBC-SHA:"
                        "PSK-AES128-CBC-SHA256:"
                        "PSK-AES128-CBC-SHA:"
                        "DHE-PSK-AES256-GCM-SHA384:"
                        "DHE-PSK-CHACHA20-POLY1305:"
                        "DHE-PSK-AES256-CCM:"
                        "DHE-PSK-AES128-GCM-SHA256:"
                        "DHE-PSK-AES128-CCM:"
                        "DHE-PSK-AES256-CBC-SHA:"
                        "DHE-PSK-AES128-CBC-SHA256:"
                        "DHE-PSK-AES128-CBC-SHA:"
                        "ECDHE-PSK-CHACHA20-POLY1305:"
                        "ECDHE-PSK-AES256-CBC-SHA:"
                        "ECDHE-PSK-AES128-CBC-SHA256:"
                        "ECDHE-PSK-AES128-CBC-SHA"
                    )
                }
                self.application = app
                super(StandaloneApplication, self).__init__()

            def load_config(self):
                config = dict([(key, value) for key, value in self.options.items()
                               if key in self.cfg.settings and value is not None])
                for key, value in config.items():
                    self.cfg.set(key.lower(), value)

            def load(self):
                return self.application

        StandaloneApplication(self.app).run()


class DemoApiServer(SimpleApiServer):

    def handle(self):
        return "Hello World from %s" % self.__class__.__name__

    # noinspection PyMethodMayBeStatic
    def handle_hello_world(self):
        return json.dumps({"Hello": "World"})


def main():
    create_ca = False
    # Duplicate subjects will exist
    # add unique_subject = no to index.attr
    create_server_and_client = False

    # At one point Chrome was saying the cert was untrusted, so I removed
    # all certs and tried again (ROOT CA CErt that is)

    # I removed underscores from the CA name and I'm getting a different
    # chrome error now

    # Chrome wants subjectAlternateName

    # Firefox works,

    # # Create a CA
    import pathlib
    import os
    import raik.utils.crypt
    import shutil

    certificates = pathlib.Path(os.getcwd()) / "certificates"
    root_ca = certificates / "root_ca"
    i_ca = certificates / "int_ca"
    clients_cert = certificates

    s_private = clients_cert / "api.tnet.private.pem"
    s_cert = clients_cert / "api.tnet.pem"
    chain_path = i_ca / "certs" / "ca-chain.cert.pem"
    # i_path = i_ca / "certs" / "ca-chain.cert.pem"

    if create_ca:
        if root_ca.exists():
            shutil.rmtree(root_ca)
        root_ca.mkdir()
        if i_ca.exists():
            shutil.rmtree(i_ca)
        i_ca.mkdir()
        raik.utils.crypt.create_root_ca(str(root_ca))
        chain_path = raik.utils.crypt.create_intermediate_ca(root_ca, i_ca, "Intermediate1")

    if create_server_and_client or create_ca:
        if clients_cert.exists():
            shutil.rmtree(clients_cert)
        clients_cert.mkdir()
        try:
            s_private, s_cert = raik.utils.crypt.create_server_certificate(
                i_ca, clients_cert, "api.tnet"
            )
            raik.utils.crypt.create_client_certificate(
                i_ca, clients_cert, "xps"
            )
        except Exception as e:
            print(e)
            import traceback
            print(traceback.print_exc())
            # noinspection PyUnresolvedReferences
            print(' '.join(e.cmd))
            raise

    # Create Demo Server
    x = DemoApiServer(
        key_path=s_private,
        cert_path=s_cert,
        chain_path=chain_path
    )
    x.gunicorn_run()

    # noinspection SpellCheckingInspection
    """
        # There's been a lot of effort to get the client and server certificates to work
        # with Chrome.  Specific additions needed for Chrome to be happy:
        #
        # * subjectAltName
        # * Special Ciphers
        
        # References:  
        https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https
        
        # Openssl commands to test
        # 
        openssl s_client --connect api.tnet:8443 \
        -cert_chain ./i_ca/certs/ca-chain.cert.pem \
        -CAfile ./root_ca/certs/ca.cert.pem \
        -cert ./clients_certs/xps.pem \
        -key ./clients_certs/xps.private.pem
         
        
        # https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate?page=2&tab=Votes
        
        # Finally got Chrome to work, but used certutil to add cert and not chrome GUI:
        # List: certutil -d sql:$HOME/.pki/nssdb -L
        # Delete:  certutil -d sql:$HOME/.pki/nssdb -D -n "RAIK Test Root CA - RAIK"
        # Add Root CA: certutil -d sql:$HOME/.pki/nssdb -A -t "C,C,C" -n "RAIK Test Root CA - RAIK" 
                                -i ./tmp/root_ca/certs/ca.cert.pem
        # Restart Chrome!
        # This is wrong:  
            https://chromium.googlesource.com/chromium/src/+/master/docs/linux_cert_management.md
        #
        # Add a client cert:  pk12util -d sql:$HOME/.pki/nssdb -i ./clients_certs/xps.pfx
        #
        # openssl x509 -text -in ./clients_certs/api.tnet.pem
        # openssl req -in ./clients_certs/api.tnet.private.pem.csr -noout -text
        
        # Netscape type:  SSL Server vs SSL Client
        """


if __name__ == "__main__":
    main()
