"""
A function for sending a signed message as a broadcast command across the local network.
"""
import logging
logger = logging.getLogger('RAIK_COMMS:send_signed_anon')

