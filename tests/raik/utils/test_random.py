import unittest
from raik.utils.random import *

import random
random.seed()


class TestRandomMethods(unittest.TestCase):
    def test_get_true_random(self):
        # This is just testing a return value was achieved, rather than the quality of randomness
        b = get_true_random(1000)
        self.assertEqual(len(b), 1000)

    def test_get_random_double(self):
        for i in range(1000):
            d = get_random_double()
            self.assertIsInstance(d, float)
            self.assertGreaterEqual(d, 0.0)
            self.assertLessEqual(d, 1.0)

        for i in range(1000):
            d = get_random_double()
            self.assertIsInstance(d, float)
            self.assertGreaterEqual(d, 0.0)
            self.assertLessEqual(d, 1.0)

    def test_get_random_int(self):
        for i in range(1000):
            l = random.randint(0, 10000000)
            u = random.randint(l, 10000000)
            d = get_random_int(l, u)
            self.assertIsInstance(d, int)
            self.assertGreaterEqual(d, l)
            self.assertLessEqual(d, u)

    def test_get_random_length_bytes(self):
        # Thorough testing isn't needed as all the functions this method uses are tested
        l = random.randint(0, 10000)
        u = random.randint(l, 10000)
        d = get_random_length_bytes(l, u)
        self.assertIsInstance(d, bytes)
        self.assertGreaterEqual(len(d), l)
        self.assertLessEqual(len(d), u)

    def test_get_random_ipv6_private_address(self):
        # Thorough testing isn't needed as all the functions this method uses are tested
        a = get_random_ipv6_private_address()
        h = '%X' % a
        self.assertEqual(len(h), 8*4)
        self.assertEqual(h[:2], 'FD')
        import ipaddress
        try:
            _ = ipaddress.ip_address(a)
        except ValueError:
            self.assert_(False)
        try:
            _ = ipaddress.IPv6Address(a)
        except ValueError:
            self.assert_(False)

        a = get_random_ipv6_private_address(as_string=True)
        self.assertEqual(len(a), 8*4+7)
        self.assertEqual(h[:2], 'FD')
        try:
            _ = ipaddress.ip_address(a)
        except ValueError:
            self.assert_(False)
        try:
            _ = ipaddress.IPv6Address(a)
        except ValueError:
            self.assert_(False)


    def test_get_random_mac_address(self):
        a = get_random_mac_address()

        self.assertEqual(len(a), 17)
        self.assertEqual(a.count('-'), 5)

if __name__ == '__main__':
    unittest.main()
