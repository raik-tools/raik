import unittest
from .. import System
import stat
import shutil
import os
import importlib


class TestScanModule(unittest.TestCase):
    def setUp(self):
        importlib.reload(System)
        temp_cmd = '/tmp/raik_test_cmd'
        self.temp_cmd = temp_cmd
        if os.path.exists(temp_cmd):
            self.tearDown()
        os.makedirs(temp_cmd)
        os.environ['PATH'] += ':' + temp_cmd

    def tearDown(self):
        os.environ['PATH'] = os.environ['PATH'].replace(':' + self.temp_cmd, '')
        shutil.rmtree(self.temp_cmd)

    def sim_rpi_system_stats(self):
        # System.hw_type = 'rpi'
        script_name = os.path.join(self.temp_cmd, 'vcgencmd')
        with open(script_name, 'w') as f:
            f.write(
                '#!/bin/bash\n'
                'echo "temp=41.6\'C"\n'
            )
        st = os.stat(script_name)
        os.chmod(script_name, st.st_mode | stat.S_IEXEC)
        importlib.reload(System)

    def test_basic(self):
        ret = System.scan_module()
        self.assertIsInstance(ret, dict)
        self.assertIn('cpu/utilization', ret)
        self.assertIn('mem/utilization', ret)
        self.assertIn('cpu/temp', ret)

    def rpi_system_check(self):
        ret = System.scan_module()
        self.assertIsInstance(ret, dict)
        self.assertIn('cpu/utilization', ret)
        self.assertIn('mem/utilization', ret)
        self.assertIn('cpu/temp', ret)

    @unittest.skipIf(System.hw_type != 'rpi', 'Test can only run on a RPI')
    def test_hw_type_rpi(self):
        self.rpi_system_check()

    @unittest.skipIf(System.hw_type == 'rpi', 'Test only needs to run when simulating RPI')
    def test_sim_rpi(self):
        self.sim_rpi_system_stats()
        self.rpi_system_check()
