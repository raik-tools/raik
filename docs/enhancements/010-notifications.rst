=============
Notifications
=============

:Enhancement: 10
:Keyword: E010-notifications
:Type: Feature
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:


Overview
========

The Notifications system is a standard feature found in any SCADA and DCS system.  Notifications
are required to be reliable and perform as quickly as possible.

This system needs to be designed such that it makes compliance with industry best practices (ie ASM, EEMUA) easy.

Background
----------

This documentation was originally captured in Redmine #367

Status Comments
===============

This document is still in draft.

TODO:  It would make a lot of sense to link to requirements documentation or some kind of problem analysis
TODO:  Maybe this is component documentation?

More TODO Items:

* Close sockets on auth fail (403).  These seem to remain open.  This applies to streaming as well


Can close out Redmine.

References
==========

* ASM - TODO
* EEMUA - TODO
* ISA - TODO
* :doc:`Notifications Interface </interfaces/notifications>`

Guidelines for usage
====================

There are many industrial standards and best-practices for the use of notification and alarming systems.  The key
message is to ensure the system is not abused.  All systems raising notifications should do so at a the lowest
priority possible while still addressing any risks associated with the notification.  This applies to system
notifications as well as those configured by end users.

Features
========

* Notification Priorities
* Acknowledgement Models


Pro Features
============

Planned professional features include:
* Automatic auditing and reporting of all system and configured notifications to ensure they meet best practice guide-lines.

TODO:  Link to Backlog when available.

Definition
==========

A notification is an event which is intended to perform an alert intended for potential human inspection or interaction.
Examples of notifications include:

* Logging events (debug, info, warning, etc...)
* Alarm events

Not all notifications are required to be immediately accessed by a human, however, they may be accessed to review
system performance and state.

Notifications are not system events that are not intended as a means of communication of system state to a human.


Scope
=====

The notifications system is will interact with various components of a RAIK system:

* RAIK Scanning Logic
* RAIK Services
* An Event Storage System
* A HMI system for human acknowledgment

The notifications system will provide a common interface for system access to

* Raise an notification
* Listen to notifications
* Listen to notification life cycle events (eg. acknowledged)

The notification system will provide an interface for HMI such that notifications can be interacted with by humans:

* View and filter notifications
* Acknowledge notifications

Specification
=============

# TODO:  Migrate information from Redmine to here

Notification life-cycle
-----------------------

There are two cases of notification:  Those requiring acknowledgement and those not requiring acknowledgement.

In combination, the life-span of a notification can either be instant or a longer duration.


.. uml ::

    [*] --> New : Raise Event
    New --> Closed : Instant
    New --> AwaitingAcknowledge : Instant with Ack.
    AwaitingAcknowledge --> Closed : Instant Acknowledged
    New --> Active
    Active --> Closed : Ack not needed
    Active --> AwaitingAcknowledge : Ack Required
    Closed --> [*]


Location
========

The Notification interface can be found in the following locations:

* :py:mod:`raik.communications.local`



Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.