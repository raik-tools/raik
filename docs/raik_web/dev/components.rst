==========
Components
==========

First and foremost, care needs to be taken on a risk basis in the development choices for each component of the raik
display system.  Both E002 and development policies (minimal dependencies) need to be observed.

This document exists because of components developed in the past that went unused.  This is inefficient and annoying.

The basis of these components is the mockup design.

Display
=======

This is a summary, see :doc:`displays` for more info.  The display has two parts:

* the live display used for operation and monitoring which will comprise of all custom logic (minimal to no
    dependencies apart from Django).

.. note::

    The live display should minimise on JS code and size to improve performance and maintenance.  However, the alarm
    list may require more complex operations and it is possible it will pull in third party dependencies for the front
    end.  If this is the case, it might be best to have a simple alarm summary in the live view and then detailed alarm
    investigation should be sectioned off to an independent window.

* display editing where some carefully chosen dependencies will be used to improve usability and speed up development.

The live displays are a container that provide the infrastructure to deliver:

* :doc:`/enhancements/010-notifications` - This is presented as a visible alarm widget showing current state which when
    clicked will produce an alarm list.  Currently this is a drop down but might change to its own window.
* :doc:`/enhancements/011-streaming` - This is not visible directly but produces the mechanism for all widgets to
    display data.

This component consists of:

* template:  display.html and display.js
* Functions:

    * :func:`raik_local.views.build_display_context`
    * :func:`raik_local.views.display`

There are different types of displays, currently on the map are:

* Dashboard style which uses flexbox to lay out widgets
* Graphical style which uses an SVG editor and is to be defined.

Display Editors
---------------

This is an extension to the display that allows modification.

This component is found with the following additional items:

    * :func:`raik_local.views.display_edit`


Widgets
=======

This is a summary, see :doc:`display_widgets` for more info.  As far as dependencies are concerned, these are similar
to displays with a live version and an editing version.


Widget Editor
-------------

This is an extension to the display that allows modification.

This component is found with the following additional items:

    * :func:`raik_local.views.widget_query`
    * :module:`raik_local.raik_display_widgets`

IMTable
=======

IMTable is a component being designed to be an Interactive mapping table.

Why?
----

Well, DataTable.js is already an excellent library, except it relies on JQuery which although is excellent; I find
jQuery is more like an extension to JS that doesn't provide opinion and doesn't make it as easy to modularise code like
React or Vue.js would.

Also, this was an opportunity to experience vue.js which is a framework that focuses more on better programming practice
(eg, https://vuejs.org/v2/guide/index.html#Composing-with-Components)

Scope
-----

This component is now a WIP.  It's also replacing some legacy partly used libraries.

Looking at present mock-ups, the first minimal version of it should include:

* Receive dictionary data and display the results ordered as requested in a table
* Have an editable option which allows add and delete rows with editing of rows performed by providing sample forms

    * Note that this should integrate easily to Django formsets
    * Editing can be inline or with a pop-up
    * Editing can be a new browser window (instead of modal as modals annoy me)

* More to come, considerations include:

    * Autocomplete support for cells (handled at a lower component level)

* Be extensible.  Unsure how yet.

Design
------

Currently, these are planned to be a vue.js app.  Component tree looks as follows:

.. uml ::

    MainContainer "1" *-- "many" Buttons
    MainContainer "1" *-- "1" Table
    Table "1" *-- "1" Body
    Table "1" *-- "1" Header
    Body "1" *-- "many" BodyRow
    BodyRow "1" *-- "many" BodyCell
    BodyCell "1" *-- "1" CellContent
    CellContent "1" *-- "1" StringContent
    CellContent "1" *-- "1" NumberContent
    CellContent "1" *-- "1" CheckboxContent
    CellContent "1" *-- "1" SelectContent
    CellContent "1" *-- "1" TextFieldContent
    Header "1" *-- "1" HeaderRow
    class HeaderRow {
    }
    HeaderCell "1" *-- "1" CellContent

I don't know what's going on with this diagram, it just doesn't render the HeaderRow relationship.

PyCSS
=====

The idea here is a pure python CSS builder where your style is developed in python and it produces CSS.

Alternatives include SASS and LESS, but these require other systems.  Also, these produce their own
syntax on top of CSS, 



System Displays and Configuration
=================================

These are a series of displays used by RAIK_WEB for configuring raik.  Currently, these are composed using
all existing components above.  In many cases, the early versions will be created using the display and
widget editors and then the code will be automatically generated afterwards.

* Local I/O View

    * A dashboard display
    * Hardware SVG widget

* System View

    * Dashboard Display
    * Table and Graph widgets

* RAIK Neighbourhood view

    * Dashboard display
    * RAIK Node SVG widget

* System I/O View, display list view and process view

    * Dashboard layout
    * IMTable for viewing and editing configuration.

* Function editor

    * IMTable for I/O definition and module selection

* Graphic Object Editor

    * IMTable for property animation