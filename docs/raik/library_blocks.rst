==============
Library Blocks
==============

Import
======

Library objects at the moment are only in one level.  Each programming
unit (Function / Function Block) will get it's own python module.

Importing will be at the top level, where the import mechanism will
be able to also apply some safety checks (WIP).

Functions
=========

Functions are quite simple, they are literally a python function,
they take inputs and produce outputs.  There isn't much to say about
them except that they could strip connections of their connection
meta data (see connections below).

Function Blocks
===============

Function blocks are objects with state, but work in restricted fashion.
They have inputs and state.  Inputs and State variables, under the
bonnet, implement a Connection class which is used to track relationships
between blocks and allow Function Blocks more advanced capabilities.

Function Blocks are declared with class level annotated variables and
inherits from the FunctionBlock base class.  A metaclass exists in the
hierarchy that converts each class annotation into a property with
an internal representation using a Connection class.

Outside of a Function block's method, access to properties is allowed
but Input properties (those using any of the InputTypes annotations)
can only be set, not read.  Conversely, State properties can only
be read and not set.

Inside a function block, access to properties is provided by way of
function parameters (given an input and state parameter).  These
parameters allow reading of inputs (but not writing) and allow
read / write of state variables.

Declaring a Function Block
==========================

Use type annotations on class variables,  no dunder methods are
allowed.

For state variables, and type is fine.  For input variables, the
Input types need to be used from the raik.types library.

For events to be called on change of an input or state variable, it
is possible to create on_* functions which will get triggered on
change.

Docstring for variables...

Connection
==========

A connection object is managed by the FunctionBlock base class to
allow reference tracking to occur when blocks are connected.

A number of leading DCSs (and PLCs) on the market have higher order
inputs and indirect referencing capabilities, so this object is
added now to provide growth.

Special Types
=============

InputLatch
----------

This is an input type that is useful for a event driven patterns on top
of a scan pattern.  The input latch needs a rising edge to set, and then
will reset automatically on read (for now).

More
----

More blocks may exist in the source code, this is a WIP


Developer Notes
===============

Performance
-----------

Although no testing was done, dataclasses was reviewed and instead of
using a metaclass, I made a version that uses exec and string building
to create the FunctionBlock child class's properties and init function.

This really sucked for debugging.  So for now, we'll use an explicit
approach and if performance is a concern, we have a backup pattern.

This test pattern used a decorator, but the result was not IDE friendly,
especially when using a decorator to add inheritance (like __call__).

Future
------

* Connections could be used to determine scan order within a scope,
  such as a program scope.

Program Structure Notes
=======================

Still a work in progress.  For scan purposes, it makes sense to have the
base FunctionBlock set a __call__ method and just call an entire block.
This would be the recommended approach for connecting blocks for now
within a script environment.

Although, pre-setting the connections might be more efficient.  Especially
with the connection object.