from raik.utils.google import read_sheet_range

if __name__ == "__main__":
    print(
        read_sheet_range(
            credentials_json="test.json",
            spreadsheet_id='1zvEl9gOmfL-dkzFR8oy-B2gPSWrL7idCVJKMX9dBl1w',
            key_range='Sheet1!A:B',
            heading_row=True
        )
    )
