"""
Certificate admin interface.

For a local RAIK ecosystem both private and public keys
are on the same system, so currently these commands just
focus on the current needs.

From raik_runtime:

* Create Root CA (root_path)
* Create Intermediate CA (signing_root, intermediate path, name)
* Create a server certificate (signing_ca, cert_folder, fqdn)
* Create a client certificate (signing_ca, cert_folder, fqdn)

"""
import logging
import pathlib
import shutil
from typing import List, Any

import tabulate

from ._argparse import command_subparsers
import raik.utils.crypt

logger = logging.getLogger(__name__)


cert_parser = command_subparsers.add_parser(
    name="certs",
    description="Help manage a RAIK certificate ecosystem",
    help="Certificate and Certificate Authority Commands",
)

root_sub_parser = cert_parser.add_subparsers(
    title="Certificate Authority Commands", metavar="", dest="ca_command"
)

# ----------------------------------------------------------------------
# Root CA Commands
# ----------------------------------------------------------------------


root_ca_parser = root_sub_parser.add_parser(
    name="root", description="Root CA Commands", help="Root CA Commands"
)

root_ca_parser.add_argument(
    dest="root_command", choices=["create", "list"], help="Root CA Command."
)

root_ca_parser.add_argument(
    "--path",
    help="Root CA Path",
    default="./certificates/root_ca",
    dest="root_ca_path",
    type=pathlib.Path,
)

root_ca_parser.add_argument(
    "--delete",
    help="Delete Root CA if it already exists",
    default=False,
    action="store_true",
)

# ----------------------------------------------------------------------
# Intermediate CA Commands
# ----------------------------------------------------------------------

int_ca_parser = root_sub_parser.add_parser(
    name="int", description="Intermediate CA Commands", help="Intermediate CA Commands"
)

int_ca_parser.add_argument(
    dest="int_command", choices=["create", "list"], help="Intermediate CA Command."
)

int_ca_parser.add_argument(
    "--path",
    help="Intermediate CA Path",
    default="./certificates/int_ca",
    dest="int_ca_path",
    type=pathlib.Path,
)

int_ca_parser.add_argument(
    "--name", help="Intermediate CA Name", default="IntermediateCA", dest="int_ca_name"
)

int_ca_parser.add_argument(
    "--root",
    help="Root CA Path",
    default="./certificates/root_ca",
    dest="root_ca_path",
    type=pathlib.Path,
)

int_ca_parser.add_argument(
    "--delete",
    help="Delete Intermediate CA if it already exists",
    default=False,
    action="store_true",
)

# ----------------------------------------------------------------------
# Server Cert Commands
# ----------------------------------------------------------------------

server_cert_parser = root_sub_parser.add_parser(
    name="server", description="Server Cert Commands", help="Server Cert Commands"
)

server_cert_parser.add_argument(
    "server_command", choices=["create", "renew", "revoke"], help="Server Cert Command"
)

server_cert_parser.add_argument("name", help="Server Name (FQDN)")

server_cert_parser.add_argument(
    "--path",
    help="Certificates Path",
    default="./certificates/",
    dest="certs_path",
    type=pathlib.Path,
)

server_cert_parser.add_argument(
    "--signing_ca",
    help="Signing CA Path (default Int CA)",
    default="./certificates/int_ca",
    dest="signing_ca_path",
    type=pathlib.Path,
)

# ----------------------------------------------------------------------
# Server Cert Commands
# ----------------------------------------------------------------------

client_cert_parser = root_sub_parser.add_parser(
    name="client", description="Client Cert Commands", help="Client Cert Commands"
)

client_cert_parser.add_argument(
    "client_command", choices=["create", "renew", "revoke"], help="Client Cert Command"
)

client_cert_parser.add_argument(
    "name", help="Client Name (any although FQDN would be great)"
)

client_cert_parser.add_argument(
    "--path",
    help="Certificates Path",
    default="./certificates/",
    dest="certs_path",
    type=pathlib.Path,
)

client_cert_parser.add_argument(
    "--signing_ca",
    help="Signing CA Path (default Int CA)",
    default="./certificates/int_ca",
    dest="signing_ca_path",
    type=pathlib.Path,
)


# ----------------------------------------------------------------------
# Misc Cert Commands
# ----------------------------------------------------------------------

misc_cert_parser = root_sub_parser.add_parser(
    name="misc", description="Misc Cert Commands", help="Misc Cert Commands"
)

misc_cert_parser.add_argument(
    "--print_pem_file",
    help="Prints the contents of a pem file",
    dest="print_pem_file",
    type=pathlib.Path,
)


def _map_index_entry_to_row(entry: raik.utils.crypt.OpenSslIndexEntry) -> List[Any]:
    cert_cn_pos = entry.cert_distinguished_name.index("CN=")
    return [
        entry.serial,
        entry.status,
        entry.expiration_date,
        entry.cert_distinguished_name[cert_cn_pos:],
        entry.pem_details.is_ca,
        entry.pem_details.server_auth,
        entry.pem_details.client_auth,
    ]


def _print_ca_certs(ca_path: pathlib.Path):
    ca_certs = raik.utils.crypt.list_ca_certificates(ca_path)
    headers = [
        "Serial",
        "Status",
        "Expiration Date",
        "Common Name",
        "CA",
        "Server",
        "Client",
    ]
    rows = map(_map_index_entry_to_row, ca_certs)
    print(tabulate.tabulate(tabular_data=rows, headers=headers))


def _map_pem_to_row(entry: raik.utils.crypt.PemFileDetails) -> List[Any]:
    cert_cn_pos = entry.subject.index("CN=")
    issuer_cn_pos = entry.issuer.index("CN=")
    return [
        entry.file_path,
        entry.type,
        entry.serial_number,
        entry.subject[cert_cn_pos:],
        entry.issuer[issuer_cn_pos:],
        entry.start_date,
        entry.end_date,
        entry.is_ca,
        entry.server_auth,
        entry.client_auth,
    ]


def _print_pem_contents(pem_path: pathlib.Path):
    ca_certs = raik.utils.crypt.load_pem_file_details(pem_path)
    headers = [
        "Path",
        "Type",
        "Serial Number",
        "Common Name",
        "Issuer CN",
        "Start Date",
        "End Date",
        "CA",
        "Server",
        "Client",
    ]
    rows = map(_map_pem_to_row, ca_certs)
    print(tabulate.tabulate(tabular_data=rows, headers=headers))


def main(args):
    cmd = args.ca_command
    if cmd == "root":
        cmd = args.root_command
        if cmd == "create":
            # Create a Root CA
            if args.delete and args.root_ca_path.exists():
                logger.info("Deleting existing root ca at %s", args.root_ca_path)
                shutil.rmtree(args.root_ca_path)
            if not args.root_ca_path.exists():
                args.root_ca_path.mkdir(parents=True)
                raik.utils.crypt.create_root_ca(
                    args.root_ca_path, suppress_output=False
                )
                logger.info("Root CA created at %s", args.root_ca_path)
            else:
                raise ValueError("Root CA already exists")
        elif cmd == "list":
            # List Certificates in server
            if not args.root_ca_path.exists():
                raise ValueError("Root CA does not exist")
            else:
                _print_ca_certs(args.root_ca_path)
        else:
            raise KeyError(cmd)
    elif cmd == "int":
        cmd = args.int_command
        if cmd == "create":
            # Create an Intermediate CA
            if not args.root_ca_path.exists():
                raise ValueError("Root CA not found: %s" % str(args.root_ca_path))
            if args.delete and args.int_ca_path.exists():
                logger.info("Deleting existing intermediate ca at %s", args.int_ca_path)
                shutil.rmtree(args.int_ca_path)
            if not args.int_ca_path.exists():
                args.int_ca_path.mkdir(parents=True)
                chain_file = raik.utils.crypt.create_intermediate_ca(
                    args.root_ca_path,
                    args.int_ca_path,
                    args.int_ca_name,
                    suppress_output=False,
                )
                logger.info(
                    "Intermediate CA created at %s.\n\t Chain File: %s",
                    args.int_ca_path,
                    chain_file,
                )
            else:
                raise ValueError(
                    "Intermediate CA already exists at %s" % str(args.int_ca_path)
                )
        elif cmd == "list":
            # List Certificates in server
            if not args.root_ca_path.exists():
                raise ValueError("Intermediate CA does not exist")
            else:
                _print_ca_certs(args.int_ca_path)
        else:
            raise KeyError(cmd)
    elif cmd == "server":
        cmd = args.server_command
        if not args.signing_ca_path.exists():
            raise ValueError("Signing CA not found: %s" % str(args.signing_ca_path))
        if cmd == "create":
            s_private, s_cert = raik.utils.crypt.create_server_certificate(
                args.signing_ca_path, args.certs_path, args.name, suppress_output=False
            )
            logger.info(
                "Server Certificate created.\n\t"
                "Private Key: %s\n\t"
                "Certificate: %s",
                str(s_private),
                str(s_cert),
            )
        # TODO:  Renewal is just a matter of using the old CSR and creating a new
        # certificate.
        elif cmd == "renew":
            raise NotImplementedError(
                "I started it, but my root CA expired anyway... will implement later"
            )
        #     s_private, s_cert = raik.utils.crypt.create_server_certificate(
        #         args.signing_ca_path, args.certs_path, args.name, suppress_output=False
        #     )
        #     logger.info(
        #         "Server Certificate created.\n\t"
        #         "Private Key: %s\n\t"
        #         "Certificate: %s",
        #         str(s_private),
        #         str(s_cert),
        #     )
        elif cmd == "revoke":
            raik.utils.crypt.revoke_server_certificate(
                args.signing_ca_path, args.name, suppress_output=False
            )
        else:
            raise KeyError(cmd)
    elif cmd == "client":
        cmd = args.client_command
        if not args.signing_ca_path.exists():
            raise ValueError("Signing CA not found: %s" % str(args.signing_ca_path))
        if cmd == "create":
            s_private = raik.utils.crypt.create_client_certificate(
                args.signing_ca_path, args.certs_path, args.name, suppress_output=False
            )
            logger.info(
                "Client Certificate created.\n\t" "Private Key: %s", str(s_private)
            )
        elif cmd == "revoke":
            raik.utils.crypt.revoke_server_certificate(
                args.signing_ca_path, args.name, suppress_output=False
            )
        else:
            raise KeyError(cmd)
    elif cmd == "misc":
        if args.print_pem_file:
            _print_pem_contents(args.print_pem_file)
    else:
        raise KeyError(cmd)
