from raik import FunctionBlock, StateProperty, InputProperty, ExternalStateProperty

module_interface = {"type": "function_block", "export": "SimpleDOL"}


class SimpleDOL(FunctionBlock):
    """
    This is the only place to put docstring.

    :cvar running:  The only real docstring?  It's an instance variable
    """

    # State flags
    running: bool = ExternalStateProperty(False)
    stopped: bool = ExternalStateProperty(False)
    locked: bool = ExternalStateProperty(False)
    last_started: float = None
    last_stopped: float = None

    # Inputs
    interlock: bool = InputProperty(False)

    # Latching Flags (useful for async)
    cmd_start: bool = InputProperty(False, one_shot=True)
    cmd_stop: bool = InputProperty(False)

    def block_scan(self):
        # One shot inputs
        if self.cmd_start:
            self.running = True
        if self.cmd_stop:
            self.running = False

        # Locked Inputs
        self.locked = self.interlock

        # Track stop status
        self.stopped = not self.running
