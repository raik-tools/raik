====================
Development Policies
====================

This document exists to capture the existing development policies and decisions made.

Unit-Testing
============

TODO:  We need to improve unit testing to make it more readable.  A Unit testing standard is needed capturing
common test patterns.


Libraries
=========

Minimal Third Party Dependencies
--------------------------------

Third party dependencies must be abstracted if used and only chosen on maturity.  The reason is to ensure longevity
of this development and if something changes, be able to either refactor it out easily or contribute back and maintain
it.


Web Applications
================

Multi-Page Application
----------------------

To keep things simple and compartmentalised, the application will be generally written as a traditional multi-page
application.


No JS or CSS Frameworks for operational views
---------------------------------------------

Frameworks for JS and CSS should be avoided altogether to reduce the size of operational pages.  This means most of the
work will be performed in native javascript and preferentially, very little of that.  The reasons for this:

* Keep ownership of performance and reliability of those views within the project
* Hopwfully kepe page load sizes low
* Keep performance high

When JS is used on the fronr-end, coding patterns will be used to maintain performance, such as use elemnt IDs for
look-ups instead of complex CSS queries.

This only applies for operaitonal view were risk needs to be carefully managed.  These are the views that will also
need to perform for the largest range of devices (and age).

Configuration view (non-operational ones) can be used based on the system libraries policy.

