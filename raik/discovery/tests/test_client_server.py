import unittest
from raik.discovery import *
import raik.discovery.server
import raik.discovery.client
import raik.utils.hardware
import raik.utils.crypt
import raik.utils.random
import os.path
import os
import shutil
import time
import zlib
import base64
import json
import socket
import threading
import time


class TestClientServer(unittest.TestCase):
    """
    Tests client and server compatibility
    """

    data_folder = "/tmp/TestDiscoveryClientServer"
    ca_path = os.path.join(data_folder, "TEST_ROOT_CA")

    @classmethod
    def setUpClass(cls):
        raise NotImplementedError("This is out of date with certs file")
        # if os.path.exists(cls.data_folder):
        #     shutil.rmtree(cls.data_folder)
        # os.mkdir(cls.data_folder)
        # os.mkdir(cls.ca_path)
        # # Create a CA
        # cls.root_cert = raik.utils.crypt.create_root_ca(cls.ca_path)
        # raik.utils.crypt.CERT_VALIDATION_CHAIN = cls.root_cert
        #
        # # Generate a public / private key pair
        # host_dev_name = 'test_create_discovery_packet_host'
        # host_key_name = os.path.join(cls.data_folder, host_dev_name)
        # private_key_file = host_key_name + '.pri'
        # public_key_file = host_key_name + 'pub'
        # raik.utils.crypt.generate_rsa_keys(private_key_file, public_key_file)
        # # Create a request and get the CA to sign it
        # csr_path = raik.utils.crypt.create_certificate_request(private_key_file,
        #                                                        host_dev_name)
        # cert_path = host_key_name + '.cert'
        # raik.utils.crypt.sign_certificate_request(cls.ca_path,
        #                                           csr_path,
        #                                           cert_path)
        # cls.host_dev_name = host_key_name
        # cls.host_key_name = host_key_name
        # cls.host_private_key_file = private_key_file
        # cls.host_public_key_file = public_key_file
        # cls.host_cert_file = cert_path
        # with open(cert_path, 'r') as f:
        #     cls.host_signed_public_key = f.read()
        # with open(private_key_file, 'r') as f:
        #     cls.host_private_key = f.read()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.data_folder)
        raik.discovery.server.stop_server()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_start_server(self):
        """
        Tests server startup and shutdown
        """
        # Test server execution
        t = threading.Thread(target=raik.discovery.server.start_server)
        t.start()
        time.sleep(0.6)
        self.assertTrue(t.is_alive())
        self.assertTrue(raik.discovery.server.server_is_running())
        # Check running a second instance doesn't do anything
        t2 = threading.Thread(target=raik.discovery.server.start_server)
        t2.start()
        time.sleep(0.6)
        self.assertFalse(t2.is_alive())
        self.assertTrue(raik.discovery.server.server_is_running())
        # Shutdown the server
        raik.discovery.server.stop_server()
        time.sleep(0.6)
        self.assertFalse(t.is_alive())
        self.assertFalse(raik.discovery.server.server_is_running())
        raik.discovery.server.stop_server()
        self.assertFalse(raik.discovery.server.server_is_running())

    def test_server_communications(self):
        """
        Tests server communications
        """
        # Start server
        t = threading.Thread(target=raik.discovery.server.start_server)
        t.start()
        time.sleep(0.6)
        self.assertTrue(t.is_alive())
        self.assertTrue(raik.discovery.server.server_is_running())

        # Send the server an empty packet
        # Interesting test:  Can I make this multiple devices?
        # I need another machine on the network to test.
        # HOST, PORT = "ff02::1", raik.discovery.LISTEN_PORT
        HOST, PORT = "::1", raik.discovery.LISTEN_PORT  # Local host for testing
        max_data = raik.discovery.MAX_PACKET
        discovery_packet = create_discovery_packet(self.host_signed_public_key)
        sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        sock.settimeout(2.0)
        sock.sendto(discovery_packet, (HOST, PORT))
        received_data = None
        try:
            # During testing, other hosts could exist.  Capture as many as we can
            # for now until the socket times out
            # To enable this, HOST above needs to be a broadcast address
            for i in range(100):
                received = sock.recvfrom(max_data)
                received, address = received
                # print("Received from %s:  %s" % (str(address),
                #                                  str(received, "latin_1")))
                if address[0] == "::1":
                    received_data = received
        except socket.timeout:
            pass
        raik.discovery.server.stop_server()
        # Test data received from the server.  Data should be encrypted using the host public key
        self.assertIsNotNone(received_data)
        msg = zlib.decompress(
            raik.utils.crypt.asymmetric_decrypt_with_private_key(
                self.host_private_key, received_data
            )
        )
        msg = json.loads(msg.decode())
        self.assertIn("n", msg)
        self.assertNotEqual(msg["n"], "")

        # Server should have cleaned up by now
        self.assertFalse(t.is_alive())
        self.assertFalse(raik.discovery.server.server_is_running())

    def test_client_communications(self):
        """
        Tests client communications
        """
        # Start server
        t = threading.Thread(target=raik.discovery.server.start_server)
        t.start()
        time.sleep(0.6)
        self.assertTrue(t.is_alive())
        self.assertTrue(raik.discovery.server.server_is_running())

        raik.discovery.client.HOSTS = "::1"
        raik.discovery.client.CLIENT_PUBLIC_KEY_CERTIFICATE_FILE = self.host_cert_file
        raik.discovery.client.CLIENT_PRIVATE_KEY_FILE = self.host_private_key_file
        server_data = raik.discovery.client.get_servers()
        raik.discovery.server.stop_server()

        # Check the data returned by the server
        self.assertIsInstance(server_data, dict)
        self.assertIn("::1", server_data)
        self.assertIsInstance(server_data["::1"], dict)
        self.assertIn("n", server_data["::1"])

        # Server should have cleaned up by now
        self.assertFalse(t.is_alive())
        self.assertFalse(raik.discovery.server.server_is_running())
