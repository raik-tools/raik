"""
These tests focus on testing the load_program component and supporting
functions/components.  Interfaces will be mocked to influence the load
process with interface testing kept to a minimum.
"""

import pytest
import psutil
import os

import raik
from raik import IN_PROCESS_PROGRAMS, IN_PROCESS_BLOCKS
import raik.control.function_block


def test_load_program_internal_interface(load_program):
    """
    Tests the load program function and associated interfaces.
    """
    load_program("private_connection_only_program")

    assert "private_connection_only_program" in IN_PROCESS_PROGRAMS

    program_conf = IN_PROCESS_PROGRAMS["private_connection_only_program"]

    assert raik.control.program.PROGRAM_SCOPE.private == program_conf.scope

    assert "pump3" in IN_PROCESS_BLOCKS
    block_conf = IN_PROCESS_BLOCKS["pump3"]
    assert raik.control.program.PROGRAM_SCOPE.private == block_conf.scope

    assert "pump5" in IN_PROCESS_BLOCKS
    block_conf = IN_PROCESS_BLOCKS["pump5"]
    assert raik.control.program.PROGRAM_SCOPE.private == block_conf.scope


def test_load_program_external_interface(load_program):
    """
    Tests the load program external interfaces
    """
    load_program("private_connection_only_program")

    # This program is loaded in this process, scan the process for
    # tmp files
    current_process = psutil.Process(os.getpid())
    open_files = current_process.open_files()

    expected_tmp_file_name = (
        f"RAIK_PROGRAM-private_connection_only_program-{current_process.pid}"
    )

    found_file = None
    for f in open_files:
        if f.path.endswith(expected_tmp_file_name):
            found_file = f.path

    assert found_file


def test_load_program_external_interface_data(load_program):
    """
    Tests the load program external interfaces
    """
    load_program("private_connection_only_program")

    programs = raik.control.program.list_programs()
    assert "private_connection_only_program" in programs
    ext_conf = programs["private_connection_only_program"]

    assert {
        "name": "private_connection_only_program",
        "scope": raik.control.program.PROGRAM_SCOPE.private,
        "blocks": {
            "pump3": "SimpleDOL",
            "pump5": "SimpleDOL",
            "pump6": "SimpleDOL",
        },
    } == ext_conf


def test_load_program_duplicate(load_program):
    """
    Tests an error when loading a program twice
    """
    load_program("private_connection_only_program")

    with pytest.raises(ValueError) as exc_info:
        load_program("private_connection_only_program")

    assert (
        exc_info.value.args[0]
        == "Program private_connection_only_program already loaded"
    )


def test_load_program_duplicate_external_scope(
    load_program, mock_program_external_interface
):
    """
    Tests loading a program whose name is already occupied by a
    different scope
    """
    mock_program_external_interface(
        "private_connection_only_program",
        {"scope": raik.control.program.PROGRAM_SCOPE.user},
    )

    # Load a user scope program
    with pytest.raises(ValueError) as exc_info:
        load_program("private_connection_only_program")

    assert (
        exc_info.value.args[0] == "Program private_connection_only_program "
        "loaded in the user space"
    )


# This test is not needed as once any program is loaded, it is registered with a temp file
# that will get trapped by the external test above anyway.
# def test_load_program_block_internal_duplicate(load_program, mock_internal_block):
#     """
#     Multiple private scoped programs can't share block names
#     """
#     # Mock an existing block
#     mock_internal_block('pump3')
#
#     # Load a user scope program
#     with pytest.raises(ValueError) as exc_info:
#         load_program('private_connection_only_program')
#
#     assert exc_info.value.args[0] == 'Program private_connection_only_program loading ' \
#                                      'duplicate block pump3'


def test_load_program_block_external_duplicate(
    load_program, mock_program_external_interface
):
    """
    A block can't clash with another block in te same accessible
    scope.  Although, a block can clash with one it doesn't have
    access to (not tested).
    """
    mock_program_external_interface(
        "private_connection_only_program_v2",
        {"scope": "user", "blocks": {"pump3": "SimpleDOL"}},
    )

    # Load a user scope program
    with pytest.raises(ValueError) as exc_info:
        load_program("private_connection_only_program")

    assert (
        exc_info.value.args[0] == "Program private_connection_only_program loading "
        "duplicate block pump3"
    )


def test_list_programs(load_program, mock_program_external_interface):
    """
    Checks the listing of multiple programs from various scopes.
    The test only spot checks the config data as it will evolve over time.
    """
    mock_program_external_interface("not_a_program_1", {"scope": "user"})
    mock_program_external_interface("not_a_program_2", {"scope": "host"})
    load_program("private_connection_only_program")
    load_program("my_program")

    programs = raik.control.program.list_programs()

    assert "not_a_program_1" in programs
    assert "not_a_program_2" in programs
    assert "private_connection_only_program" in programs
    assert "my_program" in programs
    assert len(list(programs.keys())) == 4


def test_list_blocks(load_program, mock_program_external_interface):
    """
    List available blocks from various scopes.
    The blocks listing from the program is kept small as additional
    info will be called via REDIS.
    """
    mock_program_external_interface(
        "not_a_program_1",
        {
            "scope": "user",
            "blocks": {
                "motor1": "Motor",
                "motor2": "Motor",
                "motor3": "Motor",
            },
        },
    )
    mock_program_external_interface("not_a_program_2", {"scope": "host"})
    load_program("private_connection_only_program")
    load_program("my_program")

    blocks = raik.control.program.list_blocks()

    assert {
        "motor1": ["not_a_program_1", "Motor", []],
        "motor2": ["not_a_program_1", "Motor", []],
        "motor3": ["not_a_program_1", "Motor", []],
        "pump3": ["private_connection_only_program", "SimpleDOL", ["my_program"]],
        "pump5": ["private_connection_only_program", "SimpleDOL", []],
        "pump6": ["private_connection_only_program", "SimpleDOL", []],
        "pump1": ["my_program", "SimpleDOL", []],
        "pump2": ["my_program", "SimpleDOL", []],
        "pumps2_3_running": ["my_program", "And", []],
    } == blocks
