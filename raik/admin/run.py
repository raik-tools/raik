import sys
import os
import dataclasses
import logging
import pathlib
from ._argparse import command_subparsers

import raik.control.program

logger = logging.getLogger(__name__)

run_parser = command_subparsers.add_parser(
    name='run',
    description='Run a RAIK program.',
    help='This method is used to execute programs in your RAIK project.'
)


# If not run from a control directory, this will execute from the test fixtures
run_parser.add_argument(
    dest='program',
    help="Name of the control program to execute",
)

run_parser.add_argument(
    '--run-once', dest='run_once',
    help="Run the process once only.  Warning:  Clean up is not performed after execution,"
         " so external block data will still be accessible even after the program ends.",
    action='store_true'
)
run_parser.add_argument(
    '--test-db', dest='test_db',
    help="When set this will use the test redis DB for IPC instead of the production one",
    action='store_true'
)


@dataclasses.dataclass
class RunArgs:
    program: str
    run_once: bool
    test_db: bool


def main(args: RunArgs):
    # Get the program name
    program = args.program
    logger.debug('Running program %s', program)

    # Look for the program and load it
    try:
        # noinspection PyPackageRequirements
        import control
        try:
            # Not sure why this is needed again, unbound local error
            # was being raised.
            import raik.control.program
            raik.control.program.load_program(program)
        except ImportError:
            logger.error("Program not found")
            sys.exit(1)
    except ImportError:
        # This seems to happen in VENV terminal
        logger.debug("Can't find control lib.  ADding CWD to path")
        try:
            sys.path.append(os.getcwd())
            # noinspection PyPackageRequirements
            import control
            try:
                # Not sure why this is needed again, unbound local error
                # was being raised.
                import raik.control.program
                raik.control.program.load_program(program)
            except ImportError:
                logger.error("Program not found")
                sys.exit(1)
        except ImportError:
            logger.warning("Can't find control lib.  Adding test fixtures to path")
            try:
                # noinspection PyUnresolvedReferences
                import tests.raik.control.fixtures
                # noinspection PyProtectedMember
                fixture_path = pathlib.Path(
                    str(tests.raik.control.fixtures.__path__._path[0])
                )
                logger.debug('Appending Fixtures to path %s', fixture_path)
                sys.path.append(str(fixture_path))
            except ImportError:
                logger.error('Test fixtures not found either')
                sys.exit(1)
            # Not sure why this is needed again, unbound local error
            # was being raised.
            import raik.control.program
            raik.control.program.load_program(program)

    # Prepare the context / environment
    if args.test_db:
        import raik.communications.local
        # noinspection PyProtectedMember
        raik.communications.local._MSG_STORE['config'] = [
            raik.communications.local.DB_CONFIG_TEST, None
        ]
        # noinspection PyProtectedMember
        raik.communications.local._MSG_STORE['val'] = [
            raik.communications.local.DB_VAL_TEST, None
        ]

    # Execute the program
    if args.run_once:
        logger.info('Run program once')
        raik.control.program.execute_program(program)
        print(raik.control.program.get_program_state_str(program))
    else:
        raise NotImplementedError()
