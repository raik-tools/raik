============
RAIK Library
============

This folder contains the master raik library which is a library of commands and functions that can be executed by
the raik controller.

This library itself is not a package but a global directory of packages that can be executed or imported in various
raik implementations.

A Raik controller instance will likely want to specify additional functions and routines to be executed by the raik
controller.  Some examples of these can be found in the raik_examples folder which contains examples of a raik site
configuration.

Care should be taken that package names within the site configuration do not clash with library package names.  To
facilitate this a naming convention will be enforced where every package must be prefixed with "raik_control" (to
serve as a namespace).

Location Settings
=================

To overwrite the locations used by the RAIK controller for the raik control library and/or the site configuration, make
use of the raik.control.functions.set_paths function.
