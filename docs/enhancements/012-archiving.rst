=========
Streaming
=========

:Enhancement: 12
:Keyword: E012-archiving
:Type: Feature
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:

Overview
========



Background
----------



Status Comments
===============

This document is still in draft.



References
==========

* TODO


Guidelines for usage
====================


Features
========



Pro Features
============


Definition
==========



Scope
=====


Specification
=============

Data Format
-----------

life-cycle
----------



Location
========
