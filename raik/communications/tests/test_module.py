import raik
import raik.enable_testing
import unittest
from datetime import datetime
import time
import pytz
import raik.communications
import raik.communications.local
import redis
import json
import logging
logger = logging.getLogger(__name__)


class TestSerializer(unittest.TestCase):
    def test_dict_value_retrieval(self):
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        ks = raik.communications.connect_system_key_store()
        ks.set('test', start)
        end = ks.get('test')
        self.assertEqual(start, end)

    def test_init_zero_size_key_store(self):
        r = redis.StrictRedis(db=5)
        r.set('a', 1)
        r.set('b', 1)
        r.set('c', 1)
        self.assertGreater(r.dbsize(), 0)
        ks = raik.communications.init_system_store()
        self.assertEqual(
            len(ks),
            0,
            "Key store wasn't cleared on init from the module"
        )

    def test_connect(self):
        s1 = raik.communications.connect_system_key_store()
        s1.set('hello', 'world')

        s2 = raik.communications.connect_system_key_store()
        self.assertEqual(s2.get('hello'), 'world')


class TestLocal(unittest.TestCase):
    # def setUp(self):

    def setUp(self):
        # Note, sometimes some basic tests consistently fail but then work fine under debug.  I suspect
        # some kind of cache or race condition, but can't prove ie.
        # Debug mode doesn't help, because it works fine.
        # A 1s delay in setUp doesn't work.
        # Calling the redis read multiple times doesn't work.
        # And yet, more complex tests seem to be working.
        # Running individual tests works
        # Running the TestCase works
        # Running all tests in communications does not.

        # It seems to break when running the events test module first

        raik.communications.local.clear_all_values()
        raik.communications.local.clear_all_config()

    def tearDown(self):
        # raik.communications.local.clear_all_values()
        # raik.communications.local.clear_all_config()
        pass

    def test_set_config(self):
        """
        Tests that set config results in a value written for the local redis instance
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_config_key'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=12
        )
        raik.communications.local.set_config(key, val)
        stored_val = ks.get(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

    def test_delete_config(self):
        """
        Tests deletion of a key
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_config_key'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=12
        )
        raik.communications.local.set_config(key, val)
        stored_val = ks.get(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

        raik.communications.local.delete_config(key)
        stored_val = ks.get(key)
        self.assertIsNone(stored_val)

    def test_get_config(self):
        """
        Tests that get config results in a value read for the local redis instance
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_config_key'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=12
        )
        ks.set(key, json.dumps(val).encode('utf-8'))
        stored_val = raik.communications.local.get_config(key)
        # Note:  Sometimes this fails, but I don't know why. It works fine when run in debug.
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

    def test_read_write_config(self):
        """
        Simple write a config value and read the results and ensure they are the same.
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_config(channel, start)
        end = raik.communications.local.get_config(channel)
        self.assertEqual(start, end)

    def test_read_bad_config(self):
        """
        Tests that None is returned if the config value doesn't exist.
        """
        end = raik.communications.local.get_config('non_existent')
        self.assertEqual(end, None)

    def test_clear_config(self):
        """
        Sets a config value, checks the return and then clears the system, rechecking.
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_config(channel, start)
        end = raik.communications.local.get_config(channel)
        self.assertEqual(start, end)
        raik.communications.local.clear_all_config()
        end = raik.communications.local.get_config(channel)
        self.assertNotEqual(start, end)
        self.assertEqual(end, None)

    def test_search_config_keys(self):
        """
        Tests searching for keys
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_config(channel + "1", start)
        raik.communications.local.set_config(channel + "2", start)
        raik.communications.local.set_config(channel + "3", start)
        raik.communications.local.set_config(channel + "4", start)

        self.assertEqual(
            ('test_channel1', 'test_channel2', 'test_channel3', 'test_channel4'),
            tuple(sorted((raik.communications.local.search_config_keys("*"))))
        )

    def test_set_value(self):
        """
        Tests that set config results in a value written for the local redis instance
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_set_value'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=15
        )
        raik.communications.local.set_value(key, val)
        stored_val = ks.get(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

    def test_delete_value(self):
        """
        Tests deletion of a value
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_set_value'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=15
        )
        raik.communications.local.set_value(key, val)
        stored_val = ks.get(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

        raik.communications.local.delete_values(key)
        stored_val = ks.get(key)
        self.assertIsNone(stored_val)

    def test_get_value(self):
        """
        Tests that get config results in a value read for the local redis instance
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_config_key'
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=15
        )
        ks.set(key, json.dumps(val).encode('utf-8'))
        stored_val = raik.communications.local.get_value(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, json.loads(ks.get(key).decode('utf-8')))

    def test_read_write_values(self):
        """
        Simple write a data value and read the results and ensure they are the same.
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_value(channel, start)
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)

    def test_read_bad_value(self):
        """
        Tests that None is returned if the data point doesn't exist.
        """
        end = raik.communications.local.get_value('non_existent')
        self.assertEqual(end, None)

    def test_clear_values(self):
        """
        Sets a value, checks the return and then clears the system, rechecking.
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_value(channel, start)
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)
        raik.communications.local.clear_all_values()
        end = raik.communications.local.get_value(channel)
        self.assertNotEqual(start, end)
        self.assertEqual(end, None)

    def test_search_config_keys(self):
        """
        Tests searching for keys
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_value(channel + "1", start)
        raik.communications.local.set_value(channel + "2", start)
        raik.communications.local.set_value(channel + "3", start)
        raik.communications.local.set_value(channel + "4", start)

        self.assertEqual(
            ('test_channel1', 'test_channel2', 'test_channel3', 'test_channel4'),
            tuple(sorted((raik.communications.local.search_value_keys("*"))))
        )


    def test_clear_other_namespace(self):
        """
        Sets a value, checks the return and then clears all config values, rechecking message is still okay and that
        it hasn't been cleared when clearing config (ie, values preserved).
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_value(channel, start)
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)
        raik.communications.local.clear_all_config()
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)

    def test_unique_namespace(self):
        """
        Tests that writing a value doesn't change a config value of the same name
        """
        start = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        channel = 'test_channel'
        raik.communications.local.set_value(channel, start)
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)

        # Set the same name to a message
        msg_start = 'hello'
        raik.communications.local.set_config(channel, msg_start)
        msg_end = raik.communications.local.get_config(channel)
        self.assertEqual(msg_start, msg_end)

        # Retrieve Values again
        end = raik.communications.local.get_value(channel)
        self.assertEqual(start, end)

    def test_set_values(self):
        """
        Write multiple values at once
        """
        values = {
            'a': 12,
            'b': datetime.now(pytz.utc).timestamp(),
            'c': ['hello', 'test'],
            'd': {'hello': 'Goodbye'}
        }
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=15
        )
        raik.communications.local.set_values(values)
        for k, v in values.items():
            stored_val = json.loads(ks.get(k).decode('utf-8'))
            self.assertIsNotNone(stored_val)
            self.assertEqual(v, stored_val)

    def test_get_values(self):
        """
        Write multiple values at once
        """
        values = {
            'a': 12,
            'b': datetime.now(pytz.utc).timestamp(),
            'c': ['hello', 'test'],
            'd': {'hello': 'Goodbye'}
        }
        ks = redis.StrictRedis(
            host=raik.communications.local._REDIS_HOST,
            port=raik.communications.local._REDIS_PORT,
            db=15
        )
        for k, v in values.items():
            ks.set(k, json.dumps(v).encode('utf-8'))

        v = raik.communications.local.get_values(values.keys())
        self.assertIsNotNone(v)
        self.assertEqual(v, values)

    def test_data_stream(self):
        """
        Tests data streaming from the local interface.  Data streaming occurs from the value area and not the
        configuration area.
        """
        # Still exploring sustainable design - passing objects, basic variables etc...
        # and how to handle this with converting code to C
        # Read some interesting data warehousing articles, can't remember them anymore
        init_data = {
            'a': 2,
            'b': 3,
            'c': 4,
            'd': 3
        }
        raik.communications.local.set_values(init_data)
        dataset_keys = ['a', 'b', 'c', 'd']

        # Implementation note:  REDIS Pub/Sub might be a good tool to use for real time communications,
        # however, it has no concept of redis DB or the key space.  It's used for message transmission
        # and not for a values cache.  If we want both, we would have to use the key space as well as
        # the pub/sub.  It may be possible to set up some LUA to assist in doing both.
        # Correction:  Redis allows subscription to keyspace notifications

        # Experiment with using a context manager for resource management
        # The data stream contains it's own data update poll rate
        # The data stream can be read at any time
        # When read, the data stream will return all updates since the last read.
        read_period = 0.5
        test_timing_precision = 2
        with raik.communications.local.open_data_stream(dataset_keys, read_period) as s:

            # Wait for first update
            ret_data = s.recv()  # Non Blocking call but also contains initialisation information
            self.assertEqual(ret_data['data'], init_data)
            self.assertGreater(ret_data['datum'], 0)
            self.assertIsInstance(ret_data['timezone'], int)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period)
            self.assertEqual(ret_data['data_id'], 1)
            self.assertIsInstance(ret_data['cycle'], float)
            self.assertEqual(ret_data['cycle'], read_period)

            init_data = {
                'a': 4,
                'd': 6
            }
            raik.communications.local.set_values(init_data)

            # It's assumed in this test that <1s has passed and so a subsequent read will show nothing
            ret_data = s.recv()  # Non Blocking call
            self.assertEqual(ret_data['data'], {})
            self.assertEqual(ret_data['data_id'], 1)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period)

            # Now a blocking read
            ret_data = s.recv(True)  # Blocking call
            self.assertEqual(ret_data['data'], init_data)
            self.assertEqual(ret_data['data_id'], 2)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period*2)

            ret_data = s.recv(full_set=True)  # Get all data
            self.assertEqual(ret_data['data'], {
                    'a': 4,
                    'b': 3,
                    'c': 4,
                    'd': 6
            })
            self.assertGreater(ret_data['datum'], 0)
            self.assertIsInstance(ret_data['timezone'], int)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period*2)
            self.assertEqual(ret_data['data_id'], 2)
            self.assertIsInstance(ret_data['cycle'], float)
            self.assertEqual(ret_data['cycle'], read_period)

    def test_data_stream_error_long_polls(self):
        """
        Test basic errors in data stream calling
        """
        init_data = {
            'a': 2,
            'b': 3,
            'c': 4,
            'd': 3
        }
        raik.communications.local.set_values(init_data)
        dataset_keys = ['a', 'b', 'c', 'd']

        def break_it():
            read_period = 0.5
            with raik.communications.local.open_data_stream(dataset_keys, read_period) as s:
                s.open()
                # If we actually get this far, then the process hasn't stalled
                self.assertTrue(True)
                # Cause process to stall and throw an assertion
                with s._lock:
                    time.sleep(read_period*2)  # Cause thread errors
                time.sleep(read_period)  # Needed to allow thread a chance to append errors before recv
                s.recv()

        self.assertRaises(RuntimeError, break_it)

    def test_data_stream_error_no_data(self):
        """
        There needs to be positive confirmation that the data requested doesn't exist or is not working.
        None should do the trick.
        """
        dataset_keys = ['a', 'b', 'c', 'd']

        read_period = 0.5
        with raik.communications.local.open_data_stream(dataset_keys, read_period) as s:
            s.open()
            self.assertTrue(True)
            # Wait for errors to be recorded by no data existing
            r_data = s.recv()
        self.assertEquals(r_data['data'],
                          {
                              'a': None,
                              'b': None,
                              'c': None,
                              'd': None
                          })

    def test_data_stream_error_partial_data(self):
        """
        Missing values will be replaced with None
        """
        init_data = {
            'a': 2,
            'b': 3,
            'c': 4,
        }
        raik.communications.local.set_values(init_data)

        dataset_keys = ['a', 'b', 'c', 'd']

        read_period = 0.5
        with raik.communications.local.open_data_stream(dataset_keys, read_period) as s:
            s.open()
            self.assertTrue(True)
            # Wait for errors to be recorded by no data existing
            r_data = s.recv()

        self.assertEqual(r_data['data'],
                         {
                             'a': 2,
                             'b': 3,
                             'c': 4,
                             'd': None
                         })

    def test_data_stream_add_tags(self):
        """
        Tests data streaming from the local interface.  The stream will begin with no tags in the list
        and will test for tags being added and removed.
        """
        init_data = {
            'a': 2,
            'b': 3,
            'c': 4,
            'd': 3
        }
        raik.communications.local.set_values(init_data)

        read_period = 0.5
        test_timing_precision = 2
        with raik.communications.local.open_data_stream([], read_period) as s:

            # Wait for first update with no tags
            ret_data = s.recv()  # Non Blocking call but also contains initialisation information
            self.assertEqual(ret_data['data'], {})
            self.assertGreater(ret_data['datum'], 0)
            self.assertIsInstance(ret_data['timezone'], int)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period)
            self.assertEqual(ret_data['data_id'], 1)
            self.assertIsInstance(ret_data['cycle'], float)
            self.assertEqual(ret_data['cycle'], read_period)

            # It's assumed in this test that <1s has passed and so a subsequent read will show nothing
            ret_data = s.recv()  # Immediate read of stream cache
            self.assertEqual(ret_data['data'], {})
            self.assertEqual(ret_data['data_id'], 1)
            self.assertEqual(round(ret_data['offset'], test_timing_precision), read_period)

            # Add tags to the interface
            s.add(['a', 'b'])

            raik.communications.local.set_values({
                'a': 4,
                'd': 6
            })

            # Read the added tags
            ret_data = s.recv(True)  # Blocking until data_stream has scanned
            self.assertEqual(ret_data['data'], {'a': 4, 'b': 3})

            # Check empty data on the next update
            ret_data = s.recv(True)  # Blocking until data_stream has scanned
            self.assertEqual(ret_data['data'], {})

            # Remove a tag from the interface
            s.remove(['a'])
            raik.communications.local.set_values({
                'a': 6,
                'b': 7,
                'd': 8
            })

            # Read the current data
            ret_data = s.recv(True)  # Blocking until data_stream has scanned
            self.assertEqual({'b': 7}, ret_data['data'])

            # Add one more tag and do a full read
            s.add(['c'])
            ret_data = s.recv(blocking=True, full_set=True)  # Get all data
            self.assertEqual({
                    'b': 7,
                    'c': 4,
            }, ret_data['data'])

    def test_push_pop_value_basic(self):
        """
        Tests queue manipulation
        """
        val = {
            'at': datetime.now(pytz.utc).timestamp(),
            'r': [],
            'p': None
        }
        key = 'test_push_pop_value'
        # Test empty value
        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNone(stored_val)

        # Test set Value
        raik.communications.local.push_value(key, val)
        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(val, stored_val)

    def test_push_pop_value_multiple(self):
        """
        Tests queue manipulation
        """
        vals = [
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 1
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 2
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 3
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 4
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 5
            }
        ]

        key = 'test_push_pop_value_multiple'

        for v in vals:
            raik.communications.local.push_value(key, v)

        for v in vals:
            stored_val = raik.communications.local.pop_value(key)
            self.assertIsNotNone(stored_val)
            self.assertEqual(v, stored_val)

    def test_push_pop_values(self):
        """
        Tests queue manipulation
        """
        vals = [
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 1
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 2
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 3
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 4
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 5
            }
        ]

        key = 'test_push_pop_value_multiple'

        # Test empty response first
        stored_vals = raik.communications.local.pop_all_values(key)
        self.assertIsNotNone(stored_vals)
        self.assertEqual(stored_vals, [])

        for v in vals:
            raik.communications.local.push_value(key, v)

        stored_vals = raik.communications.local.pop_all_values(key)
        self.assertIsNotNone(stored_vals)
        self.assertEqual(stored_vals, vals)

        # Make sure list is gone
        stored_vals = raik.communications.local.pop_all_values(key)
        self.assertIsNotNone(stored_vals)
        self.assertEqual(stored_vals, [])

    def test_push_pop_value_error(self):
        """
        Tests exceeding queue length which will still raise an exception
        if the length has exceeded.
        """
        vals = [
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 1
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 2
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 3
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 4
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 5
            }
        ]

        key = 'test_push_pop_value_error'

        raik.communications.local.push_value(key, vals[0], limit=3)
        raik.communications.local.push_value(key, vals[1], limit=3)
        raik.communications.local.push_value(key, vals[2], limit=3)

        with self.assertRaises(IndexError):
            raik.communications.local.push_value(key, vals[3], limit=3)

        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(vals[0], stored_val)

        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(vals[1], stored_val)

        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNotNone(stored_val)
        self.assertEqual(vals[2], stored_val)

        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNone(stored_val)

    def test_delete_queue(self):
        """
        Tests value deletion
        """
        vals = [
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 1
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 2
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 3
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 4
            },
            {
                'at': datetime.now(pytz.utc).timestamp(),
                'r': [],
                'p': 5
            }
        ]

        key = 'test_delete_queue'

        for v in vals:
            raik.communications.local.push_value(key, v)

        raik.communications.local.delete_values(key)

        stored_val = raik.communications.local.pop_value(key)
        self.assertIsNone(stored_val)
