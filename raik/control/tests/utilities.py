"""
Test utilities
"""
import multiprocessing


def enable_multiprocess_coverage():
    # http://blog.schettino72.net/posts/python-code-coverage-multiprocessing.html
    # https://bitbucket.org/ned/coveragepy/issues/117/enable-coverage-measurement-of-code-run-by
    # Under pycharms, the run_coverage.py file needs to be updated to include:
    #      main(["combine"])
    # Just before
    #      main(["xml", "-o", coverage_file + ".xml", "--ignore-errors"])
    def coverage_multiprocessing_process():  # pragma: no cover
        try:
            # noinspection PyUnresolvedReferences
            from coverage.collector import Collector
            # noinspection PyUnresolvedReferences
            from coverage import coverage
        except ImportError:
            return

        # detect if coverage was running in forked process
        # noinspection PyProtectedMember
        if Collector._collectors:
            # noinspection PyProtectedMember
            original = multiprocessing.Process._bootstrap

            class ProcessWithCoverage(multiprocessing.Process):
                def _bootstrap(self):
                    try:
                        cov = coverage(data_suffix=True)
                    except:
                        import traceback
                        traceback.print_exc()
                        raise

                    cov.start()
                    try:
                        return original(self)
                    finally:
                        cov.stop()
                        cov.save()

            return ProcessWithCoverage

    process_coverage = coverage_multiprocessing_process()
    if process_coverage:
        multiprocessing.Process = process_coverage
