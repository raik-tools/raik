"""
Tests the function block class itself
"""
import pytest

from raik import PROGRAM_SCOPE


# TODO:  Is one-shot the right term?

# noinspection PyUnresolvedReferences
def test_input_one_shot_sequence(simple_dol):
    """
    Test a sequence to check that after setting high, a read
    of the data point will cause it to reset.  Useful for async event
    triggers.
    """
    # Declare the blocks
    pump = simple_dol()

    # Set a start command
    pump.cmd_start = True

    # Perform an internal read
    assert pump._proxy.cmd_start is True
    # Second read should reset
    assert pump._proxy.cmd_start is False

    # Set a start command
    pump.cmd_start = True
    # Perform an internal read
    assert pump._proxy.cmd_start is True
    # Second read should reset
    assert pump._proxy.cmd_start is False


# noinspection PyUnresolvedReferences
def test_state_change_on_call(simple_dol):
    # Declare the blocks
    pump = simple_dol()

    # Set a start command
    pump.cmd_start = True
    assert not pump.running
    pump()
    assert pump.running


def test_one_shot_read():
    pass


def test_rising_edge_detect():
    pass


def test_connect_blocks(simple_dol):
    # Declare the blocks
    pump1 = simple_dol()
    pump2 = simple_dol()

    # Connect blocks
    pump1.interlock = pump2.running

    # Set a start command
    pump2.cmd_start = True
    pump2()

    # Verify Pump1 state change before and after execution.
    assert not pump1.locked
    pump1()
    assert pump1.locked


def test_proxy_block(simple_dol, external_block):
    """
    Tests that a block and proxy can communicate
    :return:
    """
    pump_x = simple_dol()
    pump_x.name = "pump_x"
    pump_x.scope = PROGRAM_SCOPE.user

    pump_x_external = external_block()
    pump_x_external.name = "pump_x"

    # Scan pump_x so that it is published
    pump_x()

    # Test Init State
    assert not pump_x.running

    # Run/Connect proxy and check initial state
    pump_x_external()
    assert not pump_x_external.running

    # Make Change
    pump_x.cmd_start = True
    pump_x()

    # Update proxy
    pump_x_external()

    # Test resulting state
    assert pump_x.running
    assert pump_x_external.running


def test_proxy_private_block_fails(simple_dol, external_block):
    """
    A block in a private scope is not accessible externally
    :return:
    """
    pump_x = simple_dol()
    pump_x.name = "pump_x"
    pump_x.scope = PROGRAM_SCOPE.private
    pump_x_external = external_block()
    pump_x_external.name = "pump_x"

    with pytest.raises(RuntimeError) as e:
        pump_x_external()
        assert e.args == "Block not found or the block is private"
