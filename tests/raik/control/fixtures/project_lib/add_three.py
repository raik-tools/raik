from raik import Number

module_interface = {"type": "function_block", "export": "add_three"}


def add_three(a: Number, b: Number, c: Number) -> Number:
    x = a + b + c
    return x
