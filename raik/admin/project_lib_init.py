"""
These utilities exist as part of creating a project lib __init__ file.

Although this file could dynamically load modules, for now the approach
is to generate the code for the file as the result is simpler and more
useful for code inspectors.
"""
import sys
from typing import Tuple, List
import pathlib
import importlib


def process_folder_imports(project_lib_dir: pathlib.Path) -> List[Tuple[str, str]]:
    """
    Returns a list of module name, main import name
    """
    exports = []

    old_path = sys.path
    sys.path = [str(project_lib_dir)] + old_path

    for python_module_file in project_lib_dir.listdir('*.py'):
        module_name = python_module_file.purebasename
        mod = importlib.import_module(module_name)
        mod_interface = getattr(mod, 'module_interface', None)
        if mod_interface:
            exports.append(
                (module_name, mod_interface['export'])
            )

    sys.path = old_path
    return exports
