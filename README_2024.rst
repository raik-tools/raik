======
README
======

Broken Library
==============

This library is broken in many places as I slowly migrate the structure
and update deps.  The following should work:

* raik.utils.crypt



Introduction
============

WARNING:  This is a legacy code base based on an idea that I never
had time to complete or maintain.  However, there are still a number
of utilities I use here and I do intend to continue this project
as a hobby in the future.  Consider all documentation out of date.


Reflection
==========

The initial RAIK was so focussed on creating something that would auto
discover, auto connect and auto everything (aiming on usability) that
it never really developed something functional.  The intention was
something with a formal front end, and so a lot of attention went to
developing a front end first.  However, in hindsight it seems pointless
having a front end to something that does nothing.  This new version
will prioritise on functionality and will likely never have a proper
front end (or if it does have a front end there has been so much
development in the world of IoT that an existing system interacting with
MQTT will be sufficient).


RAIK in 2024
============

A lot of water has flowed under the bridge.  While I no longer have a desire
for the original RAIK product, some of the concepts in RAIK would still be
useful in my own automation and robotics projects.

The original version was cobbled together literally while travelling on
the train to and from work each day.  Understandably, it is not particularly
coherent and well aimed.

It should have started off with a mock up of a control project, not
with the creation of the underlying tools.

In 2024 I would like to have a set of tools that makes it possible to
create control projects on RPI Picos and RPIs (cross platform).  RAIK_Integrations
is already handling network based communications (via MQTT), so this just
needs to focus on a local project.

Philosophy:

* A code based framework for the declaration of control with minimal tricks
  to maximise platform compatibility

Structure:

* A simple program with no dark magic (main.py)

Using PlantUML for creating control diagrams can be an extension in the
future, for now, keep is simple and explicit.

How to get there?

* A new branch is probably best and then remove irrelevant code.

Quick research:

* What is the state of letsencrypt for private networks?  If the private network
  has a local domain such as local.garythompson.au then a public domain name
  check can be performed and LE can be used on local machines.  However, if
  the local network is not named as per a public one then local CA and trusts
  are required to be distributed.


Project Usage
=============

See the original README.rst file for some more details.