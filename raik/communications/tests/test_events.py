import unittest
import raik.enable_testing
from raik.communications.local import *
raik.enable_testing.USELESS_VAR += 1


class TestEventsUseCases(unittest.TestCase):
    """
    A collection of use cases considered for the alarms and event system.

    The first attempt recorded a large number of on/off like events that doesn't need to come to the
    events section as these states will be recorded with their historical data.

    Alarms are things that go wrong and events include operator actions, automatic actions and so forth.

    """

    def setUp(self):
        clear_all_values()

    def test_simple_trip(self):
        """
        What would be expected of a normal pump.
        """
        events = get_current_events()
        self.assertEqual(len(events), 0)

        set_event('pump01', 'TRIPPED', level=NOTICE, event_time=1.0)
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'pump01',
                'state': 'TRIPPED',
                'time': 1.0,
                'level': 'NOTICE',
                'msg': ''
            }
        )

        clear_event('pump01', event_time=2.0)
        events = get_current_events()
        self.assertEqual(len(events), 0)
        event = get_event('pump01')
        self.assertEqual(
            event, {
                'tag_name': 'pump01',
                'state': '',
                'last_state': 'TRIPPED',
                'time': 2.0,
                'level': 'NOTICE',
                'last_time': 1,
            }
        )

    def test_repeated_event(self):
        """
        Tests when the same event if periodically raised without being cleared
        """
        events = get_current_events()
        self.assertEqual(len(events), 0)

        set_event('pump01', 'TRIPPED', level=NOTICE, event_time=1.0)
        set_event('pump01', 'TRIPPED', level=NOTICE, event_time=2.0)
        set_event('pump01', 'TRIPPED', level=NOTICE, event_time=3.0)
        set_event('pump01', 'TRIPPED', level=WARNING, event_time=4.0)
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'pump01',
                'state': 'TRIPPED',
                'time': 1.0,
                'level': 'WARNING',
                'msg': ''
            }
        )

        event = get_event('pump01')
        self.assertEqual(
            event, {
                'tag_name': 'pump01',
                'state': 'TRIPPED',
                'time': 1.0,
                'recurrence': 4,
                'recent_time': 4.0,
                'level': 'WARNING',
                'msg': ''
            }
        )

        clear_event('pump01', event_time=5.0)
        events = get_current_events()
        self.assertEqual(len(events), 0)
        event = get_event('pump01')
        self.assertEqual(
            event, {
                'tag_name': 'pump01',
                'state': '',
                'last_state': 'TRIPPED',
                'time': 5.0,
                'level': 'WARNING',
                'last_time': 1.0,
            }
        )

    def test_simple_events(self):
        """
        Tests a number of unrelated events and how the current events table would look.
        At this stage, sorting is expected to be in reverse chronological order and then by severity.
        """
        events = get_current_events()
        self.assertEqual(len(events), 0)

        set_event('pump01', 'TRIPPED', level=ALERT, event_time=1.0)
        set_event('Temperature02', 'VERY HIGH', level=WARNING, event_time=2.0)
        set_event('Level01', 'HIGH', level=WARNING, event_time=2.0)
        set_event('SumpPump', 'RUNNING', level=NOTICE, event_time=5.0, msg="The sump pump has auto started, it will "
                                                                           "stop itself after 2 minutes.")

        events = get_current_events()
        self.assertEqual(len(events), 4)

        expected_results = [
            {
                'tag_name': 'SumpPump',
                'state': 'RUNNING',
                'time': 5.0,
                'level': 'NOTICE',
                'msg': "The sump pump has auto started, it will stop itself after 2 minutes."
            },
            {
                'tag_name': 'Temperature02',
                'state': 'VERY HIGH',
                'time': 2.0,
                'level': 'WARNING',
                'msg': ''
            },
            {
                'tag_name': 'Level01',
                'state': 'HIGH',
                'time': 2.0,
                'level': 'WARNING',
                'msg': ''
            },
            {
                'tag_name': 'pump01',
                'state': 'TRIPPED',
                'time': 1.0,
                'level': 'ALERT',
                'msg': ''
            }
        ]

        for i, v in enumerate(events):
            self.assertEqual(expected_results[i], v)

    def test_clear_events(self):
        """
        Clear all events in the system. This is needed for other system tests and is not intended as an end user
        function but is part of a reset to the system.
        """
        events = get_current_events()
        self.assertEqual(len(events), 0)

        set_event('pump01', 'TRIPPED', level=ALERT, event_time=1.0)
        set_event('Temperature02', 'VERY HIGH', level=WARNING, event_time=2.0)
        set_event('Level01', 'HIGH', level=WARNING, event_time=2.0)
        set_event('SumpPump', 'RUNNING', level=NOTICE, event_time=5.0, msg="The sump pump has auto started, it will "
                                                                           "stop itself after 2 minutes.")

        events = get_current_events()
        self.assertEqual(len(events), 4)

        expected_results = [
            {
                'tag_name': 'SumpPump',
                'state': 'RUNNING',
                'time': 5.0,
                'level': 'NOTICE',
                'msg': "The sump pump has auto started, it will stop itself after 2 minutes."
            },
            {
                'tag_name': 'Temperature02',
                'state': 'VERY HIGH',
                'time': 2.0,
                'level': 'WARNING',
                'msg': ''
            },
            {
                'tag_name': 'Level01',
                'state': 'HIGH',
                'time': 2.0,
                'level': 'WARNING',
                'msg': ''
            },
            {
                'tag_name': 'pump01',
                'state': 'TRIPPED',
                'time': 1.0,
                'level': 'ALERT',
                'msg': ''
            }
        ]

        for i, v in enumerate(events):
            self.assertEqual(expected_results[i], v)

        clear_all_events()
        events = get_current_events()
        self.assertEqual(len(events), 0)

    def test_get_event_hash(self):
        """
        Tests the event hash value
        """
        events = get_current_events()
        self.assertEqual(len(events), 0)
        h1 = get_event_hash()
        h2 = get_event_hash()
        self.assertEqual(h1, h2)

        set_event('pump01', 'TRIPPED', level=ALERT, event_time=1.0)
        h2 = get_event_hash()
        self.assertNotEqual(h1, h2)
        h1 = get_event_hash()
        self.assertEqual(h1, h2)

        set_event('Temperature02', 'VERY HIGH', level=WARNING, event_time=2.0)
        h2 = get_event_hash()
        self.assertNotEqual(h1, h2)
        h1 = get_event_hash()
        self.assertEqual(h1, h2)

        set_event('Level01', 'HIGH', level=WARNING, event_time=2.0)
        h2 = get_event_hash()
        self.assertNotEqual(h1, h2)
        h1 = get_event_hash()
        self.assertEqual(h1, h2)

        set_event('SumpPump', 'RUNNING', level=NOTICE, event_time=5.0, msg="The sump pump has auto started, it will "
                                                                           "stop itself after 2 minutes.")
        h2 = get_event_hash()
        self.assertNotEqual(h1, h2)
        h1 = get_event_hash()
        self.assertEqual(h1, h2)

    def test_process_escalation_alarms_with_clear(self):
        """
        A simple test on what happens when an alarm goes bad and then recovers
        """
        # Examples of a sequence of alarms for a process
        set_event('flow1', 'LOW', level=WARNING, event_time=1)
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'LOW',
                'time': 1,
                'level': 'WARNING',
                'msg': ''
            }
        )

        set_event('flow1', 'VERY LOW', level=ALARM, event_time=2)

        self.assertRaises(ValueError, clear_event, 'flow1', state='LOW')

        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'VERY LOW',
                'time': 2,
                'level': 'ALARM',
                'msg': ''
            }
        )

        event = get_event('flow1')
        self.assertEqual(
            event,
            {
                'tag_name': 'flow1',
                'state': 'VERY LOW',
                'time': 2,
                'level': 'ALARM',
                'msg': '',
                'stack': [
                    {
                        'tag_name': 'flow1',
                        'state': 'LOW',
                        'time': 1,
                        'level': 'WARNING',
                        'msg': ''
                    },
                ]
            }
        )

        clear_event('flow1', state='VERY LOW', event_time=3)
        events = get_current_events()
        self.assertEqual(len(events), 1, 'No events expected: \n%s' % str(events))
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'LOW',
                'time': 1,
                'level': 'WARNING',
                'msg': ''
            }
        )

        event = get_event('flow1')
        self.assertEqual(
            event, {
                'tag_name': 'flow1',
                'state': 'LOW',
                'last_state': 'VERY LOW',
                'time': 1,
                'level': 'WARNING',
                'last_level': 'ALARM',
                'last_time': 2,
                'msg': ''
            }
        )

        clear_event('flow1', state='LOW', event_time=4)
        events = get_current_events()
        self.assertEqual(len(events), 0)
        event = get_event('flow1')
        self.assertEqual(
            event, {
                'tag_name': 'flow1',
                'state': '',
                'last_state': 'LOW',
                'time': 4,
                'level': 'WARNING',
                'last_time': 1,
            }
        )

    def test_simple_scope_by_time(self):
        """
        A simple test of how alarms assigned a scope work together to auto override
        The default behaviour is to override chronologically
        """
        # Examples of a sequence of alarms for a process
        set_event('flow1', 'LOW', level=WARNING, scope='pump02', event_time=1)
        set_event('flow1', 'VERY LOW', level=ALARM, scope='pump02', event_time=2)
        set_event('pump02', 'TRIPPED', level=NOTICE, scope='pump02', event_time=3)

        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'pump02',
                'state': 'TRIPPED',
                'time': 3,
                'level': 'NOTICE',
                'scope': 'pump02',
                'msg': ''
            }
        )

        event = get_event('pump02')
        self.assertEqual(
            event, {
                'tag_name': 'pump02',
                'state': 'TRIPPED',
                'time': 3,
                'level': 'NOTICE',
                'scope': 'pump02',
                'msg': '',
                'scope_events': [  # Not sure if we just get a list of event tags, or all data?
                    {
                        'tag_name': 'flow1',
                        'state': 'VERY LOW',
                        'time': 2,
                        'level': 'ALARM',
                        'scope': 'pump02',
                        'msg': '',
                        'stack': [
                            {
                                'tag_name': 'flow1',
                                'state': 'LOW',
                                'time': 1,
                                'level': 'WARNING',
                                'scope': 'pump02',
                                'msg': ''
                            },
                        ]
                    }
                ]
            }
        )

    def test_event_persistence(self):
        """
        Confirm persistence activities
        :return:
        """
        set_event('flow1', 'LOW', level=WARNING, event_time=1)
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'LOW',
                'time': 1,
                'level': 'WARNING',
                'msg': ''
            }
        )

        set_event('flow1', 'VERY LOW', level=ALARM, event_time=2, persistence=EVENT_ACK)

        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'VERY LOW',
                'time': 2,
                'level': 'ALARM',
                'acknowledged': False,
                'msg': ''
            }
        )

        clear_event('flow1', state='VERY LOW', event_time=3)
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'VERY LOW',
                'time': 2,
                'level': 'ALARM',
                'acknowledged': False,
                'msg': ''
            }
        )
        acknowledge_event('flow1', 'VERY LOW', user='op1', location='PC2')
        events = get_current_events()
        self.assertEqual(len(events), 1)
        self.assertEqual(
            events[0], {
                'tag_name': 'flow1',
                'state': 'LOW',
                'time': 1,
                'level': 'WARNING',
                'msg': ''
            }
        )

        event = get_event('flow1')
        self.assertEqual(
            event, {
                'tag_name': 'flow1',
                'state': 'LOW',
                'last_state': 'VERY LOW',
                'time': 1,
                'msg': '',
                'level': 'WARNING',
                'last_level': 'ALARM',
                'last_time': 2,
                'last_ack_user': 'op1',
                'last_ack_location': 'PC2'
            }
        )

        clear_event('flow1', state='LOW', event_time=4)
        events = get_current_events()
        self.assertEqual(len(events), 0)
        event = get_event('flow1')
        self.assertEqual(
            event, {
                'tag_name': 'flow1',
                'state': '',
                'last_state': 'LOW',
                'time': 4,
                'level': 'WARNING',
                'last_time': 1,
            }
        )

if __name__ == '__main__':
        unittest.main()
