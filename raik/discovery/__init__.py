"""
Methods used for discovering devices on a local network.
"""
import subprocess
import raik.utils.hardware
import raik.utils.crypt
import time
import math
import base64
import zlib
import tempfile
import os
import socket
import logging

logger = logging.getLogger("raik.discovery")

TIME_SENSITIVITY = 5
MAX_PACKET = 1420
LISTEN_PORT = 61001
CLIENT_PORT = 61002

# https://en.wikipedia.org/wiki/IPv6_address#Multicast_addresses
# http://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml
# http://apple.stackexchange.com/questions/30609/how-can-i-see-my-ipv6-neighbors

# ip -6 neighbor show
# ping6 -c 3 -nI eth1 ff02::1
#  ip -6 neigh
# Note:  probably don't want to appear on ip 6 pings, would like devices to be silent, except for discovery packet
# There's also a tool called NDP.

__do_multicast_ping = lambda eth: subprocess.check_output(
    ("ping6", "-c", "10", "-nI", eth, "ff02::1")
)
__cmd_ip6_neighbours = ("ip", "-6", "neigh")
__valid_nud = ("STALE", "REACHABLE", "NOARP", "PERMANENT")
__rpi_vendor_id = "b8:27:eb"


def neighbour_discovery():
    """
    Performs a simple network discovery with a broadcast ping packet.
    :return: List of neighbour IP addresses and MAC addresses
    """
    # Set out a ping on each interface
    for interface in raik.utils.hardware.list_ll_ifs():
        __do_multicast_ping(interface)
    # Collect IP 6 Neighbor data
    for l in subprocess.check_output(__cmd_ip6_neighbours).decode().split("\n"):
        l = l.strip()
        if not l or "FAILED" in l:
            continue
        l = l.split()
        ip6_addr = l[0]
        assert l[1] == "dev", l
        dev = l[2]
        assert l[3] == "lladdr", l
        lladdr = l[4]
        nud = l[5]
        # if nud in __valid_nud:
        yield (ip6_addr, dev, lladdr)


def get_current_symmetric_key():
    """
        Obscurity is not security.  This will need to be improved
        someday.  It's mainly to avoid replay (conceptually)
    :return:
    """
    t = math.floor(time.time())
    t -= t % TIME_SENSITIVITY
    return ("RAIK %i" % t).encode(encoding="latin_1")


def create_discovery_packet(certificate):
    """
    This function creates a packet (bytes) that will be used for transmitting on the network
    for the purposes of getting discovery data from other RAIK nodes.  It does so by sending out
    a time sensitive broadcast containing a signed public key with which to encrypt return data with.
    The return data will contain host information that will be stored by this node.

    The goal of this packet is to send a packet that can be trusted by other RAIK nodes.  It then
    needs to allow other RAIK nodes to securely send their host details back to this node.

    If someone knows this code, then they can easily spoof their own packets.  However, the protection lies in the
    certificate being signed.
    In the future we might drop the symmetric encryption part, right now it's just for fun and to
    have the code in case we want it later.
    :param certificate: String of signed public key certificate in PEM format.
    """
    packet_data = zlib.compress(raik.utils.crypt.decode_cert_text(certificate))
    if len(packet_data) > (MAX_PACKET - 4):
        raise ValueError("Certificate too big for discovery packet")
    return raik.utils.crypt.symmetric_encrypt(
        get_current_symmetric_key(), packet_data + "RAIK".encode("latin_1")
    )


def get_cert_from_discovery_packet(discovery_packet):
    """
    Given a discovery packet, retrieves the certificate from the packet and validates it.
    A chain file is specified by setting raik.utils.crypt.CERT_VALIDATION_CHAIN
    :param discovery_packet: Raw data retrieved from the network
    :return: certificate data encoded as 'latin_1'
    """
    # Decode and ensure the packet starts with "RAIK"
    # Packet is currently encoded with the current time
    sym_key = get_current_symmetric_key()
    packet_data = raik.utils.crypt.symmetric_decrypt(sym_key, discovery_packet)
    if "RAIK" != packet_data[-4:].decode("latin_1"):
        raise ValueError("Invalid discovery packet or packet is too old")
    packet_data = packet_data[:-4]
    packet_data = zlib.decompress(packet_data)
    packet_data = base64.b64encode(packet_data)
    packet_data = packet_data.decode("latin_1")
    i = range(int(len(packet_data) / 64) + 1)
    packet_data = "\n".join(
        ["-----BEGIN CERTIFICATE-----"]
        + [packet_data[a * 64 : (a + 1) * 64] for a in i]
        + ["-----END CERTIFICATE-----\n"]
    )
    # Save the file temporarily for openssl validation
    with tempfile.NamedTemporaryFile() as f:
        f.write(packet_data.encode())
        f.flush()
        if not raik.utils.crypt.verify_certificate(f.name):
            raise ValueError("Discovery packet failed certificate validation")
    # Return the data
    return packet_data


def get_host_data(small_data=False):
    """
    Returns data for this host to share with other hosts.  This is an abbreviated dictionary with the
    following keys:
    * n - Host name
    * os - OS Name
    * os_r - OS Release (excluded in short form)
    * os_v - OS Version (excluded in short form)
    * a - Architecture
    * ll - Link data (another dictionary, keyed per DEV, containing a tuple of (mac, [addresses])
    * u - up time
    * id - a UUID for the host.

    :param small_data: Reduces to the length of the data by omitting information/
    :return: Dictionary containing values as described above
    """
    host_name = socket.gethostname()
    os_details = os.uname()

    link_data = {}
    for dev, mac in raik.utils.hardware.get_mac_addresses():
        if dev in link_data:
            logger.error("Unexpected system configuration:  multiple MACs for %s" % dev)
            continue
        # logger.debug('Adding device %s with MAC (%s) to link_data', dev, mac)
        link_data[dev] = [mac, []]

    for dev, addr in raik.utils.hardware.list_addr_info(ipv6_only=True):
        if dev not in link_data:
            logger.error(
                "Host provided interface address for unknown interface %s" % dev
            )
            continue
        # logger.debug('Adding address %s to device %s', addr, dev)
        link_data[dev][1].append(addr)

    if small_data:
        # Purge MAC Addresses that aren't connected
        del_dev = []
        for dev, dev_data in link_data.items():
            if not dev_data[1]:
                del_dev.append(dev)
        for dev in del_dev:
            del link_data[dev]

    host_data = {
        "n": host_name,
        "os": os_details.sysname,
        "a": os_details.machine,
        "ll": link_data,
        "u": raik.utils.hardware.get_up_time(),  # This will also make sure the data packet changes
        # on subsequent requests
        "id": raik.utils.hardware.get_machine_id(),
    }
    if not small_data:
        host_data.update({"os_r": os_details.release, "os_v": os_details.version})

    return host_data
