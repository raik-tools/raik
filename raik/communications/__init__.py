"""
    Provides functions for communication across the various raik systems.  It implements simple persistent messaging and
    registration protocols.
"""
import redis
import json
from datetime import datetime
import pytz


###############################################################################
#  The following code should be refactored out for a simpler, flatter structure
###############################################################################

SYSTEM_KEY_STORE = None

# Internal constants
_DEFAULT_HOST = 'localhost'
_DEFAULT_PORT = 6379
_SYSTEM_STORE_DB = 5

# Module level constants
NODE_HB_TIMEOUT = 10


class RedisKeyStoreWrapper:
    """
        A lightweight class to wrap key store operations
    """
    def __init__(self, host=_DEFAULT_HOST, port=_DEFAULT_PORT):
        self._ks = redis.StrictRedis(host=host, port=port, db=_SYSTEM_STORE_DB)

    def get(self, k):
        v = self._ks.get(k)
        if v is None:
            return None
        return json.loads(v.decode('utf-8'))

    def set(self, k, v):
        self._ks.set(k, json.dumps(v).encode('utf-8'))

    def clear(self):
        if self._ks.dbsize() > 0:
            self._ks.flushdb()

    def __len__(self):
        return self._ks.dbsize()


def init_system_store():
    """
    This needs to be called to initialise the module's key store.
    :return: KeyStoreWrapper
    """
    global SYSTEM_KEY_STORE
    SYSTEM_KEY_STORE = connect_system_key_store()
    SYSTEM_KEY_STORE.clear()
    return SYSTEM_KEY_STORE


def connect_system_key_store():
    global SYSTEM_KEY_STORE
    if SYSTEM_KEY_STORE is not None:
        return SYSTEM_KEY_STORE
    return RedisKeyStoreWrapper()


def timestamp_seconds():
    """
    Calculates the timestamp to be used in communications.  Currently it's seconds (float) since
    the start of 1970 in UTC.

    This is always a seconds value and is used for simple calculations.

    :return: The system timestamp
    """
    return datetime.now(pytz.utc).timestamp()
