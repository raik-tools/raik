# import operator  # There are some possibly unsafe tricks in this lib
# import functools

from raik import FunctionBlock, InputArrayProperty, StateProperty


module_interface = {"type": "function_block", "export": "And"}


class And(FunctionBlock):
    """
    A simple And block used to preserve connections between function
    blocks.  This is a special block because it allows multiple inputs
    to be assigned.

    :cvar out:  The output of the block
    :cvar in_array:  The inputs to perform a logical AND with
    """

    # State flags
    out: bool = StateProperty(False)

    # Inputs
    in_array: bool = InputArrayProperty()

    def block_scan(self):
        if not self.in_array:
            self.out = False
        # state.out = functools.reduce(operator.and_, inputs.in_array) # TODO
        self.out = self.in_array
