import unittest
import importlib
from .. import PiFace_Digital
import pifacedigitalio


class TestScanModule(unittest.TestCase):
    def setUp(self):
        importlib.reload(pifacedigitalio)
        importlib.reload(PiFace_Digital)
        # PiFace_Digital.reset_module()

    def test_no_module(self):
        """
            Tests error when no module exists
        """
        PiFace_Digital.m = None
        ret = PiFace_Digital.scan_module()
        self.assertIsInstance(ret, dict)
        self.assertIn('error', ret)
        self.assertEqual(ret['error'], 'Module not loaded')

    @unittest.skipIf(PiFace_Digital.m is None, 'Physical module not detected')
    def test_real_module(self):
        """
            Tests the real module if it is available
        """
        self.assertIsNotNone(PiFace_Digital.m)
        self.fail('Implementation not complete')

    def test_fake_module(self):
        """
            Fakes the module behaviour
        """
        # Set up fake modules
        class FakePort:
            def __init__(self):
                self.value = 0

        class FakeModule:
            def __init__(self):
                self.input_port = FakePort()
                self.output_port = FakePort()
            # def deinit_board(self):
            #     pass

        # PiFace_Digital.m = 'Fake'
        # in_object = FakeChannels()
        # out_object = FakeChannels()
        # PiFace_Digital.m_out = out_object
        # PiFace_Digital.m_in = in_object
        pifacedigitalio.PiFaceDigital = FakeModule
        importlib.reload(PiFace_Digital)
        f = PiFace_Digital.m

        # Perform Test no input arguments
        f.input_port.value = 0x01
        f.output_port.value = 0x01
        ret = PiFace_Digital.scan_module()
        self.assertIsInstance(ret, dict)
        self.assertNotIn('error', ret)
        expected_in = {
            'DI/0': True,
            'DI/1': False,
            'DI/2': False,
            'DI/3': False,
            'DI/4': False,
            'DI/5': False,
            'DI/6': False,
            'DI/7': False
        }
        self.assertDictEqual(ret, expected_in)
        self.assertEqual(f.output_port.value, 0x00)

