import unittest
import raik.utils.hardware
from raik.utils.hardware import *
import subprocess
import os
import tempfile


class TestNICFunctions(unittest.TestCase):
    """
    Unit test for NIC functions.
    """
    @classmethod
    def setUpClass(cls):
        # link_dev = {}
        # for l in subprocess.check_output(['ip', '-o', 'link']):
        #     _, _dev, _data = l.split(':', 2)
        #     link_dev[_dev] = _data.split('\\')
        # cls.link_dev = link_dev
        #
        # addresses = []
        # for l in subprocess.check_output(['ip', '-o', 'addr']):
        #     l = l.split(':', 1)[2].split('\\')
        #
        #
        #     link_dev[_dev] = _data.split('\\')
        # cls.link_dev = link_dev
        cls.link_dev = os.listdir('/sys/class/net/')
        cls.addresses_6 = {}
        with open('/proc/net/if_inet6', 'r') as f:
            for l in f:
                l = l.split()
                _dev = l[-1]
                _addr = l[0]
                cls.addresses_6.setdefault(_dev, []).append(_addr)

    def test_get_mac_addresses(self):
        x = get_mac_addresses()
        for dev, addr in x:
            self.assertIn(dev, self.link_dev)
            with open('/sys/class/net/%s/address' % dev, 'r') as f:
                m = f.read().strip()
            self.assertEqual(addr, m)

    def test_list_addr_info(self):
        x = list_addr_info()
        for dev, addr in x:
            self.assertNotEqual(dev, '')
            self.assertTrue('.' in addr or ':' in addr)

    def test_list_addr6_info(self):
        x = list_addr_info(ipv6_only=True)
        for dev, addr in x:
            self.assertNotEqual(dev, '')
            self.assertIn(':', addr)
            self.assertNotIn('.', addr)

    def test_list_ll_ifs(self):
        x = list_ll_ifs()
        for dev in x:
            self.assertNotEqual(dev, '')


class TestSysFunctions(unittest.TestCase):
    """
    Unit test for System functions.
    """
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_up_time(self):
        x = get_up_time()
        self.assertIsInstance(x, int)
        self.assertNotEqual(x, 0)
        self.assertGreater(x, 0)

    def test_get_machine_id(self):
        temp = tempfile.NamedTemporaryFile(buffering=0)
        raik.utils.hardware.UUID_LOCATION = temp.name
        temp.write('Hello UUID!'.encode('ASCII'))
        self.assertEqual(get_machine_id(), 'Hello UUID!')
        temp.close()
        self.assertEqual(get_machine_id(), '')
