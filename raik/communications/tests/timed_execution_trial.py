import time
import json
import timeit

period = 0.5
current_time = time.time()
datum = current_time
current_offset = current_time - datum
sleep_time = period - current_offset % period
start_test = time.time()
end_test = start_test
error = start_test - end_test

for i in range(10):
    current_time = time.time()
    start_test = current_time - current_time % period
    wake_up = start_test + period
    sleep_time = wake_up - current_time
    time.sleep(sleep_time * 0.8)
    while time.time() < wake_up:
        time.sleep(0.00005)
        # pass  # This option is very accurate but consumes CPU
    end_test = time.time()
    error = (end_test - start_test) - period
    print('%f,  Error: %f' % (wake_up, error))

# Notes:  The error seems to be consistent to about 10% of the sleep time.

# Additional timing tests

# Note:  Manual timing shows that time.time() is significantly faster than any datetime option.

# # Testing different ways of combining k,v pairs
# def value_map(_x):
#     return json.loads(_x.decode('utf-8'))
#
# snap = {'c': 3, 'a': 2}
#
#
# def filter_values(_x):
#     if _x[1] != snap.get(_x[0]):
#         return True
#     else:
#         return False
#
# keys = ['a', 'b', 'c', 'd']
# values = [b'1', b'2', b'3', b'4']
#
#
# t = timeit.timeit('list(filter(filter_values, zip(keys, map(value_map, values))))',
#                   setup="from __main__ import filter_values, keys, value_map, values")
# print('Chaining iterators with filter:  %f' % t)
#
# t = timeit.timeit('dict(zip(keys, map(value_map, values)))',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap")
# print('Make a dict (no filter):  %f' % t)
#
# t = timeit.timeit('{k: value_map(v) for k,v in zip(keys, values)}',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap")
# print('Dict comprehension (no filter):  %f' % t)
#
# t = timeit.timeit('{k: v for k,v in zip(keys, map(value_map, values))}',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap")
# print('Dict comprehension with map(no filter):  %f' % t)
#
# t = timeit.timeit('{k: v for k,v in zip(keys, map(value_map, values)) if snap.get(k) != v}',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap")
# print('Dict comprehension with map with filter:  %f' % t)
#
# t = timeit.timeit('r = {}\n'
#                   'for i, k in enumerate(keys):\n'
#                   '    v = value_map(values[i])\n'
#                   '    if snap.get(k) != v:\n'
#                   '        r[k] = v',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap")
# print('Full out with filter:  %f' % t)
#
#
# import threading
# l = threading.Lock()
# t = timeit.timeit('r = {}\n'
#                   'with l:\n'
#                   '    for i, k in enumerate(keys):\n'
#                   '        v = value_map(values[i])\n'
#                   '        if snap.get(k) != v:\n'
#                   '            r[k] = v',
#                   setup="from __main__ import filter_values, keys, value_map, values, snap, l")
# print('Full out with filter and lock:  %f' % t)
