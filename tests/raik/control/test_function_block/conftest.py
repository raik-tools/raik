import pytest
from typing import Type, Union

from tests.raik.control.fixtures.project_lib.simple_dol import SimpleDOL
from raik import ExternalBlock


@pytest.fixture
def simple_dol(raik_local_communications) -> Type[SimpleDOL]:
    return SimpleDOL


@pytest.fixture
def external_block(
    raik_local_communications,
) -> Union[Type[ExternalBlock], Type[SimpleDOL]]:
    return ExternalBlock
