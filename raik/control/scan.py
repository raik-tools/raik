"""
    This file is dedicated to the Scan cycle and functions intended to execute in a dedicated context
    as part of emulating a real time scan cycle.  This has been relocated, onto comments:

    Continuous logic scan.

    The control module is responsible for executing logic continuously based on a configuration file.

    The scan cycle maintained by the control module is typical of real time scan loops:
    * Collect inputs
    * Process function on an event
    * Write to the outputs

    Each function that is configured to scan continuously will do so in a separate process to ensure that one failure
    doesn't interrupt the rest of the system.

    Recover from all errors where possible, unless a fatal error occurs (such as a scan timing out) where the system
    should fail.

    Note that this system is designed for parallelism.  In a typical PLC, interrupts call the functions and often
    the functions will execute one after the other for a particular class of interrupt (noting that some scans can
    be paused to allow other higher priority interrupts to run).  The intention with this system is to call all methods
    in parallel and let the OS manage interruption through process priority (nice values).  Determinism in sequence is
    then not required as it is expected that sequence sensitive logic will all exist within a single function.
    Determinism of completion and timing is provided by timeouts for each function.

"""
import os
import tempfile
import raik.communications.local
import signal
import importlib
import time
import logging
import traceback
import threading
from raik.control.functions import set_paths, validate_function_body, compile_function, execute_function
import raik.control.functions

logger = logging.getLogger('raik.control.scan')
retry_time_long = 20
retry_time_medium = 10


def continuous_scan(instance_id):
    """
    This function executes continuously.  Ending the function can only be done by ending the thread / process calling
    this function.  Status for this instance is written back into the local value buffer as "<instance_id>_status.

    The status is a dictionary with the following keys:
    's' - Text describing the execution status.  This can be 'starting', 'running', 'terminated', 'stalled running'
          or a message for an exception.
    'lt' - (only while running) System epoch time that the last execution started
    't' - (only while running) Duration of the last execution
    '_p' - FUTURE:  The process ID of the running process
    'n' - FUTURE: The time that the next run is expected to execute.  If 'lt' < 'n' < now then the process can be
          considered as having stalled.
    '_t' - An internal value of time to indicate when the value was last updated

    This function will perform the following continuously:
    * Read configuration based on a process instance ID
    * Test and compile the function bytecode
    * Continuously do the following until an error occurs, then return to the outer loop
    ** Call the execute function method
    ** Update the execution status
    * Perform as much error trapping as possible

    Note that if the function is taking too long, there's no means of interrupting the execution within this function.
    This function is expecting to run in its own process, so it is up to the external caller to monitor the status
    and kill the process if it has stalled

    :param instance_id: the name of the instance in the library
    """
    control_lib_path, site_config_path = raik.control.functions.get_paths()

    status_channel = instance_id + '_status'
    logger.info('Starting execution for %s', instance_id)
    logger.debug('Module control lib path: %s', control_lib_path)
    logger.debug('Module site path: %s', site_config_path)
    pid = os.getpid()
    # os.environ['RAIK_PROCESS'] = instance_id
    temp_file_name = os.path.join(tempfile.gettempdir(), 'RAIK_%i' % pid)
    temp_file = open(temp_file_name, 'w')
    temp_file.write(instance_id)
    temp_file.flush()
    conf = raik.communications.local.get_config(instance_id)

    # Conf may return None, this could be a race condition.  Perform a retry
    retry = 0
    if not conf and retry < 3:
        time.sleep(0.5)
        logger.debug('No configuration returned, retrying')
        conf = raik.communications.local.get_config(instance_id)
        retry += 1
    if conf:
        conf_age = time.time() - conf['_t']
        if conf_age > 10:
            raise TimeoutError('Configuration age is too old to be processed.')
        logger.debug('Configuration found')
    else:
        logger.debug('No configuration found')

    class SignalException(Exception):
        pass

    # noinspection PyUnusedLocal
    def signals_handler(signum, frame):
        """
        Handle all signals gracefully, this is used in the continuous scan method.
        :param signum: signal number
        :param frame: execution frame?
        :return:
        """
        logger.info('Signal %i received by process' % signum)
        if signum == signal.SIGUSR1:
            logger.debug('Request to refresh config received')
            test_conf = raik.communications.local.get_config(instance_id)
            if not test_conf:
                conf['_t'] = time.time()
                raik.communications.local.set_config(instance_id, conf)
        else:
            logger.debug('Request terminate process received')
            temp_file.close()
            raise SignalException('Signal %i terminated process' % signum)
    signal.signal(signal.SIGABRT, signals_handler)
    # signal.signal(signal.SIGFPE, signals_handler) - could lead to a infinite loop
    signal.signal(signal.SIGILL, signals_handler)
    signal.signal(signal.SIGINT, signals_handler)
    # signal.signal(signal.SIGSEGV, signals_handler) - could lead to a infinite loop
    signal.signal(signal.SIGTERM, signals_handler)
    signal.signal(signal.SIGUSR1, signals_handler)

    # Configuration checking thread
    # config_check_thread_active = False
    # def config_check_thread():
    #     nonlocal config_check_thread_active
    #     config_check_thread_active = True
    #     while config_check_thread_active:
    #         time.sleep(3)
    #         # Write back config in case of data loss
    #         test_conf = raik.communications.local.get_config(instance_id)
    #         if not test_conf:
    #             conf['_t'] = time.time()
    #             raik.communications.local.set_config(conf)
    # conf_thread = threading.Thread(target=config_check_thread)
    # conf_thread.start()
    loop_status = ''
    while True:
        # This is intended to be "bulletproof" and keep trying even if an exception occurs.
        # noinspection PyBroadException
        loop_status = 'starting'
        logger.info('Starting Major loop for function instance %s', instance_id)
        # TODO:  Update this to honour namespace
        # noinspection PyBroadException
        try:
            raik.communications.local.set_value(status_channel,
                                                {'s': loop_status, '_t': time.time()})
            if not conf:
                raise RuntimeError('Configuration for %s not found!  Process aborting.' % instance_id)
            fx_name = conf['function']

            # Find the function, check site specific first then import system
            # set the control_fx as the function object.
            fx_file_name = os.path.join(site_config_path, fx_name + '.py')
            logger.debug('Function file name: %s', fx_file_name)
            control_fx = None
            if os.path.exists(os.path.join(control_lib_path, 'raik_control_io', fx_name + '.py')):
                logger.debug('Loading function from I/O library: %s', fx_name)
                import sys
                logger.debug(sys.path)
                # Don't allow override of I/O function names, scan these first
                lib_mod = importlib.import_module('raik_control_io.' + fx_name)
                control_fx = getattr(lib_mod, 'scan_module')
            elif os.path.exists(fx_file_name):
                logger.debug('Function found in path')
                # Linux allows multiple access to files that are be written to.  This means, we could
                # end up with an empty string or incomplete function.  We'll need to test for function
                # completion.
                def_pos = None
                for _ in (1, 2, 3,):
                    with open(fx_file_name, 'r+') as f:
                        file_text = f.read()
                    if 'def ' in file_text and file_text[-6:] == '# EOF\n':
                        def_pos = file_text.index('def ')
                        break
                    time.sleep(0.3)
                if def_pos is None:
                    raise OSError('Control file is being written to or is corrupted: %s' % fx_file_name)
                body_start = file_text.index(':', def_pos + 1) + 1
                body_end = file_text.rindex('return ')
                try:
                    validate_function_body(file_text[body_start:body_end])
                except SyntaxError as e:
                    loop_status = 'error'
                    raik.communications.local.set_value(status_channel, {
                        's': 'Function syntax error: %s' % str(e),
                        '_t': time.time()
                    })
                    time.sleep(retry_time_long)
                    continue
                try:
                    control_fx = compile_function(fx_name, file_text)
                except KeyError:
                    loop_status = 'error'
                    raise Exception('Error compiling function %s in script: \n%s' % (fx_name, file_text))
            else:
                logger.debug('Searching for function in control library %s', control_lib_path)
                # Search the libraries, in alphabetical order and import accordingly
                for lib_path in sorted(os.listdir(control_lib_path)):
                    lib_path = os.path.join(control_lib_path, lib_path)
                    if os.path.isdir(lib_path) and lib_path != site_config_path:
                        fx_file_name = os.path.join(lib_path, fx_name + '.py')
                        logger.debug('Testing: %s', fx_file_name)
                        if os.path.exists(fx_file_name):
                            # Found the file in the import library
                            lib_name = os.path.basename(lib_path)
                            lib_mod = importlib.import_module(lib_name + '.' + fx_name)
                            control_fx = getattr(lib_mod, fx_name)

            # If no function to call, set error and pause before re-looping
            if control_fx is None:
                loop_status = 'error'
                raise FileNotFoundError('Function %s not found' % fx_name)

            # A function should now be set, determine the schedule and execute the function
            schedule_type = conf['timing']['type']
            if schedule_type == 'cyclic':
                period_s = conf['timing']['period']
                timeout = 0.8 * period_s  # This is arbitrary, but ideally we should be finished 80% before the next run
                # execute the function continuously
                execution_time = -1.0
                start_time = -1.0
                loop_status = 'running'
                raik.communications.local.set_value(status_channel, {'s': loop_status,
                                                                     'lt': start_time,
                                                                     't': execution_time,
                                                                     '_t': time.time()
                                                                     })

                def scan_check_thread():
                    while True:
                        time.sleep(max(3.0, period_s))
                        # If the task is running and the last start time is older than the period, mark it as stalled
                        if loop_status == 'running':
                            if start_time < (time.time() - period_s):
                                raik.communications.local.set_value(status_channel, {'s': 'stalled running',
                                                                                     'lt': start_time,
                                                                                     't': execution_time,
                                                                                     '_t': time.time()
                                                                                     })
                                logger.debug('Scan check thread detecting stall')
                                break
                        else:
                            break
                    logger.debug('Scan check thread aborting')

                t = threading.Thread(target=scan_check_thread)
                t.start()

                while True:
                    start_time = time.time()
                    raik.communications.local.set_value(status_channel, {'s': 'running',
                                                                         'lt': start_time,
                                                                         't': execution_time,
                                                                         '_t': time.time()
                                                                         })
                    execution_time = execute_function(conf['inputs'], control_fx, conf['outputs'], timeout, start_time)
                    sleep_time = period_s - (time.time() - start_time)
                    time.sleep(sleep_time)

            else:
                raise AttributeError('Unknown function timing: %s' % schedule_type)

        except SignalException as e:
            logger.warning("Process terminated: %s" % str(e))
            raik.communications.local.set_value(status_channel, {'s': 'terminated',
                                                                 '_t': time.time()})
            break
        except RuntimeError as e:
            logger.error("Process RunTimeError: %s" % str(e))
            raik.communications.local.set_value(status_channel, {'s': 'terminated',
                                                                 '_t': time.time()})
            break
        except Exception as e:
            raik.communications.local.set_value(status_channel, {'s': 'exception: %s' % str(e),
                                                                 '_t': time.time()
                                                                 })
            logger.warning('Exception occurred in continuous scan')
            logger.warning(traceback.print_exc())
            time.sleep(retry_time_medium)
            continue
        except:
            import sys
            e = sys.exc_info()[0]
            raik.communications.local.set_value(status_channel, {'s': 'unexpected exception: %s' % str(e),
                                                                 '_t': time.time()
                                                                 })
            time.sleep(retry_time_medium)
            continue
    # config_check_thread_active = False
    logger.info('Closing temporary file')
    temp_file.close()
    os.remove(temp_file_name)
    logger.info('Leaving Thread')
    # logger.info('Waiting for config check thread to end')
    # conf_thread.join(5)
