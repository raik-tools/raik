def add_two(a, b, **kwargs):
    """
    Not a particularly useful function, but exists to test library imports
    :param a: a
    :param b: b
    :return: a + b
    """
    x = a + b
    return {'x': x}
# EOF
