from typing import TypeVar
import decimal

Number = TypeVar('Number', int, float, decimal.Decimal)
