"""
    RAIK Global
"""
import os

# This module must be imported first and this global flag set to True before any other module is loaded.  Each module
# that operates differently under testing, will refer to this variable.

# TEST_MODE needs to get refactored out in favour of pytest fixtures
TEST_MODE = False
TEST_CALLBACKS = []
SYSTEM_CONFIGURATION_LOCATION = '/etc/raik'
PORTAGE_HOME = '/var/tmp/portage'
ROOT_HOME = '/home/root'
PORTAGE_PUB_KEY = '/var/tmp/portage/id_rsa.pub'
ROOT_PUB_KEY = '/var/tmp/portage/root_id_rsa.pub'
PORTAGE_PACKAGE_KEYWORDS = '/etc/portage/package.accept_keywords'
RAIK_USERNAME = 'raikad'
RAIK_HOME = '/home/raikad'
RAIK_RUNTIME_NAME = 'raik_runtime'
RAIK_RUNTIME_PATH = os.path.join(RAIK_HOME, RAIK_RUNTIME_NAME)
RAIK_LIB_NAME = 'raik'
RAIK_LIB_PATH = os.path.join(RAIK_HOME, RAIK_LIB_NAME)
RAIK_WEB_NAME = 'raik_web'
RAIK_WEB_PATH = os.path.join(RAIK_HOME, RAIK_WEB_NAME)
RAIK_USER_PUB_KEY = RAIK_HOME + '/.ssh/id_rsa.pub'
RAIK_USER_AUTH_KEYS = RAIK_HOME + '/.ssh/authorized_keys'
