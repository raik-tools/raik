import raik.enable_testing
import unittest
from datetime import datetime
import pytz
from uuid import uuid4
import raik.communications
from raik.communications import access
import random
random.seed()

raik.communications.init_system_store()


def sim_node_system_init(n_id, age=0, first_request=False):
    """
    Simulates node initialisation of node system data on first node contact.
    :param n_id: id of a node
    :param age: How many seconds old to "init" the node.
    :param first_request: Simulate this packet to include a request for data, but no return data
    :return:
    """
    sim_data_down = {
        'rt': first_request and datetime.now(pytz.utc).timestamp() or None
    }
    sim_data_up = {
        'at': datetime.now(pytz.utc).timestamp() - age,
        'rt': None,
        'p': None
    }
    raik.communications.SYSTEM_KEY_STORE.set(
        'status:up:' + n_id,
        sim_data_up
    )
    raik.communications.SYSTEM_KEY_STORE.set(
        'status:down:' + n_id,
        sim_data_down
    )


def sim_node_communications(n_id, update_heartbeat=True, sim_payload=True,
                            payload_age=3):
    """
    Simulates communications back from a node.
    :param n_id: The id of the node to simulate
    :param update_heartbeat: Simulate the heartbeat being updated
    :param trigger_request: Triggers a request to the node for health data
    :param sim_payload: Simulates the payload back from the node.
    :param payload_age: Specify the age of the payload
    :return:
    """
    status_up_id = 'status:up:' + n_id
    # as well as the d_up['rt'] value
    d_up = raik.communications.SYSTEM_KEY_STORE.get(status_up_id)
    status_down_id = 'status:down:' + n_id
    d_down = raik.communications.SYSTEM_KEY_STORE.get(status_down_id)
    down_rt = d_down and d_down['rt'] or None
    last_rt = d_up['rt']

    now = datetime.now(pytz.utc).timestamp()

    if update_heartbeat:
        d_up['at'] = now

    if sim_payload:
        # If there is a new request for data, indicate so
        new_request = last_rt is not None and down_rt is not None and down_rt > last_rt \
                      or (last_rt is None and down_rt is not None)

        # If the last request was recent, update the pay load
        if last_rt and (now - last_rt) <= 10:
            pay_load = {
                'cpu': random.random(),
                'mem': random.random(),
                'net': random.random(),
                't_cpu': random.random() * 100,
                'up': random.randint(60, 1000000),
                'time': now - 1.2,
                'rt': down_rt - payload_age  # Previous request read
            }
            d_up['p'] = pay_load

        if new_request:
            d_up['rt'] = down_rt

    raik.communications.SYSTEM_KEY_STORE.set(status_up_id, d_up)
    return d_up


def sim_full_loop(n_id):
    """
        Brings the image to a state where there's full node data
    :param n_id:
    :return:
    """
    sim_node_system_init(n_id)              # <-- Node makes contact
    access.get_node_status_detail(n_id)     # <-- This sends a request
    sim_node_communications(n_id, payload_age=0)           # <-- This should update to say it's read the request
    sim_node_communications(n_id, payload_age=0)           # <-- The payload should update on this cycle


class TestNodeStatus(unittest.TestCase):

    def test_get_node_status(self):
        """
        Tests both the get_node_status and get_node_status_detail methods
        """
        raik.communications.init_system_store()

        n_id = str(uuid4())
        d = access.get_node_status_detail(n_id)
        self.assertIsNone(d, "Data for uuid returned unexpectedly")
        self.assertIsNone(access.get_node_status(n_id), "Data for uuid returned unexpectedly")

        # Simulate an old node
        sim_node_system_init(n_id, age=600)
        d = access.get_node_status_detail(n_id)
        self.assertEqual(d['status'], 'Offline')
        self.assertEqual(access.get_node_status(n_id), 'Offline')

        # Simulate a node without a request
        sim_node_system_init(n_id)
        self.assertEqual(access.get_node_status(n_id), 'Online')

        # The moment we ask for detail data, a request will be sent
        d = access.get_node_status_detail(n_id)  # <-- This sends a request
        self.assertEqual(d['status'], 'Requested')

        # Simulate a node with a request, but no payload
        d = access.get_node_status_detail(n_id)  # <-- A request is already sent
        sim_node_communications(n_id)            # <-- This should update to say it's read the request
        self.assertEqual(d['status'], 'Requested')
        self.assertEqual(access.get_node_status(n_id), 'Online')

        # Simulate a node that should have read the data
        d = access.get_node_status_detail(n_id)  # <-- Will see the last sim communications step
        self.assertEqual(d['status'], 'Request Read')
        self.assertEqual(access.get_node_status(n_id), 'Online')

        # Simulate a node with a request and an updated payload
        sim_node_communications(n_id)            # <-- The payload should update on this cycle
        d = access.get_node_status_detail(n_id)
        self.assertEqual(d['status'], 'Online')
        self.assertEqual(access.get_node_status(n_id), 'Online')
        self.assertIsNotNone(d['metrics'])
        self.assertIsNotNone(d['metrics']['cpu'])
        self.assertIsNotNone(d['metrics']['time'])
        self.assertIsNotNone(d['metrics']['mem'])

        # Simulate a node with an out of date payload
        # sim_node_communications(n_id, payload_age=60)  # <-- The node will
        # d = access.get_node_status_detail(n_id)
        # self.assertEqual(d['status'], 'Request Read')
        # self.assertEqual(access.get_node_status(n_id), 'Online')


