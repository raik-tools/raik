import logging
import pifacedigitalio
logger = logging.getLogger('RAIK_IO:PiFace_Digital')
m = None
m_in = None
m_out = None
try:
    # Module Init
    m = pifacedigitalio.PiFaceDigital()
    m_in = m.input_port
    m_out = m.output_port
except Exception as e:
    logger.error('Unable to load module:  %s' % str(e))


# def reset_module():
#     global m, m_in, m_out, logger
#     # De-init existing module if it exists
#     if m and isinstance(m, pifacedigitalio.PiFaceDigital):
#         m.deinit_board()
#     m_in = None
#     m_out = None
#     m = None
#     try:
#         # Module Init
#         m = pifacedigitalio.PiFaceDigital()
#         m_in = m.input_port
#         m_out = m.output_port
#     except Exception as e:
#         logger.error('Unable to load module:  %s' % str(e))
# reset_module()


# Scanning
def scan_module(**outputs):
    """
    PiFace Digital I/O Board (without expansion) inputs and outputs.

    :param outputs: See outputs description in docs.
    :return:  See inputs description in docs.

    Inputs (return value)
    ------
    DI/0 to DI/7:  Corresponding to board digital inputs 0 to 7
    error:  If a board error occurs, this will return an error string.

    Outputs (as arguments)
    -------
    DO/0 to DO/7:  Corresponding to board digital outputs 0 to 7

    """

    if m is None:
        return {
            'DI/0': None,
            'DI/1': None,
            'DI/2': None,
            'DI/3': None,
            'DI/4': None,
            'DI/5': None,
            'DI/6': None,
            'DI/7': None,
            'error': 'Module not loaded'
        }

    # Read inputs
    v_in = m_in.value

    # Write outputs
    v_out = (
        (outputs.get('DO/0') or 0) +
        (outputs.get('DO/1') or 0) * 0x02 +
        (outputs.get('DO/2') or 0) * 0x04 +
        (outputs.get('DO/3') or 0) * 0x08 +
        (outputs.get('DO/4') or 0) * 0x10 +
        (outputs.get('DO/5') or 0) * 0x20 +
        (outputs.get('DO/6') or 0) * 0x40 +
        (outputs.get('DO/7') or 0) * 0x80
    )
    m_out.value = v_out

    return {
        'DI/0': v_in & 0x01,
        'DI/1': v_in & 0x02,
        'DI/2': v_in & 0x04,
        'DI/3': v_in & 0x08,
        'DI/4': v_in & 0x10,
        'DI/5': v_in & 0x20,
        'DI/6': v_in & 0x40,
        'DI/7': v_in & 0x80
    }
# EOF
