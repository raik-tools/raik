import unittest
from raik.utils.deploy import *
import subprocess
import os
import sys
import tempfile
import time
import socket
import shutil

SSHD_PORT = 64022
SSHD_CONFIG_TEMPLATE = """
# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.
Port %(port)s
ListenAddress 127.0.0.1
ListenAddress ::1

# HostKeys for protocol version 2
HostKey %(host_key)s
#HostKey /etc/ssh/ssh_host_dsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

RSAAuthentication yes
PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile     %(auth_key)s

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#RhostsRSAAuthentication no
# similar for protocol version 2
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# RhostsRSAAuthentication and HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
ChallengeResponseAuthentication yes

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM no

# NOTE:  According to the manual, is UsePAM is enabled then sshd must
# run as a ROOT user.  

AllowAgentForwarding no
AllowTcpForwarding no
#GatewayPorts no
#PermitTTY yes
PrintMotd no
PrintLastLog no
#TCPKeepAlive yes
#UseLogin no
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
PidFile %(pid_file)s
#MaxStartups 10:30:100
PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# override default of no subsystems
# Subsystem       sftp    /usr/lib/misc/sftp-server
Subsystem sftp internal-sftp

# allow the use of the none cipher
#NoneEnabled no

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server

# Allow client to pass locale environment variables #367017
AcceptEnv LANG LC_*

# Research:  https://serverfault.com/questions/344295/is-it-possible-to-run-sshd-as-a-normal-user
# 
# NOTE:  It appears that UsePrvilegeSeparation is not supported on OpenSSH >= 7.5
# Whichi s probably why this used to work.  I can turn of PAM and it seems to work but then 
# password authentication is no longer available.
#
# Extra bits for running as non-root
UsePrivilegeSeparation no
StrictModes no
LogLevel DEBUG3
# LogLevel QUIET
SyslogFacility DAEMON
# AllowedUsers
"""
SSH_CONFIG = SSHD_CONFIG_TEMPLATE % ({
    'host_key': '/etc/ssh/ssh_host_rsa_key',
    'pid_file': '/run/sshd.pid',
    'auth_key': '/tmp/test_ssh/authorized_keys',
    'port': SSHD_PORT
})


class TestSSHAccess(unittest.TestCase):
    """
    Unit test for SSH access.  For this to work, a local SSH server needs to be
    instantiated on a test port.  Everything will run as the user running this test.

    """

    # Might consider using environment variables in the future if this test becomes automated.
    # For now - just enter in password during test

    # In the future these tests should probably make use of some kind of container where we can
    # test communication and run ssh as "root" with a confined PAM implementation.

    # WARNING:  See notes above on why this is no longer available (OpenSSH >= 7.5).
    pwd = ''

    @classmethod
    def setUpClass(cls):
        temp_host_key_name = '/tmp/TestSSHAccess_host_rsa_key'
        temp_client_key_name = '/tmp/TestSSHAccess_client_rsa_key'
        temp_pid_file = '/tmp/TestSSHAccess_sshd_pid'
        temp_auth_file = '/tmp/TestSSHAccess_auth'
        temp_sshd_conf_file = '/tmp/TestSSHAccess_sshd_conf'

        for x in [temp_host_key_name, temp_client_key_name, temp_pid_file, temp_auth_file,
                  temp_sshd_conf_file]:
            if os.path.exists(x):
                os.remove(x)

        # Generate test keys
        for x in [temp_host_key_name, temp_client_key_name]:
            subprocess.check_output([
                'ssh-keygen',
                '-t',
                'rsa',
                '-b',
                '4096',
                '-f',
                x,
                '-N',
                ''
            ])

        sshd_config = SSHD_CONFIG_TEMPLATE % ({
            'host_key': temp_host_key_name,
            'pid_file': temp_pid_file,
            'auth_key': temp_auth_file,
            'port': SSHD_PORT
        })

        with open(temp_sshd_conf_file, 'w') as f:
            f.write(sshd_config)

        # Start SSHD
        # /usr/sbin/sshd -dD -f /tmp/test_ssh/sshd_conf
        cls.sshd_p = subprocess.Popen([
            '/usr/sbin/sshd',
            '-D',
            # '-e',  # Log to std out
            '-E', temp_pid_file + '.log',  # Log to file
            # '-ddd',  # Debug mode, server won't fork and will exit on first disconnect.  Not needed with log file.
            '-f',
            temp_sshd_conf_file
        ])
        # Wait until server is receiving connections
        for i in range(10):
            try:
                s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
                s.settimeout(10)
                s.connect(('::1', SSHD_PORT))
                s.close()
            except ConnectionRefusedError:
                time.sleep(0.2)
        # time.sleep(2)
        cls.temp_client_key_name = temp_client_key_name
        cls.temp_auth_file = temp_auth_file

    @classmethod
    def tearDownClass(cls):
        cls.sshd_p.kill()

    def setUp(self):
        with open(self.temp_client_key_name + '.pub', 'r') as fr:
            with open(self.temp_auth_file, 'w') as f:
                f.write(fr.read())

    def test_basic_connect(self):
        """
        This is just to make sure the class SSH server is working.
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        ssh_client_args = [
            'ssh',
            '-p', str(SSHD_PORT),
            '-VVV'
        ]

        commands = [
            'ls'
        ]

        ssh_client_args += [
            '%s@127.0.0.1' % current_username,
            '-o', 'StrictHostKeyChecking=no',  # Note:  This needs to be removed
            '-o', 'UserKnownHostsFile=/dev/null',
            '-i', self.temp_client_key_name,
            "%s 2>&1" % '\n'.join(commands)
        ]
        subprocess.check_call(ssh_client_args, timeout=10.0, stderr=subprocess.DEVNULL)

    def test_execute_remote_commands_pubkey(self):
        """
        Tests normal execution without any priviledges which simply passes to openssh client.
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        self.assertIsNotNone(current_username)

        # Test normal run
        output = execute_remote_commands_pubkey(
            current_username, '127.0.0.1', ['ls -la', 'env pwd'], port=SSHD_PORT,
            client_pri_key=self.temp_client_key_name, ignore_ret_code=True
        )
        self.assertGreater(len(output.split()), 1)
        self.assertIn('/home/', output)

        # Again for IPv6
        output = execute_remote_commands_pubkey(
            current_username, '::1%lo', ['ls -la', 'env'], port=SSHD_PORT,
            client_pri_key=self.temp_client_key_name, ignore_ret_code=True
        )
        # Note that ssh doesn't respond to ::1%lo and will return an error.
        # self.assertGreater(len(output.split()), 1)
        # self.assertIn('/home/', output)

        # Test timeout exception
        self.assertRaises(RuntimeError, execute_remote_commands_pubkey,
                          current_username, '::2', ['ls'], port=SSHD_PORT,
                          client_pri_key=self.temp_client_key_name)

        # Test command timeout
        self.assertRaises(RuntimeError, execute_remote_commands_pubkey,
                          current_username, '127.0.0.1', ['sleep 3'], port=SSHD_PORT,
                          client_pri_key=self.temp_client_key_name, ignore_ret_code=True,
                          timeout=0.5)

        # Test Non zero exit (tty command will do this)
        self.assertRaises(RuntimeError, execute_remote_commands_pubkey,
                          current_username, '::1', ['tty'], port=SSHD_PORT,
                          client_pri_key=self.temp_client_key_name)

        # Test ignore return value
        output = execute_remote_commands_pubkey(
            current_username, '::1', ['tty'], ignore_ret_code=True, port=SSHD_PORT,
            client_pri_key=self.temp_client_key_name
        )
        self.assertIn('tty', output)

    def test_simple_ssh_client_disconnected(self):
        """
        Tests all the commands to a disconnected client
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        self.assertIsNotNone(current_username)
        test_script_path = '/tmp/test_script/'
        if os.path.exists(test_script_path):
            shutil.rmtree(test_script_path)
        os.makedirs(test_script_path)
        with open(os.path.join(test_script_path, 'src_1.txt'), 'w') as f:
            f.write('hello world')

        with SimpleSSHClient('::1',
                             username=current_username,
                             private_key=self.temp_client_key_name,
                             port=SSHD_PORT) as ssh_client:
            ssh_client.close()

            # File exists
            x = os.path.join(test_script_path, 'src_1.txt')
            self.assertRaises(ConnectionError, ssh_client.exists, x)

            # Perform file transfer
            y = os.path.join(test_script_path, 'dest_1.txt')
            self.assertRaises(ConnectionError, ssh_client.transfer, x, y)

            # Directory listing
            self.assertRaises(ConnectionError, ssh_client.listdir, test_script_path)

            # Run remote command
            self.assertRaises(ConnectionError, ssh_client.run, 'ls %s' % test_script_path)

            # Run as sudo?
            self.assertRaises(ConnectionError, ssh_client.sudo, 'ls %s' % test_script_path)

            # Make dir
            x = os.path.join(test_script_path, 'test_dir')
            self.assertRaises(ConnectionError, ssh_client.mkdir, x)

            # Remove
            self.assertRaises(ConnectionError, ssh_client.remove, y)

            # Make dirs
            x = os.path.join(test_script_path, 'test_dirs')
            self.assertFalse(os.path.exists(x))
            x = os.path.join(x, 'test_dirs')
            self.assertRaises(ConnectionError, ssh_client.makedirs, x)
            self.assertFalse(os.path.exists(x))

            # Load file
            self.assertRaises(ConnectionError, ssh_client.open, x)

    def test_simple_ssh_client_password(self):
        """
        This will fail unless a password is provided as a class variable.  A better solution is needed.
        Test the simple ssh client using a password
        :return:
        """
        current_username = os.environ.get('LOGNAME')

        with SimpleSSHClient('::1',
                             username=current_username,
                             password=self.pwd,
                             port=SSHD_PORT) as ssh_client:
            x, y = ssh_client.sudo('pwd')
            self.assertIn(current_username, x)
            x, y = ssh_client.sudo('>&2 echo "$LOGNAME"')
            self.assertIn(current_username, y)

            x, y = ssh_client.sudo('>&2 echo "$LOGNAME"', current_username)
            self.assertIn(current_username, y)

    def test_simple_ssh_client_normal(self):
        """
        Test all normal flows of the SimpleSSHClient
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        self.assertIsNotNone(current_username)
        test_script_path = '/tmp/test_script/'
        if os.path.exists(test_script_path):
            shutil.rmtree(test_script_path)
        os.makedirs(test_script_path)
        with open(os.path.join(test_script_path, 'src_1.txt'), 'w') as f:
            f.write('hello world')

        with SimpleSSHClient('::1',
                             username=current_username,
                             private_key=self.temp_client_key_name,
                             port=SSHD_PORT) as ssh_client:
            # File exists
            x = os.path.join(test_script_path, 'src_1.txt')
            self.assertTrue(ssh_client.exists(x))

            # Perform file transfer
            y = os.path.join(test_script_path, 'dest_1.txt')
            ssh_client.transfer(x, y)
            self.assertTrue(os.path.exists(y))

            # Directory listing
            x = ssh_client.listdir(test_script_path)
            self.assertIn('dest_1.txt', x)
            self.assertIn('src_1.txt', x)

            # Bad Directory listing
            self.assertRaises(FileNotFoundError, ssh_client.listdir, os.path.join(test_script_path, 'bogus'))

            # Run remote command
            x, err = ssh_client.run('ls %s' % test_script_path)
            self.assertIn('dest_1.txt', x)
            self.assertIn('src_1.txt', x)

            # Run as sudo?
            self.assertRaises(ValueError, ssh_client.sudo, 'ls')

            # Make dir
            x = os.path.join(test_script_path, 'test_dir')
            ssh_client.mkdir(x)
            self.assertTrue(x)

            # Remove file
            ssh_client.remove(y)
            self.assertFalse(os.path.exists(y))
            # Remove dir
            ssh_client.remove(x)
            self.assertFalse(os.path.exists(x))

            # Make dirs
            x = os.path.join(test_script_path, 'test_dirs')
            self.assertFalse(os.path.exists(x))
            x = os.path.join(x, 'test_dirs')
            ssh_client.makedirs(x)
            self.assertTrue(os.path.exists(x))

    def test_simple_ssh_client_long_command(self):
        """
        This will fail unless a password is provided as a class variable.  A better solution is needed.
        Tests both sudo and run with a long running password
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        # Might consider using environment variables in the future if this test becomes automated.
        # For now - just enter in password during test

        with SimpleSSHClient('::1',
                             username=current_username,
                             password=self.pwd,
                             port=SSHD_PORT) as ssh_client:
            x, y = ssh_client.sudo('sleep 2; echo "done"')
            self.assertIn('done', x)

            x, y = ssh_client.run('sleep 2; echo "done"')
            self.assertIn('done', x)

    def test_simple_ssh_client_file_contents(self):
        """
        Test all normal flows of the SimpleSSHClient
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        self.assertIsNotNone(current_username)
        test_script_path = '/tmp/test_script/'
        if os.path.exists(test_script_path):
            shutil.rmtree(test_script_path)
        os.makedirs(test_script_path)
        with open(os.path.join(test_script_path, 'src_1.txt'), 'w') as f:
            f.write('hello world')

        with SimpleSSHClient('::1',
                             username=current_username,
                             private_key=self.temp_client_key_name,
                             port=SSHD_PORT) as ssh_client:
            # Read the content of a file
            x = os.path.join(test_script_path, 'src_1.txt')
            f = ssh_client.open(x)
            self.assertIsNotNone(f)
            self.assertEqual(f.read().decode(), 'hello world')
            f.close()

            # Test context manager
            with ssh_client.open(x) as f:
                self.assertEqual(f.read().decode(), 'hello world')

    def test_auth_key(self):
        """
        Test Adding a "key" to the auth key file
        :return:
        """
        current_username = os.environ.get('LOGNAME')
        auth_location = '~/.ssh/authorized_keys_raik_testing'
        if os.path.exists(os.path.expanduser(auth_location)):
            os.remove(os.path.expanduser(auth_location))

        with SimpleSSHClient('::1',
                             username=current_username,
                             private_key=self.temp_client_key_name,
                             port=SSHD_PORT) as ssh_client:
            # Test Empty file in home dir
            with open(self.temp_client_key_name) as f:
                temp_pub_key = f.read()
            ssh_client.add_auth_pub_key(temp_pub_key, auth_location)
            self.assertTrue(os.path.exists(os.path.expanduser(auth_location)))
            with open(os.path.expanduser(auth_location)) as f:
                auth_data = f.read()
            self.assertEqual(auth_data.count(temp_pub_key), 1)

            # Test add again only results in one entry
            ssh_client.add_auth_pub_key(temp_pub_key, auth_location)
            with open(os.path.expanduser(auth_location)) as f:
                auth_data = f.read()
            self.assertEqual(auth_data.count(temp_pub_key), 1)

            # Test existing file in home dir
            existing_data = '1\n2\n3'
            with open(os.path.expanduser(auth_location), 'w') as f:
                f.write(existing_data)
            ssh_client.add_auth_pub_key(temp_pub_key, auth_location)
            with open(os.path.expanduser(auth_location)) as f:
                auth_data = f.read()
            self.assertEqual(auth_data.count(temp_pub_key), 1)
            self.assertTrue(auth_data.startswith(existing_data + '\n'))

                # Clean up
            os.remove(os.path.expanduser(auth_location))
