======
README
======

.. NOTE::
    Some kind of rewrite is being performed to reimagine RAIK in order to make it easier and
    clearer to use and extend.  See README_2024.

Broken Library
==============

This library is broken in many places as I slowly migrate the structure
and update deps.  The following should work:

* raik.utils.crypt



Introduction
============

WARNING:  This is a legacy code base based on an idea that I never
had time to complete or maintain.  However, there are still a number
of utilities I use here and I do intend to continue this project
as a hobby in the future.  Consider all documentation out of date.


Project Usage
=============

.. ATTENTION::
    This needs updating and testing to using Python Poetry (WIP)

This is based on using Python Poetry.

To create a new RAIK project, see the following steps:

1. Create a new directory to hold the project

2. Add the following to your poetry dependencies::

    raik = { git = "https://gitlab.com/raik-tools/raik.git", branch = "dev" }

3. Within your poetry environment::

    raik-admin init

The init method will create the following directory structure

* certs/ - a location where certificates will be managed
* project_lib/ - A package for library blocks for the project
* project_lib/__init__.py
* control/  - Control strategies / functions
* control/__init__.py
* conf.py   - Project configuration

To configure your project, note the following guidelines:

* Add functional code to project_lib and control packages
* The project_lib and control packages will automatically include into
  their respective namespaces the contents of the folder.  What this
  means is that a module called "add_two.py" with a function "def add_two"
  can be imported anywhere simply by specifying `from project_lib import
  add_two`.



Development
===========

TBA
