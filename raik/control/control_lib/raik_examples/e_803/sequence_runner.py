"""
=============
RAIK Sequence
=============

Intro
=====

This files provides the infrastructure required for running a stepped sequence.

"""
# Standard Imports
import logging
import asyncio
import time
import enum
import threading
from typing import Callable, Union, List
# Selected Imports
# End Imports
logger = logging.getLogger(__name__)


class SequenceState(enum.Enum):
    INIT = 'init'
    WAIT = 'wait'
    RUN = 'run'
    DONE = 'done'
    TIMEOUT = 'timeout'


class SequenceStep:
    """
    To move into the system code, just here for testing
    This will take advantage of asyncio green threads, but note that an equivalent exists in the pre-existing
    python libraries for concurrent threads and processes.  For the scope of a sequence, I'm aiming for a design
    where the network resolves itself rather than explicit sequence programming.  There's no need to significant
    parallelism.  All steps forming a sequence shall be combined and executed at once, then the main functions
    will resolve independently.

    Timers will be added to abort steps that are taking too long.  Note that asyncio is not thread safe, so the
    better pattern will be to launch a thread in _main that performs the task and then within main periodically
    check on the running task, or set a timer event (asyncio might have a timer event)

    All steps must be linked using the link_next method.

    These were originally defined as data classes, but __hash__ and other inheritance isn't working.

    """
    def __init__(self, step_name):
        self.step_name: str = step_name
        self.timeout_seconds: float = 10.0
        # self.prev_step: Union[None, 'SequenceStep'] = None  # Branching will be made a special step type
        # self.next_step: Union[None, 'SequenceStep'] = None  # Branching will be made a special step type
        self.prev_step: List['SequenceStep'] = []  # Branching will be made a special step type
        self.next_step: List['SequenceStep'] = []  # Branching will be made a special step type
        # transition_test: Union[None, Callable[[], bool]]
        self.state: SequenceState = SequenceState.INIT  # See Typing lib about an enum?

        # Private data
        self._is_done: asyncio.Future = None  # Note that this may need to be reset when loops occur
        self._all_steps: dict = None
        self._event_loop: asyncio.AbstractEventLoop = None

        # Main thread (to protect from non asyncio processes)
        self._t_future = None  # Set in the exec once the loop is created
        self._t = threading.Thread(target=self._main_thread)

    def __hash__(self):
        return hash(
            (self.step_name, str(type(self)))
        )

    def __repr__(self):
        return f'<{str(type(self))}: {self.step_name}>'


    def _main(self):
        raise NotImplementedError()

    def _main_thread(self):
        res = self._main()
        self._event_loop.call_soon_threadsafe(self._t_future.set_result, res)

    async def main(self):
        assert self._event_loop is not None, 'Must be called from the correct execute method'
        if len(self.prev_step) > 0:
            self.state = SequenceState.WAIT
            logger.info('Step: %-20s \t\tWaiting', self.step_name)
            await asyncio.gather(*[a._is_done for a in self.prev_step])
        else:
            logger.info('Step: %-20s \t\tFirst Step', self.step_name)
        # Execute any callable fields and update the fields with the values (delayed execution)
        self.state = SequenceState.RUN
        logger.info('Step: %-20s \t\t\tRunning', self.step_name)
        try:
            self._t.start()
            logger.info('Step: %-20s \t\t\t\tmain await', self.step_name)
            await self._t_future
            # result = self._t_future.result()
            self._t_future.result()
        except asyncio.TimeoutError:
            self.state = SequenceState.TIMEOUT
            logger.info('Step: %-20s \tTimeout', self.step_name)
            loop = asyncio.get_event_loop()
            loop.stop()
        else:
            # We could restore the executable fields if we want to re-use the step instance.
            self.state = SequenceState.DONE
            logger.info('Step: %-20s \tDone', self.step_name)
        finally:
            self._is_done.set_result(True)

    def get_results(self):
        return None

    def link_next(self, next_step: 'SequenceStep') -> 'SequenceStep':
        assert self.step_name, 'A Step name must always be provided'
        assert next_step.step_name, 'A Step name must always be provided'

        # Create a sequence "global" all steps method to assist later with execution and reporting
        if self._all_steps is None and next_step._all_steps is not None:
            all_steps = next_step._all_steps
            assert self.step_name not in all_steps, 'Step name %s duplicated' % self.step_name
            all_steps[self.step_name] = self
            self._all_steps = all_steps
        elif self._all_steps is not None and next_step._all_steps is None:
            all_steps = self._all_steps
            assert next_step.step_name not in all_steps, 'Step name %s duplicated' % next_step.step_name
            all_steps[next_step.step_name] = next_step
            next_step._all_steps = all_steps
        elif self._all_steps is not None and next_step._all_steps is not None:
            if self._all_steps == next_step._all_steps:
                pass
            else:
                raise NotImplementedError('Sequence combination is not yet implemented')
        else:
            assert self.step_name != next_step.step_name, 'Step name %s must be unique' % self.step_name
            all_steps = {
                self.step_name: self,
                next_step.step_name: next_step
            }
            self._all_steps = all_steps
            next_step._all_steps = all_steps

        # Link the steps
        # WIP:  This is a quick trial to see how asyncio handles parallel tasks
        if next_step not in self.next_step:
            self.next_step.append(next_step)
        if self not in next_step.prev_step:
            next_step.prev_step.append(self)
        return next_step

    def execute_sequence(self):
        assert self._all_steps, 'All steps must be defined, did you use link_steps?'
        # For testing / proof of concept
        loop = asyncio.get_event_loop()
        mains = []
        for step in self._all_steps.values():
        # for step in reversed(list(self._all_steps.values())):
            logger.info('Preparing step: %s', step.step_name)
            step._is_done = asyncio.Future(loop=loop)
            step._event_loop = loop
            step._t_future = asyncio.Future(loop=loop)
            mains.append(step.main())
        loop.run_until_complete(asyncio.gather(
            *mains, loop=loop
        ))
        loop.close()


# A pattern is emerging where connection points should be a callable or generator.  Otherwise, they can be
# set as a parameter on an instance level?

class GoogleSheetRead(SequenceStep):
    """Can we make this a dataclass syntax to indicate instance parameters?"""

    def __init__(self, *args, **kwargs):
        self.credentials_file: str = ''
        self.sheet_id: str = ''
        self.cell_range: str = ''
        super().__init__(*args, **kwargs)

    def _main(self):
        logger.info('Now in class main %s', self)
        time.sleep(5)

    def get_results(self) -> str:
        return ''


class Send(SequenceStep):
    def __init__(self, *args, **kwargs):
        self.send_type: str = 'anon_cert_broadcast'
        self.send_header: Union[str, Callable[[], str]] = 'data'
        self.send_data: Union[str, Callable[[], str]] = ''
        super().__init__(*args, **kwargs)

    def _main(self):
        logger.info('Now in class main %s', self)
        time.sleep(5)


class Recv(SequenceStep):
    def __init__(self, *args, **kwargs):
        self.recv_type: str = 'raik_broadcast'
        self.recv_verify: str = 'true'
        self.recv_header: str = 'data'
        self.timeout: float = 3.0
        # These attributes are set by recv logic but can also be parameters for filtering
        self.recv_from: str = ''
        super().__init__(*args, **kwargs)

    def _main(self):
        logger.info('Now in class main %s', self)
        time.sleep(5)


class Transfer(SequenceStep):
    def __init__(self, *args, **kwargs):
        self.tf_from: str = ''
        self.tf_to: str = ''
        self.tf_data: str = ''
        super().__init__(*args, **kwargs)

    def _main(self):
        logger.info('Now in class main %s', self)
        time.sleep(5)


class Function(SequenceStep):
    def __init__(self, *args, **kwargs):
        self.task: callable = None
        super().__init__(*args, **kwargs)

    def _main(self):
        logger.info('Now in class main %s', self)
        time.sleep(5)
