"""
A simple program that a just causes rotating state between two DOLs.
"""
# noinspection PyUnresolvedReferences
from project_lib.simple_dol import SimpleDOL
from raik import PROGRAM_SCOPE


SCOPE = PROGRAM_SCOPE.user


# Internal Blocks
pump3 = SimpleDOL()  # Just for interface reporting


def pre_scan():
    """
    Runs before blocks are executed, a good place to calculate un connected inputs.
    Although, blocks are a preferred mechanism, this capacity is just to
    test the ability to update inputs and read state externally
    :return:
    """
    if pump3.running:
        pump3.cmd_start = False
        pump3.cmd_stop = True
    else:
        pump3.cmd_start = True
        pump3.cmd_stop = False


if __name__ == "__main__":
    pass
