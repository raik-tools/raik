===================
RAIK Script Trigger
===================

:Enhancement: 803
:Keyword: E803-script_trigger
:Type: Project
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: In Progress
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
    :depth: 2
    :local:

Overview
========

This is a smaller project that aims to develop the ability for more complex logic, event handling and communications.

A project exists where a script needs to be executed based on distributed input data.  A quick scenario is as follows:

Primary Workflow:

* Node A - On a time based event, broadcasts a secured signal A, the payload is retrieved from an online location
           intended as a kill-switch.
* Node B - Sees the signal from Node A (can authenticate it but not decode it) and emits secured Signal B
* Node C - Receives Signal A and B, verifies them and uses the payloads to execute a script
* Node C - Upon script completion, broadcasts a signal D


Secondary Workflow:

* Node A - Upon seeing Signal D, initiates secure communication with Node C and receives data


Background
----------

Blank

Alternatives
------------

Blank

Status Comments
===============

This document is still in draft.


References
==========

Blank

Features
========

TODO when design is noted, some rough starts

* Sequence steps - functions designed to act as part of a sequence
* Charts - Connecting functions


Pro Features
============

TODO

Description
===========

Features for making complex logic easy to implement are hoped to be achieved.  This also includes more then just
plain old functions in RAIK control.

Consider building logic with diagram syntax

Logic Description
-----------------

Node A
^^^^^^

Sequence (init is on randomized time trigger, on fail default is to raise warning and exit sequence)

1. Request google sheets value
2. On done:  Run logic (one scan) - Wrap package and send on local network
3. On done:  Wait for network signal (timeout 15 minutes)
4. On done:  Communicate - Special block for secure transfer to/from
5. On done:  Run logic - Save data (payload from previous step)

TODO:  Combine UML with a logic description / create a logic file to demonstrate this from the top down

.. uml ::

    start

    :GoogleSheetRead:get_code
    Fetches key from the web]

    floating note right: On failure the sequence raises an error event and aborts.

    :Send:broadcast_packet]

    :Recv:confirmation
    Waits for up to 15 minutes for the confirmation]

    :Transfer:get_results]

    :Function:save_data]

    stop

.. _Activity Diagram: http://plantuml.com/activity-diagram-beta
.. _SFC: https://en.wikipedia.org/wiki/Sequential_function_chart

TODO:  Method to generate a key and perhaps save it - one shot?

Node B
^^^^^^

First logic is a continuous function with events:

* Listen for communications.   On signal received run logic to send broadcast

Node C
^^^^^^

Sequence to initiate script

* Init
* Parallel Branch

    * Start Listening and wait for network signal A.  Done is when network signal has been received in last 1 min
    * Start Listening and wait for network signal B.  Done is when network signal has been received in last 1 min

* Recombine and run logic which combines data from last steps
* Run Script, taking input from last step
* Broadcast Signal
* Restart

Sequence for data transfer

* Init
* Wait for Network signal
* Transmit data based on network signal to source
* Restart

Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.