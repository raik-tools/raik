import logging
import psutil
import subprocess
import os
logger = logging.getLogger('RAIK_IO:System')

# Init cpu times and other counters that are based on calling differences
psutil.cpu_percent()

# Capture HW platform
hw_type = None

# Detect Raspberry PI platform
try:
    subprocess.check_output(['which', 'vcgencmd'])
    hw_type = 'rpi'
except Exception:
    pass

# Detect lm_sensors
if not hw_type:
    try:
        subprocess.check_output(['which', 'sensors'])
        hw_type = 'lm_sensors'
    except Exception:
        pass


# Scanning
def scan_module(**outputs):
    """
    Host system data such as memory, cpu utilization, temperatures etc...
    :param outputs:  This module doesn't currently accept outputs
    :return: See documentation.

    Inputs (return value)
    ------
    cpu/utilization:  0.00 to 1.00 of system reported total CPU usage
    mem/utilization:  0.00 to 1.00 of total system RAM used
    cpu/temp:  Temperature of the CPU as reported by on board sensors
    """
    cpu_temp = None
    if hw_type == 'rpi':
        output = subprocess.check_output(['vcgencmd', 'measure_temp']).decode().strip()
        cpu_temp = float(output.split('=')[1][:-2])
    elif hw_type == 'lm_sensors':
        cpu_temps = []
        output = subprocess.check_output(['sensors']).decode().splitlines()
        for l in output:
            if l.startswith('Core '):
                temp_string = l[7:l.index('(')].strip()
                temp_string = temp_string[:-2]
                cpu_temps.append(float(temp_string))
        cpu_temp = max(cpu_temps)

    return {
        'cpu/utilization': psutil.cpu_percent(),
        'mem/utilization': psutil.virtual_memory().percent,
        'cpu/temp': cpu_temp
    }
# EOF
