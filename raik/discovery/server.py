"""
RAIK Discovery server (as per interface #281).

The server listens for a UDP packet on port 61001.

A certificate chain file is needed for this module.  Refer to handler documentation below.

"""
import socketserver
import socket
import raik.discovery
import raik.utils.crypt
import json
import os.path
import zlib
import logging

logger = logging.getLogger(__name__)

_server = None


class __RAIKDiscoveryHandler(socketserver.BaseRequestHandler):
    """
    This handler receives data for a UDP connection, validates it and then returns data
    to the originating sender giving host details for this host.
    A certificate chain file will be needed for verification.  It can be pointed to in raik.utils.crypt
    """

    def handle(self):
        logger.info("Discovery packet handler called")
        discovery_packet = self.request[0].strip()
        # Validate received data
        try:
            packet_data = raik.discovery.get_cert_from_discovery_packet(
                discovery_packet
            )
        except ValueError:
            logger.warning(
                "Ignoring data received in RAIK Discovery handler.  Bad packet or failed validation."
            )
            return
        logger.info("Discovery packet verified, using content to encrypt host data")
        # noinspection PyBroadException
        try:
            host_data = json.dumps(raik.discovery.get_host_data(small_data=True))
            host_data_enc = zlib.compress(host_data.encode())
            logger.debug(
                "Preparing host data for encryption of length %i", len(host_data_enc)
            )
            raise NotImplementedError(
                "This function needs to update to use the extract public key method"
            )
            host_data_enc = raik.utils.crypt.asymmetric_encrypt_with_public_key(
                packet_data, host_data_enc
            )
            request_socket = self.request[1]
            logger.debug(
                "Returning data to %s:%i",
                self.client_address[0],
                self.client_address[1],
            )
            request_socket.sendto(host_data_enc, self.client_address)
        except Exception as e:
            # This is not expected to happen because of unit testing in other functions.
            # And if this did occur, we currently don't have tests to create bad or malformed
            # packets.  This is added for graceful ignoring of problems.
            # See Bug #287
            logger.warning(
                "Unhandled exception occurred: (%s) %s", type(e).__name__, str(e)
            )


class UDP6Server(socketserver.UDPServer):
    address_family = socket.AF_INET6


def start_server():
    """
    Starts the discovery server.  This is a blocking call until
    stop_server is run.  This can be called directly or as a thread.
    """
    global _server
    if _server is not None:
        return
    logger.info("Starting server")
    server = UDP6Server(("::", raik.discovery.LISTEN_PORT), __RAIKDiscoveryHandler)
    _server = server
    try:
        server.serve_forever()
    except Exception as ex:
        if _server:
            logger.error(
                "Server failed, unhandled exception (%s): %s"
                % (type(ex).__name__, str(ex))
            )
            _server = None


def server_is_running():
    """
    Used to check the server is active.
    :return: True or False
    """
    if _server is None:
        return False
    return True


def stop_server(thread_stop=True):
    """
    Stops the discovery server
    :param thread_stop:  Specify if the server was started in a thread.  For some reasons, server.shutdown hangs if
    the server was not started in a thread and shutdown is called.  This is a workaround for now.
    """
    global _server
    if _server is None:
        return
    logger.info("Shutting down server")
    # Clear global variable to indicate intention
    server = _server
    _server = None
    if thread_stop:
        # The Event in the BaseServer code for self._BaseServer__is_shut_down.set() hangs if the server is running
        # in the main thread and a signal calls this command.
        server.shutdown()
    server.server_close()
    logger.info("Server Shutdown")
