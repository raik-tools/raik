"""
Just a note that there is still some legacy with these tests.  They
aren't well isolated.  Additions to this file should work to improve
isolation from other functions in the lib (without the need to mock,
it's ok not to mock third party deps).
"""

import pytest
from raik.utils.crypt import *
import raik.utils.crypt
import raik.utils.random
import raik.discovery


class RsaKeyPair(NamedTuple):
    root_path: pathlib.Path
    private_path: pathlib.Path
    public_path: pathlib.Path
    fqdn: str


@pytest.fixture
def temp_certs_folder(tmpdir) -> pathlib.Path:
    """
    Provides a pathlib object for use as a temporary folder.
    The folder is not cleaned up after the test, but each invocation is
    gaurunteed a unique folder
    """
    # Pytest uses a maintenance mode lib for path, convert it to pathlib
    tmpdir = pathlib.Path(str(tmpdir))
    logger.info("Creating temporary folder for testing: %s", tmpdir)
    return tmpdir


@pytest.fixture
def rsa_key_pair_paths(temp_certs_folder) -> RsaKeyPair:
    """
    Provides a path to a temporary public / private key for rsa encryption.  The
    keys are not created in this fixture.  It uses a random / temp path that is
    cleaned up after the test.
    """
    key_subject_name = "rsa_key_pair_fixture"
    pri_file = temp_certs_folder / raik.utils.crypt.rsa_private_key_name(
        key_subject_name
    )
    pub_file = temp_certs_folder / raik.utils.crypt.rsa_public_key_name(
        key_subject_name
    )

    return RsaKeyPair(temp_certs_folder, pri_file, pub_file, key_subject_name)


def test_generate_rsa_keys__files_created(rsa_key_pair_paths):
    """
    Test files are created
    """
    generate_rsa_keys(rsa_key_pair_paths.private_path, rsa_key_pair_paths.public_path)

    assert rsa_key_pair_paths.private_path.exists()
    assert rsa_key_pair_paths.public_path.exists()


def test_generate_rsa_keys__round_trip_encryption(rsa_key_pair_paths):
    """
    Round trip test on rsa keys with symmetric encryption
    """
    generate_rsa_keys(rsa_key_pair_paths.private_path, rsa_key_pair_paths.public_path)

    import rsa

    with rsa_key_pair_paths.private_path.open("r") as f:
        k = f.read()
        private_key = rsa.PrivateKey.load_pkcs1(k.encode("utf-8"))

    with rsa_key_pair_paths.public_path.open("r") as f:
        k = f.read()
        pub_key = rsa.PublicKey.load_pkcs1(k.encode("utf-8"))

    msg = "Hello World"
    msg_c = rsa.encrypt(msg.encode("utf-8"), pub_key)
    assert msg == rsa.decrypt(msg_c, private_key).decode(
        "utf-8"
    ), "Test decrypted message matches original"

    # # Comment out temporary performance testing
    # import timeit
    #
    # # Time Encryption
    # global msg, msg_c, pub_key, private_key
    # msg = msg.encode("utf-8")
    # iterations = 1000
    # rsa_encrypt_test = timeit.timeit(
    #     "rsa.encrypt(msg, pub_key)",
    #     number=iterations,
    #     setup="import rsa; from  raik.utils.tests.test_crypt import msg, pub_key",
    # )
    # rsa_decrypt_test = timeit.timeit(
    #     "rsa.decrypt(msg_c, private_key)",
    #     number=iterations,
    #     setup="import rsa; from raik.utils.tests.test_crypt import msg_c, private_key",
    # )
    # # import Crypto.Cipher.PKCS1_OAEP
    # import Crypto.Cipher.PKCS1_v1_5
    # import Crypto.Hash.SHA
    # import Crypto.Random
    # import Crypto.PublicKey.RSA
    #
    # crypto_key = Crypto.PublicKey.RSA.importKey(private_raw_key)
    # crypto_h = Crypto.Hash.SHA.new(msg)
    # crypto_cipher = Crypto.Cipher.PKCS1_v1_5.new(key=crypto_key)
    # global crypto_cipher, crypto_h
    # crypto_encrypt = timeit.timeit(
    #     "crypto_cipher.encrypt(msg+crypto_h.digest())",
    #     number=iterations,
    #     setup="from raik.utils.tests.test_crypt import crypto_cipher, msg, crypto_h",
    # )
    # msg_c = crypto_cipher.encrypt(msg + crypto_h.digest())
    # crypto_d_size = Crypto.Hash.SHA.digest_size
    # sentinel = Crypto.Random.new().read(15 + crypto_d_size)
    # global sentinel
    # crypto_decrypt = timeit.timeit(
    #     "crypto_cipher.decrypt(msg_c, sentinel)",
    #     number=iterations,
    #     setup="from raik.utils.tests.test_crypt import crypto_cipher, msg_c, sentinel",
    # )
    # x = 1

    # The above test simply encrypts a message using a public key and checks the private key
    # can decrypt it.  The encryption is only one way.
    # So the public key user can verify the private key message, a hash is
    # generated using the private key
    # that also produces the same hash using a similar algorithm from the public key.
    # The hash is called the signature.
    signature = rsa.sign(msg.encode(), private_key, "SHA-256")
    assert rsa.verify(msg.encode(), signature, pub_key), "Testing RSA verification"


def test_generate_rsa_keys__integration_with_generate_csr_for_existing_key(
    rsa_key_pair_paths,
):
    """
    This is to test that the key is successful in the creation of a certificate
    signing request.
    """
    generate_rsa_keys(rsa_key_pair_paths.private_path, rsa_key_pair_paths.public_path)
    generate_csr_for_existing_key(rsa_key_pair_paths.private_path)
    assert (
        rsa_key_pair_paths.root_path
        / raik.utils.crypt.csr_file_name(rsa_key_pair_paths.fqdn)
    ).exists(), "A CSR was not generated"


class CaPaths(NamedTuple):
    root: OpenSslData
    int: OpenSslData
    base: pathlib.Path


@pytest.fixture
def ca_paths(temp_certs_folder) -> CaPaths:
    """
    Returns all the paths in a RAIK administered CA
    """
    root_ca_name = "TEST_ROOT_CA"
    int_ca_name = "TEST_CA"
    return CaPaths(
        root=OpenSslData.from_ca_name(
            parent_dir=temp_certs_folder, ca_name=root_ca_name
        ),
        int=OpenSslData.from_ca_name(parent_dir=temp_certs_folder, ca_name=int_ca_name),
        base=temp_certs_folder,
    )


def test_create_root_ca__files_created(ca_paths):
    """
    Test that the files were created on disk
    """
    create_root_ca(ca_paths.root.base_dir)
    assert ca_paths.root.conf_path.exists()
    assert ca_paths.root.cert.exists()


def test_create_root_ca__validate_cert(ca_paths):
    """
    Validates the root ca cert
    """
    create_root_ca(ca_paths.root.base_dir)

    # Verify the root certificate
    cert = decode_pem_certificate(ca_paths.root.cert)
    assert "RAIK" in str(cert.subject)
    assert "C=AU" in str(cert.subject)


def test_create_intermediate_ca__integration_with_root(ca_paths):
    """
    Tests the creation of an intermediate CA.
    """
    create_root_ca(ca_paths.root.base_dir)

    csr_path = (
        ca_paths.int.base_dir
        / "csr"
        / raik.utils.crypt.csr_file_name(ca_paths.int.ca_name)
    )

    # Generate intermediate CA
    chain_path = create_intermediate_ca(
        ca_paths.root.base_dir,
        ca_paths.int.base_dir,
        suppress_output=False,
        name=ca_paths.int.ca_name,
    )
    assert ca_paths.int.conf_path.exists()
    assert csr_path.exists()
    assert chain_path.exists()
    assert len(ca_paths.root.index_data()) == 1, "The root CA has not listed the int ca"

    # Verify an empty index.txt file
    assert len(ca_paths.int.index_data()) == 0


def test_create_certificate_request__exists(ca_paths):
    """
    A simple check that a CSR has been generated by this method.
    """
    node_name = "test_raik_node_1"
    pub_key_path = ca_paths.base / raik.utils.crypt.rsa_public_key_name(node_name)
    private_key_path = ca_paths.base / raik.utils.crypt.rsa_private_key_name(node_name)
    generate_rsa_keys(private_key_path, pub_key_path)

    # Create a certificate request
    create_certificate_request(private_key_path, node_name)

    expected_csr_path = ca_paths.base / raik.utils.crypt.csr_file_name(node_name)
    assert expected_csr_path.exists()


@pytest.mark.parametrize(
    "msg",
    [
        pytest.param("001" * 137, id="simple string"),
        pytest.param("001" * 141, id="simple string 2"),
        pytest.param("0011" * 200, id="a longer string"),
    ],
)
def test_symmetric_encryption__round_trip(msg):
    """
    A round trip test for encrypt and decrypt
    """
    key = raik.utils.random.get_random_length_bytes(32, 40)
    msg = msg.encode()

    e_msg = symmetric_encrypt(key, msg)
    d_msg = symmetric_decrypt(key, e_msg)

    assert msg != e_msg
    assert msg == d_msg


class FullChainIssuedCert(NamedTuple):
    ca: CaPaths
    cert_path: pathlib.Path
    public_key: bytes
    private_key: bytes


@pytest.fixture
def ca(ca_paths) -> CaPaths:
    create_root_ca(ca_paths.root.base_dir)
    create_intermediate_ca(
        ca_paths.root.base_dir,
        ca_paths.int.base_dir,
        suppress_output=False,
        name=ca_paths.int.ca_name,
    )
    return ca_paths


@pytest.fixture
def issued_cert(ca, rsa_key_pair_paths) -> FullChainIssuedCert:
    host_name = "ISSUED_CERT_FIXTURE"
    raik.utils.crypt.generate_rsa_keys(
        rsa_key_pair_paths.private_path, rsa_key_pair_paths.public_path
    )

    # Create a request and get the CA to sign it
    csr_path = raik.utils.crypt.create_certificate_request(
        rsa_key_pair_paths.private_path, host_name
    )
    cert_path = csr_path.parent / raik.utils.crypt.cert_file_name(host_name)
    raik.utils.crypt.sign_certificate_request(ca.int.base_dir, csr_path, cert_path)
    return FullChainIssuedCert(
        ca=ca,
        cert_path=cert_path,
        public_key=get_rsa_public_key_from_file(rsa_key_pair_paths.public_path),
        private_key=get_rsa_private_key_from_file(rsa_key_pair_paths.private_path),
    )


def test_create_server_certificate(ca):
    """
    A spot check test that a valid server certificate is created
    """
    fqdn = "test_create_server_certificate"
    private_key, cert = create_server_certificate(
        signing_ca=ca.int.base_dir, cert_folder=ca.base, fqdn=fqdn
    )
    assert private_key.exists()
    assert cert.exists()


class ServerCert(NamedTuple):
    chain_path: pathlib.Path
    private_key_path: pathlib.Path
    cert_path: pathlib.Path
    hostname: str
    ca: CaPaths


@pytest.fixture
def server_cert(ca) -> ServerCert:
    fqdn = "test_create_server_certificate"
    private_key, cert = create_server_certificate(
        signing_ca=ca.int.base_dir, cert_folder=ca.base, fqdn=fqdn
    )
    return ServerCert(
        chain_path=ca.int.chain_path,
        private_key_path=private_key,
        cert_path=cert,
        hostname=fqdn,
        ca=ca,
    )


def test_create_client_certificate(ca):
    """
    A spot check test that a valid server certificate is created
    """
    fqdn = "test_create_client_certificate"
    pxf_path = create_client_certificate(
        signing_ca=ca.int.base_dir, cert_folder=ca.base, fqdn=fqdn
    )
    assert pxf_path.exists()


def test_verify_certificate__true(server_cert):
    """
    Tests a valid certificate
    """
    assert verify_certificate(server_cert.cert_path, chain_file=server_cert.chain_path)


def test_verify_certificate__false(server_cert):
    """
    Tests an invalid certificate.  This is the same as the True code
    except we don't pass through a cert chain, so the root cert can't
    be validated.
    """
    assert not verify_certificate(server_cert.cert_path)


def test_list_ca_certificates__server_entry(server_cert):
    """
    A simple test to see a server listing
    """
    certs = list_ca_certificates(server_cert.ca.int.base_dir)
    server_entry = certs[0]
    assert len(certs) == 1
    assert server_entry.expiration_date
    assert "RAIK" in server_entry.cert_distinguished_name
    assert "CN=test_create_server_certificate" in server_entry.cert_distinguished_name
    assert server_entry.revocation_date is None
    assert OpenSslCertStatus.valid == server_entry.status
    assert server_entry.serial == 4096  # The docs says this should be hex.
    assert server_entry.serial == server_entry.pem_details.serial_number


@pytest.mark.parametrize(
    "msg",
    [
        pytest.param("001" * 13, id="simple string"),
        pytest.param("001" * 11, id="simple string 2"),
    ],
)
def test_asymmetric_encryption__round_trip_from_x509_ok(msg, issued_cert):
    msg = msg.encode()
    e_msg = asymmetric_encrypt_with_public_key(issued_cert.public_key, msg)
    assert isinstance(e_msg, bytes)

    d_msg = asymmetric_decrypt_with_private_key(issued_cert.private_key, e_msg)

    assert msg == d_msg
    assert msg != e_msg


@pytest.mark.parametrize(
    "msg",
    [
        pytest.param("0011" * 200, id="a longer string"),
    ],
)
def test_asymmetric_encryption__msg_too_long(msg, issued_cert):
    msg = msg.encode()
    with pytest.raises(TypeError):
        asymmetric_encrypt_with_public_key(issued_cert.public_key, msg)
