"""
Creates a new logic block for RAIK.  Can be used with parameters, and if parameters are missing a prompt will be
provided.
"""
import logging
import os
import argparse
import socket
import glob
import re
import sys
import getpass
import uuid
import raik

import raik.control.functions
from raik.utils.deploy import execute_remote_commands_pubkey, SimpleSSHClient
from raik.utils.hardware import UUID_LOCATION

DEFAULT_CONTROL_LIB, DEFAULT_SITE_CONFIG = raik.control.functions.get_paths()

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument('--raik_control_lib_path', type=str, default=DEFAULT_CONTROL_LIB,
                    help='RAIK system control lib location (Default: %s)' % DEFAULT_CONTROL_LIB)

parser.add_argument('--side_config_path', metavar='site_conf', type=str, default=DEFAULT_SITE_CONFIG,
                    help='RAIK site configuration location (Default: %s)' % DEFAULT_SITE_CONFIG)


# parser.add_argument('--password', type=str, default='', nargs='?', const='*',
#                     help='If a password is supplied (for connection to the remote host, then authorized key files can'
#                          ' be updated using password authentication.  Otherwise, the remote device needs to have the '
#                          'public key of the local host added to its authorized keys file.  If this is not followed'
#                          'by a password, a prompt will appear asking for the password')

# parser.add_argument('--init', type=str, default='openrc', choices=['openrc', 'systemd'],
#                     help='Specify which init system to use')
#
# parser.add_argument('--skip_wheel', nargs='?', default=False, const=True,
#                     help='Skip wheel processing')
#
# parser.add_argument('--skip_clone', nargs='?', default=False, const=True,
#                     help='Skip raik and raik_runtime cloning')
#
# parser.add_argument('--debug', nargs='?', default=False, const=True,
#                     help='Set log output to debug level')
#
# parser.add_argument('--remote_root_home', type=str, default=raik.ROOT_HOME,
#                     help='Location of system ROOT on the remote machine.  '
#                          ' (Default: %s)' % raik.ROOT_HOME)

args = parser.parse_args()

# Initialise modules
raik.control.functions.set_paths(
    control_lib=args.raik_control_lib_path,
    site_config=args.side_config_auth
)

# Logging configuration
# fh = logging.FileHandler(os.path.join(current_path, 'deploy_runtime.log'))
fh = logging.StreamHandler()
if args.debug:
    fh.setLevel(logging.DEBUG)
else:
    fh.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logging.root.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
# noinspection PyUnresolvedReferences
logger.root.addHandler(fh)

def main():
    pass
    # TODO:  Lets work out the cascade of events

# TODO e005 indicates that module names need to be unique, but this is over complicating things and we miss out on
# namespace?

"""
Command plan:

* New

    * Type = function
    
        * Raise not implemented
    
    * Type = sequence
    
        * Sequence package name (checks for creation)
        * Sequence name (checks for unique)  # TODO:  We should keep the namespaces
    
* Update


"""

if __name__ == '__main__':
    main()
