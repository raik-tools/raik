"""
    Methods for interrogating system hardware.
"""
import subprocess
import os.path
from raik import SYSTEM_CONFIGURATION_LOCATION

__cmd_ip_link = ('ip', '-o', 'link')
__cmd_ip_addr = ('ip', '-o', 'addr')
UUID_LOCATION = os.path.join(SYSTEM_CONFIGURATION_LOCATION, 'uuid')


def list_addr_info(ipv6_only=False):
    """
    Get a list of ethernet addresses on the system
    :param ipv6_only: Only return IPV6 addresses to reduce total length of data.
    :return: A list of devices
    """
    for l in subprocess.check_output(__cmd_ip_addr).decode().split('\n'):
        l = l.strip()
        if not l:
            continue
        l = l.split(':', 1)[1].split('\\')
        link_data = l[0].split()
        dev = link_data[0]
        if dev == 'lo':
            continue
        if_type = link_data[1]
        if ipv6_only and if_type == 'inet':
            continue
        if_addr = link_data[2]
        if if_type in ('inet', 'inet6'):
            yield (dev, if_addr)


def list_ll_ifs():
    """
    Get's a list of network interfaces with a link local IPv6 address.
    :return: A list of devices
    """
    for l in subprocess.check_output(__cmd_ip_addr).decode().split('\n'):
        l = l.strip()
        if not l:
            continue
        l = l.split(':', 1)[1].split('\\')
        link_data = l[0].split()
        dev = link_data[0]
        if_type = link_data[1]
        #  if_addr = link_data[2]
        if 'brd' in link_data:
            #  if_broadcast = link_data[4]
            link_data = ' '.join(link_data[5:])
        else:
            link_data = ' '.join(link_data[3:])
        if 'scope link' in link_data and if_type == 'inet6':
            yield dev


def get_mac_addresses():
    """
    :return: A list of all MAC addresses and their link devices
    """
    for l in subprocess.check_output(__cmd_ip_link).decode().split('\n'):
        l = l.strip()
        if not l:
            continue
        l = l.split(':', 2)
        dev = l[1].strip()
        l = l[2].split('\\')
        # link_data = l[0].split()
        addr_data = l[1].split()
        if_type = addr_data[0]
        if_addr = addr_data[1]
        if if_type == 'link/ether':
            yield (dev, if_addr)


def get_up_time():
    """
    :return: System up-time in seconds as an integer.  Note that there is a limit of 497 days
    due to counter overflow.
    """
    with open('/proc/uptime', 'r') as f:
        up_str = f.read()
    return int(up_str.split('.')[0])


def get_machine_id():
    """
    :return: A string containing a UUID found as per the module variable UUID_LOCATION.  If no file exists
             then an empty string is returned.
    :returns: str
    """

    global UUID_LOCATION
    uuid = ''
    if os.path.exists(UUID_LOCATION):
        with open(UUID_LOCATION, 'r') as f:
            uuid = f.read()
    return uuid
