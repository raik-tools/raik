"""
Encryption Utilities

Utilities developed a long the way.

"""
import logging
import pathlib

from ._argparse import command_subparsers
import raik.utils.crypt
import raik.utils.random
import base64

logger = logging.getLogger(__name__)


crypt_parser = command_subparsers.add_parser(
    name="crypt",
    description="Random Encryption Utilities",
    help="Random Encryption Utilities",
)

crypt_sub_parser = crypt_parser.add_subparsers(
    title="Crypt Commands", metavar="", dest="crypt_command"
)

# ----------------------------------------------------------------------
# Secret Key generation
# ----------------------------------------------------------------------

secret_key_parser = crypt_sub_parser.add_parser(
    name="secret_key", description="Secret Keys", help="Secret Keys"
)

secret_key_parser.add_argument(
    dest="secret_key_command", choices=["create_file"], help="Command"
)

secret_key_parser.add_argument(
    "path", help="File to save the key to"
)


# ----------------------------------------------------------------------
# File Manipulation
# ----------------------------------------------------------------------

file_parser = crypt_sub_parser.add_parser(
    name="file", description="File manipulation", help="File manipulation"
)

file_parser.add_argument(
    dest="file_command", choices=["encrypt", "decrypt"], help="Command"
)

file_parser.add_argument(
    "in_file", help="Input File"
)

file_parser.add_argument(
    "out_file", help="Output File"
)

file_parser.add_argument(
    "key_file", help="Key File in B64 format"
)


def main(args):
    cmd = args.crypt_command
    if cmd == "secret_key":
        cmd = args.secret_key_command
        if cmd == "create_file":
            # Create a secret key file
            key_bytes = raik.utils.random.get_random_bytes(56, as_base_64=True)
            key_file = pathlib.Path(args.path)
            with key_file.open('wb') as f:
                f.write(key_bytes)
        else:
            raise KeyError(cmd)
    elif cmd == "file":
        cmd = args.file_command
        if cmd == "encrypt":
            # Load Key File
            # noinspection DuplicatedCode
            key_file = pathlib.Path(args.key_file)
            with key_file.open('r') as f:
                key_bytes = f.read()
            key_bytes = base64.b64decode(key_bytes)
            # Load Source Data
            in_file = pathlib.Path(args.in_file)
            with in_file.open('rb') as f:
                in_data = f.read()
            # Encrypt
            out_data = raik.utils.crypt.symmetric_encrypt(
                key=key_bytes,
                data=in_data
            )
            # Save output
            out_file = pathlib.Path(args.out_file)
            with out_file.open("wb") as f:
                f.write(out_data)
        elif cmd == "decrypt":
            # Load Key File
            # noinspection DuplicatedCode
            key_file = pathlib.Path(args.key_file)
            with key_file.open('r') as f:
                key_bytes = f.read()
            key_bytes = base64.b64decode(key_bytes)
            # Load Source Data
            in_file = pathlib.Path(args.in_file)
            with in_file.open('rb') as f:
                in_data = f.read()
            # Decrypt
            out_data = raik.utils.crypt.symmetric_decrypt(
                key=key_bytes,
                cipher_text=in_data
            )
            # Save output
            out_file = pathlib.Path(args.out_file)
            with out_file.open("wb") as f:
                f.write(out_data)
        else:
            raise KeyError(cmd)
    else:
        raise KeyError(cmd)
