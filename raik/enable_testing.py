"""
    Import this module to enable testing.  It mostly exists to pass PEP8 tests for module level imports.
"""
import raik
import warnings

raik.TEST_MODE = True
USELESS_VAR = 0

warnings.warn('Enable testing will be replaced with PyTest Fixtures')


def re_enable_testing():
    for fx in raik.TEST_CALLBACKS:
        fx()
