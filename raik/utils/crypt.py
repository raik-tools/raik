import dataclasses
import datetime
import enum
import functools
import subprocess
import os
import logging
import tempfile
import pathlib
import base64
from functools import singledispatch
from typing import Tuple, List, ClassVar, Optional, NamedTuple, Iterable

from cryptography import x509

from cryptography.hazmat.backends import default_backend
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
import Crypto.Util.number

import raik.utils.random
import raik.utils._ssl_templates
from raik.utils.datetime import strptime_as_utc, naive_to_utc

logger = logging.getLogger(__name__)

# Arrange path for reference files.
_module_path = os.path.join(os.path.dirname(os.path.abspath(__file__)))

# This can be overridden by calling modules to specify an alternative
# location for the certificate chain
# needed for certificate validation.
# noinspection PyUnresolvedReferences
CERT_VALIDATION_CHAIN = os.path.join(_module_path, "cert_chain.crt")


def rsa_private_key_name(fqdn: str) -> str:
    return f"{fqdn}.private.pem"


def rsa_public_key_name(fqdn: str) -> str:
    return f"{fqdn}.pub.pem"


def pkcs_client_key_and_cert_name(fqdn: str) -> str:
    return f"{fqdn}.pfx"


def cert_file_name(fqdn: str) -> str:
    return f"{fqdn}.cert.pem"


def csr_file_name(fqdn: str) -> str:
    return f"{fqdn}.cert.csr"


def csr_path_from_private_key_path(private_key_path: pathlib.Path) -> pathlib.Path:
    fqdn = ".".join(private_key_path.name.split(".")[:-2])
    return private_key_path.parent / csr_file_name(fqdn)


def server_cert_name(fqdn: str) -> str:
    return f"{fqdn}.server.pem"


def client_cert_name(fqdn: str) -> str:
    return f"{fqdn}.client.pem"


CA_CHAIN_FILE_NAME = "ca-chain.pem"


# Shared functions for deploying and maintaining RAIK Nodes.
def generate_rsa_keys(pri_file_name, pub_file_name, bits=2048, pubexp=65537):
    """
    Generate an RSA Key pair for general usage.  Output files are in PEM format.
    :param pri_file_name: Name of the private file (such as test.pri.pem)
    :param pub_file_name: Name of the public key file (such as test.pub.pem)
    :param bits: Length of the key
    :param pubexp: exponent (large prime) to use for the key
    :return:
    """
    logger.info(
        "Generating RSA keys:  Public (%s) and Private (%s)",
        pri_file_name,
        pub_file_name,
    )
    gen_key_args = [
        "openssl",
        "genpkey",
        "-algorithm",
        "RSA",
        "-out",
        str(pri_file_name) + ".pkcs8",
        "-pkeyopt",
        "rsa_keygen_bits:%i" % bits,
        "-pkeyopt",
        "rsa_keygen_pubexp:%i" % pubexp,
    ]
    subprocess.check_call(
        gen_key_args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
    )

    gen_rsa_key_args = [
        "openssl",
        "rsa",
        "-in",
        str(pri_file_name) + ".pkcs8",
        "-traditional",
        "-out",
        str(pri_file_name),
    ]
    subprocess.check_call(
        gen_rsa_key_args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
    )

    gen_pub_args = [
        "openssl",
        "rsa",
        "-in",
        str(pri_file_name) + ".pkcs8",
        "-RSAPublicKey_out",
        "-out",
        str(pub_file_name),
    ]
    subprocess.check_call(
        gen_pub_args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
    )

    os.remove(str(pri_file_name) + ".pkcs8")


def _extract_certificate_body(
    pem_text: str, header: str, footer: str, cert_type: str
) -> bytes:
    if not pem_text.startswith(header):  # pragma: no cover
        raise ValueError(f"{cert_type} missing header")
    if not pem_text.endswith(footer):  # pragma: no cover
        raise ValueError(f"{cert_type} missing footer")
    header_len = len(header)
    footer_pos = -1 * len(footer)
    cert_data = pem_text[header_len:footer_pos].replace("\n", "")
    packet_data = base64.b64decode(cert_data)
    return packet_data


def extract_certs_from_pem(x509_file: pathlib.Path) -> Iterable[str]:
    """
    Removes all the certificates from a file and returns each cert
    """
    with x509_file.open("r") as f:
        data = f.read()

    footer_len = len(FOOTER_X509)
    current_header_start = data.index(HEADER_X509)
    current_footer_end = data.index(FOOTER_X509) + footer_len
    while current_header_start >= 0:
        cert_body = data[current_header_start:current_footer_end]
        yield cert_body
        current_header_start = data.find(HEADER_X509, current_footer_end)
        current_footer_end = data.find(FOOTER_X509, current_footer_end) + footer_len


HEADER_X509 = "-----BEGIN CERTIFICATE-----"
FOOTER_X509 = "-----END CERTIFICATE-----"
HEADER_RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----"
FOOTER_RSA_PRIVATE_KEY = "-----END RSA PRIVATE KEY-----"
HEADER_RSA_PUBLIC_KEY = "-----BEGIN RSA PUBLIC KEY-----"
FOOTER_RSA_PUBLIC_KEY = "-----END RSA PUBLIC KEY-----"


def decode_cert_text(pem_text: str) -> bytes:
    """
    Decode a PEM text string.
    :param pem_text:
    :return:
    """
    return _extract_certificate_body(
        pem_text=pem_text,
        header=f"{HEADER_X509}\n",
        footer=f"{FOOTER_X509}\n",
        cert_type="Certificate",
    )


def get_rsa_private_key_from_file(file_path: pathlib.Path) -> bytes:
    with file_path.open("r") as f:
        pem_text = f.read()
    return _extract_certificate_body(
        pem_text=pem_text,
        header=f"{HEADER_RSA_PRIVATE_KEY}\n",
        footer=f"{FOOTER_RSA_PRIVATE_KEY}\n",
        cert_type="Private Key",
    )


def get_rsa_public_key_from_file(file_path: pathlib.Path) -> bytes:
    with file_path.open("r") as f:
        pem_text = f.read()

    return _extract_certificate_body(
        pem_text=pem_text,
        header=f"{HEADER_RSA_PUBLIC_KEY}\n",
        footer=f"{FOOTER_RSA_PUBLIC_KEY}\n",
        cert_type="Public Key",
    )


# noinspection PyUnusedLocal
@singledispatch
def decode_pem_certificate(pem_text) -> x509.Certificate:  # pragma: no cover
    pass


@decode_pem_certificate.register
def _(pem_text: str) -> x509.Certificate:
    cert = x509.load_pem_x509_certificate(pem_text.encode(), default_backend())
    return cert


@decode_pem_certificate.register
def _(pem_text: pathlib.Path) -> x509.Certificate:
    with pem_text.open("r") as f:
        return decode_pem_certificate(f.read())


def create_pfx_file(private_key, cert_path, pfx_file_path, password="RAIK"):
    """
    Combine a private key (PEM format) and a signed certificate (PEM Format)
    into a PFX file.

    :param private_key:
    :param cert_path:
    :param pfx_file_path: Name of the private file (such as test.pri.pem)
    :param password:
    :return:
    """
    combine_key_args = [
        "openssl",
        "pkcs12",
        "-export",
        "-out",
        str(pfx_file_path),
        "-inkey",
        str(private_key),
        "-in",
        str(cert_path),
        "-password",
        "env:pfx_p",
    ]
    subprocess.check_call(
        combine_key_args,
        stderr=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
        env={"pfx_p": password},
    )


def generate_csr_for_existing_key(private_key_path: pathlib.Path):
    """
    Given a private key file, this generates a .csr file from that key.
    :return:
    """
    gen_key_args = [
        "openssl",
        "req",
        "-key",
        str(private_key_path),
        "-new",
        "-out",
        str(csr_path_from_private_key_path(private_key_path)),
        "-subj",
        "/C=AU/ST=Queensland/L=Brisbane/O=RAIK/CN=127.0.0.1",
    ]
    subprocess.check_call(
        gen_key_args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
    )


def prep_ca_structure(root_path, default_policy="policy_strict", name="ca"):
    """
    Prepares a folder for containing CA data.
    Includes the generation of a password protected public / private key pair.
    It's up to the user to relocate the password file, which exists in the private folder as
    'passphrase',  Additionally, extra file permissions should be set (700 or 400) on the private
    folder.
    No exceptions are handled, so file system or subprocess exceptions may be raised.
    :param root_path: The folder path (will be created if needed) to contain all the CA data
    :param default_policy: Default CA policy, recommended 'policy_strict' or 'policy_loose'
    :param name:  A name to use instead of 'ca' for file names.
    :return:
    """
    root_path = pathlib.Path(root_path).absolute()
    if not root_path.exists():
        root_path.mkdir(parents=True)
    for new_folder in ("certs", "crl", "csr", "newcerts", "private"):
        (root_path / new_folder).mkdir()
    # os.chmod(os.path.join(root_path, 'private'), 700)
    with (root_path / "index.txt").open("w") as _:
        pass
    with (root_path / "serial").open("w", encoding="UTF-8") as f:
        f.write("1000")

    with (root_path / "openssl.cnf").open("w", encoding="UTF-8") as f:
        f.write(
            raik.utils._ssl_templates.CA_TEMPLATE
            % {
                "root_path": str(root_path),
                "name": name,
                "default_policy": default_policy,
            }
        )

    with (root_path / "private" / "passphrase").open("wb") as f:
        f.write(raik.utils.random.get_random_length_bytes(32, 56))

    private_key_path = root_path / "private" / rsa_private_key_name(name)

    # Create Root Private Key
    openssl_args = [
        "openssl",
        "genpkey",
        "-algorithm",
        "RSA",
        "-out",
        str(private_key_path),
        "-pkeyopt",
        "rsa_keygen_bits:4096",
        "-aes-256-cbc",
        "-pass",
        "file:private/passphrase",
    ]
    subprocess.check_call(
        openssl_args,
        cwd=root_path,
        stderr=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
    )


def create_root_ca(root_path: pathlib.Path, suppress_output=True, age_years=10):
    """
    Generates a CA folder structure given the path of the CA
    No exceptions are handled, so file system or subprocess exceptions may be raised.
    :param root_path: Path to contain the CA files in
    :param suppress_output: Suppress Stdout and stderr
    :param age_years: Number of years to create CA for
    :return: Location of the root public certificate
    """
    root_path = pathlib.Path(root_path).absolute()
    prep_ca_structure(root_path)
    root_name = "ca"
    private_key_path = root_path / "private" / rsa_private_key_name(root_name)
    cert_path = root_path / "certs" / cert_file_name(root_name)

    # Create Root Certificate
    openssl_args = [
        "openssl",
        "req",
        "-config",
        "openssl.cnf",
        "-key",
        str(private_key_path),
        "-passin",
        "file:private/passphrase",
        "-new",
        "-x509",
        "-days",
        f"{age_years * 365}",
        "-sha256",
        "-extensions",
        "v3_ca",
        "-out",
        str(cert_path),
        "-subj",
        "/C=AU/ST=Queensland/L=Brisbane/O=RAIK/OU=RAIK Development/CN=RAIK Test Root CA",
    ]

    kw_args = {}
    if suppress_output:
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, cwd=root_path, **kw_args)
    return cert_path


def create_intermediate_ca(
    signing_ca_path,
    intermediate_path,
    name="IntermediateCA",
    suppress_output=True,
    age_years=5,
) -> pathlib.Path:
    """
    Creates an intermediate CA from a parent CA.

    :param signing_ca_path: The path of the signing CA
    :param intermediate_path: Path to contain the CA files in
    :param name:  name of the intermediate.  Should follow file system naming rules.
    :param suppress_output: Suppress Stdout and stderr
    :param age_years: Number of years to create CA for
    :return: The path to the chain file for the intermediate CA
    """
    intermediate_path = pathlib.Path(intermediate_path).absolute()
    signing_ca_path = pathlib.Path(signing_ca_path).absolute()
    prep_ca_structure(intermediate_path, "policy_site_ca", name)
    csr_path = intermediate_path / "csr" / csr_file_name(name)
    private_key_path = intermediate_path / "private" / rsa_private_key_name(name)

    # Create Intermediate Certificate Request
    openssl_args = [
        "openssl",
        "req",
        "-config",
        "openssl.cnf",
        "-key",
        str(private_key_path),
        "-passin",
        "file:private/passphrase",
        "-new",
        "-sha256",
        "-out",
        str(csr_path),
        "-subj",
        "/C=AU/ST=Queensland/L=Brisbane/O=RAIK/OU=RAIK Development/CN=RAIK Test %s CA"
        % name,
    ]

    kw_args = {}
    if suppress_output:  # pragma: no cover
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, cwd=intermediate_path, **kw_args)

    cert_path = intermediate_path / "certs" / cert_file_name(name)

    chain_file = sign_ca_certificate(
        signing_ca_path,
        csr_path,
        cert_path,
        suppress_output=suppress_output,
        age_years=age_years,
    )

    return chain_file


def sign_ca_certificate(signing_ca_path, csr, cert, suppress_output=True, age_years=1):
    """
    Given a signing CA path and a CSR, the CSR is signed and a certificate
    for a CA is created. This will also create a certificate chain file for
    distribution where this CA needs to be known.  It does this by looking
    for a chain file at the signing CA (if none exists, takes the signing
    CA certificate) and appends this generated certificate.  The end
    destination is in the same folder as the generated certificate.
    :param signing_ca_path: Path to a CA
    :param csr: Path to a CSR
    :param cert: Path to where the cert is written (including file name)
    :param suppress_output: Suppress Stdout and stderr
    :param age_years: Number of years to create cert for
    :return: A path to a certificate chain file for distribution.  This
             needs to be securely distributed
    """
    signing_ca_path = pathlib.Path(signing_ca_path).absolute()
    csr = pathlib.Path(csr).absolute()
    cert = pathlib.Path(cert).absolute()

    openssl_args = [
        "openssl",
        "ca",
        "-batch",
        "-config",
        "openssl.cnf",
        "-passin",
        "file:private/passphrase",
        "-extensions",
        "v3_intermediate_ca",
        "-days",
        f"{age_years * 365}",
        "-notext",
        "-md",
        "sha256",
        "-in",
        str(csr),
        "-out",
        str(cert),
    ]

    kw_args = {}
    if suppress_output:  # pragma: no cover
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, cwd=signing_ca_path, **kw_args)
    dest_cert_folder = cert.parent
    src_cert_path = signing_ca_path / "certs" / CA_CHAIN_FILE_NAME

    if not src_cert_path.exists():
        # Get certificate name from configuration file
        with (signing_ca_path / "openssl.cnf").open("r") as f:
            for line in f:
                if line.startswith("certificate"):
                    f_name = line.split("certs/")[1].strip()
                    break
        src_cert_path = signing_ca_path / "certs" / f_name
    chain_file = dest_cert_folder / CA_CHAIN_FILE_NAME
    with chain_file.open("w") as out_f:
        with src_cert_path.open("r") as f:
            out_f.write(f.read())
        with cert.open("r") as f:
            out_f.write(f.read())
    return chain_file


def create_certificate_request(
    private_key_path: pathlib.Path,
    fqdn: str,
    country: str = "AU",
    state: str = "Queensland",
    location: str = "Brisbane",
    organisation: str = "RAIK",
    organisation_unit: str = "RAIK Development",
    suppress_output=True,
) -> pathlib.Path:
    """
    Create a certificate request to sign a public/private key pair.
    :param private_key_path: File name of the private key file
    :param fqdn: Name of the device to use in the certificate.  This
                 is either a hostname or a fully qualified domain name.
    :param country:
    :param state:
    :param location:
    :param organisation: These fields need to match the signing authority
    :param organisation_unit:
    :param suppress_output: Suppress Stdout and stderr
    :return: the csr file name
    """
    # Create a temporary configuration file
    logger.info(
        "Generating certificate request for %s with key %s", fqdn, private_key_path
    )
    f = tempfile.NamedTemporaryFile(mode="w", encoding="UTF-8", delete=False)
    request_params = {
        "common_name": fqdn,
        "country": country,
        "state": state,
        "location": location,
        "organisation": organisation,
        "organisation_unit": organisation_unit,
    }
    f.write((raik.utils._ssl_templates.SSL_REQUEST % request_params))
    f.close()
    # Create a Certificate Request
    csr_file_path = private_key_path.parent / csr_file_name(fqdn)
    logger.debug(
        "Generating CSR from config at %s from SSL_REQUEST template using %s args",
        f.name,
        request_params,
    )
    openssl_args = [
        "openssl",
        "req",
        # '-config', 'openssl.cnf',
        "-config",
        f.name,
        "-key",
        str(private_key_path),
        # '-passin', 'file:private/passphrase',
        "-new",
        "-sha256",
        "-out",
        str(csr_file_path),
        "-subj",
        f"/C={country}/ST={state}/L={location}"
        f"/O={organisation}/OU={organisation_unit}"
        f"/CN={fqdn}",
    ]

    kw_args = {}
    if suppress_output:
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, **kw_args)
    logger.debug("Removing temporary config %s", f.name)
    os.remove(f.name)
    return csr_file_path


def sign_certificate_request(
    signing_ca_path, csr, cert, suppress_output=True, client_cert=False, age_years=1
):
    """
    Sign a certificate request.
    :param signing_ca_path: Path of the CA data folder
    :param csr: Certificate sign request file path to sign
    :param cert: Certificate file to save the signed certificate
    :param suppress_output: Suppresses STDOUT and STDERR
    :param client_cert: Specifies this is a client certificate to create,
                        otherwise it's a server certificate.
    :param age_years: Number of years to create cert for
    :return: None
    """
    logger.info("Signing CSR %s using %s and creating %s", csr, signing_ca_path, cert)
    openssl_args = [
        "openssl",
        "ca",
        "-batch",
        "-config",
        "openssl.cnf",
        "-passin",
        "file:private/passphrase",
    ]
    if client_cert:
        openssl_args += ["-extensions", "usr_cert"]
    else:
        openssl_args += ["-extensions", "server_cert"]
    openssl_args += [
        "-days",
        f"{age_years * 365}",
        "-notext",
        "-md",
        "sha256",
        "-in",
        str(csr.absolute()),
        "-out",
        str(cert.absolute()),
    ]

    kw_args = {}
    if suppress_output:
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, cwd=signing_ca_path, **kw_args)


def _create_host_csr(cert_folder, fqdn, suppress_output=True) -> pathlib.Path:
    cert_folder = pathlib.Path(cert_folder).absolute()
    s_private = cert_folder / rsa_private_key_name(fqdn)
    s_pub = cert_folder / rsa_public_key_name(fqdn)
    generate_rsa_keys(s_private, s_pub)
    csr = create_certificate_request(s_private, fqdn, suppress_output=suppress_output)
    return csr


# TODO:  It would be worth upstream skipping generating pub/pri key pairs if they
# already exist in the given path
def create_server_certificate(
    signing_ca: pathlib.Path, cert_folder: pathlib.Path, fqdn: str, suppress_output=True
) -> Tuple[pathlib.Path, pathlib.Path]:
    """
    Generates a Server Private Key and Signed Certificate.

    All files are in PEM format.

    :param signing_ca:
    :param cert_folder:
    :param fqdn:
    :param suppress_output: Suppresses STDOUT and STDERR
    :return: Path to the key and signed certificate
    """
    logger.info("Generating server certificate for %s using %s", fqdn, signing_ca)
    s_csr = _create_host_csr(cert_folder, fqdn, suppress_output)
    s_private = cert_folder / rsa_private_key_name(fqdn)
    signing_ca = pathlib.Path(signing_ca).absolute()
    signed_cert = cert_folder / server_cert_name(fqdn)
    sign_certificate_request(
        signing_ca, s_csr, signed_cert, suppress_output=suppress_output
    )
    return s_private, signed_cert


OPEN_SSL_DATE_FORMAT = "%y%m%d%H%M%SZ"


class OpenSslCertStatus(enum.Enum):
    valid = "V"
    revoked = "R"
    expired = "E"
    unknown = ""


class PemType(enum.Enum):
    private_key = "Private Key"
    public_key = "Public Key"
    x509_cert = "x509 Certificate"
    x509_chain = "x509 Certificate Chain"


@dataclasses.dataclass()
class PemFileDetails:
    type: PemType
    file_path: pathlib.Path

    # x509 Stats
    subject: Optional[str] = None
    issuer: Optional[str] = None
    start_date: Optional[datetime] = None
    end_date: Optional[datetime] = None
    serial_number: Optional[int] = None
    version: Optional[str] = None

    # Basic Constraints
    is_ca: Optional[bool] = None

    # Key Usage
    digital_signature: Optional[bool] = None
    content_commitment: Optional[bool] = None
    key_encipherment: Optional[bool] = None
    data_encipherment: Optional[bool] = None
    key_agreement: Optional[bool] = None
    key_cert_sign: Optional[bool] = None
    crl_sign: Optional[bool] = None

    # Extended Usage
    server_auth: Optional[bool] = None
    client_auth: Optional[bool] = None

    @classmethod
    def from_x509_text(
        cls, pem_type: PemType, file_path: pathlib.Path, x509_text: str
    ) -> "PemFileDetails":
        # Move this to a separate function and produce a different data type
        # that is nested.  This data type can be used elsewhere
        cert = decode_pem_certificate(x509_text)

        # subject is the same as cert_distinguished_name when formatted this way
        subject = "/".join([a.rfc4514_string() for a in cert.subject.rdns])
        subject = f"/{subject}"

        issuer = "/".join([a.rfc4514_string() for a in cert.issuer.rdns])
        issuer = f"/{issuer}"

        start_date = naive_to_utc(cert.not_valid_before)
        end_date = naive_to_utc(cert.not_valid_after)  # Same as end_date, these are UTC

        serial_number = cert.serial_number
        version = cert.version.name

        # It doesn't like dotted_string, but it works.
        # noinspection PyUnresolvedReferences
        extensions = {e.oid.dotted_string: e.value for e in cert.extensions}
        basic_constraints_oid = "2.5.29.19"
        basic_constraints: x509.BasicConstraints = extensions.get(basic_constraints_oid)
        key_usage_oid = "2.5.29.15"
        key_usage: x509.KeyUsage = extensions.get(key_usage_oid)
        extended_key_usage_oid = "2.5.29.37"
        extended_key_usage: x509.ExtendedKeyUsage = extensions.get(
            extended_key_usage_oid
        )

        if basic_constraints:
            is_ca = basic_constraints.ca
        else:
            is_ca = None

        if key_usage:
            digital_signature = key_usage.digital_signature
            content_commitment = key_usage.content_commitment
            key_encipherment = key_usage.key_encipherment
            data_encipherment = key_usage.data_encipherment
            key_agreement = key_usage.key_agreement
            key_cert_sign = key_usage.key_cert_sign
            crl_sign = key_usage.crl_sign
        else:
            digital_signature = None
            content_commitment = None
            key_encipherment = None
            data_encipherment = None
            key_agreement = None
            key_cert_sign = None
            crl_sign = None

        if extended_key_usage:
            extended_keys = [e.dotted_string for e in extended_key_usage]
            server_auth_oid = "1.3.6.1.5.5.7.3.1"
            client_auth_oid = "1.3.6.1.5.5.7.3.2"
            server_auth = server_auth_oid in extended_keys
            client_auth = client_auth_oid in extended_keys
        else:
            server_auth = None
            client_auth = None

        return cls(
            type=pem_type,
            file_path=file_path,
            subject=subject,
            issuer=issuer,
            start_date=start_date,
            end_date=end_date,
            serial_number=serial_number,
            version=version,
            is_ca=is_ca,
            digital_signature=digital_signature,
            content_commitment=content_commitment,
            key_encipherment=key_encipherment,
            data_encipherment=data_encipherment,
            key_agreement=key_agreement,
            key_cert_sign=key_cert_sign,
            crl_sign=crl_sign,
            server_auth=server_auth,
            client_auth=client_auth,
        )


class OpenSslIndexEntry(NamedTuple):
    status: OpenSslCertStatus
    expiration_date: datetime
    revocation_date: Optional[datetime]
    revocation_reason: Optional[str]
    serial: int
    cert_file_name: Optional[str]
    cert_distinguished_name: str
    pem_details: Optional[PemFileDetails]


def load_pem_file_details(pem_path: pathlib.Path) -> List[PemFileDetails]:
    with pem_path.open("r") as f:
        data = f.read()
    if HEADER_RSA_PRIVATE_KEY in data:
        return [PemFileDetails(type=PemType.private_key, file_path=pem_path)]
    elif HEADER_RSA_PUBLIC_KEY in data:
        return [PemFileDetails(type=PemType.public_key, file_path=pem_path)]
    else:
        cert_count = data.count(HEADER_X509)
        if cert_count == 0:
            raise NotImplementedError(f"Unknown Pem File at {pem_path}")
        elif cert_count == 1:
            cert_type = PemType.x509_cert
        else:
            cert_type = PemType.x509_chain

        return list(
            map(
                lambda d: PemFileDetails.from_x509_text(
                    pem_type=cert_type, file_path=pem_path, x509_text=d
                ),
                extract_certs_from_pem(pem_path),
            )
        )


@dataclasses.dataclass
class OpenSslData:
    """
    This class encapsulates the structure of an Open SSL CA folder with
    additional RAIK values
    """

    ca_name: str
    base_dir: pathlib.Path

    ssl_conf_name: ClassVar[str] = "openssl.cnf"
    certs_folder_name: ClassVar[str] = "certs"
    main_cert_name: ClassVar[str] = "ca.cert.pem"
    index_name: ClassVar[str] = "index.txt"

    @classmethod
    def from_ca_name(cls, parent_dir: pathlib.Path, ca_name: str) -> "OpenSslData":
        """
        Instantiates this class from a parend dir and name.  The CA name is the
        first folder created in the parent and contains the OpenSSL files.
        """
        return cls(
            ca_name=ca_name,
            base_dir=parent_dir / ca_name,
        )

    @functools.cached_property
    def conf_path(self) -> pathlib.Path:
        return self.base_dir / self.ssl_conf_name

    @functools.cached_property
    def index_path(self) -> pathlib.Path:
        return self.base_dir / self.index_name

    @functools.cached_property
    def chain_path(self) -> pathlib.Path:
        return self.base_dir / self.certs_folder_name / CA_CHAIN_FILE_NAME

    @functools.cached_property
    def cert(self) -> pathlib.Path:
        return self.base_dir / self.certs_folder_name / self.main_cert_name

    def _map_index_row(self, text: str) -> OpenSslIndexEntry:
        # noinspection SpellCheckingInspection
        """
        This retrieves standard certificate index data as well as
        additional useful / interesting data.

        Useful information includes

        * Subject
        * issuer (CN or subject?)
        * start date
        * end date
        * purpose

        This data can be inspected using openssl as well with:

        openssl x509 -in cf-process-node.tnet.pem -noout -subject
        openssl x509 -in cf-process-node.tnet.pem -noout -issuer
        openssl x509 -in cf-process-node.tnet.pem -noout -startdate
        openssl x509 -in cf-process-node.tnet.pem -noout -enddate
        openssl x509 -in cf-process-node.tnet.pem -noout -purpose
        """
        cols = text.split("\t")

        status = OpenSslCertStatus(cols[0])

        expiration_date = strptime_as_utc(cols[1], OPEN_SSL_DATE_FORMAT)

        rev = list(filter(bool, cols[2].split(",")))
        if len(rev) == 0:
            revocation_date = None
            revocation_reason = None
        elif len(rev) == 1:
            revocation_date = datetime.datetime.strptime(rev[0], OPEN_SSL_DATE_FORMAT)
            revocation_reason = None
        elif len(rev) == 2:
            revocation_date = datetime.datetime.strptime(rev[0], OPEN_SSL_DATE_FORMAT)
            revocation_reason = rev[1]
        else:
            raise NotImplementedError

        serial_str = cols[3]
        serial = int(serial_str, 16)

        file_name = cols[4]
        if file_name == "unknown" or not file_name.strip():
            file_name = None

        cert_path = self.base_dir / "newcerts" / f"{serial_str}.pem"
        if cert_path.exists():
            cert_details = load_pem_file_details(cert_path)
            assert len(cert_details) == 1
            cert_details = cert_details[0]
        else:
            cert_details = None

        cert_distinguished_name = cols[5].strip()

        return OpenSslIndexEntry(
            status=status,
            expiration_date=expiration_date,
            revocation_date=revocation_date,
            revocation_reason=revocation_reason,
            serial=serial,
            cert_file_name=file_name,
            cert_distinguished_name=cert_distinguished_name,
            pem_details=cert_details,
        )

    def index_data(self) -> List[OpenSslIndexEntry]:
        with self.index_path.open("r") as f:
            return list(map(self._map_index_row, f))

    def ca_cert_as_index(self) -> OpenSslIndexEntry:
        """
        Returns the CA Cert as an index row (fakes placing the CA main cert
        in the index).
        """
        cert_details = load_pem_file_details(self.cert)
        assert len(cert_details) == 1
        cert_details = cert_details[0]

        status = OpenSslCertStatus.unknown
        revocation_date = None
        revocation_reason = None
        serial = cert_details.serial_number
        file_name = str(self.cert)
        expiration_date = cert_details.end_date
        cert_distinguished_name = cert_details.subject

        return OpenSslIndexEntry(
            status=status,
            expiration_date=expiration_date,
            revocation_date=revocation_date,
            revocation_reason=revocation_reason,
            serial=serial,
            cert_file_name=file_name,
            cert_distinguished_name=cert_distinguished_name,
            pem_details=cert_details,
        )


def list_ca_certificates(ca_path: pathlib.Path) -> List[OpenSslIndexEntry]:
    """
    List Certificates in the provided CA path.  The folder containing the
    CA is expected to also be the name of the CA.
    """
    data = OpenSslData.from_ca_name(ca_path.parent, ca_path.name)

    return [data.ca_cert_as_index()] + data.index_data()


def revoke_server_certificate(signing_ca, fqdn, suppress_output=True):
    """
    Revoke a server certificate by name

    :param signing_ca:
    :param fqdn:
    :param suppress_output: Suppresses STDOUT and STDERR
    :return: Path to the key and signed certificate
    """
    signing_ca = pathlib.Path(signing_ca).absolute()
    new_certs_path = signing_ca / "newcerts"

    cert_path = ""
    cert_date = None
    for pem_file in new_certs_path.glob(server_cert_name("*")):
        with pem_file.open("r") as f:
            pem_data = f.read()
        cert = decode_pem_certificate(pem_data)
        common_names = cert.subject.get_attributes_for_oid(x509.NameOID.COMMON_NAME)
        if cert_date is None or cert_date < cert.not_valid_before:
            for common_name in common_names:
                if common_name.value == fqdn:
                    cert_path = pem_file
                    cert_date = cert.not_valid_before
                    break

    openssl_args = [
        "openssl",
        "ca",
        "-config",
        "openssl.cnf",
        "-passin",
        "file:private/passphrase",
        "-revoke",
        str(cert_path),
    ]

    kw_args = {}
    if suppress_output:
        kw_args.update({"stderr": subprocess.DEVNULL, "stdout": subprocess.DEVNULL})

    subprocess.check_call(openssl_args, cwd=signing_ca, **kw_args)


# noinspection DuplicatedCode
def create_client_certificate(
    signing_ca: pathlib.Path, cert_folder: pathlib.Path, fqdn, suppress_output=True
):
    """
    Generates a Client Signed Certificate and Key combo

    All files are in PEM format.

    :param signing_ca:
    :param cert_folder:
    :param fqdn:
    :param suppress_output: Suppresses STDOUT and STDERR
    :return: Path to the PFX File for the client
    """
    # Key Pair
    s_csr = _create_host_csr(cert_folder, fqdn, suppress_output)
    s_private = cert_folder / rsa_private_key_name(fqdn)

    signed_cert = cert_folder / client_cert_name(fqdn)
    sign_certificate_request(
        signing_ca,
        s_csr,
        signed_cert,
        client_cert=True,
        suppress_output=suppress_output,
    )
    # pfx file
    pfx_file = cert_folder / pkcs_client_key_and_cert_name(fqdn)
    create_pfx_file(s_private, signed_cert, pfx_file)
    return pfx_file


def verify_certificate(certificate_file: pathlib.Path, chain_file=None) -> bool:
    """
    This is simpler to just use OpenSSL for.  Libraries can do it, but OpenSSL
    can do it better.

    Given a certificate chain file and a certificate, verify the certificate is ok
    :param certificate_file: full path to certificate file.  If not specified, the file is read from
    the same path as this module as 'cert_chain.crt'.
    :param chain_file: full path to chain file
    """
    logger.debug("Verifying certificate")
    if chain_file is None:
        chain_file = CERT_VALIDATION_CHAIN

    if not os.path.exists(chain_file):
        logger.warning(
            "Certificate %s is being verified but "
            "no chain file is found (trying %s), using system file",
            certificate_file,
            chain_file,
        )
        openssl_args = ["openssl", "verify", certificate_file]
    else:
        openssl_args = ["openssl", "verify", "-CAfile", chain_file, certificate_file]
    try:
        subprocess.check_call(
            openssl_args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL
        )
    except subprocess.CalledProcessError:
        return False
    return True


def symmetric_encrypt(key: bytes, data: bytes) -> bytes:
    """
    Symmetrically encrypts the data given the key
    :param key: Key to use for encryption
    :param data: Data to encrypt
    :return:
    """
    iv = raik.utils.random.get_random_length_bytes(16, 16)
    # Hash Key to make it the right length
    key_hash = SHA256.new()
    key_hash.update(key)
    key = key_hash.digest()

    # Pad data
    padding_len = (16 - len(data) % 16) or 16
    padding = chr(padding_len) * padding_len
    data += padding.encode("ascii")

    enc_obj = AES.new(key, AES.MODE_CBC, iv)
    cipher_text = enc_obj.encrypt(data) + iv
    return cipher_text


def symmetric_decrypt(key: bytes, cipher_text: bytes) -> bytes:
    """
    Symmetrically decrypt the data given the key
    :param key: Key to use for encryption
    :param cipher_text: Data to encrypt
    :return:
    """
    iv = cipher_text[-16:]
    cipher_text = cipher_text[:-16]

    # Hash Key to make it the right length
    key_hash = SHA256.new()
    key_hash.update(key)
    key = key_hash.digest()

    enc_obj = AES.new(key, AES.MODE_CBC, iv)
    data = enc_obj.decrypt(cipher_text)

    # Remove padding
    padding_len = -1 * data[-1]
    data = data[:padding_len]

    return data


def asymmetric_encrypt_with_public_key(key: bytes, data: bytes) -> bytes:
    """
    Asymmetrically encrypts the data given the key.  If the key provided is a signed certificate
    then the chain file needs to be set, otherwise certificate testing won't be performed.
    :param key: Key to use for encryption in X.509, PKCS or OpenSSH format.
    :param data: Data to encrypt
    :return:
    """
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)

    # Calculate maximum data length
    # From the source code of PKCS1_OAEP, needed for debugging
    mod_bits = Crypto.Util.number.size(key.n)
    q, r = divmod(mod_bits, 8)
    k = r and (q + 1) or q
    # noinspection PyProtectedMember,PyUnresolvedReferences
    h_len = cipher._hashObj.digest_size
    m_len = len(data)
    ps_len = k - m_len - 2 * h_len - 2
    logger.debug(
        "Executing cipher on message of len %i (Max that can fit: %i)",
        m_len,
        m_len + ps_len,
    )
    if ps_len < 0:
        raise TypeError(
            "Data can't be greater then key length.  Unable to encrypt by %i" % ps_len
        )

    cipher_text = cipher.encrypt(data)
    return cipher_text


def asymmetric_decrypt_with_private_key(key: bytes, cipher_text: bytes) -> bytes:
    """
    Symmetrically decrypt the data given the key
    :param key: Key to use for encryption
    :param cipher_text: Data to encrypt
    :return:
    """
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)
    data = cipher.decrypt(cipher_text)
    return data
