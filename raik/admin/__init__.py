import logging

from raik.admin._logging import init_logging
from raik.admin._argparse import parser

# Trigger parsers
# Setup / Configure
from raik.admin import init

# Control
from raik.admin import start, run

# Communications
from raik.admin import discover

# Certificates
from raik.admin import certs

# Encryption
from raik.admin import crypt


logger = logging.getLogger(__name__)


def main():
    init_logging()
    logger.debug("start")
    args = parser.parse_args()
    try:
        globals()[args.admin_command].main(args)
    except KeyError as e:
        logger.error('Command %s is not configured', e.args[0])
    except ValueError as e:
        logger.error(e.args[0])
