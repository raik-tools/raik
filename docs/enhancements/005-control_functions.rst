=================
Control Functions
=================

:Enhancement: 5
:Keyword: E005-control_functions
:Type: Feature
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:


Overview
========

The purpose of a RAIK controller is to execute functions based on events.  The standards and features associated
with these functions is found here.

Background
----------

TODO:  Look for Redmine docs to Migrate

Status Comments
===============

This document is still in draft.


References
==========

TODO - External references

Features
========

* TODO Link to stories with the E005 keyword

Pro Features
============


Definition
==========

Overview
--------

A control function is an atomic piece of code that is executed based on event.

Functions have input and output only.

Function Blocks (to be defined later) have state, which can be pre-configured.  These will be defined
by a future enhancemnt.

Unlike real time systems, each instance of a control function will execute in parallel on multi process systems but
they need to provide a guarantee completion time otherwise they will be considered failed and their outputs will be
forced to a fail state.

.. warning::

    This specification is incomplete.  See additional warnings below


Functions
---------

Functions represent simple executables.  They are a simple input and output method and have no state.


Classes
-------

A class is a more advanced alternative to a function.  It groups a number of variables and functions to represent a
particular object.  The advantage of classes are:
* Complex functions can be broken down into more manageable chunks but are part of the same object
* They have state and can remember values between executions


Execution
---------

This starts to influence the design of :doc:`/enhancements/004-soft-controller`.

Functions are executed based on a schedule.  The functions execute in parrallel. leaving it to the OS.

Functions can call other functions.  Functions can describe the interaction of function blocks.  Function blocks
are called within their function?

To resolve:
* Do Function Blocks get scheduled on their own?  I don't see why not.


Criteria
========

* Functions need to be able to be executed in as synchronous manner if dependent on other outputs
* Isochronous functions will execute on wall clock intervals for synchronicity between systems
* Functions can also be executed on event
* Functions can be executed on calendar
* A function library will exist for both "approved / RAIK maintained" functions and user site specific functions
* Functions shall be automatically tested and include coverage reports


Function Specification
======================

See :ref:`function_execution_research` for prior research.

Execution
---------

Functions are presently designed to be executed within a eval / exev environment.  This contains significant security
risks which will be managed by inspecting code for security issues and tightly controlling import statements.  It
is not the greatest security, as opposed to using AST to create custom syntax, but provides the greater flexibility.
Also, to put in context, the only users adding code in this environment are the ones who would be penalised for
getting it wrong, so user access escurity will be a strong focus.


Security
--------

Functions shall be tightly controlled with access outside of the function on a whitelist basis and tightly
controlled.  No imports are allowed.  Not some imports can imply others (so importing module A which internally
imports module B results in being able to access module B).  Care must be taken to prevent this.

Additional treatment of function code:

* double underscore and single underscore calls are forbidden.  This is not perfect as a user could easily get around
  this calling get_attr with obscured functions.  We may have to override get_attr somehow if it is allowed.
* classes are dangerous, they can not be defined directly as there are plenty of ways to get past security with them
* TODO:  Link to / review security mechanisms in code parsing.


Function Storage Specification
==============================

Functions shall be saved to disk in a manner where they can be meaningfully accessed, executed and tested
locally.

TODO:  Look for file specs in Redmine


Issues in Redmine to review and migrate:

* http://redmine.tnet:3000/issues/239  <-- This is the main one to migrate
* http://redmine.tnet:3000/issues/256
* http://redmine.tnet:3000/issues/250
* http://redmine.tnet:3000/issues/245  <-- Pertains to meta-data for control values

   * Meta-data includes engineering units and so forth.  Perhaps these should just be referenced and we leave
     it to function and I/O declaration / configuration for validation and describing these.

* http://redmine.tnet:3000/issues/243  <-- Engineering units and enumartion management / simplification
* http://redmine.tnet:3000/issues/231  <-- Function Blocks / Functions

* PASS

Notes:

* Tests are contained a library (not stored with the file) to avoid loading extraneous code at runtime.
* User libraries can be developed in addition to system libraries
* Unit testing support exists for all libraries
* Currently structure is only one level deep (no deeper)
* The need for process restarts on library change should be signalled somehow (risks managed, perhaps detected based on
   imports)


Overview
--------

A Library folder (currently raik/control/control_lib) contains all the standard libraries for RAIK.  These are system
libraries tested and maintained by the RAIK project.  This location can be changed in the 'control_lib_path' variable
within raik/control/__init__.py.

A second variable exists, called "site_config_path".  This is an additional location that can be used for site specific
configuration or user functions that don't form part of the RAIK standard library.  This is the first location checked
for a function (and hence a function duplicating a name from the standard library will be loaded instead of the
standard library function).  Functions found in this location must pass additional validation checks, whereas functions
in the standard library are loaded as is.

Within these folders are multiple function_name.py files.  The function name must be unique amongst all libraries (this
will likely change to follow a more python style of naming).

Current Layout:

* control_lib

    * raik_control_io --  This contains all I/O reading and writing functions and can not be overridden by site versions.
      These are special case and each python file must contain a method called "scan_module" which receives a dictionary
      of outputs and returns a dictionary of inputs.

        * PiFace_Digital.py -- TODO - Add to a standard library documentation section
        * System.py -- TODO - Add to a standard library documentation section

    * raik_control_standard -- this contains standard raik control functions (python files) that can be called directly
        and mapped to I/O or can be called by other functions.

* site_config -- Contains python files for site / user specific control tasks.


Function Names
--------------

Function names must be globally unique.  A function is contained within a python file of the same name and detailed
separately (below).  The raik controller first looks for that function in the site configuration folder and if not
found, searches the standard library.

Exception:  IO scanning function names can not be overridden.  All IO scanning functions are located in the main
library under 'raik_control_io' sub folder.

Function details
----------------

A function can be located either in a sub folder in the main library folder or in a site config folder.

The content of a function is python code, but has bee heavily restricted by RAIK to avoid security and stability
issues.  To avoid writing a completely new parser, RAIK locks down what is accessible to all functions (standard
library included).  Site specific functions undergo additional validation checks before loading.

Main Library folder
-------------------

A library folder (either standard or site specific) is not a python module (no __init__.py).  This is intentional and
it must be able to stand alone.  The raik control process will manually import functions from within this folder.

Sub folder
----------

The main library folder is organised into sub folders purely for organisation (this structure may change in the future).
A function name must be unique across all folders and the folders in the main library are also Python modules.
The purpose of them being modules is to simplify running test cases using the standard python unit testing tools.


Function File Specification
===========================

Current Function File Format
----------------------------

.. warning::

    This specification is incomplete.  We're waiting for a basic function editor to exist to understand how easy
    it will be to perform code completion in the front end.  At that point, this specification may change to group
    all inputs into an enumeration like variables.


User input is body only, text passing will occur to try and check the input is safe.

Functions will be saved in files so that GIT can manage them.  Files will be <funciton_name>.py (exception is I/O).

File format:

* Import area - one line each
* def <function name>(a1, a2, a3):
*    doc_string
*    body
*    return {'r1': r1, 'r2': r2}
* Comment "#EOF\n" so that multiple processes can know when a file is complete.

All parameters will support units (see #243).  The intended units are specified in the function definition, and when
the function is compiled, an automatic conversion is done from the inputs.  In other words, when creating a function,
the input and outputs will be specified in what units they need to be.  Then, when inputs are mapped, the system will
automatically ensure that the inputs are converted appropriately.




Function File Example
---------------------

This is a current file format proposal (WIP, stale since 2016-03-12)

.. code-block:: python

   import math

   def ideal_gas_law(
           # User Parameters
           P1=None,
           P2=None,
           V1=None,
           V2=None,
           T1=None,
           T2=None,
           ref_pressure = None,
           last_outputs = None,
           # System Parameters
           data = None,
           history = None,
           system = None
   ):
       """
       Given any of the optional parameters, this equation will return what it can from the ideal gas law written as:
       P1/P2 = V1/V2 = T1/T2
       :param P1: Absolute Pressure
       :param P2: Absolute Pressure
       :param V1: Volume
       :param V2: Volume
       :param T1: Absolute Temperature
       :param T2: Absolute Temperature
       :param ref_pressure: If the given pressures are not absolute, then this is used to convert them.
       :param last_outputs:  A dictionary of the previous output values, updated automatically
       :param data: A dictionary that can be used for storing data between function calls
       :param history:  If enabled, this can be used to get the history of any parameters with historical data
       :param system:  A system object that gives access to other items.
       :return: All parameters known and that are able to be calculated (P1 through T2)
       """

       # Automatically generated validation

       # ---- User Code ---- #

       # ---- End User Code ---- #

       # Return dictionary
       return {
           'P1': P1,
           'P2': P2,
           'V1': V1,
           'V2': V2,
           'T1': T1,
           'T2': T2,
       }
   # EOFX

   # Unit testing

   # EOF






Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.