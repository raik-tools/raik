import argparse

# Universal options
universal_parser = argparse.ArgumentParser(add_help=False)
universal_parser.add_argument("-v", "--verbose", help="increase output verbosity",
                              action="count", default=0)

# First level command
parser = argparse.ArgumentParser(
    description='RAIK Admin Commands',
    parents=[universal_parser]
)

# RAIK Commands
command_subparsers = parser.add_subparsers(
    title='Commands',
    # description='command_subparsers this',
    # help='Something something command_subparsers',
    metavar='',
    dest='admin_command'
)
