"""
A Function Block (class) implementation for receiving signed data.

The fun part is working out how to generate instances of this block within a function and link them together

Need to update this file to consider other imports and tools to be loaded in by scan manager.  I was thinking
of making a class to represent a "diagram" but perhaps just a module is enough.

TODO:  Reviewing existing code, to make some recommendations:
All function functions from the library are called with kwargs and the values for the kwards are populated using a
map against the local redis.   Issues this faces include:

* Dealing with a large number of arguments, name-spacing these arguments might be beneficial
* Cascading values from one function to another doesn't always need to be cached.

Consider creating some template control files with a script to generate particular files with a template,
maybe asking questions about what to include.

"""
import logging

logger = logging.getLogger("RAIK_COMMS:recv_signed_anon")

__raik_style__ = "class"


class RecvSignalAnon:
    def __init__(self):
        """
        This is internal use only for RAIK.  It is used on instantiation but shouldn't perform any execution
        """
        pass

    def load(self):
        """
        This is what happens when the class is first loading
        """

    def execute(self, inputs):
        pass
