import logging.config
import raik.enable_testing
import unittest
import raik.control.management
import raik.control.functions
import raik.control.scan
import raik.communications.local
import time
import sys
import os
import os.path
import shutil
import psutil
logger = logging.getLogger(__name__)
raik.enable_testing.re_enable_testing()

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(process)d.%(thread)d %(levelname)s %(name)s:%(lineno)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    # 'loggers': {
    #     'daphne': {
    #         'handlers': ['console'],
    #         # 'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
    #         'level': 'WARNING',
    #     },
    # },
    'root': {
        'handlers': ['console'],
        'level': 'DEBUG',
    }
}
logging.config.dictConfig(LOGGING)

# Additional tests:
# Change the configuration and only the affected process should change PID
# Create a stalled process and have the bootstrap method should restart it, the PID should change


class TestStartController(unittest.TestCase):
    """
    These methods test the starting of control processes with its own control library created under /tmp.
    """
    def setUp(self):
        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_values()
        raik.control.retry_time_medium = 1.0
        raik.control.retry_time_long = 1.0
        # Set up 'standard' config library:
        root_path = '/tmp/control_test'
        if os.path.exists(root_path):
            shutil.rmtree(root_path)
        os.mkdir(root_path)
        os.mkdir(os.path.join(root_path, 'test_lib'))
        with open(os.path.join(root_path, 'test_lib/__init__.py'), 'w') as f:
            f.write('\n')

        # Set up IO Library
        config_path = os.path.join(root_path, 'raik_control_io')
        os.mkdir(config_path)
        with open(os.path.join(root_path, 'raik_control_io/__init__.py'), 'w') as f:
            f.write('\n')

        # Set up site library
        config_path = os.path.join(root_path, 'raik_control_config')
        os.mkdir(config_path)

        # Make libs accessible
        original_path, original_config = raik.control.functions.get_paths()
        self.root_path = root_path
        self.config_path = config_path
        raik.control.functions.set_paths(
            root_path,
            config_path
        )
        # Remove original paths to allow custom I/O library
        if original_path in sys.path:
            sys.path.remove(original_path)
        if original_config in sys.path:
            sys.path.remove(original_config)

    def tearDown(self):
        raik.control.management.kill_all()
        raik.communications.local.clear_all_values()
        raik.communications.local.clear_all_events()
        raik.communications.local.clear_all_config()
        raik.control.functions.set_paths()
        # Remove Custom paths to avoid future import issues.
        if self.root_path in sys.path:
            sys.path.remove(self.root_path)
        if self.config_path in sys.path:
            sys.path.remove(self.config_path)

    def test_start_processes_create_processes(self):
        """
            Multi process testing is getting a bit challenging.
            This test simply ensures that multiple processes are forked from this one.  It performs the
            following:
                * Creates a function that will "phone home" and places it in the library
                * Create a configuration that will execute multiple instances of this function
                * Call run once with the appropriate configuration
                * Check the module level list of processes for each function instance.  Compare the PID
                    with the output of the "phone home" functions PID.
                * If each line in the configuration file is a separate process, success.
        """
        # Set up site library
        fx_name = 'say_hello_a'
        test_io_name = 'test_a'
        raik.control.functions.default_imports['os'] = os
        with open(os.path.join(self.root_path, 'raik_control_config/%s.py' % fx_name), 'w') as f:
            f.write('def %s(**kwargs):\n' % fx_name)
            f.write('    return {"x": os.getpid()}\n# EOF\n')

        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address"
            "%(io)s_1, VI, None\n"
            "%(io)s_2, VI, None\n"
            "%(io)s_3, VI, None\n"
            "%(io)s_4, VI, None\n"
            "%(io)s_5, VI, None\n"
            "not_used_1, VF, 3\n"
            "not_used_2, VI, 3\n"
            "not_used_3, VB, 3\n"
            '%(fx)s_1, %(fx)s, cyclic, {"period": 0.1}, x=%(io)s_1\n'
            '%(fx)s_2, %(fx)s, cyclic, {"period": 0.1}, x=%(io)s_2\n'
            '%(fx)s_3, %(fx)s, cyclic, {"period": 0.1}, x=%(io)s_3\n'
            '%(fx)s_4, %(fx)s, cyclic, {"period": 0.1}, x=%(io)s_4\n'
            '%(fx)s_5, %(fx)s, cyclic, {"period": 0.1}, x=%(io)s_5\n' % {'fx': fx_name, 'io': test_io_name}
        )

        # Test outputs don't exist
        self.assertIsNone(raik.communications.local.get_value(test_io_name + '_1'))
        self.assertIsNone(raik.communications.local.get_value(test_io_name + '_2'))
        self.assertIsNone(raik.communications.local.get_value(test_io_name + '_3'))
        self.assertIsNone(raik.communications.local.get_value(test_io_name + '_4'))
        self.assertIsNone(raik.communications.local.get_value(test_io_name + '_5'))
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertIsNone(raik.communications.local.get_value('not_used_2'))
        self.assertIsNone(raik.communications.local.get_value('not_used_3'))

        raik.control.management.start_processes(control_configuration)

        # Test that values now exist
        self.assertIsInstance(raik.communications.local.get_value(test_io_name + '_1'), int)
        self.assertIsInstance(raik.communications.local.get_value(test_io_name + '_2'), int)
        self.assertIsInstance(raik.communications.local.get_value(test_io_name + '_3'), int)
        self.assertIsInstance(raik.communications.local.get_value(test_io_name + '_4'), int)
        self.assertIsInstance(raik.communications.local.get_value(test_io_name + '_5'), int)
        self.assertIsInstance(raik.communications.local.get_value('not_used_1'), float)
        self.assertIsInstance(raik.communications.local.get_value('not_used_2'), int)
        self.assertIsInstance(raik.communications.local.get_value('not_used_3'), bool)

        # Read PID for each process and make sure it isn't this one
        this_pid = os.getpid()

        # Check the PIDs are unique
        pid_set = [this_pid]
        for test_number in range(1, 6):
            test_pid = raik.communications.local.get_value(test_io_name + '_%i' % test_number)
            self.assertNotIn(test_pid, pid_set)
            pid_set.append(test_pid)

        # Shutdown
        raik.control.management.close_all()
        for test_number in range(1, 6):
            status = raik.communications.local.get_value('%s_%i_status' % (fx_name, test_number))
            self.assertEqual(status['s'], 'terminated')

    def test_start_processes_create_module_IO(self):
        """
            Test the configuration creation of I/O modules
        """
        # Set up site library
        raik.control.functions.default_imports['os'] = os
        with open(os.path.join(self.root_path, 'raik_control_io/Random_IO.py'), 'w') as f:
            f.write('import random\n')
            f.write('random.seed()\n')
            f.write('def scan_module(**kwargs):\n')
            f.write('    return {'
                    '        "DI/0": random.random() > 0.7, \n'
                    '        "DI/1": random.random() > 0.7, \n'
                    '        "DI/2": random.random() > 0.7, \n'
                    '        "DI/3": random.random() > 0.7, \n'
                    '        "DI/4": random.random() > 0.7, \n'
                    '        "DI/5": random.random() > 0.7, \n'
                    '        "DI/6": random.random() > 0.7, \n'
                    '        "DI/7": random.random() > 0.7 \n'
                    '    }\n'
                    '# EOF\n')

        control_configuration = (
            "test_DI1, DI, Random_IO/DI/0\n"
            "test_DI2, DI, Random_IO/DI/1\n"
            "test_DI4, DI, Random_IO/DI/3\n"
            "test_DI7, DI, Random_IO/DI/6\n"
            "test_DO1, DO, Random_IO/DO/1\n"
            "test_DO2, DO, Random_IO/DO/2\n"
            "test_DO3, DO, Random_IO/DO/3\n"
        )

        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO'))

        raik.control.management.start_processes(control_configuration)

        # Test Function has been created
        # Note that inputs / outputs refer to the context of the function, not of the system a a whole.
        function_config = raik.communications.local.get_config('Random_IO')
        self.assertIsInstance(function_config, dict)
        self.assertEqual(function_config.get('function'), 'Random_IO')
        self.assertEqual(function_config['outputs']['DI/0'], 'test_DI1')
        self.assertEqual(function_config['outputs']['DI/1'], 'test_DI2')
        self.assertEqual(function_config['outputs']['DI/6'], 'test_DI7')
        self.assertEqual(function_config['inputs']['DO/1'], 'test_DO1')
        self.assertEqual(function_config['inputs']['DO/2'], 'test_DO2')
        self.assertEqual(function_config['inputs']['DO/3'], 'test_DO3')

        # Test Function is running
        while raik.communications.local.get_value('Random_IO_status') is None:
            time.sleep(0.01)
        while raik.communications.local.get_value('Random_IO_status')['s'] != 'running':
            time.sleep(0.01)
        while raik.communications.local.get_value('test_DI1') is None:
            time.sleep(0.01)

        self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)

        # Shutdown
        raik.control.management.close_all()
        self.assertEqual(raik.communications.local.get_value('Random_IO_status')['s'], 'terminated')

    def test_start_processes_exceptions(self):
        """
            Test Exceptions in run_once that don't involve process spawning
        """
        # Set up site library
        fx_name = 'say_hello'
        raik.control.functions.default_imports['os'] = os
        with open(os.path.join(self.root_path, 'raik_control_config/%s.py' % fx_name), 'w') as f:
            f.write('def %s(**kwargs):\n' % fx_name)
            f.write('    return {"x": os.getpid()}\n# EOF\n')

        # Test Bad config lines abort the whole process
        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address\n"
            "not_used_1, VF, A\n"
        )
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertRaises(ValueError, raik.control.management.start_processes, control_configuration)

        # Test Bad name aborts the whole process
        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address\n"
            "bootstrap, DI, Random\n"
        )
        self.assertRaises(ValueError, raik.control.management.start_processes, control_configuration)
        self.assertEqual(len(raik.control.management.find_raik_control_processes()), 0)

        # Test Bad config lines abort the whole process
        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address\n"
            "not_used_1, VA, A\n"
        )
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertRaises(ValueError, raik.control.management.start_processes, control_configuration)

        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address\n"
            "not_used_1, F, 3.0\n"
        )
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertRaises(ValueError, raik.control.management.start_processes, control_configuration)

        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name,Type,Address\n"
            "not_used_1, VF, 3.0\n"
        )
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertRaises(ValueError, raik.control.management.start_processes, control_configuration)

        # No new lines
        control_configuration = None
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertRaises(AttributeError, raik.control.management.start_processes, control_configuration)

    def test_run_multi_create_processes(self):
        """
            Multi process testing is getting a bit challenging.
            This test simply ensures that multiple processes are forked from this one.  It performs the
            following:
                * Creates a function that will "phone home" and places it in the library
                * Create a configuration that will execute multiple instances of this function
                * Call run once with the appropriate configuration
                * Check the module level list of processes for each function instance.  Compare the PID
                    with the output of the "phone home" functions PID.
                * If each line in the configuration file is a separate process, success.
        """
        # Set up site library
        fx_name = 'say_hello'
        raik.control.functions.default_imports['os'] = os
        with open(os.path.join(self.root_path, 'raik_control_config/%s.py' % fx_name), 'w') as f:
            f.write('def %s(**kwargs):\n' % fx_name)
            f.write('    return {"x": os.getpid()}\n# EOF\n')

        control_configuration = (
            "# A comment line for the sake of testing\n"
            "Name, Type, Address"
            "test_1, VI, None\n"
            "test_2, VI, None\n"
            "test_3, VI, None\n"
            "test_4, VI, None\n"
            "test_5, VI, None\n"
            "not_used_1, VF, 3\n"
            "not_used_2, VI, 3\n"
            "not_used_3, VB, 3\n"
            '%(fx)s_1, %(fx)s, cyclic, {"period": 0.1}, x=test_1\n'
            '%(fx)s_2, %(fx)s, cyclic, {"period": 0.1}, x=test_2\n'
            '%(fx)s_3, %(fx)s, cyclic, {"period": 0.1}, x=test_3\n'
            '%(fx)s_4, %(fx)s, cyclic, {"period": 0.1}, x=test_4\n'
            '%(fx)s_5, %(fx)s, cyclic, {"period": 0.1}, x=test_5\n' % {'fx': fx_name}
        )

        # Test outputs don't exist
        self.assertIsNone(raik.communications.local.get_value('test_1'))
        self.assertIsNone(raik.communications.local.get_value('test_2'))
        self.assertIsNone(raik.communications.local.get_value('test_3'))
        self.assertIsNone(raik.communications.local.get_value('test_4'))
        self.assertIsNone(raik.communications.local.get_value('test_5'))
        self.assertIsNone(raik.communications.local.get_value('not_used_1'))
        self.assertIsNone(raik.communications.local.get_value('not_used_2'))
        self.assertIsNone(raik.communications.local.get_value('not_used_3'))

        raik.control.management.start_processes(control_configuration)

        # Test that values now exist
        self.assertIsInstance(raik.communications.local.get_value('test_1'), int)
        self.assertIsInstance(raik.communications.local.get_value('test_2'), int)
        self.assertIsInstance(raik.communications.local.get_value('test_3'), int)
        self.assertIsInstance(raik.communications.local.get_value('test_4'), int)
        self.assertIsInstance(raik.communications.local.get_value('test_5'), int)
        self.assertIsInstance(raik.communications.local.get_value('not_used_1'), float)
        self.assertIsInstance(raik.communications.local.get_value('not_used_2'), int)
        self.assertIsInstance(raik.communications.local.get_value('not_used_3'), bool)

        # Read PID for each process and make sure it isn't this one
        this_pid = os.getpid()

        # Check the PIDs are unique
        pid_set = [this_pid]
        for test_number in range(1, 6):
            test_pid = raik.communications.local.get_value('test_%i' % test_number)
            self.assertNotIn(test_pid, pid_set)
            pid_set.append(test_pid)

        # Run again
        logger.info('PID set before re-run: %s' % str(pid_set))
        raik.control.management.start_processes(control_configuration)
        time.sleep(1)  # Wait for processes to run
        for test_number in range(1, 6):
            test_pid = raik.communications.local.get_value('test_%i' % test_number)
            self.assertNotEqual(test_pid, this_pid)
            if test_pid not in pid_set:
                raik.control.management.kill_all()
                self.fail("Processes have been recreated, the test won't be able to complete.  Make sure "
                          "tests is configured properly to handle parallel tests")

        # Shutdown
        raik.control.management.close_all()
        for test_number in range(1, 6):
            status = raik.communications.local.get_value('%s_%i_status' % (fx_name, test_number))
            self.assertEqual(status['s'], 'terminated')

    def test_launch_cyclic_process(self):
        """
            Test launch_cyclic_process and tests a restart of the process.
        """
        # Set up site library
        raik.control.functions.default_imports['os'] = os
        with open(os.path.join(self.root_path, 'raik_control_io/Random_IO.py'), 'w') as f:
            f.write('import random\n')
            f.write('random.seed()\n')

            f.write('def scan_module(**kwargs):\n')
            f.write('    return {'
                    '        "DI/0": random.random() > 0.7, \n'
                    '        "DI/1": random.random() > 0.7, \n'
                    '        "DI/2": random.random() > 0.7, \n'
                    '        "DI/3": random.random() > 0.7, \n'
                    '        "DI/4": random.random() > 0.7, \n'
                    '        "DI/5": random.random() > 0.7, \n'
                    '        "DI/6": random.random() > 0.7, \n'
                    '        "DI/7": random.random() > 0.7 \n'
                    '    }\n'
                    '# EOF\n')

        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO'))

        # Note that inputs and outputs refer to the context of the executing function, not the system as a whole.
        function_config = {
            'function': 'Random_IO',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test_DO1',
                'DO/2': 'test_DO2',
                'DO/3': 'test_DO3'
            },
            'outputs': {
                'DI/0': 'test_DI1',
                'DI/1': 'test_DI2',
                'DI/6': 'test_DI7',
            }
        }

        # Launch Function
        raik.control.management.launch_cyclic_process('Random_IO', function_config)

        # Test Function is running
        retry_count = 0
        while raik.communications.local.get_value('Random_IO_status') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function creation timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('Random_IO_status')['s'] != 'running' and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

        self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
        self.assertEqual(raik.communications.local.get_value('Random_IO_status')['s'], 'running')

        # Shutdown
        raik.control.management.close_all()
        self.assertEqual(raik.communications.local.get_value('Random_IO_status')['s'], 'terminated')

        # Restart
        raik.control.management.launch_cyclic_process('Random_IO', function_config)

        # Test Function is running
        retry_count = 0
        while raik.communications.local.get_value('Random_IO_status') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function creation timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('Random_IO_status')['s'] != 'running' and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

        self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
        self.assertEqual(raik.communications.local.get_value('Random_IO_status')['s'], 'running')

        # Kill tasks this time
        raik.control.management.kill_all()

        # Restart
        raik.control.management.launch_cyclic_process('Random_IO', function_config)

        # Test Function is running
        retry_count = 0
        while raik.communications.local.get_value('Random_IO_status') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function creation timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('Random_IO_status')['s'] != 'running' and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
        retry_count = retry_count == 6 and 6 or 0
        while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
            time.sleep(0.2)
            retry_count += 1
        self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

        self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
        self.assertEqual(raik.communications.local.get_value('Random_IO_status')['s'], 'running')

        # Close gracefully
        raik.control.management.close_all()


class TestStartControllerUsingLibrary(unittest.TestCase):
    """
    This loads functions from the default library as opposed to creating its own library
    """
    def setUp(self):
        import raik.enable_testing  # Not always run on first execution
        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_values()
        raik.communications.local.clear_all_events()
        raik.control.retry_time_medium = 1.0
        raik.control.retry_time_long = 1.0

    def tearDown(self):
        raik.control.management.kill_all()
        raik.communications.local.clear_all_values()
        raik.communications.local.clear_all_events()
        raik.communications.local.clear_all_config()

    def test_basic_system_random(self):
        """
            Tests some system and random I/O
        """
        # Control Configuration
        control_configuration = (
            'Name, Type, Address\n'
            "test_DI1, DI, Random/DI/0\n"
            "test_DI2, DI, Random/DI/1\n"
            "test_DI4, DI, Random/DI/3\n"
            "test_DI7, DI, Random/DI/6\n"
            'system.cpu.percent, AI, System/cpu/utilization\n'
            'system.mem.percent, AI, System/mem/utilization\n'
            'system.cpu.temp, AI, System/cpu/temp\n'
            'test_1, DI, Random/fixed/True\n'
            'test_2, DI, Random/fixed/False\n'
            'test_3, AI, Random/total/0\n'
        )

        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random'))
        self.assertIsNone(raik.communications.local.get_config('System'))

        raik.control.management.start_processes(control_configuration)

        # Test Function has been created
        # Note that inputs / outputs refer to the context of the function, not of the system a a whole.
        function_config = raik.communications.local.get_config('Random')
        self.assertIsInstance(function_config, dict)
        self.assertEqual(function_config.get('function'), 'Random')
        self.assertEqual(function_config['outputs']['DI/0'], 'test_DI1')
        self.assertEqual(function_config['outputs']['DI/1'], 'test_DI2')
        self.assertEqual(function_config['outputs']['DI/6'], 'test_DI7')

        # Test Function is running
        while raik.communications.local.get_value('Random_status') is None:
            time.sleep(0.01)
        while raik.communications.local.get_value('Random_status')['s'] != 'running':
            time.sleep(0.01)
        while raik.communications.local.get_value('test_DI1') is None:
            time.sleep(0.01)

        self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)

        # No shutdown so that calling tests can execute with running processes.

    def test_launch_cyclic_process_rerun(self):
        """
            Tests that a cyclic process can't be run twice and that the instance_id must be unique
        """
        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance'))

        # Note that inputs and outputs refer to the context of the executing function, not the system as a whole.
        function_config = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test_DO1',
                'DO/2': 'test_DO2',
                'DO/3': 'test_DO3'
            },
            'outputs': {
                'DI/0': 'test_DI1',
                'DI/1': 'test_DI2',
                'DI/6': 'test_DI7',
            }
        }

        # Launch Function
        original_process_pid = raik.control.management.launch_cyclic_process('Random_IO_Instance', function_config)

        # Test Function is running
        def test_running():
            nonlocal self
            retry_count = 0
            while raik.communications.local.get_value('Random_IO_Instance_status') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function creation timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('Random_IO_Instance_status')['s'] != 'running' \
                    and retry_count <= 5:
                logger.info(raik.communications.local.get_value('Random_IO_Instance_status')['s'])
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

            self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
            self.assertEqual(raik.communications.local.get_value('Random_IO_Instance_status')['s'], 'running')

        test_running()

        # Restart
        next_process_pid = raik.control.management.launch_cyclic_process('Random_IO_Instance', function_config)

        test_running()

        # Validate PID is the same as before
        self.assertEqual(original_process_pid, next_process_pid)

        # Close gracefully
        raik.control.management.close_all()

    def test_launch_cyclic_process_rerun_no_cache(self):
        """
            Tests that processes are not recreated when the local bootstrap cache is lost.
        """
        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance'))

        # Note that inputs and outputs refer to the context of the executing function, not the system as a whole.
        function_config = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test_DO1',
                'DO/2': 'test_DO2',
                'DO/3': 'test_DO3'
            },
            'outputs': {
                'DI/0': 'test_DI1',
                'DI/1': 'test_DI2',
                'DI/6': 'test_DI7',
            }
        }

        # Launch Function
        original_process = raik.control.management.launch_cyclic_process('Random_IO_Instance', function_config)

        # Test Function is running
        def test_running():
            nonlocal self
            retry_count = 0
            while raik.communications.local.get_value('Random_IO_Instance_status') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function creation timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('Random_IO_Instance_status')['s'] != 'running' \
                    and retry_count <= 5:
                logger.info(raik.communications.local.get_value('Random_IO_Instance_status')['s'])
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

            self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
            self.assertEqual(raik.communications.local.get_value('Random_IO_Instance_status')['s'], 'running')

        test_running()

        # Clear cache and restart
        raik.control.management._child_processes = {}
        next_process = raik.control.management.launch_cyclic_process('Random_IO_Instance', function_config)

        test_running()

        # Validate PID is the same as before
        self.assertEqual(original_process, next_process)

        # Close gracefully
        raik.control.management.close_all()

    def test_find_raik_control_processes(self):
        """
            Tests the ability to find processes with lost information.  This also tests that configuration data is
            restored by the appropriate processes.
        """
        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance1'))
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance2'))
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance3'))

        # Note that inputs and outputs refer to the context of the executing function, not the system as a whole.
        function_config1 = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test1_DO1',
                'DO/2': 'test1_DO2',
                'DO/3': 'test1_DO3'
            },
            'outputs': {
                'DI/0': 'test1_DI1',
                'DI/1': 'test1_DI2',
                'DI/6': 'test1_DI7',
            }
        }
        function_config2 = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test2_DO1',
                'DO/2': 'test2_DO2',
                'DO/3': 'test2_DO3'
            },
            'outputs': {
                'DI/0': 'test2_DI1',
                'DI/1': 'test2_DI2',
                'DI/6': 'test2_DI7',
            }
        }
        function_config3 = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test3_DO1',
                'DO/2': 'test3_DO2',
                'DO/3': 'test3_DO3'
            },
            'outputs': {
                'DI/0': 'test3_DI1',
                'DI/1': 'test3_DI2',
                'DI/6': 'test3_DI7',
            }
        }
        # Launch Function
        pid1 = raik.control.management.launch_cyclic_process('Random_IO_Instance1', function_config1)
        pid2 = raik.control.management.launch_cyclic_process('Random_IO_Instance2', function_config2)
        pid3 = raik.control.management.launch_cyclic_process('Random_IO_Instance3', function_config3)

        # Clear cache and restart
        raik.control.management._child_processes = {}
        raik.communications.local.clear_all_values()
        raik.communications.local.clear_all_config()

        original_set = {pid1, pid2, pid3}
        found_set = raik.control.management.find_raik_control_processes()

        # Validate PID is the same as before
        self.assertEqual(original_set, found_set)

        # Drop timers in configuration
        function_config1_update = raik.communications.local.get_config('Random_IO_Instance1')
        function_config2_update = raik.communications.local.get_config('Random_IO_Instance2')
        function_config3_update = raik.communications.local.get_config('Random_IO_Instance3')

        self.assertIsNotNone(function_config1_update)
        self.assertIsNotNone(function_config2_update)
        self.assertIsNotNone(function_config3_update)
        # Check the Test time is newer
        self.assertGreater(function_config1_update['_t'], function_config1['_t'])
        self.assertGreater(function_config2_update['_t'], function_config2['_t'])
        self.assertGreater(function_config3_update['_t'], function_config3['_t'])
        # Remove dynamic parts for next comparison
        del function_config1_update['_t']
        del function_config2_update['_t']
        del function_config3_update['_t']
        del function_config1['_t']
        del function_config2['_t']
        del function_config3['_t']

        # Validate configuration exists
        self.assertEqual(function_config1, function_config1_update)
        self.assertEqual(function_config2, function_config2_update)
        self.assertEqual(function_config3, function_config3_update)

        # Close gracefully
        raik.control.management.close_all()
        found_set = raik.control.management.find_raik_control_processes()
        self.assertEqual(found_set, set())

    def test_launch_cyclic_process_stale(self):
        """
            Tests stale detection of a frozen or failed process.  This uses the config to check that the process
            is running and that it is the right process (not another process using the same PID).  It also checks
            if the process is stale (based on new meta-data not yet formally documented).
            Run a process and kill the function instance
        """
        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random_IO_Instance'))

        # Note that inputs and outputs refer to the context of the executing function, not the system as a whole.
        function_config = {
            'function': 'Random',
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            'inputs': {
                'DO/1': 'test_DO1',
                'DO/2': 'test_DO2',
                'DO/3': 'test_DO3'
            },
            'outputs': {
                'DI/0': 'test_DI1',
                'DI/1': 'test_DI2',
                'DI/6': 'test_DI7',
            }
        }

        # Launch Function
        original_process_pid = raik.control.management.launch_cyclic_process('Random_IO_Instance', function_config)

        # Test Function is running
        def test_running():
            nonlocal self
            retry_count = 0
            while raik.communications.local.get_value('Random_IO_Instance_status') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function creation timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('Random_IO_Instance_status')['s'] != 'running' \
                    and retry_count <= 5:
                logger.info(raik.communications.local.get_value('Random_IO_Instance_status')['s'])
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function Running status check timed out')
            retry_count = retry_count == 6 and 6 or 0
            while raik.communications.local.get_value('test_DI1') is None and retry_count <= 5:
                time.sleep(0.2)
                retry_count += 1
            self.assertLessEqual(retry_count, 5, 'Function I/O Test timed out')

            self.assertIsInstance(raik.communications.local.get_value('test_DI1'), bool)
            self.assertEqual(raik.communications.local.get_value('Random_IO_Instance_status')['s'], 'running')

        test_running()

        self.assertTrue(raik.control.management.test_process_is_running('Random_IO_Instance'))

        p = psutil.Process(original_process_pid)
        # TODO:  This works up to this point, but then exit is not controlled - it may be caused by pycharm debug
        # mode - Confirmed, debug mode doesn't like to be forked.  Maybe consider an alternative to forking where
        # the process is launched by command line instead as a new process
        time.sleep(5)
        logger.info('Terminate')
        p.terminate()
        while p.status() in (psutil.STATUS_RUNNING, psutil.STATUS_SLEEPING, psutil.STATUS_DISK_SLEEP,
                             psutil.STATUS_WAKING, psutil.STATUS_IDLE, psutil.STATUS_WAITING):
            time.sleep(0.2)  # Might be needed to
        logger.info('status updated')
        time.sleep(5)
        self.assertFalse(raik.control.management.test_process_is_running('Random_IO_Instance'))

        # Close gracefully
        # TODO:  This is not exiting in a controlled manner
        logger.info('Clsoe gracefully')
        raik.control.management.close_all()  # needs kill or verification that close works (timeout)
        time.sleep(5)
        logger.info('done')

    def test_get_configuration(self):
        """
        This test checks the get_configuration functions that retrieve the current online configuration of the
        controller ensuring it is up to date.  This ensures that configuration is retrieved live.
        :return:
        """
        # Control Configuration
        control_configuration = (
            '# Module Configuration\n'
            'Name, Type, Address\n'
            'test_DI1, DI, Random/DI/0\n'
            'test_DI2, DI, Random/DI/1\n'
            'test_DI4, DI, Random/DI/3\n'
            'test_DI7, DI, Random/DI/6\n'
            'system.cpu.percent, AI, System/cpu/utilization\n'
            'system.mem.percent, AI, System/mem/utilization\n'
            'system.cpu.temp, AI, System/cpu/temp\n'
            'test_1, DI, Random/fixed/True\n'
            'test_2, DI, Random/fixed/False\n'
            'test_3, AI, Random/total/0\n'
            ''
            '# Soft I/O\n'
            '# Name, Type, Default\n'
            'result_1, VF, 0.0\n'
            ''
            '# Name, Type, Schedule, Schedule_parameters, Mapping\n'
            'add_test, add_two, cyclic, {"period": 0.1}, a=system.cpu.percent, b=system.cpu.temp, '
            'x=result_1'
        )

        # Test Function doesn't exist
        self.assertIsNone(raik.communications.local.get_config('Random'))
        self.assertIsNone(raik.communications.local.get_config('System'))

        self.assertIsNone(raik.control.management.get_online_configuration())

        config_data = raik.control.management.start_processes(control_configuration)
        online_config = raik.control.management.get_online_configuration()

        self.assertEqual(config_data['io_configs'], online_config['io_configs'])
        self.assertEqual(config_data['function_configs'], online_config['function_configs'])
        self.assertEqual(config_data['module_configs'], online_config['module_configs'])
        self.assertEqual(config_data, online_config)

        # Close gracefully
        raik.control.management.close_all()

        self.assertIsNone(raik.control.management.get_online_configuration())
