import logging.config

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(process)d.%(thread)d %(levelname)s %(name)s:%(lineno)d %(message)s"
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
    },
    # 'loggers': {
    #     'daphne': {
    #         'handlers': ['console'],
    #         # 'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
    #         'level': 'WARNING',
    #     },
    # },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
}


def init_logging():
    logging.config.dictConfig(LOGGING)
