import logging
import random
random.seed()
logger = logging.getLogger('RAIK_IO:Random')

bool_values = (True, False)
digital_toggles = [
    random.choice(bool_values),
    random.choice(bool_values),
    random.choice(bool_values),
    random.choice(bool_values)
]
tot_0 = 0.0


# Scanning
def scan_module(**outputs):
    """
    Pseudo Random Inputs and Outputs and Simulation Inputs and Outputs

    :param outputs: This module does not accept outputs.
    :return:  See inputs description in docs.

    Inputs (return value)
    ------
    DI/0 to DI/7: 8 Random Digital Inputs
    AI/0 to AI/7: 8 Random floating point values currently limited in 0.00 to 1.00
    DT/0 to DT/3: 4 Toggling Digital values with random starting point.  Toggles each scan
    total/0:  Totalises for ever
    Fixed/True:  Always True
    Fixed/False: Always False

    """
    global digital_toggles, tot_0
    # Update internal states
    for i, v in enumerate(digital_toggles):
        digital_toggles[i] = not v
    tot_0 += 0.01

    # Return values
    return {
        'DI/0': random.choice(bool_values),
        'DI/1': random.choice(bool_values),
        'DI/2': random.choice(bool_values),
        'DI/3': random.choice(bool_values),
        'DI/4': random.choice(bool_values),
        'DI/5': random.choice(bool_values),
        'DI/6': random.choice(bool_values),
        'DI/7': random.choice(bool_values),
        'AI/0': random.random(),
        'AI/1': random.random(),
        'AI/2': random.random(),
        'AI/3': random.random(),
        'AI/4': random.random(),
        'AI/5': random.random(),
        'AI/6': random.random(),
        'AI/7': random.random(),
        'DT/0': digital_toggles[0],
        'DT/1': digital_toggles[1],
        'DT/2': digital_toggles[2],
        'DT/3': digital_toggles[3],
        'total/0': tot_0,
        'fixed/True': True,
        'fixed/False': False,
    }
# EOF
