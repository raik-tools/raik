"""
This file contains process management routines and is used for starting processes.

This module provides functions that are used to load configuration and generate the appropriate
processes.  The functions are intended to be able to be re-run as process IDs (instance_id) are globally unique.

There have been a number of renamed functions from this module:
* This module used to be called bootstrap, it is now management.  It is not intended to import this directly
  but instead import the more public functions through raik.control
* run_once is now called start_processes

"""
import json
import raik.communications.local
import multiprocessing
import time
from raik.control.scan import continuous_scan
import logging
import psutil
import signal
import re
# import os
# import traceback
logger = logging.getLogger('raik.control.management')

_child_processes = {}
_online_check = 0
_online_check_age = 3.0
_last_online_data = {}
__names_blacklist = ('management',)

# To be compatible with HTML4 to make front ends easier:
# https://www.w3.org/TR/html4/types.html#type-id
allowed_names_re = re.compile(r'^[A-Za-z0-9_:.-]+$')

def launch_cyclic_process(instance_id, function_config):
    """
    This function launches the given function (with function configuration) in a separate process.  It ensures that
    it won't launch the function if it is already running and will perform an integrity check in case the continuous
    scan process has aborted or stalled.

    It is expected that each running function will have

    :param instance_id:  A unique identifier for the function that will be executed by the process cyclically
    :param function_config:
    :return: The Process class for the launched process.
    """
    global _child_processes
    logger.debug('Checking launch cyclic process %s' % instance_id)
    find_raik_control_processes()  # This is needed on launch to ensure meta data is up to date.
    if instance_id in _child_processes:
        logger.debug('Checking launch cyclic process %s - already running' % instance_id)
        return _child_processes[instance_id]
    logger.debug('Checking launch cyclic process %s - Creating Process' % instance_id)

    # Apply time stamp to config to ensure process can check it is getting latest (not debris) config
    function_config['_t'] = time.time()

    # Set config
    raik.communications.local.set_config(instance_id, function_config)

    # Launch process
    p = multiprocessing.Process(target=continuous_scan, args=(instance_id,), name=instance_id)
    p.start()

    # Wait for some kind of status change before proceeding. A race condition might be occurring with redis and
    # multiple processes.  Unit tests for this might be unreliable.
    retry_count = 0
    status = raik.communications.local.get_value(instance_id + '_status')
    while not status and retry_count <= 3:
        time.sleep(0.5)
        status = raik.communications.local.get_value(instance_id + '_status')
        retry_count += 1
    assert status
    assert (time.time() - status['_t']) < 10

    logger.info('Started process for %s on PID %i', instance_id, p.pid)
    # Save results
    _child_processes[instance_id] = p.pid
    return p.pid


def find_raik_control_processes():
    """
    Returns a list of running RAIK control processes.
    This is based on continuous_scan creating one and only one open file in tmp.  with RAIK_<pid> as the name.
    The content of the file includes the function name.
    :return:
    """
    logger.debug('Searching for process data')
    process_list = {}
    for p in psutil.process_iter():
        try:
            open_files = p.open_files()
        except psutil.AccessDenied:
            continue
        tmp_name = 'RAIK_%i' % p.pid
        tmp_name_len = -1 * len(tmp_name)
        if len(open_files) >= 1:
            for f in open_files:
                if f.path[tmp_name_len:] == tmp_name:
                    logger.debug('Process found: %i', p.pid)
                    with open(f.path, 'r') as a:
                        process_list[p.pid] = a.read()
                    break
    # Check and config is up to date
    verify_configs = []
    for pid, instance_id in process_list.items():
        if instance_id not in _child_processes:
            _child_processes[instance_id] = pid
        function_config = raik.communications.local.get_config(instance_id)
        if not function_config:
            # Send signal to process to refresh / update its function config
            p = psutil.Process(pid)
            p.send_signal(signal.SIGUSR1)
            verify_configs.append(instance_id)
    # Wait for configs to recover if they don't exist
    for instance_id in verify_configs:
        retry_count = 0
        while raik.communications.local.get_config(instance_id) is None and retry_count <= 5:
            time.sleep(0.5)
            retry_count += 1
        # TODO:  This might be a hard line to test.  I might need to spawn a dummy process in a test that doesn't
        # update config but mimics all other necessary behaviour
        if retry_count > 5:
            logger.debug('Raising Timeout error')
            raise TimeoutError('Process %s failed to restore configuration in sufficient time' % instance_id)
    logger.debug('Processes found: %s', process_list)
    return set(process_list.keys())


def test_process_is_running(instance_id):
    """
    Given a instance_id this tests is the process is still running.
    :param instance_id: The name of the function
    :return: True if running, False otherwise
    """
    # Check ig PID Exists
    pid = _child_processes.get(instance_id)
    if not pid:
        find_raik_control_processes()
    pid = _child_processes.get(instance_id)
    if not pid:
        return False
    # Check process state
    p = psutil.Process(pid)
    if p.status() not in (psutil.STATUS_RUNNING, psutil.STATUS_SLEEPING, psutil.STATUS_DISK_SLEEP,
                          psutil.STATUS_WAKING, psutil.STATUS_IDLE, psutil.STATUS_WAITING):
        return False
    logger.debug(p.status())
    # Check if process is active
    status_channel = instance_id + '_status'
    instance_status = raik.communications.local.get_value(status_channel)
    if instance_status['s'] == 'running' and instance_status['_t'] > (time.time() - 5):
        return True
    # Get Process signal to update config
    p.send_signal(signal.SIGUSR1)
    process_config = raik.communications.local.get_config(instance_id)
    if process_config['_t'] > (time.time()-0.5):
        return True
    return False


def start_processes(control_configuration):
    """
    This method refreshes all RAIK processes and will start them if they haven't started.  It
    performs the following steps:
        * Read control configuration
        * Look for existing sub processes
            * If they exist, compare configurations and restart if need be
            * If they don't exist, start them
        * Starts Input processes and checks health
        * Starts output processes and checks health
        * Starts control function processes

    This function returns once all the processes have started, but this doesn't mean they have performed a complete
    execution each.

    :param control_configuration: Multi-line string containing configuration CSV data (See #234).
    :return: A dictionary represeting the processed configuration.
    """
    global _child_processes

    virtual_io = {}
    real_io = {}
    function_configs = {}
    io_configs = {}
    names = set()
    load_time = time.time()
    
    def validate_name(_n):
        nonlocal names
        if not allowed_names_re.match(_n):
            raise ValueError('Name "%s" is now allowed, restrict characters to A-Z, a-z, 0-9, hyphen (-) '
                             'underscore (_), colon (:) and period (.).' % _n)
        if _n in names:
            raise ValueError('Name "%s" has already been defined' % _n)
        names.add(_n)

    # Convert control configuration into a set of tasks
    for row in control_configuration.split('\n'):
        l = row.strip()
        if not l:
            continue
        if l[0] == '#':
            # Comment
            continue
        if l.startswith('Name, Type'):
            # Heading
            continue
        l = l.split(', ')
        if len(l) == 3:
            # This is an I/O row
            name, io_type, address = l
            validate_name(name)
            if io_type[0] == 'V':
                default_val = address
                if default_val == 'None':
                    virtual_io[name] = None
                elif io_type[1] == 'F':
                    virtual_io[name] = float(default_val)
                elif io_type[1] == 'I':
                    virtual_io[name] = int(default_val)
                elif io_type[1] == 'B':
                    virtual_io[name] = default_val.lower() == 'true'
                else:
                    # Not sure what to do here, keep going or raise an exception?
                    # At this point, if the configuration file is wrong, we shouldn't start.
                    raise ValueError('Bad configuration line for virtual I/O: "%s"' % row.strip())
                io_configs[name] = {
                    'type': io_type,
                    'default': default_val,
                }
            elif io_type[0] in ['D', 'A']:
                real_io[name] = address
                io_configs[name] = {
                    'type': io_type,
                    'address': address,
                }
            else:
                raise ValueError('Bad configuration line, unknown type: "%s"' % row.strip())
        elif len(l) >= 5:
            name, io_type, schedule, schedule_param = l[:4]
            validate_name(name)
            mapping = dict([a.split('=') for a in l[4:]])
            schedule_param = json.loads(schedule_param)
            schedule_param.update({'type': schedule})
            function_configs[name] = {
                'function': io_type,
                'timing': schedule_param,
                'inputs': mapping,
                'outputs': mapping,
                '__type': 'logic'
            }
        else:
            raise ValueError('Bad configuration line, unknown columns: "%s"' % row.strip())
        if name in __names_blacklist:
            raise ValueError('Name is using an reserved word, change configuration name: %s' % name)
        if name[0] == '_':
            raise ValueError('Names can not start with an underscore, change configuration name: %s' % name)

    # Real/virtual I/O defined and function configs are now defined.
    io_configs['_t'] = load_time
    raik.communications.local.set_config('_io_config', io_configs)

    # Physical Inputs and outputs are scanned on a per module basis
    io_modules = {}
    for name in real_io:
        module, area, channel = real_io[name].split('/')
        if module not in io_modules:
            io_modules[module] = [('/'.join([area, channel]), name)]
        else:
            io_modules[module].append(('/'.join([area, channel]), name))
    # Create I/O processes
    module_configs = {}
    for module in io_modules:
        io_map = {}
        for io in io_modules[module]:
            module_ch, name = io
            io_map[module_ch] = name

        function_config = {
            'function': module,
            'timing': {
                'type': 'cyclic',
                'period': 0.3
            },
            '__type': 'io',
            'inputs': io_map,
            'outputs': io_map
        }
        module_configs[module] = function_config
        launch_cyclic_process(module, function_config)

    # Create Virtual I/O
    for k, v in virtual_io.items():
        # Not yet sure what to do with this one.  It may lead into issues later when updating running config.
        # Only set to default if we haven't spawned children before:
        # if not __child_processes:
        raik.communications.local.set_value(k, v)

    # Launch processes
    for instance_id, function_config in function_configs.items():
        launch_cyclic_process(instance_id, function_config)

    # Check process statuses have cleared - doesn't guarantee processes have cycled at least once
    for k in _child_processes:
        while raik.communications.local.get_value(k + '_status') is None:
            time.sleep(0.1)

    return {
        'io_configs': io_configs,
        'function_configs': function_configs,
        'module_configs': module_configs
    }


def get_online_configuration():
    """
    Polls all running processes and returns a configuration structure based on currently
    executing functions.
    :return:
    """
    global _child_processes, _online_check, _last_online_data, _online_check_age
    current_time = time.time()
    if _last_online_data and (current_time - _online_check) < _online_check_age:
        # If called in close succession, assume old data
        _online_check = current_time
        logger.debug('Returning Last Data: %s', _last_online_data)
        return _last_online_data
    else:
        _last_online_data = {}
        _online_check = current_time

    # Clear processes to ensure we are not getting stagnant data
    _child_processes = {}

    # Ensure the process list is up to date
    find_raik_control_processes()

    # Return none if there are no running processes
    if len(_child_processes) == 0:
        logger.debug('No child processes found')
        return None

    # Process the configuration of each running function
    io_configs = raik.communications.local.get_config('_io_config')
    function_configs = {}
    module_configs = {}

    for instance_id in _child_processes.keys():
        function_config = raik.communications.local.get_config(instance_id)
        # config_type = function_config.pop('__type')
        config_type = function_config['__type']
        if config_type == 'io':
            module = function_config['function']
            # Update configuration
            module_configs[module] = function_config
        elif config_type == 'logic':
            # Update configuration
            function_configs[instance_id] = function_config
    _last_online_data = {
        'io_configs': io_configs,
        'function_configs': function_configs,
        'module_configs': module_configs
    }
    return _last_online_data


def close_all(timeout=15):
    """
    This method closes all processes launched by this process
    """
    global _child_processes
    logger.info('Closing all processes.')
    start_time = time.time()
    del_keys = []
    # Send terminate signal
    for k, p in _child_processes.items():
        psutil.Process(p).terminate()
        del_keys.append(k)

    # Wait for termination
    logger.info('Waiting for processes to enter terminated state')
    for k, p in _child_processes.items():
        while raik.communications.local.get_value(k + '_status')['s'] != 'terminated':
            logger.info('%s Status: %s', k, raik.communications.local.get_value(k + '_status')['s'])
            if time.time() > start_time + timeout:
                # Check status for each process
                running_count = 0
                for old_process in _child_processes.values():
                    running_count += (psutil.Process(old_process).is_running() and 1 or 0)
                if running_count > 0:
                    # Todo:  List processes still running?
                    # TODO:  Consider moving to PyTest and restructure slowly (starting with sequence runner) so
                    # that code is easily executed independently
                    raise RuntimeError('Failed to close running tasks.  %i processes still active' % running_count)
                else:
                    raise RuntimeError('RAIK Processes failed to terminate correctly')
            else:
                time.sleep(1)

    for k in del_keys:
        del(_child_processes[k])


def kill_all():
    """
    This is a less graceful method to close all and doesn't wait for any terminate handler in the child.
    :return:
    """
    logger.info('Killing all processes - refresh child process list in order to terminate.')
    find_raik_control_processes()
    # logger.debug('\n\t\t'.join(traceback.format_stack()[-3:]))
    logger.info('Killing all processes.')
    global _child_processes
    del_keys = []
    for k, p in _child_processes.items():
        p = psutil.Process(p)
        if p.is_running():
            logger.warning('Killing Processes:  Spawned process %i is still alive and being terminated.' % p.pid)
            p.kill()
            p.wait(timeout=5)
        del_keys.append(k)

    if del_keys:
        raik.communications.local.delete_config(*del_keys)
        for k in del_keys:
            raik.communications.local.delete_values(k + '_status')
            del (_child_processes[k])
