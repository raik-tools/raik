"""
This largely exists to prevent cyclic imports
"""

import collections

PROGRAM_SCOPE = collections.namedtuple(
    'PROGRAM_SCOPE',
    ('private', 'user', 'host')
)(private='private', user='user', host='host')
