=========
Robomower
=========

:Enhancement: 801
:Keyword: E802-robomower
:Type: Project
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
    :depth: 2
    :local:

Overview
========

I've been a bit stagnant and lacking motivation to develop RAIK.  I'm passionate about a number of the potential
outcomes, but at the moment the immediate projects and use cases are getting a bit tedious and boring (especially
developing a HMI).  I'm now focusing efforts onto something that is abit more fun and make me feel more like a
mechatronics Engineer.  In this case, I want ot automate my lawn mower.

This is an application example and personal project.

Background
----------


Alternatives
------------

By one for a few thousand dollars, but where's the fun plus I'm stuck with a Robotic mower when at times I may want
to do it the ol' fashion way.

Status Comments
===============

This document is still in draft.


References
==========


Research
--------

Without doing a detail design, there are some obvious technologies that will be very likely required.  These are
researched here and includes rough costing (in AUD).

TODO:  I need to put together an actual design so I can order the right parts (before I get carried away).  The design
will be based on a rough ideal.

Batteries
^^^^^^^^^

This thing will need power, and since it will be mowing often I want to ensure longevity of the batteries used.

Based on the current state of what is available, I'm considering:

* LiFePO4 Batteries - These seem to be easiest found in replacement car batteries or some RC application.  They have
    a really high cycle count, but I'm struggling to find information on improving this.
* Li-Ion - I'm not sure which anode type this specifically refers to, I'm guessing cobalt.


Places where I seem to be able to get Batteries in Australia are:
* https://batterydoctor.com.au/store/7-2v-6-7ah-high-capacity-liion-battery-with-ce180-2-1mm-dc-jack.html
* https://www.master-instruments.com.au/products/63959/UR18650RX.html

Some resources for battery longevity:
* http://batteryuniversity.com/learn/article/how_to_prolong_lithium_based_batteries

    * I'm unclear on how the tests are run.  When they look at DoD are they charging to 100% each time even in the
      10% DoD case?
    * Is the cycle count based on the charge cycle independent of how low it is or based on 100% equivalent?  So a
      10% DoD might get 15,000 cycles but that's equivalent to 1500 100% cycles.
    * The comment above is reflected in the comments on the page.  Search for Comments with DoD in them.
    * In short, others have calculated "run time" for each level of discharge to calculate an optimum.

Optimal battery charge point is still unclear, but what is clear is that I'll want / need some means of monitoring
battery usage as well as controlling when charge turns on and off.

Li-Ion Charging
^^^^^^^^^^^^^^^

A quick summary from Battery University:

* TODO

Links:

* Comprehensive review of chargers:  http://www.lygte-info.dk/info/indexBatteriesAndChargers%20UK.html
* Ripple effects:  https://www.sciencedirect.com/science/article/pii/S030626191630808X

Some products of interest

* An Arduino project for programmable charging:
    http://henrysbench.capnfatz.com/henrys-bench/arduino-projects-tips-and-more/arduino-18650-battery-charger-project-1/
* A mini programmable power supply (MingHe B3603)

    * Review: https://www.eevblog.com/forum/reviews/b3603-dcdc-buck-converter-mini-review-and-how-the-set-key-could-be-fatal/
    * Available on eBay for around $12 to $30.  Not sure if there is a difference in quality.  Will need to order
      samples to see.
    * Pay $30 for local, will see how overseas order goes.

* A USB power meter (get a couple)

* Current Sensor

    * ACS712 Current Sensor for $8 locally or $3 from China
    * INA219 Current and Voltage Sensor, $15 locally or $3 from China (different board)

        * https://www.littlebirdelectronics.com.au/ina219-high-side-dc-current-sensor-breakout-26v-3
        * https://openhomeautomation.net/power-monitoring-arduino-ina219

UPS
^^^

This is not just about charging, but we want the unit to behave like a UPS.

* https://diyodemag.com/projects/power_outage_detector_arduino_ups

    * Charge Module: https://www.jaycar.com.au/arduino-compatible-lithium-battery-usb-charger-module/p/XC4502
    * DC DC Module: https://www.jaycar.com.au/arduino-compatible-5v-dc-to-dc-converter-module/p/XC4512

* http://www.robotroom.com/Weather-Station/Schematic-of-solar-panel-charger-circuit.gif
* https://www.modmypi.com/raspberry-pi/power-1051/ups-boards-1051/ups-pico
* Solar panel projcts might give some hints

    * https://www.voltaicsystems.com/blog/powering-a-raspberry-pi-from-solar-power/
    * https://www.digikey.com.au/en/articles/techzone/2016/jul/how-to-use-solar-cells-to-power-a-raspberry-pi-3-single-board-computer
    * https://www.kickstarter.com/projects/sunair/suncontrol-diy-solar-power-for-the-raspberry-pi-ar

Switching between battery and main:

* https://www.raspberrypi.org/forums/viewtopic.php?t=26993
* http://www.barryhubbard.com/raspberry-pi/5v-uninterruptable-power-supply/
* https://electronics.stackexchange.com/questions/151341/how-to-switch-between-two-dc-power-sources-powering-a-motor-on-an-electric-vehic


Power Monitoring
^^^^^^^^^^^^^^^^

* $15 AC Current sensor: https://core-electronics.com.au/non-invasive-current-sensor-30a.html

* Arduino Energy Monitor Project: https://openenergymonitor.org/forum-archive/node/58.html


Solar
^^^^^

https://www.sparkfun.com/news/1131

https://www.sparkfun.com/products/746

Solar is very variable.  Consider using super capacitors to build up charge and then use some
kind of switching circuit that will then use that charge to charge the destination.  

For $5 you can get a 10F / 2.5V super capcitor.  

Remember voltage will go down during discharge (I'll need to model).  

1 Coulomb is 1 ampere per second.

1 Farad is is one coloumb of charge at one volt.

http://batteryuniversity.com/learn/article/whats_the_role_of_the_supercapacitor

q (Coulombs) = C (Farads) x V (Volts)

We want to calculate how much current we can get for a boost/buck converter when the voltage.  Lets assume 
we're going to 15 V (6 x super caps in series each rated for 10F).  

Total cap = 1 / (sum of 1/C).  In this case, 1 / (6/10) = 1.7F at 15V.

Caps starts at V1 and we'll cut them out at V2.  We want to know how much energy has been delivered.

Note we're assuming that we have a 4Ah 12VDC Li-ion battery to charge / compare with.  

P = VI in Watts or Joulles / second
P = E/t

This battery will deliver 48 W in one hour or 48W * 3600s = 172kJ

https://brilliant.org/wiki/energy-stored-capacitor/

The amount of Energy in a capacitor is U = 1/2 x qV

In this case, 0.5 x 1.7 x 15^2 = 192J

If the solar panels at peak were 6V / 615mA (mismatched voltage, just looking at rough cycling)

At peak this would deliver 3.5W of power.  This would charge these caps in 54 seconds.  

Assume 50% capacity for caps (~100J) and 80% efficiency of energy transfer.  We would be transferring 80J
to the batteries every minute.  This would take 35h at peak performance to fill the batteries.  If we moved to 
2 x 9w panels (around $300 worth and 18W), This thing will be turning on and off a lot.

We're better off having a second battery that trickle charges and then cut over to the other battery.

https://community.agl.com.au/t5/Renewables-and-Energy-Technology/Lifespan-of-Lithium-Ion-Solar-Batteries/td-p/2000


Mower
^^^^^

My current mower is a Ryobi 36V electric lawn mower.  The battery pack can be seen disassembled here:

* https://www.youtube.com/watch?v=HpTwZBBRV3c




Features
========

This specifically refers to features required in RAIK to support this project.

* TODO

Description
===========



Components
----------



Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.
