"""
Useful tools for correctly managing python datetime objects
"""
import datetime


def naive_to_utc(naive_datetime: datetime) -> datetime:
    """
    A simple function that takes a naive time and sets it as UTC with
    the same time (no time conversion)
    """
    return naive_datetime.replace(tzinfo=datetime.timezone.utc)


def strptime_as_utc(date_string: str, date_format: str) -> datetime:
    d = datetime.datetime.strptime(date_string, date_format)
    return naive_to_utc(d)
