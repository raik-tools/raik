===============
Self Sufficient
===============

:Enhancement: 2
:Keyword: E002-self-sufficient
:Type: Theme
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:


Overview
========

This follows on from a batteries included theme found in the Python language and combines it with common technologies
within the control system industry.

Background
----------

This documentation was originally captured in Redmine #367

Status Comments
===============

This document is still in draft.


References
==========

TODO - External references

Features
========



Pro Features
============


Definition
==========

The system is able to support itself with minimal dependencies or easily managed / deployed dependencies.  Automation
needs to start in-house as part of a product goal for it to be not just an automation platform but a partly
autonomous one as well.

Criteria
========

* Software Development and Deployment needs to be automated
* The infrastructure the system relies on or could be advantageous to the product needs to be integrated, monitored
  and automated.


Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.