"""

This file performs a test to compare different control implementation options and performance.
It's an overly simplistic example, the idea is that all code can exist in a singular function (like in many modern
control systems) which can get unwieldy (especially with different code paths) or use Python Classes as a utility
for organising contextual code.  Note, however, that each use case is not a direct port as the class version
would be handling it's own state between executions.

The purpose of the code will be the same.  It will dictionaries for I/O (likely the real implementation, except
that we might use some syntax parsing to make it simpler for the user to develop without having to worry about
dictionary syntax).

The function version of the code is the model.

Noted differences:
* Class overhead is about double that of function.  But we're still talking microseconds on a slow laptop.

Functions
---------

Functions represent simple executables.  They are a simple input and output method and have no state.


Function Blocks
---------------

Is this terminology appropriate?  Perhaps we can call them classes?

A function block in Siemens land is a function with state.  It has inputs, outputs and state variables.

We could do it as classes where we have a code builder that
allows restricted access to creating these classes.

Alternatively, we do a mockup of a class using as strict function.  This seems easier / simpler (and a class is just
syntactic sugar in Python anyway, kind of).

Function block examples (IEC 61131-3 and as seen in DCS):

* Performs a single function, can store multiple state variables
* PID, Manual loader, valve, motor control.  All are fairly light weight and clear.  Execution is continuous and
  state is managed through execution with a defined sequence (not event based).
* All state is tested and resolved in a single scan

Class / OOP Examples

* Pump - What parrallel code paths could there be?

    * Execute primary control
    * Performance monitoring

* Machine

    * Sequences?

There is a good use case to differentiate but for now the focus will be on achieving it with functions (until
we can work out how classes can be abused).

The big functions vs function blocks debate and implementation details in the headings below.

PseudoCode for Functions
^^^^^^^^^^^^^^^^^^^^^^^^

Pro:
* Simple
* Closer to existing standards
Con:
* In practice, ends up with very very large function blocks and less split up of operations.  These can easily
    be 500+ lines of code and can be difficult to maintain in logic.


A PID example, focussing on the algorithm and basic processing (like tracking etc..)

.. code-block:: python

    def pid(input, output, set_points, state):

        process_input_limits()
        process_set_points()
        get_last_state_for_algorithm()
        do_algorithm()
        return_outputs()

A valve example

.. code-block:: python

    def valve_main(input, output, set_points, state):

        check_input_states()
        check_alarms()
        check_commands()
        calculate_usage_metrics_and_save_state()
        return_outputs()

    def valve_reliability_monitoring(input, output_ set_points, state):

        check_last_state()
        calculate_changes()
        store_results()
        return_none()

    def valve(input, output, set_points, state):

        valve_main(input, output, set_points, state)
        valve_reliability_monitoring(input, output, set_points, state)

A machine example with sequence

.. code-block:: python

    def machine_sequence_start(input, output, set_points, state):

        check_inputs()
        if state.do_sequence_a:
            init_sequence()
        else:
            exit()

    def machine_sequence_stop(input, output, set_points, state):

        check_inputs()
        if state.do_sequence_a:
            init_sequence()
        else:
            exit()

    def machine(input, output, set_points, state):

        machine_sequence_start()
        machine_sequence_stop()


A NAS example

.. code-block:: python

    def disk_eject_command():

        check_status()
        eject_disk()
        test_result()

    def disk():

        check_status()
        collect_data()


Performance of different approaches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See this code for details.


Thoughts on different approaches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An interesting question, based on my experience, how would I want to configure a controller using tools from
either software development or DCS config?

* Consider the following areas:

    * Input processing
    * Split up functions
    * Performance
    * Referencing state and history
    * Interlock processing

* Data model

    * One option is user controls most of the function
    * use front end to split up function into parts

Advantages of functions:
* Simple
* More reliably implemented
* Easier to control code

Advantages of classes:
* Better code reorganisation
* More flexibility
* Private functions and variables


"""
import time
import timeit
with_redis_sim = False
redis_load_time = 5/(10**6)
test_iterations = 1000


# The function version first

def function_valve_control(inputs: dict, last_outputs: dict, operator: dict, state: dict):
    opened = inputs['opened']
    closed = inputs['closed']
    last_open = last_outputs['open']
    cmd_open = operator['cmd_open']
    cmd_close = operator['cmd_close']
    toggle = state['toggle']
    error = state['error']

    if opened and closed:
        error = True
        this_open = False
    else:
        if cmd_open and not last_open:
            this_open = True
        elif cmd_close and last_open:
            this_open = False
        else:
            this_open = last_open

    toggle = not toggle

    state['toggle'] = toggle
    state['error'] = error

    return {
        'open': this_open
    }


function_data = {
    'inputs': {
        'opened': True,
        'closed': False
    },
    'outputs': {
        'open': False
    },
    'operator': {
        'cmd_open': False,
        'cmd_close': True
    },
    'state': {
        'toggle': True,
        'error': False
    }
}


def sim_get_data(key):
    if with_redis_sim:
        time.sleep(redis_load_time)
    return function_data[key]


def sim_set_data(key, value):
    if with_redis_sim:
        time.sleep(redis_load_time)


def do_function():

    x = function_valve_control(
        inputs=sim_get_data('inputs'),
        last_outputs=sim_get_data('outputs'),
        operator=sim_get_data('operator'),
        state=sim_get_data('state'),
    )

    sim_set_data('next_outputs', x)


# Now a class based version

class ClassValve:

    def __init__(self, state):
        self.toggle = state['toggle']
        self.error = state['error']
        self.last_output = False  # Always start at 0.  Likely this will come from state so it can be made persistent
        self.cmd_open = False
        self.cmd_close = True

    def get_state(self):
        return {
            'toggle': self.toggle,
            'error': self.error
        }

    def set_operator(self, operator):
        self.cmd_open = operator['cmd_open']
        self.cmd_close = operator['cmd_close']

    def update(self, inputs):
        opened = inputs['opened']
        closed = inputs['closed']

        if opened and closed:
            self.error = True
            this_open = False
        else:
            if self.cmd_open and not self.last_output:
                this_open = True
            elif self.cmd_close and self.last_output:
                this_open = False
            else:
                this_open = self.last_output

        self.toggle = not self.toggle
        self.last_output = this_open

        return {
            'open': this_open
        }


instance = ClassValve(function_data['state'])


def do_class():

    # With the class option, we could look at reducing how often it saves state or make it a tuning option for blocks
    # to save state or not.

    instance.set_operator(sim_get_data('operator'))
    x = instance.update(
        inputs=sim_get_data('inputs')
    )

    sim_set_data('next_outputs', x)
    sim_set_data('next_state', instance.get_state())


print('Function')
print(timeit.timeit('do_function()', globals=locals(), number=test_iterations))

print('\n\nClass')
print(timeit.timeit('do_class()', globals=locals(), number=test_iterations))
