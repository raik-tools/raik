=========
Streaming
=========

:Enhancement: 11
:Keyword: E011-streaming
:Type: Feature
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:

Overview
========

The streaming system focuses on the non-critical communication of data between components and modules.

Background
----------

This documentation was originally captured in Redmine #351

Status Comments
===============

This document is still in draft.

To file:

* HB Message (will change?)
* client_conf_hash

Redmine notes copied, need to be integrated.


References
==========

* :doc:`Streaming Interface </interfaces/streaming>`

TODO: Find example model.

Streaming is an asynchronous communication method where only data changes are communicated to the client.


Guidelines for usage
====================

This system is designed to expect failure.  It is up to end devices to check integrity of the communication, as
this operates at OSI layer 7.

Streaming is initiated by a client.  As only data changes are transmitted, it's up to the client to ensure
integrity of data state.

Features
========



Pro Features
============


Definition
==========



Scope
=====

* RAIK local web interface
* RAIK Controller
* RAIK Archives

Specification
=============

Data Format
-----------

life-cycle
----------



Location
========

The streaming interface can be found in the following locations:

* :py:mod:`raik.communications.local`



Original Notes from Redmine
===========================

Notes below are to be sorted

Design Details
--------------

The design for the data stream is as follows:

The following applies to a web socket connection.  A worker process or thread will be spawned dedicated to each data stream.
General Notes:

* A unicast model has been adopted as this will be easier to transport over different networks and to encrypt.
* If a multicast or a broadcast is required, it can be considered later.  Current design should be modular so not to exclude these options later.

Protocol:

* A request is made for a stream data set. A data set ID is passed as part of this request.

    * A pre-defined stream associated with a display is accessed on the url /feed/display/x/ where x is the display id
    * Ad-hoc requests can be made to '/feed/' however their scan rate is slower than pre-defined data.

* On connection, All current values (and timestamps) are delivered.
* Updates are streamed from the server.  Each update will have an incrementing number.  Timing is controlled by the server.
* The server only sends data that has changed since the last update.
* On close of connection, the server will close its process.
* The client can close its connection any-time
* Client messages to the server are JSON objects with each property being a command and the value of the property being the command parameter.  The client messages are queued by the server and the entire queue is squashed and processed on each server scan.  Commands include:

    * 'r':  Always 'True' and instructs the server to provide a full dataset on the next update
    * 'hb':  A number isn't repeated consecutively.  This is the heartbeat to the server.
    * 'add':  Add tags to the data set (list of tags).  Adding can use wildcards and keywords as follows:

        * Basic glob patterns:  \*, ?, [ae], [a-e], [^ae]

    * 'remove':  Remove tags from the data set (list of tags).  Uses wild cards like add_tags
    * 'client_conf':  This is a hashed value that is repeated back to the client with each update.  This is so that the client can take responsibility for successful command execution.

* The client must send a heartbeat signal to the server to indicate connection activity.  This only needs to be sent up to every 5s.  A timeout will occur and the socket closed if this does not happen.
* If there are no data updates the server will send the data frame but with no data points in it.  This will also act as a heartbeat to the server.
* If a server exception occurs or the server is unable to stream data, then the websocket will be closed or connection won't be accepted.  Error codes are as follows (https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent)

    * 1: SOCKET_CLOSE_CODE_NORMAL - Normal / graceful shutdown confirmation.
    * 4000 : Unknown error.
    * 4001 : SOCKET_CLOSE_CODE_CONTROLLER_ERROR:  Connection rejected, there is a controller error (either it is not running or not configured.
    * 4003 : SOCKET_CLOSE_CODE_REQUEST_ERROR: There is an error in the request URL from the client
    * 4040 : SOCKET_CLOSE_CODE_STREAM_UNKNOWN_ERROR: An unknown error has occurred where the data stream is not responsive

Data structures:

* Each data item will be a tuple of (value, timestamp).  This may extend in the future.

    * This will be a JSON encoded packet.  Timestamp will be floating point seconds since the server advised datum (can be negative).  Timestamp will be in the timezone advised by the server.

* Each data set will be a dictionary.  The key is the point name and he value is a data item tuple
* On connection, the server will send an initialising dictionary with keys and values as follows:

    * datum - the time in UTC unix seconds that all times are referenced against.  This is an integer value.
    * timezone - data set time zone
    * offset - data offset to apply for the server time.  This is a float in seconds.
    * data - a complete data set for the request
    * data_id - a number that will increment on each update
    * cycle - a time that the server is expected to return updates.  This is in floating point seconds.

* On data updates, the server will only send data_id, offset and data.


Detail notes:

* Abuse is still available in creating multiple threads on the server side.  Mechanisms will need to be in place to optimize and prevent abuse.

Notes from one of the tests
^^^^^^^^^^^^^^^^^^^^^^^^^^^

TODO:  disconnect is not always guarunteed to be called.  Channels is written to expect failure.
https://channels.readthedocs.io/en/stable/getting-started.html
The above link says that disconnect will get called NEARLY all of the time
TODO:  On connect we must send back accept or close: True to indicate what to do with the socket during handshake.
https://github.com/django/channels/issues/293 also supports that disconnect isn't reliable.


The approximate protocol is as follows:

    * On Connect, the websocket will accept the connection if it is available for data streaming, otherwise
      errors will be sent in an error code on a rejected websocket handshake (indicating an exception).
    * Once the socket is accepted, data is streamed at a fixed rate (rather than polled).  Data is streamed
      on an as-changed basis only.  In the stream is a stream ID number which is a consecutive number
    * If the client misses a number in the sequence, it can ask for a refresh of all data which is added
      to the stream on the next update cycle.
    * The client shall send a periodic heartbeat message.  If one is not received in five cycles of streaming
      the stream will end and the socket will close.
    * The client can close the connection anytime which will close the stream.
