"""
This file is dedicated to function management and loading functions from the library.
"""
import raik.communications.local
import time
import os.path
import sys
import logging
logger = logging.getLogger(__name__)

default_imports = {
    'time': time
}

# Add the import path for the control libraries
# Set up import scope (see interface #239)
# The control library path is the standard RAIK library of functions.
# It contains sub folders where a function can be found.  Function names are globally unique.
control_lib_path = None
# Site config folder is where site specific functions are found (not in sub folders).
site_config_path = None


def set_paths(control_lib=None, site_config=None):
    """
    Override Library location.  If a location isn't specified it will default to the system default.
    :param control_lib:
    :param site_config:
    :return:
    """
    global control_lib_path, site_config_path
    if control_lib:
        control_lib_path = control_lib
    else:
        control_lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'control_lib')
    if site_config:
        site_config_path = site_config
    else:
        site_config_path = os.path.join(control_lib_path, 'raik_examples')

    # Ensure the control_lib path is set
    if control_lib_path not in sys.path:
        sys.path.append(control_lib_path)
set_paths()


def get_paths():
    return control_lib_path, site_config_path


def validate_function_body(function_body):
    """
    Performs validation on a function body to ensure it's "safe", as best as possible.
    Nothing is returned, but SyntaxErrors raised if there are issues.
    The checks are quite simple and can be improved using regex, but right now it's not seen as a
    major inconvenience to just ban string sequences.
    :param function_body:
    :type function_body: str
    :raises: SyntaxError
    """
    if '__' in function_body:
        raise SyntaxError('Double underscores are not allowed')
    if 'import ' in function_body:
        raise SyntaxError('Import statements are not allowed')
    # I'm sure these are easy to work around by compiling strings.  Tha above should be sufficient.
    # if 'self' in safe_body:
    #     raise SyntaxError('The word "self" is not allowed, anywhere.')
    # if 'class ' in safe_body:
    #     raise SyntaxError('The word "class" is not allowed, anywhere.')


def create_function(function_name, function_body, input_names, output_names):
    """
    This method creates a function body returning the string result if it is safe to use.
    :param function_name: Name of the function
    :type function_name: str
    :param function_body: Contents of the function.  All output variables are expected to be set.
    :type function_body: str
    :param input_names: Set of input parameter names
    :type input_names: tuple
    :param output_names: set of output parameter names
    :type output_names: tuple
    :return: String containing complete function definition.
    """
    safe_body = function_body.strip()
    # Add some safety checks
    validate_function_body(safe_body)

    # Indent all the lines
    safe_body = safe_body.split('\n')
    safe_body = '\n    '.join(safe_body)
    return (
        "def %s(%s):\n"
        "    %s\n"
        "    return {%s}"
    ) % (
        function_name,
        ', '.join(input_names),
        safe_body,
        ', '.join(["'%s': %s" % (a, a) for a in output_names])
    )


def compile_function(function_name, function_text):
    """
    Given a string, this method will compile a function object that can be called.
    It provides a limited set of builtins for use.
    :param function_name: The name of the function within the text.
    :type function_name: str
    :param function_text: The string containing a complete function definition.
    :type function_text: str
    :return: function
    """
    global default_imports
    g = {'__builtins__': {
        # 'ArithmeticError': ArithmeticError,
        # 'AssertionError': AssertionError,
        # 'AttributeError': AttributeError,
        # 'BaseException': BaseException,
        # 'BlockingIOError': BlockingIOError,
        # 'BrokenPipeError': BrokenPipeError,
        # 'BufferError': BufferError,
        # 'BytesWarning': BytesWarning,
        # 'ChildProcessError': ChildProcessError,
        # 'ConnectionAbortedError': ConnectionAbortedError,
        # 'ConnectionError': ConnectionError,
        # 'ConnectionRefusedError': ConnectionRefusedError,
        # 'ConnectionResetError': ConnectionResetError,
        # 'DeprecationWarning': DeprecationWarning,
        # 'EOFError': EOFError,
        # 'Ellipsis': Ellipsis,
        # 'EnvironmentError': EnvironmentError,
        # 'Exception': Exception,
        'False': False,
        # 'FileExistsError': FileExistsError,
        # 'FileNotFoundError': FileNotFoundError,
        # 'FloatingPointError': FloatingPointError,
        # 'FutureWarning': FutureWarning,
        # 'GeneratorExit': GeneratorExit,
        # 'IOError': IOError,
        # 'ImportError': ImportError,
        # 'ImportWarning': ImportWarning,
        # 'IndentationError': IndentationError,
        # 'IndexError': IndexError,
        # 'InterruptedError': InterruptedError,
        # 'IsADirectoryError': IsADirectoryError,
        # 'KeyError': KeyError,
        # 'KeyboardInterrupt': KeyboardInterrupt,
        # 'LookupError': LookupError,
        # 'MemoryError': MemoryError,
        # 'NameError': NameError,
        'None': None,
        # 'NotADirectoryError': NotADirectoryError,
        # 'NotImplemented': NotImplemented,
        # 'NotImplementedError': NotImplementedError,
        # 'OSError': OSError,
        # 'OverflowError': OverflowError,
        # 'PendingDeprecationWarning': PendingDeprecationWarning,
        # 'PermissionError': PermissionError,
        # 'ProcessLookupError': ProcessLookupError,
        # 'ReferenceError': ReferenceError,
        # 'ResourceWarning': ResourceWarning,
        # 'RuntimeError': RuntimeError,
        # 'RuntimeWarning': RuntimeWarning,
        # 'StopIteration': StopIteration,
        # 'SyntaxError': SyntaxError,
        # 'SyntaxWarning': SyntaxWarning,
        # 'SystemError': SystemError,
        # 'SystemExit': SystemExit,
        # 'TabError': TabError,
        # 'TimeoutError': TimeoutError,
        'True': True,
        # 'TypeError': TypeError,
        # 'UnboundLocalError': UnboundLocalError,
        # 'UnicodeDecodeError': UnicodeDecodeError,
        # 'UnicodeEncodeError': UnicodeEncodeError,
        # 'UnicodeError': UnicodeError,
        # 'UnicodeTranslateError': UnicodeTranslateError,
        # 'UnicodeWarning': UnicodeWarning,
        # 'UserWarning': UserWarning,
        # 'ValueError': ValueError,
        # 'Warning': Warning,
        # 'ZeroDivisionError': ZeroDivisionError,
        # '_': globals()['_'],
        # '__build_class__': __build_class__,
        # '__debug__': __debug__,
        # '__doc__': __doc__,
        # '__import__': __import__,
        # '__loader__': __loader__,
        # '__name__': __name__,
        # '__package__': __package__,
        # '__spec__': __spec__,
        'abs': abs,
        'all': all,
        'any': any,
        'ascii': ascii,
        'bin': bin,
        'bool': bool,
        'bytearray': bytearray,
        'bytes': bytes,
        # 'callable': callable,
        'chr': chr,
        # 'classmethod': classmethod,
        # 'compile': compile,
        'complex': complex,
        # 'copyright': copyright,
        # 'credits': credits,
        # 'delattr': delattr,
        'dict': dict,
        # 'dir': dir,
        'divmod': divmod,
        'enumerate': enumerate,
        # 'eval': eval,
        # 'exec': exec,
        # 'execfile': execfile,
        # 'exit': exit,
        'filter': filter,
        'float': float,
        'format': format,
        'frozenset': frozenset,
        # 'getattr': getattr,
        # 'globals': globals,
        'hasattr': hasattr,
        'hash': hash,
        # 'help': help,
        'hex': hex,
        # 'id': id,
        # 'input': input,
        'int': int,
        # 'isinstance': isinstance,
        # 'issubclass': issubclass,
        'iter': iter,
        'len': len,
        # 'license': license,
        'list': list,
        # 'locals': locals,
        'map': map,
        'max': max,
        'memoryview': memoryview,
        'min': min,
        'next': next,
        # 'object': object,
        'oct': oct,
        # 'open': open,
        'ord': ord,
        'pow': pow,
        # 'print': print,
        # 'property': property,
        # 'quit': quit,
        'range': range,
        'repr': repr,
        'reversed': reversed,
        'round': round,
        # 'runfile': runfile,
        'set': set,
        # 'setattr': setattr,
        'slice': slice,
        'sorted': sorted,
        # 'staticmethod': staticmethod,
        'str': str,
        'sum': sum,
        # 'super': super,
        'tuple': tuple,
        # 'type': type,
        # 'vars': vars,
        'zip': zip,
    }}
    g.update(default_imports)
    exec(function_text, g)
    return g[function_name]


def execute_function(input_map, fx, output_map, timeout=None, start_time=None):
    """
    The method performs a single control scan that does the following:
     * Load the inputs defined in the configuration (keys are function inputs)
     * Execute the method defined in the function, mapping the inputs to the function
     * Write the outputs of the function to the appropriate location

    This exists separately from the continuous scan solely to make testing easier.  Exception handling is expected
    to occur outside of this function.

    There is no return from this method, exceptions will be raised if there is an error.

    Note:  Inputs and outputs refer to the context of the called function.  So inputs are
    parameters to the function and outputs are return values.

    :param input_map: Dictionary mapping function parameters to inputs in the local value store.
    :type input_map: dict
    :param fx: Byte compiled function to execute
    :type fx: function
    :param output_map: Same as for input map, but for function outputs.
    :type output_map: dict
    :param timeout: If this is not none, outputs are only written if the execution completed prior to this value.
    :type timeout: float
    :param start_time: For efficiency, this can be provided by the calling function as time.time()
    :type start_time: float
    :return: execution time in float seconds if successful, an exception object if an exception occurred or
             False is some other error occurred
    """
    input_values = {}
    for i in input_map.values():
        if i not in input_values:
            input_values[i] = raik.communications.local.get_value(i)

    kwargs = {}
    for k, i in input_map.items():
        kwargs[k] = input_values[i]

    start_time = start_time or time.time()

    output = fx(**kwargs)
    logger.debug('Execute Function Results: %s', output)

    duration = time.time() - start_time
    if timeout is not None:
        if duration > timeout:
            raise TimeoutError("Process finished in %fs instead of under %fs" % (duration, timeout))

    for k, v in output.items():
        if k in output_map:
            raik.communications.local.set_value(output_map[k], v)

    return duration
