import os.path
import sys
sys.path.append(os.path.join(os.path.curdir, '..', '..'))
import raik.discovery.server


raik.discovery.server.start_server()