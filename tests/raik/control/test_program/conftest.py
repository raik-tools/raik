import pytest

# import os
# from typing import Callable

import raik
import raik.control.function_block


@pytest.fixture
def load_program(tmp_project, raik_local_communications):
    """
    This fixture ensures all in_process programs are removed after each test

    It wraps the load program call
    """

    yield raik.control.program.load_program
    for program_name in list(raik.control.program.IN_PROCESS_PROGRAMS.keys()):
        raik.control.program.del_in_process_program(program_name)


@pytest.fixture
def mock_program_external_interface():
    """
    This fixture ensures all in_process programs are removed after each test

    It wraps the load program call
    """
    mocked_externals = {}

    def mock_external(module_name, config):
        nonlocal mocked_externals
        assert module_name not in mocked_externals
        min_config = {"scope": raik.control.program.PROGRAM_SCOPE.private, "blocks": {}}
        min_config.update(config)
        config = min_config
        # noinspection PyTypeChecker
        conf = raik.control.program.ProgramConfig(
            name=module_name,
            scope=config["scope"],
            blocks=config["blocks"],
            module=None,
        )
        conf.save_run_file()
        mocked_externals[module_name] = conf

    yield mock_external

    for program_name in mocked_externals:
        mocked_externals[program_name].close_run_file()


@pytest.fixture
def mock_internal_block():
    """
    This fixture mocks the existence of an internal block
    """
    mocked_internals = []

    def mock_internal(_block_name):
        nonlocal mocked_internals
        assert _block_name not in mocked_internals
        assert _block_name not in raik.control.program.IN_PROCESS_BLOCKS
        mocked_block = raik.control.function_block.FunctionBlock()
        mocked_block.name = _block_name
        mocked_block.scope = raik.control.program.PROGRAM_SCOPE.private
        raik.control.program.IN_PROCESS_BLOCKS[_block_name] = mocked_block
        mocked_internals.append(_block_name)

    yield mock_internal

    for block_name in mocked_internals:
        del raik.control.program.IN_PROCESS_BLOCKS[block_name]


@pytest.fixture
def get_block() -> callable:
    """
    Retrieves a block
    """

    def get_block(block_name: str) -> raik.control.function_block.FunctionBlock:
        return raik.control.program.IN_PROCESS_BLOCKS[block_name]

    return get_block


@pytest.fixture
def get_external_block() -> callable:
    """
    Retrieves an external block.

    This assumed the block has been registered, otherwise a runtime error will result.

    When a program finishes it could very well clean up it's data, which means
    external block calls will only work with the extra block running?
    """

    def get_block(block_name: str) -> raik.control.function_block.ExternalBlock:
        block = raik.control.function_block.ExternalBlock()
        block.name = block_name
        block.connect()
        return block

    return get_block
