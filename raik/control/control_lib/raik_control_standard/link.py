def link(a, **kwargs):
    """
    Links the input and writes to the output
    :param a: input
    :return: b:  output
    """
    return {'b': a}
# EOF
