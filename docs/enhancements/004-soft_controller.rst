=================
Control Functions
=================

:Enhancement: 4
:Keyword: E004-soft_controller
:Type: Feature
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 2
   :local:


Overview
========

This project started with the idea of developing a distributed soft controller on commodity hardware that provides
industrial grade features and can be competitive with proprietary options from Allen Bradley, Siemens, Emerson and
so forth.

Background
----------

TODO:  Look for Redmine docs to Migrate

* http://redmine.tnet:3000/issues/234


Status Comments
===============

This document is still in draft.


References
==========

TODO - External references

* :doc:`/enhancements/005-control_functions`

Features
========

* Parallel scan processes sharing the "namespace" of the running user.  Scanning occurs in multiple processes.  (Note
   user namespace is achieved by nature of the scanning temp file being restricted to the executing user).

* TODO Link to stories with the E005 keyword


Pro Features
============


Definition
==========


Criteria
========



Specification
=============





Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.