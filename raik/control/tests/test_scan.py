import raik.enable_testing
import logging.config
import unittest
import raik.control.scan
import raik.control.functions
import raik.control.management
import raik.communications.local
import multiprocessing
import time
import os
import os.path
import sys
import shutil
from .utilities import enable_multiprocess_coverage
logging.basicConfig(level=logging.DEBUG)

enable_multiprocess_coverage()

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(process)d.%(thread)d %(levelname)s %(name)s:%(lineno)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    # 'loggers': {
    #     'daphne': {
    #         'handlers': ['console'],
    #         # 'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
    #         'level': 'WARNING',
    #     },
    # },
    'root': {
        'handlers': ['console'],
        'level': 'DEBUG',
    }
}
logging.config.dictConfig(LOGGING)


class TestFunctionScan(unittest.TestCase):
    """
        Tests the continuous scan capabilities of the control module.
    """
    def setUp(self):
        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_values()
        raik.control.retry_time_medium = 1.0
        raik.control.retry_time_long = 1.0
        # Set up 'standard' config library:
        root_path = '/tmp/control_test'
        if os.path.exists(root_path):
            shutil.rmtree(root_path)
        os.mkdir(root_path)
        os.mkdir(os.path.join(root_path, 'test_lib'))
        with open(os.path.join(root_path, 'test_lib/__init__.py'), 'w') as f:
            f.write('\n')
        with open(os.path.join(root_path, 'test_lib/test_add_two.py'), 'w') as f:
            f.write('def test_add_two(a, b):\n')
            f.write('    x = a + b\n')
            f.write('    return {"x": x}\n# EOF\n')

        # Set up site library
        config_path = os.path.join(root_path, 'raik_control_config')
        os.mkdir(config_path)
        with open(os.path.join(root_path, 'raik_control_config/test_add_three.py'), 'w') as f:
            f.write('def test_add_three(a, b, c):\n')
            f.write('    x = a + b + c\n')
            f.write('    return {"x": x}\n# EOF\n')

        # Make libs accessible
        sys.path.append(root_path)
        self.root_path = root_path
        self.config_path = config_path
        raik.control.functions.set_paths(
            root_path,
            config_path
        )
        self.process = None

    def tearDown(self):
        raik.control.functions.set_paths()
        if self.process is not None:
            self.process.terminate()
        raik.control.management.kill_all()

    def test_continuous_execution_import(self):
        instance_id = 'test_add_two_1'
        function_config = {
            'function': 'test_add_two',
            'timing': {
                'type': 'cyclic',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }

        # Set up configuration and input values
        raik.communications.local.set_config(instance_id, function_config)
        raik.communications.local.set_value('test_1', 3)
        raik.communications.local.set_value('test_2', 12)
        raik.communications.local.set_value('test_3', None)

        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        while True:
            status = raik.communications.local.get_value('test_add_two_1_status')
            if status is not None:
                break
        self.assertIn(status['s'], ['starting', 'running'])

        # Check the variables and status
        while raik.communications.local.get_value('test_3') is None:
            pass
        self.assertEqual(raik.communications.local.get_value('test_3'), 15)

        # Change an input, sleep
        raik.communications.local.set_value('test_1', 3)
        raik.communications.local.set_value('test_2', 1)
        raik.communications.local.set_value('test_3', None)

        # Check the variable and status
        while raik.communications.local.get_value('test_3') is None:
            pass
        self.assertEqual(raik.communications.local.get_value('test_3'), 4)

        # Close the continuous process
        t.terminate()

        # Check the status is not running
        time.sleep(1)
        status = raik.communications.local.get_value('test_add_two_1_status')
        self.assertEqual(status['s'], 'terminated')

    def test_continuous_execution_site_config(self):
        instance_id = 'test_add_three_1'
        function_config = {
            'function': 'test_add_three',
            'timing': {
                'type': 'cyclic',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2',
                'c': 'test_4'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }

        # Set up configuration and input values
        raik.communications.local.set_config(instance_id, function_config)
        raik.communications.local.set_value('test_1', 3)
        raik.communications.local.set_value('test_2', 12)
        raik.communications.local.set_value('test_4', 1)
        raik.communications.local.set_value('test_3', None)

        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        while True:
            status = raik.communications.local.get_value('test_add_three_1_status')
            if status is not None:
                break
        self.assertIn(status['s'], ['starting', 'running'])

        # Check the variables and status
        while raik.communications.local.get_value('test_3') is None:
            pass
        self.assertEqual(raik.communications.local.get_value('test_3'), 16)

        # Change an input, sleep
        raik.communications.local.set_value('test_1', 3)
        raik.communications.local.set_value('test_2', 1)
        raik.communications.local.set_value('test_3', None)

        # Check the variable and status
        while raik.communications.local.get_value('test_3') is None:
            pass
        self.assertEqual(raik.communications.local.get_value('test_3'), 5)

        # Close the continuous process
        t.terminate()

        # Check the status is not running
        time.sleep(1)
        status = raik.communications.local.get_value('test_add_three_1_status')
        self.assertEqual(status['s'], 'terminated')

    def test_continuous_execution_missing_func(self):
        instance_id = 'test_add_not_there_1'
        function_config = {
            'function': 'test_not_there',
            'timing': {
                'type': 'cyclic',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }

        # Set up configuration and input values
        self.assertIsNone(raik.communications.local.get_value('test_add_not_there_1_status'))
        raik.communications.local.set_config(instance_id, function_config)
        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        time.sleep(0.2)
        status = raik.communications.local.get_value('test_add_not_there_1_status')
        self.assertIn('not found', status['s'])

        # Check that the system tries again in a short while
        while 'not found' in status['s']:
            status = raik.communications.local.get_value('test_add_not_there_1_status')
        self.assertEqual(status['s'], 'starting')

    def test_continuous_execution_stalled_func(self):
        # Set up site library
        with open(os.path.join(self.config_path, 'test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    while True:\n')
            f.write('        pass\n')
            f.write('    return {"x": x}\n# EOF\n')

        instance_id = 'test_forever_1'
        function_config = {
            'function': 'test_forever',
            'timing': {
                'type': 'cyclic',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }

        # Set up configuration and input values
        raik.communications.local.set_config(instance_id, function_config)
        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        status = raik.communications.local.get_value('test_forever_1_status')
        while status is None:
            status = raik.communications.local.get_value('test_forever_1_status')
        self.assertEqual(status['s'], 'starting')
        time.sleep(4.0)
        status = raik.communications.local.get_value('test_forever_1_status')
        self.assertEqual(status['s'], 'stalled running')

    def test_continuous_execution_bad_config(self):
        # Set up site library
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    while True:\n')
            f.write('        pass\n')
            f.write('    return {"x": x}\n# EOF\n')

        instance_id = 'test_forever_1'
        function_config = {
            'function': 'test_forever',
            'timing': {
                'type': 'user1',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }

        # Set up configuration and input values
        raik.communications.local.set_config(instance_id, function_config)
        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        status = raik.communications.local.get_value('test_forever_1_status')
        while status is None:
            status = raik.communications.local.get_value('test_forever_1_status')
        self.assertEqual(status['s'], 'starting')
        while status['s'] == 'starting':
            status = raik.communications.local.get_value('test_forever_1_status')
        status = raik.communications.local.get_value('test_forever_1_status')
        self.assertIn('Unknown function timing', status['s'])

    def test_continuous_execution_no_config(self):
        # Set up site library

        # Check starting point
        self.assertIsNone(raik.communications.local.get_value('test_forever_1_status'))

        # Set up configuration and input values

        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=('non_existing_function',))
        self.process = t
        t.start()
        status = None
        while status is None or 'running' in status['s'] or status['s'] == 'starting':
            status = raik.communications.local.get_value('non_existing_function_status')
        status = raik.communications.local.get_value('non_existing_function_status')
        self.assertIn('terminated', status['s'])

    def test_continuous_execution_errors(self):
        # This will probably run faster if split up into separate functions or a class.
        # Set up site library
        raik.control.functions.default_imports['BaseException'] = BaseException
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    while True:\n')
            f.write('        1/0\n')
            f.write('    return {"x": x}\n# EOF\n')

        instance_id = 'test_forever_1'
        function_config = {
            'function': 'test_forever',
            'timing': {
                'type': 'cyclic',
                'period': 0.2
            },
            'inputs': {
                'a': 'test_1',
                'b': 'test_2'
            },
            'outputs': {
                'x': 'test_3'
            },
            '_t': time.time()
        }
        # Check starting point
        self.assertIsNone(raik.communications.local.get_value('test_forever_1_status'))

        # Set up configuration and input values
        raik.communications.local.set_config(instance_id, function_config)
        # Execute the function as a thread
        t = multiprocessing.Process(target=raik.control.scan.continuous_scan, args=(instance_id,))
        self.process = t
        t.start()
        status = None
        while status is None or 'running' in status['s'] or status['s'] == 'starting':
            status = raik.communications.local.get_value('test_forever_1_status')
        status = raik.communications.local.get_value('test_forever_1_status')
        self.assertIn('exception', status['s'])

        # Change the error - syntax error
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    import os\n')
            f.write('    x = os.getcwd()\n')
            f.write('    return {"x": x}\n# EOF\n')
        while 'exception' in status['s'] or 'stalled' in status['s'] or status['s'] in ['starting', 'running']:
            status = raik.communications.local.get_value('test_forever_1_status')
        self.assertIn('syntax error', status['s'])

        # Change the error - Bad function name
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever2(a, b):\n')
            f.write('    x = os.getcwd()\n')
            f.write('    return {"x": x}\n# EOF\n')
        while 'syntax error' in status['s'] or 'stalled' in status['s'] or status['s'] in ['starting', 'running']:
            status = raik.communications.local.get_value('test_forever_1_status')
        self.assertIn('Error compiling', status['s'])

        # Change the error - Slow process
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    x = 1\n')
            f.write('    time.sleep(0.18)\n')
            f.write('    return {"x": x}\n# EOF\n')
        while 'Error compiling' in status['s'] or 'stalled' in status['s'] or status['s'] in ['starting', 'running']:
            status = raik.communications.local.get_value('test_forever_1_status')
        # self.assertIn(status['s'], ['starting', 'running'])  # This might be needed instead
        self.assertIn('Process finished', status['s'])

        # Change the error - Custom exception
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    raise BaseException("Got ya")\n')
            f.write('    return {"x": x}\n# EOF\n')
        while 'Process finished' in status['s'] or 'stalled' in status['s'] or status['s'] in ['starting', 'running']:
            status = raik.communications.local.get_value('test_forever_1_status')
        self.assertIn('unexpected exception', status['s'])

        # Change the error - File still open for writing
        with open(os.path.join(self.root_path, 'raik_control_config/test_forever.py'), 'w') as f:
            f.write('def test_forever(a, b):\n')
            f.write('    raise BaseException("Got ya")\n')
            f.write('    return {"x": x}\n# EOF\n')
            while 'starting' not in status['s']:
                status = raik.communications.local.get_value('test_forever_1_status')
            while status['s'] == 'starting' or status['s'] == 'running':
                status = raik.communications.local.get_value('test_forever_1_status')
            self.assertIn('Control file is being written to', status['s'])
