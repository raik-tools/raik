"""
This file contains functions used to assist with remote deployment of packages and files
"""
import subprocess
import paramiko
import stat
import time
import os.path
from logging import getLogger
logger = getLogger(__name__)

# Execute remote commands
def execute_remote_commands_pubkey(username, hostname, commands, ignore_ret_code=False, port=22,
                                   client_pri_key=None, timeout=10.0):
    """
    Executes a series of commands via SSH on a remote host.  Pub key auth is expected to be set up outside of this
    command.  Each command is a full command string (unlike subprocess module).

    This does not work with a password so SUDO commands won't work unless enabled to operate without a password
    on the remote host.

    :param username:
    :param hostname:
    :param commands:
    :param ignore_ret_code: Doesn't throw an exception if the calling process returns a bad exit code
    :param port:  SSH port to use
    :param client_pri_key:  Private key file to use for the client connection
    :return:
    """
    gen_key_args = [
        'ssh',
    ]
    if '%' in hostname and ':' in hostname:
        # An ipv6 address
        gen_key_args += ['-6']

    if client_pri_key:
        gen_key_args += ['-i', client_pri_key]

    gen_key_args += [
        '%s@%s' % (username, hostname),
        '-o', 'StrictHostKeyChecking=no',
        '-o', 'UserKnownHostsFile=/dev/null',
        '-p', str(port),
        '-qT',
        "%s 2>&1" % '\n'.join(commands)
    ]
    try:
        output = subprocess.check_output(gen_key_args, timeout=timeout, stderr=subprocess.STDOUT).decode()
    except subprocess.TimeoutExpired:
        raise RuntimeError("Process timed out trying to ssh to %s.  Is the host accessible and does "
                           "password-less access work?" % hostname)
    except subprocess.CalledProcessError as e:
        if ignore_ret_code and e.returncode != 0:
            output = e.output.decode()
        else:
            raise RuntimeError("Failed to execute one of the commands.  Non zero exit status was returned\n%s"
                               % str('\n'.join(commands)))
    return output


class SimpleSSHClient:
    """
    A library wrapper that simplifies ssh client calls
    """

    def __init__(self, hostname, username=None, password=None, private_key=None, port=22):
        self.ssh_client = None
        self._sftp = None
        ssh_client = paramiko.client.SSHClient()
        # ssh_client.set_missing_host_key_policy(paramiko.WarningPolicy())
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.hostname = hostname
        self.username = username
        self.password = password
        self.private_key = private_key
        self.port = port
        self.ssh_client = ssh_client

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def transfer(self, src, dest):
        if self._sftp:
            self._sftp.put(src, dest)
        else:
            raise ConnectionError("No connection to server")

    def sudo(self, cmd, user=None):
        """
        Perform sudo commands.
        :param cmd: A command to execute as sudo
        :param user: A user to run as (instead of default)
        :return:
        """
        if self.ssh_client:
            # Execute Sudo command
            if user:
                sudo_cmd = 'sudo -u %s -S %s' % (user, cmd)
            else:
                sudo_cmd = 'sudo -S %s' % cmd
            logger.debug('Issue sudo command: %s', sudo_cmd)
            stdin, stdout, stderr = self.ssh_client.exec_command(sudo_cmd)
            err_buf = ''
            out_buf = ''
            # Need a way to ensure the remote command has performed some processing, other than an arbitrary
            # wait.  Can probably find a solution when there's time later.
            time.sleep(0.2)
            logger.debug('Waiting for channel')
            while stderr.channel.recv_stderr_ready():
                err_buf += stderr.channel.recv_stderr(1).decode()
            err_buf = err_buf.split('\n')
            if err_buf[-1].startswith('Password:') or err_buf[-1].startswith('[sudo] password'):
                logger.debug('Sudo requested a password')
                if not self.password:
                    stdin.channel.close()
                    raise ValueError("No password provided for SUDO command")
                stdin.write('%s\n' % self.password)
                stdin.flush()
            time.sleep(0.2)
            logger.debug('Waiting for channel completion')
            err_buf = '\n'.join(err_buf)
            while stderr.channel.recv_stderr_ready():
                err_buf += stderr.channel.recv_stderr(1).decode()
            out_buf += stdout.read().decode()
            return out_buf, err_buf
        else:
            raise ConnectionError("No connection to server")

    def run(self, cmd):
        if self.ssh_client:
            stdin, stdout, stderr = self.ssh_client.exec_command(
                cmd
            )
            out_buf = stdout.read().decode()
            err_buf = stderr.read().decode()
            return out_buf, err_buf
        else:
            raise ConnectionError("No connection to server")

    def listdir(self, path):
        if self._sftp:
            return self._sftp.listdir(path)
        else:
            raise ConnectionError("No connection to server")

    def remove(self, path):
        if self._sftp:
            s = self._sftp.stat(path)
            is_dir = stat.S_ISDIR(s.st_mode)
            if is_dir:
                self._sftp.rmdir(path)
            else:
                self._sftp.remove(path)
        else:
            raise ConnectionError("No connection to server")

    def exists(self, path):
        if self._sftp:
            try:
                s = self._sftp.stat(path)  # Returns SFTPAttributes object
                if s:
                    logger.debug('stat %s: %s' % (path, str(s)))
                return True
            except FileNotFoundError:
                logger.debug('FileNotFoundError:  %s' % path)
                return False
        else:
            raise ConnectionError("No connection to server")

    # def rmtree(self, path):
    #     if self._sftp:
    #         tmp_dirs = self._sftp.listdir(path)
    #         for f in tmp_dirs:
    #             sftp_object.remove('/tmp/%s/%s' % (dest_wheel_folder, f))
    #         sftp_object.rmdir('/tmp/' + dest_wheel_folder)
    #     else:
    #         raise ConnectionError("No connection to server")

    def mkdir(self, path):
        if self._sftp:
            self._sftp.mkdir(path)
        else:
            raise ConnectionError("No connection to server")

    def makedirs(self, path):
        if self._sftp:
            full_path = ''
            for component in path.split('/'):
                full_path = '/'.join([full_path, component])
                if self.exists(full_path):
                    continue
                else:
                    self.mkdir(full_path)
        else:
            raise ConnectionError("No connection to server")

    def close(self):
        if self.ssh_client:
            self.ssh_client.close()
            self._sftp = None
            self.ssh_client = None

    def connect(self):
        # This locks up when running with PAM and SSHD in non-root mode.
        # When PAM is disabled it proceeds, but when enabled it fails
        # PAM is only needed when using passwords, for keys it is not needed.
        # Looks like a platform specific issue.  Need to perform PAM logging.
        # See notes for test_deploy.py for more information
        if self.private_key is not None:
            pkey = paramiko.RSAKey.from_private_key_file(self.private_key)
            self.ssh_client.connect(
                hostname=self.hostname,
                port=self.port,
                username=self.username,
                pkey=pkey,
                look_for_keys=False,
                allow_agent=False
            )
        else:
            self.ssh_client.connect(
                hostname=self.hostname,
                username=self.username,
                port=self.port,
                password=self.password
            )
        self._sftp = self.ssh_client.open_sftp()

    def open(self, filename, mode='r'):
        if self._sftp:
            return self._sftp.open(filename, mode)
        else:
            raise ConnectionError("No connection to server")

    def add_auth_pub_key(self, pub_key, key_path='~/.ssh/authorized_keys'):
        """
        Adds a public key to the logged in user's authorised keys file.
        :param pub_key: The public key to add to the auth file
        :param key_path:  A path to the key file.  Defaults to the logged in user's home dir.
        :return:
        """
        if key_path.startswith('~/'):
            remote_home, _ = self.run('echo $HOME')
            remote_home = remote_home.strip()
            key_path = os.path.join(remote_home, key_path[2:])

        if not self.exists(key_path):
            logger.info('Creating authorization file for %s@%s' % (self.username, self.hostname))
            with self.open(key_path, 'w'):
                pass

        with self.open(key_path, 'r+') as ssh_f:
            f_data = ssh_f.read().decode()
            if pub_key not in f_data:
                logger.info('Adding public key to authorization file for %s@%s' % (self.username, self.hostname))
                if f_data and f_data[-1] != '\n':
                    ssh_f.write('\n' + pub_key + '\n')
                else:
                    ssh_f.write(pub_key + '\n')
