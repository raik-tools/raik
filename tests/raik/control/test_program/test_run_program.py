"""
These tests focus on the running of a program and confirming it
successfully executed.  These tests should focus on testing the
program component and little else.
"""
import pytest

import raik


def test_unloaded_program_execute_exception(tmp_project):
    """
    Ensures a reasonable error if a program doesn't exist
    """
    with pytest.raises(RuntimeError) as exc_info:
        raik.control.program.execute_program("private_connection_only_program")

    assert (
        exc_info.value.args[0]
        == "Program private_connection_only_program is not loaded"
    )


def test_program_execute(load_program, get_block):
    """
    This test checks that a program can be run once.
    """
    load_program("private_connection_only_program")

    # Get Pump Fixtures and check initial conditions
    pump5 = get_block("pump5")
    pump6 = get_block("pump6")
    # All values init to False, so first test is just init values
    assert not pump5.running
    assert not pump6.running
    assert not pump6.stopped

    # Launch the script in subprocess.  Pump6 being stopped should trigger
    # pump5 to start based on the connections and scan_order.
    raik.control.program.execute_program("private_connection_only_program")

    # Verify Pump6 update values
    assert not pump6.running
    assert pump6.stopped

    # Verify pump 5 state change
    assert not pump5.stopped
    assert pump5.running


def test_execute_program_script(tmp_project, get_external_block):
    """
    This test verifies a program can be run from the shell.  To verify
    the program execution, a state change will be observed from a mocked
    external block.
    """
    import subprocess

    with pytest.raises(RuntimeError, match="Block not found or the block is private"):
        get_external_block("pump3")

    subprocess.check_call(
        ["raik-admin", "run", "--run-once", "--test-db", "user_scope_toggle_pump_3"],
        cwd=str(tmp_project),
    )

    pump3 = get_external_block("pump3")

    # Verify pump 3 state recorded
    assert pump3.running
