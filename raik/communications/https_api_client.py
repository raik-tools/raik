"""
A client that simplifies connection to the https_api_server
"""
import requests
import json
from typing import Union, Tuple


class SimpleApiClient:
    def __init__(
        self,
        server_url,
        private_key_path: Union[str, Tuple[str, str]],
        trusted_ca_cert_path: str,
    ):
        self.server_url = server_url
        self.private_key_path = private_key_path
        self.trusted_ca_cert_path = trusted_ca_cert_path

    def make_url(self, function):
        return f"{self.server_url}/{function}"

    def get_json(self, function):
        get_url = self.make_url(function)
        response = requests.get(
            get_url, verify=self.trusted_ca_cert_path, cert=self.private_key_path
        )
        if response.status_code != 200:
            raise RuntimeError("Unexpected Error Code %i" % response.status_code)
        data = response.content.decode()
        return json.loads(data)

    def post_json(self, function, data):
        post_url = self.make_url(function)
        response = requests.post(
            post_url,  data={
                "data": json.dumps(data)
            },
            verify=self.trusted_ca_cert_path, cert=self.private_key_path
        )
        if response.status_code != 200:
            raise RuntimeError("Unexpected Error Code %i" % response.status_code)
        data = response.content.decode()
        return json.loads(data)


def main():
    import pathlib

    base_path = pathlib.Path(__file__).absolute().parent.parent.parent.parent
    print(base_path)

    s = SimpleApiClient(
        server_url="https://api.tnet:8443",
        private_key_path=(
            str(base_path / "certificates" / "simple-client.pem"),
            str(base_path / "certificates" / "simple-client.private.pem"),
        ),
        trusted_ca_cert_path=str(
            base_path / "certificates" / "root_ca" / "certs" / "ca.cert.pem"
        ),
    )

    print(s.get_json("hello_world"))


if __name__ == "__main__":
    main()
