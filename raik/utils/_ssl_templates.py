"""

"""

SELF_SIGNED_SSL = """    
#-------------openssl.cnf----------------
[ req ]
default_bits = 2048 # Size of keys
default_keyfile = key.pem # name of generated keys
default_md = sha256 # message digest algorithm
string_mask = utf8only # permitted characters
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
# Variable name   Prompt string
0.organizationName = Organization Name (company)
organizationalUnitName = Organizational Unit Name (department, division)
emailAddress = Email Address
emailAddress_max = 40
localityName = Locality Name (city, district)
stateOrProvinceName = State or Province Name (full name)
countryName = Country Name (2 letter code)
countryName_min = 2
countryName_max = 2
commonName = Common Name (hostname, IP, or your name)
commonName_max = 64


#-------------------Edit this section------------------------------
countryName_default     = AU
stateOrProvinceName_default = QLD
localityName_default        = Brisbane
0.organizationName_default  = self
organizationalUnitName_default  = Dev
commonName_default          = 127.0.0.1
emailAddress_default            = admin@127.0.0.1
"""

SSL_REQUEST = """    
#-------------openssl.cnf----------------

[ req ]

default_bits = 2048 # Size of keys
default_keyfile = key.pem # name of generated keys
default_md = sha256 # message digest algorithm
string_mask = utf8only # permitted characters
req_extensions = req_ext
distinguished_name = req_distinguished_name

[ req_distinguished_name ]

# Variable name   Prompt string
0.organizationName = Organization Name (company)
organizationalUnitName = Organizational Unit Name (department, division)
emailAddress = Email Address
emailAddress_max = 40
localityName = Locality Name (city, district)
stateOrProvinceName = State or Province Name (full name)
countryName = Country Name (2 letter code)
countryName_min = 2
countryName_max = 2
commonName = Common Name (hostname, IP, or your name)
commonName_max = 64

countryName_default     = %(country)s
stateOrProvinceName_default = %(state)s
localityName_default        = %(location)s
0.organizationName_default  = %(organisation)s
organizationalUnitName_default  = %(organisation_unit)s
commonName_default          = %(common_name)s
emailAddress_default            = admin@%(common_name)s

[ alternate_names ]

DNS.1        = %(common_name)s
DNS.2        = www.%(common_name)s
DNS.3        = localhost
# DNS.4        = ftp.example.com

[ req_ext ]

subjectAltName      = @alternate_names
# subjectAltName      = DNS:%(common_name)s,DNS:www.%(common_name)s

"""

CA_TEMPLATE = """
# https://jamielinux.com/docs/openssl-certificate-authority
# OpenSSL root CA configuration file.
# Copy to `/root/ca/openssl.cnf`.

[ ca ]
# `man ca`
default_ca = CA_default
                       
[ CA_default ]
# Directory and file locations.
dir               = %(root_path)s
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand
                                      
# The root key and root certificate.
private_key       = $dir/private/%(name)s.private.pem
certificate       = $dir/certs/%(name)s.cert.pem
                                          
# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/ca.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30
                      
# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256
                          
name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no
policy            = %(default_policy)s

# Honor extensions requested of us
copy_extensions	= copy
                                 
[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of `man ca`.
countryName             = match
stateOrProvinceName     = match
localityName            = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
                                  
[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
                                  
[ policy_site_ca ]
# A site based CA with restricted signing privileges.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = match
stateOrProvinceName     = match
localityName            = match
organizationName        = match
organizationalUnitName  = match
commonName              = supplied
emailAddress            = optional
                                  
[ req ]
# Options for the `req` tool (`man req`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only
                              
# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256
                            
# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca
                           
[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address
                                               
# Optionally, specify some defaults.
countryName_default             = AU
stateOrProvinceName_default     = Queensland
localityName_default            = Brisbane
0.organizationName_default      = RAIK
organizationalUnitName_default  = testing
emailAddress_default            =
                                 
[ v3_ca ]
# Extensions for a typical CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign
                                                           
[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign
                                                           
[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection
                                              
[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth
                             
[ crl_ext ]
# Extension for CRLs (`man x509v3_config`).
authorityKeyIdentifier=keyid:always
                                   
[ ocsp ]
# Extension for OCSP signing certificates (`man ocsp`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSignin
"""
