# The majority of code in this file is auto generated.  User code is marked in with Start_User /End_User sections
"""
A sequence as described in E803.

:raik_style: sequence
:raik_schedule: random_daily(16, 17)  # This needs some consideration

.. uml ::

    start
    ' Start_User - Specify sequence using syntax from: http://plantuml.com/activity-diagram-beta

    :GoogleSheetRead:get_code
    Fetches key from the web]

    :Send:broadcast_packet]

    :Recv:confirmation
    Waits for up to 15 minutes for the confirmation]

    :Transfer:get_results]

    :Function:save_data]

    ' End_User - Run update script to update this document with the appropriate generated code
    stop

"""
# Standard Imports
import logging
from .sequence_runner import GoogleSheetRead, Send, Recv, Transfer, Function
# Selected Imports
# End Imports
logger = logging.getLogger(__name__)

# Defined Steps
get_code = GoogleSheetRead('get_code')  # This will likely be generated code?
p_broadcast_packet = GoogleSheetRead('p_broadcast_packet')  # This will likely be generated code?
broadcast_packet = Send('broadcast_packet')
confirmation = Recv('confirmation')
get_results = Transfer('get_results')

# get_code parameters - Start_User
get_code.credentials_file = 'a.creds'
get_code.sheet_id = '1234'
get_code.cell_range = 'A1'
# get_code parameters - End_User

# p_broadcast_packet parameters - Start_User
p_broadcast_packet.credentials_file = 'a.creds'
p_broadcast_packet.sheet_id = '1234'
p_broadcast_packet.cell_range = 'A1'
# p_broadcast_packet parameters - End_User

# broadcast_packet parameters - Start_User
broadcast_packet.send_header = 'retrieve'
broadcast_packet.send_data = get_code.get_results
# broadcast_packet parameters - End_User

# confirmation parameters - Start_User
confirmation.timeout = 900.0
confirmation.recv_header = 'retrieve_confirmation'
# confirmation parameters - End_User

# get_results parameters - Start_User
get_results.tf_data = 'transactions'
get_results.tf_from = confirmation.recv_from    # This needs to be yielded / callable for later execution
# get_results parameters - End_User

# save_data parameters - Start_User
def save_data_fx():
    pass

save_data = Function('save_data')
save_data.task = save_data_fx
# save_data parameters - End_User

# Link Steps
get_code.link_next(
    broadcast_packet
).link_next(
    confirmation
).link_next(
    get_results
).link_next(
    save_data
)

# Branch test, without specific branching code (assumes all prior must be done and all next started together)
get_code.link_next(p_broadcast_packet).link_next(get_results)

def main():
    # Run Sequence
    get_code.execute_sequence()

if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s  -  %(levelname)s %(name)s:%(lineno)d %(message)s'
    )
    main()
