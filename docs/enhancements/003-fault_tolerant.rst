==============
Fault Tolerant
==============

:Enhancement: 3
:Keyword: E003-fault_tolerant
:Type: Theme
:Enhancement Version: 0.1
:Target Raik Version: 0.1
:Author: Gary Thompson
:Status: Draft
:Created: TBA
:Last Modified: TBA


.. contents:: Table of Contents
   :depth: 1
   :local:


Overview
========

This is an industrial reliability theme where the system is required to be able to be fault-tolerant.

Background
----------



Status Comments
===============

This document is still in draft.


References
==========

TODO - External references

Features
========



Pro Features
============


Definition
==========


Criteria
========



Copyright
=========

TODO, in the mean time, Copyright Gary Thompson.