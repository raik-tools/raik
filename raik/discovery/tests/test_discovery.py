import pathlib
import unittest
from raik.discovery import *
import raik.utils.hardware
import raik.utils.crypt
import raik.utils.random
import os.path
import os
import shutil
import time
import zlib
import base64
import json
import socket
import uuid

# See Bug #287 as coverage is incomplete for theses tests, but acceptable at the moment.


class TestNeighbourDiscovery(unittest.TestCase):
    """
    Tests the neighbour discovery tools
    These are difficult to test because results depend greatly on what's phyiscally connected to the network.
    Only basic checks are made
    """

    def test_neighbour_discovery_return(self):
        """
        Simple test that the neighbour discovery output is sensible.
        """
        neighbours = raik.discovery.neighbour_discovery()
        local_macs = [a[1] for a in raik.utils.hardware.get_mac_addresses()]
        for neighbour in neighbours:
            addr, dev, mac_addr = neighbour
            self.assertEqual(addr[:4], "fe80")
            self.assertTrue(len(mac_addr) == 17)
            self.assertNotIn(mac_addr, local_macs)


class TestDiscoveryData(unittest.TestCase):
    """
    Tests data structures used int he process of RAIK discovery
    """

    data_folder = pathlib.Path("/tmp/TestDiscoveryData")
    ca_path = data_folder / "TEST_ROOT_CA"

    @classmethod
    def setUpClass(cls):
        cls.data_folder.mkdir()
        cls.ca_path.mkdir()
        # Create a CA
        cls.root_cert = raik.utils.crypt.create_root_ca(cls.ca_path)

        # Generate a public / private key pair
        host_dev_name = "test_create_discovery_packet_host"
        host_key_name = cls.data_folder / host_dev_name
        private_key_path = cls.data_folder / raik.utils.crypt.rsa_private_key_name(
            host_dev_name
        )
        public_key_path = cls.data_folder / raik.utils.crypt.rsa_public_key_name(
            host_dev_name
        )
        raik.utils.crypt.generate_rsa_keys(private_key_path, public_key_path)
        # Create a request and get the CA to sign it
        csr_path = raik.utils.crypt.create_certificate_request(
            private_key_path, host_dev_name
        )
        cert_path = cls.data_folder / raik.utils.crypt.cert_file_name(host_dev_name)
        raik.utils.crypt.sign_certificate_request(cls.ca_path, csr_path, cert_path)
        cls.host_dev_name = host_key_name
        cls.host_key_name = host_key_name
        cls.host_private_key_file = private_key_path
        cls.host_public_key_file = public_key_path
        cls.host_cert_file = cert_path

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree("/tmp/TestDiscoveryData")

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_current_symmetric_key(self):
        """
        Simple checks for the symmetric key criteria.  This will
        need to be improved.
        :return:
        """
        first_key = get_current_symmetric_key()
        time.sleep(TIME_SENSITIVITY + 1)
        second_key = get_current_symmetric_key()
        self.assertNotEqual(first_key, second_key)

    def test_create_discovery_packet(self):
        """
        Test the creation of a discovery packet
        :return:
        """

        # Verify certificate before using it
        self.assertTrue(
            raik.utils.crypt.verify_certificate(self.host_cert_file, self.root_cert)
        )

        with open(self.host_cert_file, "r") as f:
            host_cert_data = f.read()

        # For the moment, the discovery packet needs to be limited to 1420 bytes.
        self.assertRaises(
            ValueError,
            create_discovery_packet,
            "-----BEGIN CERTIFICATE-----\n"
            + base64.b64encode(
                raik.utils.random.get_random_length_bytes(10000, 12000)
            ).decode("latin_1")
            + "-----END CERTIFICATE-----\n",
        )
        # Check the packet is in the right order
        self.assertRaises(ValueError, create_discovery_packet, host_cert_data[3:])
        self.assertRaises(ValueError, create_discovery_packet, host_cert_data[:3])

        discovery_packet = create_discovery_packet(host_cert_data)

        self.assertIsNotNone(discovery_packet)

        # Decode and ensure the packet starts with "RAIK"
        # Packet is currently encoded with the current time
        sym_key = get_current_symmetric_key()
        packet_data = raik.utils.crypt.symmetric_decrypt(sym_key, discovery_packet)
        self.assertEqual("RAIK", packet_data[-4:].decode("latin_1"))
        packet_data = packet_data[:-4]
        packet_data = zlib.decompress(packet_data)
        packet_data = base64.b64encode(packet_data)
        packet_data = packet_data.decode("latin_1")
        i = range(int(len(packet_data) / 64) + 1)
        packet_data = "\n".join(
            ["-----BEGIN CERTIFICATE-----"]
            + [packet_data[a * 64 : (a + 1) * 64] for a in i]
            + ["-----END CERTIFICATE-----\n"]
        )
        self.maxDiff = None
        self.assertEqual(host_cert_data, packet_data)

    def test_get_cert_from_discovery_packet(self):
        """
        Test the extraction of certificates from a discvoery packet
        :return:
        """

        # Verify certificate before using it
        self.assertTrue(
            raik.utils.crypt.verify_certificate(self.host_cert_file, self.root_cert)
        )

        with open(self.host_cert_file, "r") as f:
            host_cert_data = f.read()

        # Test a bad packet
        cert_data = host_cert_data[28:-26].replace("\n", "")
        discovery_packet = zlib.compress(base64.b64decode(cert_data))
        discovery_packet = raik.utils.crypt.symmetric_encrypt(
            get_current_symmetric_key(), discovery_packet
        )
        raik.utils.crypt.CERT_VALIDATION_CHAIN = self.root_cert
        self.assertRaises(ValueError, get_cert_from_discovery_packet, discovery_packet)

        discovery_packet = create_discovery_packet(host_cert_data)

        # Test a bad validation
        raik.utils.crypt.CERT_VALIDATION_CHAIN = self.host_cert_file
        self.assertRaises(ValueError, get_cert_from_discovery_packet, discovery_packet)

        # Test a good packet
        raik.utils.crypt.CERT_VALIDATION_CHAIN = self.root_cert
        packet_data = get_cert_from_discovery_packet(discovery_packet)
        self.assertEqual(host_cert_data, packet_data)


class TestHostData(unittest.TestCase):
    """
    Tests Host data collection
    """

    def test_get_host_data(self):
        """
        Check host data structure and content
        """
        temp_host_id = tempfile.NamedTemporaryFile(buffering=0)
        temp_uuid = str(uuid.uuid4())
        temp_host_id.write(temp_uuid.encode("ASCII"))
        raik.utils.hardware.UUID_LOCATION = temp_host_id.name
        host_data = get_host_data(small_data=True)

        # Confirm data can be serialised
        try:
            host_data_str = json.dumps(host_data)
            logger.debug(len(host_data_str))
            logger.debug(host_data_str)
            host_data_bytes = zlib.compress(host_data_str.encode())
            self.assertLess(
                len(host_data_bytes), 210, "Host data too long for encryption"
            )
        except TypeError:
            self.assertTrue(
                False, "Host data contains objects that can not be serialized"
            )

        # Data type
        self.assertIsInstance(host_data, dict)
        # Top level values
        self.assertIn("n", host_data)
        self.assertIn("os", host_data)
        self.assertIn("a", host_data)
        self.assertIn("ll", host_data)
        self.assertIn("u", host_data)
        self.assertIn("id", host_data)
        self.assertEqual(host_data["id"], temp_uuid)
        for dev in host_data["ll"]:
            self.assertEqual(len(host_data["ll"][dev]), 2)
            self.assertIsInstance(host_data["ll"][dev][1], list)
            for addr in host_data["ll"][dev][1]:
                self.assertIsInstance(addr, str)

        self.assertEqual(host_data["n"], socket.gethostname())

        # Long Form
        host_data = get_host_data(small_data=False)
        self.assertIn("os_r", host_data)
        self.assertIn("os_v", host_data)

        # Clean up
        temp_host_id.close()
