==============
RAIK Objective
==============

This is a team document to describe the background and purpose of the project.

TODO:  Review and update this file

Overview
========

For End Users
-------------


For Integrators
---------------

This documentation is largely aimed at the people who will be implementing a RAIK system.  These people
include System Integrators, Engineers, DIY business owners and people who want to learn automation.


RAIK People
-----------

Product Owner:  Gary Thompson



Vision
======

TODO

The Problem
===========

Analysis
--------

Reason
^^^^^^

Automation is continuing to grow and gaining greater attention with the media buzz around IoT, AI and machine learning.
Yet traditional, reliable and highly successful techniques that have been around for decades are seemingly overlooked
partly due to their high cost and specialist focus for the heavy industry sector.  The status quo in the heavy industry
is one of large expensive systems tied down by a significant legacy and not being agile enough to meet recent and
current technological trends.  This results in a lack of offerings for small businesses or a set of offerings that
is needlessly expensive and not providing the cost value expected from modern technology (when compared to consumer
technology).


Size
^^^^

This problem affects all users of automation, although focused on industrial usage.

Value
^^^^^

The greatest value is saving time and freeing up the users from their routine labour into a space of innovation.

Value to the users will be evident in improved product yields, increased production efficiency and/or peace of mind
knowing that their investments are being continuously and automatically monitored.  By providing an affordable leap
into automation, not just the control layer buy all layers including automation, operation and management, users can be
given the same opportunities for innovation that larger companies can previously only afford.

Who
^^^

* Small business owners producing goods looking to expand and grow
    * Micro and boutique breweries were the first observed people growing in Australia who were
      having to outlay significant capital for automation solutions typically dominating the
      heavy industry sector due to a lack of modern options.
* light industry entering or expanding automation
* heavy industry already familiar with automation looking for more options
* Me - I have years of plans for automation from system to home.  This platform will be a tool I can use to achieve
  some of those goals.


What
^^^^

* Automation:  Since the industrial evolution, manual labour has been continually replaced by automation.
  Automation brings with it consistency, higher product quality and 24/7 operation.  Manual labour gets expensive
  and in many cases exposes people to safety and health risks.  There has always been fear
  of the impact automation will have, and yet as it advances there is not an observable increase in
  unemployment.  As automation reduces costs to produce goods, prices go down making goods more accessible while
  increasing access to more people and increasing the average quality of life.  Goods are consumed at such
  a rate now that it's unlikely that a manual labour work force can keep up with the demand.  As such,
  there is a continuing need for automation.  However, there are a lot more small business being seen
  wanting to expand into the area of automation, but the capital costs involved for a quality system is
  an impediment to this growth.  In addition, newer systems emerging from the IT sector (in contrast to Engineering)
  either are too general or require specialist teams to develop.

    * I'm still uncertain as to why some simple techniques like feedback control are not well known in these circles

* Agility: There's no doubt that automation has an ongoing demand and modern control systems used in heavy industry are
  outdated.  There's no doubting their reliability, but their interfaces and agility is just not keeping
  up with the trends in the rest of the technology world.  Typically offering bolt-on solutions to a dated architecture
  instead of developing a modern and new system taking advantage of the progress in the Information Technology industry.
* Accessibility and Knowledge dilution:  With more advanced electronics for hobbyists available, IoT and the present
  buzz of machine learning, there is a separate movement producing interesting automation solutions and options, but in
  isolation there is a lack of experience and knowledge from decades of automation from the heavy industries.  Costs
  to gain the experience in this area is also prohibitive.


Why
^^^

* Solving this problem can enable growing businesses and light industry to compete with the larger established companies
  by having access to more cost effective automation systems offering the same level of availability.
* Bridging the gap from PLCs and DCSs to consumer hardware using the techniques used by control system engineers will
  enable others to gain more experience with control as commonly found in heavy industry.  This can enable a mix of
  knowledge and tools, leading to greater diversity and novel solutions being developed.
* Reducing costs of production through more agile and adaptable systems, enabling users to take advantage of the latest
  advances in technology quicker.
* Hopefully, by making an easily accessible control platform with real-time performance using familiar hard-ware there
  will be an opportunity to share the same availability and performance characteristics of control systems to these
  other areas currently getting a lot of media (and hence investment) attention.
* Existing knowledge appears often overlooked.  IoT and Big Data are not knew concepts to heavy industry, but they are
  treated as a new buzz.  Machine learning as an application in automation has been around for more than 15 years
  (it is currently 2018) and yet it's treated as a new thing as it's making its way to consumer and service based
  areas.  Meanwhile, I've observed in my own career machine learning being applied unsuccessfully in areas where the
  key problem was a loss of knowledge in the industry and where problems were shown to be easily solvable from retired
  operators based on first principles without the need of specialists and the architecture machine learning brings.
  I'm not opposed to machine learning, but fear that the knowledge from Process Control applications is being lost
  as experts leave the field.  As this product grows, it would be great to develop a central solutions and education
  component that can preserve this knowledge in context to the control. Otherwise, we're not only doomed to re-invent
  the wheel, but also apply over-engineered and inefficient techniques to something that could otherwise be simple.

Where
^^^^^

* Covered in the blurbs above.
* Currently just thinking of local business in Australia


Success Criteria
----------------

Success of development is measurable by the system's ability to perform and act reliably in
a similar fashion to a modern PLC and/or DCS.

Success of the system will be first measured through beta and stress tests in a home automation
environment as well as systems automation (automation of RAIK development supported by a RAIK
platform).

Success of the concept will at first be measured by the value provided to small business owners.
Throughout the whole process vendor-lock in needs to be avoided and transparency.

Success of the project will be seen in adoption and acceptance by heavy industry, even if the
projects are minor.


Solution options
----------------

The solution was already in mind at the start of the project.  Specifically, to use consumer
hardware (RPI, Beagle bone to PC's and old laptops) with serial or network I/O interfaces
to create a soft controller.  Modern technologies used for IT availability will be utilised
where possible.

Just like real-time control systems, the solution needs to utilise a reliable scan cycle and emulate behaviour of
real-time systems using a non-deterministic platform.


Alternatives
------------

Assist developing open-source IoT applications.  At the start of the project there weren't many
dominant in the market and the scene was quite fragmented.  This space should be reviewed again.

Domain
======

What's Known
------------

* Principles of control and real time control systems (PLCs, DCSs)
* The high price of modern DCSs and direct experience with their antiquity and lack of development
* There are many excellent availability protocols and systems used by software

What are the unknowns (and assumptions)
---------------------------------------

* The applicable market
* Innovation from any of the Giants and what their roadmaps are for this psace
* the performance risks of a high available design on consumer hardware.  Realistic continuity and fail over times


Development In Focus
====================

What are we doing?
------------------

Developing a high available control system and control system platform to compete with the giants of industry (Siemens,
ABB, Rockwell, Emerson, Yokogawa and more).  We're aiming for a complete clustered and distributed solution that is
easy and fun to use.

Why will customers want this?
-----------------------------

Because hardware will be cheap and instead money can be spent on education, development, solutions and integration.

Solution Vision
---------------

* Raspberry PI and dual ethernet Beaglebones
* A web HMI with online engineering, graphical montoring and real-time updates and notifications
* Seamless fail over on hardware and software corruption.  A demo video with a redundant set up and interruption
  of a controller (via heat or mechanical damage) with the process continuing seamlessly.

Scale and Scope
---------------

Initial scope is to make my life easier and automate the systems required to maintain RAIK.
