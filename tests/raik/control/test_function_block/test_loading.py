"""
Tests the loading of function blocks
"""


# noinspection PyUnresolvedReferences
def test_import_project_lib_function_block(tmp_project):
    """
    Tests import and function execution from top level functions in
    the project lib
    """
    from project_lib import SimpleDOL
    pump = SimpleDOL()
    assert not pump.running
