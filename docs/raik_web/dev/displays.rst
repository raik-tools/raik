========
Displays
========


Display Design
==============

To migrate from Redmine
-----------------------

* http://redmine.tnet:3000/issues/349


Decisions
---------

* See :ref:`widget code`

Data Interface
--------------

The display template performs particular task to simplify widget usage.

IO Points
^^^^^^^^^

All widgets register required io points.  The display handles retrieving these points and notifying the widgets when
their data has changed.  Notification is performed using the on_update method defined by a widget.


Data Point Queries
^^^^^^^^^^^^^^^^^^

Widgets (like tables) can simply provide a query to search for data points.  This allows glob-like patterns as well as
some pre-defined tokens.

The queries are not designed to be "live" as in, if the results of a query changes once a page is rendered, then a page
refresh is required to see the changes.  In other words, the query results are computed at the time of page loading.

.. _display_widget_interface:

Widget Interface
----------------

Each widget class / type is registered in javascript via the following dictionary:

.. highlight:: c


window.RAIK_display_widget_handlers



Editor Interface
----------------

TODO

Editor Use Cases
^^^^^^^^^^^^^^^^

Add Table Widget:  <-- Done

#. Table widget insets based on an empty widget
#. Table widget queries for an updated render and tag_list then re-draws once data is available
#. Table subscribes to custom events for each item in the tab list
#. Table updates cells with data stream.

Modify table widget  <-- Part done

#. Change configuration for table
#. Table unsubscribe from all data (but not removes from stream as we don't track which widget is requiring witch data
   just yet through the event system, unless we do it manually)
#. Table queries for a new render and tag list
#. Table subscribes to the new list and renders
#. Etc.

Delete table widget  <-- Not really done, decided that editing so low usage and clean up not implemented.

#. Table unsubscribe from data stream

Looking at these simply use cases, it seems that we will need the following for a display api:

* DONE: A method for requesting new server configuration and html - this applies to all widgets and is therefore a
  standard feature of configuration changes via AJAX.
* DONE: A place holder to indicate that a widget is re-configuring itself - maybe a method
* DONE: A subscribe to data method that uses events
* An unsubscribe method

Interface note:  It would be nice if objects with wildcard tag configuration would automatically
update / shrink with response data.  But this really needs redrawing on the current design.
Stick with an F5 to see new rows for now as querying if the tag lists have changed
requires some kind of ongoing service. <--- THIS WAS DECIDED AGAINST


Display Rendering
-----------------

Displays are rendered on the back end including widgets.  On initial page load, a complete and contained display will
exist and should not require javascript to show status.  (TODO, this hasn't been extended to notifications yet).

Widgets are rendered by the server and then updated live via javascript.


Display Model
=============

.. autoclass:: raik_web.raik_local.models.Display
