import pytest
import pathlib
import sys
import shutil

from raik.admin.project_lib_init import process_folder_imports
import raik
import raik.communications.local

BASE_DIR = pathlib.Path(__file__).parent
FIXTURE_DIR = BASE_DIR / "fixtures"
FIXTURE_PROJECT_LIB = FIXTURE_DIR / "project_lib"
FIXTURE_CONTROL = FIXTURE_DIR / "control"


@pytest.fixture
def tmp_project(tmpdir) -> pathlib.Path:
    """
    Creates a fixture with a temporary project.  Adds the project to
    Python Path for use with import statements.

    The temporary project contains the following functions:

    * project_lib.add_two
    * project_lib.add_three

    :param tmpdir:
    :return:
    """
    args = raik.admin.init.InitArgs(base_dir=tmpdir)
    raik.admin.init.main(args)

    project_lib_dir = tmpdir / "project_lib"
    control_dir = tmpdir / "control"

    for fixture_module in FIXTURE_PROJECT_LIB.glob("*.py"):
        file_name = fixture_module.name
        shutil.copy(str(fixture_module), project_lib_dir / file_name)

    for fixture_module in FIXTURE_CONTROL.glob("*.py"):
        file_name = fixture_module.name
        shutil.copy(str(fixture_module), control_dir / file_name)

    # TODO:  Create an admin command that updates this file based
    # on the contents
    project_init_exports = process_folder_imports(project_lib_dir)
    with (project_lib_dir / "__init__.py").open("w") as f:
        for module_name, export_name in project_init_exports:
            f.write(f"from .{module_name} import {export_name}\n")

    old_path = sys.path[0]
    sys.path[0] = str(tmpdir)
    yield tmpdir
    sys.path[0] = old_path


@pytest.fixture
def raik_local_communications():
    # noinspection PyProtectedMember
    original_stores = raik.communications.local._MSG_STORE
    if original_stores["config"][0] == "12":
        # Already in test mode
        yield
    else:
        raik.communications.local._MSG_STORE = {"config": [12, None], "val": [15, None]}
        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_events()
        raik.communications.local.clear_all_values()

        yield

        raik.communications.local.clear_all_config()
        raik.communications.local.clear_all_events()
        raik.communications.local.clear_all_values()
        # noinspection PyProtectedMember
        raik.communications.local._MSG_STORE = original_stores
