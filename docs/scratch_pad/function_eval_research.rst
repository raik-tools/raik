.. _function_execution_research:

===========================
Function Execution Research
===========================

These notes have been ported from Redmine:


History
=======

Although this is regarding the design of the library, the design and interface are tightly related.

Some options:

* Hack python's import methods and create a directory to hold all the functions, one module per function (seems
    inefficient).
* Use eval or exec
* Use some other processor (make my own)

How does the timeit module work?

* Uses gc, compile and exec:
* Compile Works with ast, compiles into bytecode

    * The object can be given to eval or exec.

* exec

Performance should be considered.  Links:

* http://lucumr.pocoo.org/2011/2/1/exec-in-python/
* Compile whatever code I'm using before executing, we don't want to do that every time.
* Garbage collection may not occur automatically when executing code.
* http://lybniz2.sourceforge.net/safeeval.html
* http://programmers.stackexchange.com/questions/191623/best-practices-for-execution-of-untrusted-code
* The link above links to even more articles.
* https://github.com/newville/asteval
* http://stackoverflow.com/questions/10747873/convert-ast-node-into-python-object
* https://greentreesnakes.readthedocs.org/en/latest/examples.html
* http://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html

    * Seems to reinforce my suspicion that we can improve things by not allowing double underscores (or even single,
      they are meant to be private anyway).  Not a guarantee though

* http://tav.espians.com/paving-the-way-to-securing-the-python-interpreter.html

    * Don't allow classes, functional programming only?

* PyPy offers a sandbox capability

    * Serializes the programs output to stdin/out.  Not far off what I'm hoping to achieve, but I don't want someone to
      download all my source code because of a flaw in this mechanism.

* http://nedbatchelder.com/blog/201302/finding_python_3_builtins.html

    * getattr, setattr etc...

* http://blog.delroth.net/2013/03/escaping-a-python-sandbox-ndh-2013-quals-writeup/

    * Again, no double underscores

I can either implement my own AST.  I've done this before based on:

* http://stackoverflow.com/questions/3513292/python-make-eval-safe
* http://stackoverflow.com/questions/20748202/valueerror-malformed-string-when-using-ast-literal-eval
* http://www.programcreek.com/python/example/7388/ast.Expression

Since I want to give a lot of freedom, and if users want to break their process then they can.  I just want to make
sure that when this runs on my own server, people can't access my source code trivially or crash the server.

Current design:

* Define inputs in a list
* Specify additional imports etc... (white list only)
* Write a script, using the inputs
* The script is saved in a partial file
* On load, the function body is parsed for errors / issues.  Then called.

Execution notes:

* I need to use exec, as eval won't do multiple statements.
* globals will just contain my own version of __builtins__
* locals will be my own dictionary
* inside the exec string, I can generate a "def" statement and this will get compiled in to my locals dictionary.