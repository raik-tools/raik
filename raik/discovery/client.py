"""
RAIK Discovery Client (as per interface #281).
"""
import socket
import raik.discovery
import raik.utils.crypt
import json
import os.path
import zlib
import logging
logger = logging.getLogger(__name__)
_module_path = os.path.join(os.path.dirname(os.path.abspath(__file__)))

HOSTS = "ff02::1"
CLIENT_PUBLIC_KEY_CERTIFICATE_FILE = os.path.join(_module_path, 'client.cert')
CLIENT_PRIVATE_KEY_FILE = os.path.join(_module_path, 'client.pri')
RESPONSE_TIMEOUT = 3.0


def get_servers():
    """
    Sends out a broadcast to the hosts specified in the module and lists the returning host data.
    :return: A dictionary of hosts and their host data in each element.
    """
    if not os.path.exists(CLIENT_PUBLIC_KEY_CERTIFICATE_FILE):
        raise ValueError('Client public key certificate not found, unable to create discovery packet')
    if not os.path.exists(CLIENT_PRIVATE_KEY_FILE):
        raise ValueError('Client private key file not found, unable to decode discovered data')

    with open(CLIENT_PUBLIC_KEY_CERTIFICATE_FILE, 'r') as f:
        host_signed_public_key = f.read()
    with open(CLIENT_PRIVATE_KEY_FILE, 'r') as f:
        host_private_key = f.read()

    discovery_packet = raik.discovery.create_discovery_packet(host_signed_public_key)
    sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    sock.settimeout(RESPONSE_TIMEOUT)
    sock.bind(('::', raik.discovery.CLIENT_PORT))
    sock.sendto(discovery_packet, (HOSTS, raik.discovery.LISTEN_PORT))
    received_data = {}
    try:
        while True:
            received = sock.recvfrom(raik.discovery.MAX_PACKET)
            received, address = received
            # noinspection PyBroadException
            try:
                msg = zlib.decompress(raik.utils.crypt.asymmetric_decrypt_with_private_key(
                    host_private_key,
                    received
                ))
                msg = json.loads(msg.decode())
                host_data = received_data.setdefault(address[0], {})
                host_data.update(msg)
            except Exception as e:
                # See Bug #287
                logger.warning('Unhandled exception occurred decoding data from %s:  (%s) %s',
                               address[0], type(e).__name__, str(e))
    except socket.timeout:
        pass
    return received_data
