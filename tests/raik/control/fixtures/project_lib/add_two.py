from raik import Number

module_interface = {"type": "function_block", "export": "add_two"}


def add_two(a: Number, b: Number) -> Number:
    x = a + b
    return x
