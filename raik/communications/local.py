"""
=========================
raik.communications.local
=========================

This module provides functions to access shared data for local host.  It is intended to be used for global
IPC of processes using the same module on localhost.

The following key/value name spaces are implemented:

* values - This is for storing simple values which are used as inputs or outputs to calculations and are
           expected to update very frequently.
* config - This is for storing configuration information, it is not expected to update regularly and is considered
           private for this host

Name spaces are pre-defined in the module as either being a 'config' space or 'val' space.  Functionally they are
the same.

Module Summary
==============

Module use is simple, call the functions:

* get_config(key) - Place a message into the named channel
* write_config(channel, message) - retrieve a message from the named channel
* clear_all_config - The message store is persistent and doesn't clear on read.  This wipes all channels.
* get_value(key) - Get a value from the local shared key store
* set_value(key, value) - retrieve a value from the key
* clear_all_values - The message store is persistent and doesn't clear on read.  This wipes all channels.
* TODO: Events

Alarms, Events and Notifications
================================

Alarms, events and notifications can be detailed in the

Alarms, events and notifications all fall under the general category of events.  Events have a key set of properties
that define the context, relevancy and severity of the event.  Headings below provide some guidelines on how to
use these.

Tag Name
--------

The Tag name parameter generally refers the source or data point that triggered the event.  This isn't enforced,
but will make cross referencing events to data points and control elements easier.  This is the first value
that defines context for the event.

Tags can be grouped by providing a scope argument.  A scope argument refines the context as well as provides some
relevancy.

State
-----

This defines the state that the source has entered.  It's the reason for the event to be created.

States are custom defined by each event.  The guidelines here is keep them in full capital letters, use spaces
instead of underscored and try to keep below three words (one word is ideal).

Typical states include:  HIGH, VERY HIGH, LOW, VERY LOW, TRIPPED, STOPPED, STARTED, RUNNING

Levels
------

For the events component of communications, this document also provides some pre-defined event levels which are
intended as a global level indicator for getting someone's attention.  This is not to be abused,
a small subsystem might consider an alarm as critical because it won't work anymore but if there's no catastrophic
personnel, environmental or economic impact then it is not critical. Front end tools will assist further.
The level is determined by a combination of how quickly is a response needed and what the severity is if no action
is taken.
A rough guide on how to select each level is indicated below (a condition just needs to meet one of the statements
below each level or be somewhere in between based on your judgement):

* CRITICAL:

    * A response is needed very quickly (< 1 minute) and if nothing happens, the outcome is moderate (or worse)
    * A response is needed quickly ( < 3 minutes) and the outcome is major (or worse)
    * A response is needed soon ( < 10 minutes) and the outcome is catastrophic (it doesn't get worse)

* ALARM:

    * A response is needed very quickly (< 1 minute) and if nothing happens, the outcome is minor (or worse)
    * A response is needed quickly ( < 3 minutes) and the outcome is moderate (or worse)
    * A response is needed soon ( < 10 minutes) and the outcome is major
    * A response will be needed (< 1 hour) and the outcome is catastrophic

* WARNING:

    * A response is needed quickly ( < 3 minutes) and the outcome is insignificant
    * A response is needed soon ( < 10 minutes) and the outcome is minor

* SEVERE_ALERT:

    * This is just a notice that something really bad has happened, you didn't act early enough and it's too late
      to do anything about it.  Nonetheless, we're polite enough to let you know in case you didn't see the warnings
      earlier (assuming you programmed warnings before things got to this stage).  You're either rebuilding from
      scratch or going home to try something else for a change.

* ALERT:

    * This is just a notice that something undesirable has happened.  It's too late to do anything, but it's nice
      to know that you are now in damage control and will have something to do.

* NOTICE:

    * A reminder to do something or some kind of notice

* INFO:

    * These are just items to be logged and won't bother you.

* DEBUG:

    * This is reserved for users when writing code and they want to trap an event or a bug in what's been written.
    * RAIK's own codebase shouldn't be raising these in production.

Persistence
-----------

Persistence of events can be set as follows:

* EVENT_ACK:  An acknowledgement of the event is needed before it clears.
* EVENT_NO_ACK:  No acknowledgement is needed, when the event clears it will disappear.

"""
import redis
import json
import raik
import time
import threading
from typing import Union, Iterator, Any, Dict, Tuple
from contextlib import contextmanager
from logging import getLogger
logger = getLogger(__name__)

# Global Constants and Enumerations

# Event levels
CRITICAL = 50
ALARM = 40
WARNING = 30
SEVERE_ALERT = 26
ALERT = 24
NOTICE = 22
INFO = 20
DEBUG = 10

_levelToName = {
    CRITICAL: 'CRITICAL',
    ALARM: 'ALARM',
    WARNING: 'WARNING',
    SEVERE_ALERT: 'SEVERE ALERT',
    ALERT: 'ALERT',
    NOTICE: 'NOTICE',
    INFO: 'INFO',
    DEBUG: 'DEBUG',
}

_nameToLevel = {
    'CRITICAL': CRITICAL,
    'ALARM': ALARM,
    'WARNING': WARNING,
    'SEVERE ALERT': SEVERE_ALERT,
    'ALERT': ALERT,
    'NOTICE': NOTICE,
    'INFO': INFO,
    'DEBUG': DEBUG,
}


# Event persistence
EVENT_ACK = 20
EVENT_NO_ACK = 10
# FUTURE:  Consider an acknowledgement model that acknowledge only needs to occur if the event fires
#          a certain number of times (PerHour etc...)
# FUTURE:  Reminder Event

# Internal Connection parameters

_REDIS_PORT = 6379
_REDIS_HOST = 'localhost'

# A global variable for referencing the communication backend.
DB_CONFIG = 7
DB_CONFIG_TEST = 12
DB_VAL = 5
DB_VAL_TEST = 15


_MSG_STORE = {
    'config': [DB_CONFIG, None],
    'val': [DB_VAL, None]
}


def _test_mode_enable():
    global _MSG_STORE
    import warnings
    warnings.warn('Testing to be replaced by fixtures')
    logger.debug('Launching Module in Test Mode')
    _MSG_STORE = {
        'config': [DB_CONFIG_TEST, None],
        'val': [DB_VAL_TEST, None]
    }


raik.TEST_CALLBACKS.append(_test_mode_enable)

if raik.TEST_MODE:
    _test_mode_enable()


def _init_msg_store(space):
    global _MSG_STORE
    space = _MSG_STORE[space]
    db = space[0]
    ks = space[1]
    if ks is None:
        logger.debug('Initialising message store %i %s', db, ks)
        ks = redis.StrictRedis(host=_REDIS_HOST, port=_REDIS_PORT, db=db)
        space[1] = ks
    return ks


def _value_get(_x):
    return json.loads(_x.decode('utf-8'))


def _value_put(_x):
    return json.dumps(_x).encode('utf-8')


def get_config(key):
    """
    Read a config value.
    :param key: The name of the channel to read the message from
    :type key: str
    :return: an object deserialized from the messaging system.
    """
    ks = _init_msg_store('config')
    v = ks.get(key)
    if v is None:
        return None
    return _value_get(v)


def set_config(key, val):
    """
    Write a config value.
    :param key: The name of the channel to read the message from
    :type key: str
    :param val: Any structure that is json serializable.
    """
    ks = _init_msg_store('config')
    ks.set(key, _value_put(val))


def delete_config(*keys):
    """
    Delete multiple configuration values
    :param keys: Multiple parameters, each can be a key to delete
    """
    ks = _init_msg_store('config')
    ks.delete(*keys)


def clear_all_config():
    """
    Clear all config values
    """
    ks = _init_msg_store('config')
    ks.flushdb()


def search_config_keys(search_string):
    """
    Searches for keys in the database.  This provides an iterator.
    :param search_string: See search_value_keys documentation.
    :return:
    """
    ks = _init_msg_store('config')
    for k in ks.scan_iter(match=search_string):
        yield k.decode()


def get_value(key):
    """
    Read a local message from other users of this module.
    :param key: The name of the key to read the message from
    :type key: str
    :return: an object deserialized from the messaging system.
    """
    ks = _init_msg_store('val')
    v = ks.get(key)
    if v is None:
        return None
    return _value_get(v)


def set_value(key, val):
    """
    Read a local val from other users of this module.
    :param key: The name of the key to read the val from
    :type key: str
    :param val: Any structure that is json serializable.
    """
    ks = _init_msg_store('val')
    ks.set(key, _value_put(val))


def delete_values(*keys):
    """
    Delete multiple values
    :param keys: Multiple parameters, each can be a key to delete
    """
    ks = _init_msg_store('val')
    ks.delete(*keys)


def set_values(values: Union[Dict[str, Any], Iterator[Tuple[str, Any]]]):
    """
    Sets a group of values based on a dictionary of values or a list of tuples.
    :param values: The name of the key to read the val from
    :type values: list
    :type values: dict
    """
    ks = _init_msg_store('val')
    # Uses a pipeline to set multiple values
    p = ks.pipeline()
    if hasattr(values, 'items'):
        values = values.items()
    for k, v in values:
        p.set(k, _value_put(v))
    p.execute()


def get_values(keys):
    """
    Gets a group of values
    :param keys: A list of keys to fetch
    :type keys: iterable
    :returns: dict
    """
    ks = _init_msg_store('val')
    # Uses a pipeline to set multiple values
    p = ks.pipeline()
    for k in keys:
        p.get(k)
    r = p.execute()
    return dict(zip(keys, map(lambda x: _value_get(x) if x is not None else None, r)))


def clear_all_values():
    """
    The message store is persistent and the message
    """
    ks = _init_msg_store('val')
    ks.flushdb()


def search_value_keys(search_string):
    """
    Searches for keys in the database.  This provides an iterator.
    :param search_string: This can be None (for all), an exact name or a glob style match supporting:
                          * h?llo - Wildcard single character
                          * h*lo - Wildcard
                          * h[ae]llo - Set
                          * h[^e]llo - Not Set
                          * h[a-b]llo - Range set
    :return:
    """
    ks = _init_msg_store('val')
    # Scan seems to be faster than keys from some basic profiling tests.
    for k in ks.scan_iter(match=search_string, count=10000):
        yield k.decode()


def push_value(key, val, limit=10):
    """
    Creates and pushes the value into a FIFO buffer (queue) located with all value data.
    :param key: Name of the queue (unique with all values data)
    :param val: The value to push
    :param limit: A soft limit that is only checked on invocation of this command and is not persistent.
    :return:
    """
    ks = _init_msg_store('val')
    queue_l = ks.llen(key)
    if queue_l >= limit:
        raise IndexError("Queue is full")
    ks.rpush(key, _value_put(val))


def pop_value(key):
    """
    Removes a value from the queue located at key
    :param key:
    :return:
    """
    ks = _init_msg_store('val')
    v = ks.lpop(key)
    if v is None:
        return None
    return _value_get(v)


def pop_all_values(key):
    """
    Retrieves the whole list in one step
    :param key:
    :return:
    """
    ks = _init_msg_store('val')
    p = ks.pipeline()
    p.lrange(key, 0, -1)
    # p.get(key)
    p.delete(key)
    v = p.execute()[0]
    if v is None or v == []:
        return []
    return [_value_get(a) for a in v]


class _DataStream:
    """
    This is a threaded data stream.  Used as a context to receive data updates.  Keys in the keystore
    are checked periodically (in sync with the local clock) and changed keys are made available in a cache that can
    be retrieved with the recv method.

    Note that the refresh period is actually referenced of the system clock.  So if a refresh period was 0.5s then
    when the clock reads xx:xx:00.0, xx:xx:00.5, xx:xx:01.0, etc... then data is checked. This is intended to operate
    as an isochoronous scan cycle.

    See trial file in tests folder as to why this algorithm was chosen.
    """
    def __init__(self, request_keys, refresh_period):
        """
        Open a data stream for retrieving periodic data updates on a diff basis.
        :param request_keys: A list of keys to monitor
        :param refresh_period: The update period in seconds
        """

        self.keys = set(request_keys)
        self.period = refresh_period
        self.timezone = -1*time.timezone
        self.offset = 0
        # time.time() is in local seconds since epoch and is the fastest to call
        self.datum = 0.0
        self.update_count = 0
        self._recv_called = False
        self._snapshot = {k: None for k in request_keys}
        space = _MSG_STORE['val']
        self._db = space[0]
        self._ks = None
        self._update = {}
        self._updater = None
        self._lock = threading.Lock()
        self._last_recv = 0
        self.cycle_errors = []

    def open(self):
        if self._updater:
            self.close()
        ks = redis.StrictRedis(host=_REDIS_HOST, port=_REDIS_PORT, db=self._db)
        self._ks = ks
        current_time = time.time()
        self.datum = current_time - current_time % self.period
        self._updater = threading.Thread(target=_DataStream.run, args=(self,), name='_data_stream_updater')
        self._updater.start()

    def close(self):
        with self._lock:
            self._ks = None
        if self._updater:
            self._updater.join()
            self._updater = None

    def run(self):
        """
        Data scanning in a thread.
        :return:
        """
        with self._lock:
            ks = self._ks
        period = self.period
        datum = self.datum
        snap = self._snapshot
        wake_up = datum
        # noinspection PyBroadException
        try:
            while self._ks is not None:
                wake_up += period
                current_time = time.time()
                sleep_time = wake_up - current_time
                if sleep_time < 0:
                    self.cycle_errors.append('Processing took too long')
                    break
                time.sleep(sleep_time * 0.8)
                while time.time() < wake_up:
                    time.sleep(0.00005)
                    # pass  # This option is very accurate but consumes CPU
                current_time = time.time()
                self.offset = current_time - datum

                # Fetch and process data
                keys = self.keys
                p = ks.pipeline()
                for k in keys:
                    p.get(k)
                r = p.execute()
                with self._lock:
                    for i, k in enumerate(keys):
                        v = r[i]
                        if v:
                            v = _value_get(v)
                        if snap.get(k) != v:
                            # The value has changed from the last stored value
                            snap[k] = v
                            self._update[k] = v
                    self.update_count += 1
        except Exception as e:
            self.cycle_errors.append('Exception occurred during streaming processing.  Aborting.')
            self.cycle_errors.append(str(e))

    def recv(self, blocking=False, full_set=False, sleep_time=0.001):
        """
        Reads the current state of the data buffer and returns the changed data since the last read.  This returns
        a structure defined in Redmine interface #351
        :param blocking: If set to True this method will block until the underlying data has been scanned.
        :param full_set: Retrieves all data, not just changed data.
        :param sleep_time: This tunes how long to sleep for while blocking
        :return:
        """
        if not self._recv_called:
            blocking = True  # First call is always blocking to get starting point
        if blocking:
            while self._last_recv == self.update_count and not self.cycle_errors:
                time.sleep(sleep_time)
        if self.cycle_errors:
            raise RuntimeError('\n'.join(self.cycle_errors))
        with self._lock:
            recv_dict = self._update
            self._update = {}

        self._last_recv = self.update_count

        if self._recv_called and not full_set:
            return {
                'data': recv_dict,
                'data_id': self.update_count,
                'offset': self.offset
            }
        else:
            self._recv_called = True
            return {
                'data': self._snapshot,
                'datum': self.datum,
                'timezone': self.timezone,
                'offset': self.offset,
                'data_id': self.update_count,
                'cycle': self.period
            }

    def add(self, keys_list):
        """
        Add keys to the list to be monitored
        :param keys_list: A list of keys to add
        :return: None
        """
        self.keys = self.keys | set(keys_list)

    def remove(self, keys_list):
        """
        Remove keys from the list to be monitored
        :param keys_list: A list of keys to remove
        :return: None
        """
        self.keys = self.keys - set(keys_list)
        for k in keys_list:
            self._update.pop(k, None)
            self._snapshot.pop(k, None)


@contextmanager
def open_data_stream(request_keys, refresh_rate):
    ds = _DataStream(request_keys, refresh_rate)
    ds.open()
    try:
        yield ds
    finally:
        ds.close()


#
# def set_scope_mask(scope_name, tag_name):
#     "Masks an entire scope when tag_name is False"
#  Interesting idea, but then delay timers etc... would also need configuring
# and this will likely be a function at the source rather than this system?
# We could automatically clear events on mask activation?  A mask delay
# may also be needed to capture the off side?  We wouldn't want to mask
# that a tripped occurred... so not everything should be masked in a scope.

def set_event(tag_name, state, level=INFO, scope=None, persistence=EVENT_NO_ACK, event_time=None, msg='', **kwargs):
    """
    Sets an event state.  The tag_name is expected to exist when calling get_value from this module.
    :param tag_name: Name of the key which this event is associated with.
    :type tag_name: str
    :param state: A (preferably capitalised) short label for the state of the tag.
    :type state: str
    :param level: Priority / Importance level of event.  Refer to `Alarms, Events and Notifications`_.
    :type level: int
    :param scope: Optional string to identify grouping of events.
    :type scope: str
    :param persistence: Defines the life cycle of the event.  Refer to `Alarms, Events and Notifications`_.
    :type: int
    :param event_time: Optional:  The time of the event derived from time.time().  When not specified, it will default
                       to time.time()
    :type: float
    :param msg: Optional:  A custom message to provide with the event.  It can be for clarification or even support.
    :type: str
    :param kwargs:  Additional arguments to provide to the event data.
    :type kwargs: dict
    :return: None

    Implementation Comments:
    * Currently scoped and stack data repeats the tag_name or scope name.
      This could be made more efficient in data, but will require a bit more
      processing, especially to preserve custom event data (kwargs)
    * If the same event (tag and state) is triggered repeatedly, it simply updates the event data for that state

    """
    ks = _init_msg_store('val')
    event_pipe = ks.pipeline()
    event_tag_name = '__event_' + tag_name

    if not event_time:
        event_time = time.time()

    event_data = {
        'tag_name': tag_name,
        'state': state,
        'level': _levelToName[level],
        'time': event_time,
        'msg': msg
    }

    # Initialise scope related variables and update event_data
    if scope:
        event_data['scope'] = scope
        scope_key_name = '__event_scope_' + scope
    else:
        scope_key_name = '__event_scope_'

    # Add persistence information:
    if persistence == EVENT_ACK:
        event_data['acknowledged'] = False

    # The tag already has an event, stack the events or update an existing event
    old_data = ks.get(event_tag_name)
    if old_data:
        old_data = _value_get(old_data)
        # Compare old data to the current event
        if old_data['state'] == state:
            # Repeat event
            update_event = event_data
            event_data = old_data
            event_data['level'] = update_event['level']
            event_data['recurrence'] = event_data.get('recurrence', 1) + 1
            event_data['recent_time'] = update_event['time']
        else:
            stack = old_data.pop('stack', [])
            stack.append(old_data)
            event_data['stack'] = stack

        # Since events are keyed by tag_name, and the scope list is also
        # keyed by the event tag name, stacking will inherently maintain
        # the scope set.

    # For events within a scope, remove all other scoped events from the
    # current events to only show the latest event.
    if scope:
        for s in ks.smembers(scope_key_name):
            event_pipe.zrem('__current_events', s)
        event_pipe.sadd(scope_key_name, event_tag_name)

    # Add custom data to the event
    if kwargs:
        # Preserve original event_data values and override if set in kwargs.
        kwargs.update(event_data)
        event_data = kwargs

    # Update the event for this tag
    event_pipe.set(event_tag_name, _value_put(event_data))

    # Update current events set
    event_pipe.zadd('__current_events', event_time, event_tag_name)
    # Increment processing counter
    event_pipe.incr('__event_counter')
    event_pipe.execute()


def acknowledge_event(tag_name, state, user='', location='', event_time=None):
    """
    Acknowledge an event (if acknowledgement is required, otherwise it does nothing).
    :param tag_name: Event tag_name
    :param state: Which state for the tag is being acknowledged
    :param user: Optionally, what user is logged against the acknowledge
    :param location: Where was the acknowledge performed
    :param event_time: What time was the acknowledge performed (defaults to time.time())
    :return:
    """
    ks = _init_msg_store('val')
    event_tag_name = '__event_' + tag_name
    event_data = ks.get(event_tag_name)
    if not event_time:
        event_time = time.time()

    if event_data:
        event_data = _value_get(event_data)
        pending_clear = event_data.get('__pending_clear', False)
        acknowledged = event_data.get('acknowledged', True)
        if not acknowledged:
            event_data['ack_user'] = user
            event_data['ack_location'] = location
            event_data['ack_time'] = event_time
            event_data['acknowledged'] = True
            ks.set(event_tag_name, _value_put(event_data))
        if pending_clear:
            clear_event(tag_name, state, event_time=event_time)


def clear_event(tag_name, state=None, event_time=None):
    """
    Clears the current state/event for the tag.
    :param tag_name: Event tag_name
    :param state: Which state for the tag is being acknowledged
    :param event_time:  When cleared, last update time is recorded for the event tag
    :return:
    """

    ks = _init_msg_store('val')
    event_pipe = ks.pipeline()
    event_tag_name = '__event_' + tag_name
    event_data = ks.get(event_tag_name)
    if event_data:
        event_data = _value_get(event_data)
    else:
        return None

    if not event_time:
        event_time = time.time()

    # Check acknowledged
    acknowledged = event_data.get('acknowledged', True)

    if acknowledged:
        # Remove the event from current events
        event_pipe.zrem('__current_events', event_tag_name)
    else:
        # No further processing is permitted until an acknowledgement occurs
        event_data['__pending_clear'] = True
        event_pipe.set(event_tag_name, _value_put(event_data))
        event_pipe.execute()
        return None

    # Pop from the event stack and/or update the event data
    stack = event_data.pop('stack', None)
    if stack:
        if event_data['state'] != state:
            raise ValueError('Multiple events have stacked and can only be cleared in the reverse order')
        else:
            last_event = stack.pop(-1)
            last_event['last_level'] = event_data['level']
            last_event['last_time'] = event_data['time']
            last_event['last_state'] = event_data['state']
            if 'acknowledged' in event_data:
                last_event['last_ack_user'] = event_data['ack_user']
                last_event['last_ack_location'] = event_data['ack_location']
            if stack:
                last_event['stack'] = stack
            new_data = last_event
            if last_event['level'] != INFO:
                event_pipe.zadd('__current_events', last_event['time'], event_tag_name)
    else:
        new_data = {
            'tag_name': tag_name,
            'state': '',
            'level': event_data.get('level', INFO),
            'time': event_time,
            'last_state': event_data.get('state', ''),
            'last_time': event_data['time']
        }

    event_pipe.set(event_tag_name, _value_put(new_data))
    event_pipe.execute()


def get_event(tag_name):
    """
    Get the state of a tag
    :param tag_name:
    :return: Event data dictionary
    """
    ks = _init_msg_store('val')
    event_tag_name = '__event_' + tag_name
    event_data = ks.get(event_tag_name)
    if event_data:
        event_data = _value_get(event_data)
        scope = event_data.get('scope')
        if scope:
            scope_key_name = '__event_scope_' + scope
            scope_events = []
            # Not sure is we just get a list of events, or all the data.
            # Getting all the data for now, this could change.
            fetch_events = [a for a in ks.smembers(scope_key_name) if a.decode() != event_tag_name]
            for d in ks.mget(fetch_events):
                scope_events.append(_value_get(d))
            event_data['scope_events'] = scope_events
        return event_data
    else:
        return None


def get_current_events():
    """
    Gets a list of all currently active events
    :return: list of active events, each event represented by a dictionary of summary values.
    """
    ks = _init_msg_store('val')
    common_fields = ('tag_name', 'state', 'time', 'level', 'scope', 'acknowledged', 'msg')
    current_events = ks.zrange('__current_events', 0, -1)
    if not current_events:
        return tuple()
    event_data = ks.mget(reversed(current_events))
    event_history = []
    for d in event_data:
        d = _value_get(d)
        event_history.append(
            {k: d.get(k) for k in common_fields if k in d}
        )
    return tuple(event_history)


def clear_all_events():
    """
    Clears all event data in the key store
    """
    ks = _init_msg_store('val')
    event_tags = set()
    scan_id, scan_tags = ks.scan(0, match='__event*', count=10000)
    event_tags |= set(scan_tags)
    while scan_id != 0:
        scan_id, scan_tags = ks.scan(0, match='__event*', count=10000)
        event_tags.add(set(scan_tags))
    if event_tags:
        ks.delete(*tuple(event_tags))
        ks.delete('__current_events')
        ks.delete('__event_counter')


def get_event_hash():
    """
    Returns a value that indicates if the event system has received any updates or not.  If the value is the same
    on consecutive calls then no new events exist.
    :return:
    """
    ks = _init_msg_store('val')
    v = ks.get('__event_counter')
    return v and v.decode() or v
