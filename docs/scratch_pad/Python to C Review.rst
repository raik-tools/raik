==================
Python to C Review
==================


Background
==========

This consolidates some very early (Redmine #260) tasks to review performance planning around HMI updates with a C
based system.  Falling under the category of premature optimization, it wasn't incorporated but some basic tests
were performed.

Goal
====

I want to build a C code library module, need to implement:

* Dynamic compilation (be able to compile on the controllers themselves, triggered by raik control code)
* Unit testing
* Launch C based process system

This is needed for high speed performance of the PIFace digital (pulse counter)

Integration of C
================

Unit Testing
------------

There are a lot of unit testing options.  I'm investigating the following:

* http://cunit.sourceforge.net/

    * Simple
    * Does not support fixtures, would need to structure functions so that they can be tested without fixtures.
    * Doesn't seem well maintained or updated recently.
    * about 500 downloads a week still

* http://libcheck.github.io/check/

    * Still maintained
    * Not as simple
    * GoogleTest
    * For CPP
    * Probably popular and maintained
    * Failry simple
    * Has fixtures

* http://www.throwtheswitch.org/unity/

    * Simple
    * Bigger than Check (on github)


I think I'll go with Unity for now, as it doesn't require C++.


Scan Logic
----------

* Port continuous scan across to C.  Remove dynamic features - process continues to run unless terminated.
  Just need to pass a function name as an argument and it will retrieve the appropriate libraries.
* Would like to make it more flexible.  Need to work out how to compile control lib into .so files that can be
  reloaded by just stopping and restarting a single process.

Python Integration
------------------

Things to note:

* Python extensions can be written and C and support developing Python objects

    * https://docs.python.org/2/extending/extending.html
    * http://dan.iel.fm/posts/python-c-extensions/
    * This is only compatible with CPython

* Cython cross compiles Python into C and can be very quick while working with all of CPython
* Python can link to C compiled libraries using ctypes:

    * https://docs.python.org/2/library/ctypes.html#module-ctypes
    * http://stackoverflow.com/questions/5081875/ctypes-beginner
    * http://stackoverflow.com/questions/2469975/python-objects-as-userdata-in-ctypes-callback-functions

* https://cffi.readthedocs.io/en/latest/

http://stackoverflow.com/questions/4384359/quick-way-to-implement-dictionary-in-c
https://en.wikipedia.org/wiki/Hash_table

Consider simplifying the interface so that a serialisable structure is passed straight away (ie, list in stead
of dict).  From a data stream perspective, dict may not be adding that much value.

Benchmarks
==========

on main-desktop under experiments I've created a custom float and list data type which I compared instantiating the C versions to some Python versions.

There was about a 5x performance difference which is big, but not necessarily that big.

==========================  ========
Test                        Timing
==========================  ========

True == True                0.022564
True == False               0.022979
'running' == 'running'      0.022809
'running' == 'stopped'      0.027501
3.0 == 2.0                  0.029245
3.0                         0.007947
float(3.0)                  0.077183
float(3)                    0.113712
float('3')                  0.127544
Floddy()                    0.088590
Decimal(3.0)                4.957900
Decimal(3)                  0.966174
Decimal('3')                3.025394
Decimal('3') == Decimal(3)  7.881079
F(3)                        0.543504
F('3')                      0.564699
{"a": 1}                    0.085371
[]                          0.020967
[3]                         0.048824
Shoddy()                    0.065249
Shoddy([3])                 0.173271
L()                         0.466591
L([2])                      0.616672
