"""
See control/__init__.py as well

This module tests the execution of function blocks.  Function blocks
offer a more challenging test environment as tests need to be
performed to ensure state is preserved.
"""
