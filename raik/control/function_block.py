"""
Function block implementation.  See library_blocks.rst.
"""
import logging
from typing import Any

import raik.communications.local
from .program_enums import PROGRAM_SCOPE
# from .function_block_meta import FunctionBlockMeta

logger = logging.getLogger(__name__)


class Connection:
    """
    These are used as like a proxy so that when a function block output
    is assigned to an input of another block, it can track the
    connection.
    """
    def __init__(self, name: str, val: Any, block: 'FunctionBlock'):
        self.name = name
        self.val = val
        self.block = block

    def __hash__(self):
        return hash(self.val)

    def __str__(self):
        return str(self.val)

    def __repr__(self):
        return repr(self.val)

    def __bool__(self):
        return bool(self.val)


class StateConnection(Connection):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dest = []


class InputConnection(Connection):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = None


simple_getter_template = """
def _get_%(name)s(self):
  # p('simple getter')
  return self._%(name)s
"""

simple_setter_template = """
def _set_%(name)s(self, v):
  # p('simple setter')
  self._%(name)s.val = v
"""

transparent_getter_template = """
def _get_%(name)s(self):
  # p('simple getter')
  return self._%(name)s.val
"""

no_getter_template = """
def _get_%(name)s(self):
  raise AttributeError("Can not access property %(name)s") 
"""

no_setter_template = """
def _set_%(name)s(self, v):
  raise AttributeError("Can not set property %(name)s") 
"""

one_shot_getter_template = """
def _get_%(name)s(self):
  # p('os getter')
  v = self._%(name)s.val
  if v is True:    
    self._%(name)s.val = False
  return  v
"""

input_terminal_setter_template = """
def _set_%(name)s(self, v):
  # p('input setter')
  if isinstance(v, Connection):
    # p('set input connection')
    # p(v)
    i = self._%(name)s
    # assert i.source is None
    i.source = v
    v.dest.append(i)
    i.val = None
  else:
    self._%(name)s.val = v 
"""


class StateProperty:
    """
    A descriptor used to identify state properties.  State properties have the following behaviour:

    * They can not be set when called fro a FunctionBlock class
    * They can be read by any instance.

    """

    connection = StateConnection

    def __init__(self, init_value=None):
        self.init_value = init_value
        self.name = '__prop__'
        self.owner = None
        self.getter = None
        self.setter = None

    def clone(self):
        """
        can be used for internal access, to create a mocked self for
        execution
        """
        return self.__class__(self.init_value)

    def _get_getter_template(self):
        # External Access
        if issubclass(self.owner, FunctionBlock):
            return simple_getter_template % {'name': self.name}

        # Internal Access
        else:
            return transparent_getter_template % {'name': self.name}

    def _get_setter_template(self):
        # External Access
        if issubclass(self.owner, FunctionBlock):
            return no_setter_template % {'name': self.name}

        # Internal Access
        else:
            return simple_setter_template % {'name': self.name}

    def __set_name__(self, owner, name):
        """
        This gets called once on class creation and is passed the name of
        the property.  A useful place to then calculate setter and getter methods
        :param owner:
        :param name:
        :return:
        """
        self.name = name
        self.owner = owner
        exec_globals = {
            'p': print,
            'Connection': Connection
        }
        exec_locals = {}
        exec(self._get_getter_template(), exec_globals, exec_locals)
        self.getter = list(exec_locals.values())[0]

        exec_locals = {}
        exec(self._get_setter_template(), exec_globals, exec_locals)
        self.setter = list(exec_locals.values())[0]

    def __get__(self, instance, owner):
        if not instance:
            raise AttributeError()  # A requirement of Python
        return self.getter(instance)

    def __set__(self, instance, value):
        self.setter(instance, value)

    def __delete__(self, instance):
        print('delete ' + self.name)
        raise NotImplementedError()


class InputProperty(StateProperty):
    """
    A descriptor used to identify Function block Inputs.  These offer the following behaviour

    * They can be set a value or assigned a State property
    * They can not be read.

    Internally, Functionblocks can only read these properties.
    """

    connection = InputConnection

    def __init__(self, init_value=None, one_shot=False):
        """
        :param init_value:
        :param one_shot:  This indicates that the input automatically
                          resets when read internally.  It's a one shot latch.
                          this is not the same as rising edge detection.  If
                          the input is continually set high, then this will
                          read high.
        """
        super().__init__(init_value)
        self.one_shot = one_shot

    def clone(self):
        """
        can be used for internal access, to create a mocked self for
        execution
        """
        return self.__class__(self.init_value, self.one_shot)

    def _get_getter_template(self):
        # External Access
        if issubclass(self.owner, FunctionBlock):
            return no_getter_template % {'name': self.name}

        # Internal Access
        elif self.one_shot:
            return one_shot_getter_template % {'name': self.name}
        else:
            return transparent_getter_template % {'name': self.name}

    def _get_setter_template(self):
        # External access
        if issubclass(self.owner, FunctionBlock):
            return input_terminal_setter_template % {'name': self.name}

        # Internal Access
        else:
            return no_setter_template % {'name': self.name}


class InputArrayProperty(InputProperty):
    pass


class ExternalStateProperty(StateProperty):
    """
    This is a state property that can be read externally to the process/
    This class exists only for classification for the function block
    processor
    """


class FunctionBlock:

    def __init__(self):
        # These are used by the program loader
        self.name = ''
        self.scope = ''  # private, user, host

        # self._inputs = []
        self._outputs = []
        self._published_key = False

        proxy_code = [
            'class SelfProxy:',
            '  _fb = fb',
            '  def __getattr__(self, name):',
            '      return getattr(self._fb, name)',
            '    ',
            '  def __setattr__(self, name, val):',
            '    if name in self.__class__.__dict__:',
            '      # p("Set %s in proxy" % name)',
            '      object.__setattr__(self, name, val)',
            '    else:',
            '      # p("Set %s in fb" % name)',
            '      setattr(self._fb, name, val)',
            '    ',
        ]
        # proxy_init = [
        #     '  def __init__(self):',
        #     # '    self._fb = fb'
        # ]
        props = {}
        connections = {}
        output_props = []
        proxy_globals = {
            'props': props,
            'connections': connections,
            'fb': self,
            'p': print
        }
        proxy_locals = {}

        # Create internal variables to store connection objects
        # and create a proxy of self to give direct access
        for attribute_name, class_val in self.__class__.__dict__.items():
            if isinstance(class_val, StateProperty):
                proxy_code.append(f'  {attribute_name} = props["{attribute_name}"].clone()')
                props[attribute_name] = class_val
                c = class_val.connection(
                    attribute_name, class_val.init_value, self
                )
                connections[attribute_name] = c
                setattr(self, f'_{attribute_name}', c)
                proxy_code.append(f'  _{attribute_name} = connections["{attribute_name}"]')
            if isinstance(class_val, ExternalStateProperty):
                output_props.append(attribute_name)

        proxy_str = '\n'.join(proxy_code) # + proxy_init
        exec(proxy_str, proxy_globals, proxy_locals)
        self._proxy = list(proxy_locals.values())[0]()
        self.connections = connections

        for attribute_name in output_props:
            self._outputs.append([
                attribute_name,
                getattr(self, attribute_name)
            ])

    def __repr__(self):
        return f"<{str(self.__class__.__name__)}: {self.name}>"

    def __str__(self):
        state_val = []
        for prop_key, output_prop in self._outputs:
            state_val.append(f'{ prop_key }="{ output_prop.val }"')
        state_str = ', '.join(state_val)
        return f"<{str(self.__class__.__name__)}: {self.name}, { state_str }>"

    def __call__(self, **kwargs):
        # Input processing
        # Although this could be more dynamic (at time of get from the
        # connection class).  For consistency, all input values are read
        # together and will not change during execution.
        for connection in self.connections.values():
            connection_source = getattr(connection, 'source', None)
            if isinstance(connection_source, Connection):
                connection.val = connection_source.val

        # Execution
        # noinspection PyUnresolvedReferences,PyCallByClass,PyTypeChecker
        self.__class__.block_scan(self._proxy)

        # Output Processing - state sharing
        if self.scope != PROGRAM_SCOPE.private:
            if not self._published_key:
                # Init or complete init of the outputs.  This is done
                # here because name, scope and connection doesn't occur on init
                self._published_key = f'raik.blocks.{self.name}.{self.scope}'
                raik.communications.local.set_config(self._published_key, True)
                for prop_tuple in self._outputs:
                    prop_tuple[0] = f'raik.blocks.{self.name}.{self.scope}.{prop_tuple[0]}'

            output_val = []
            for prop_key, output_prop in self._outputs:
                output_val.append((prop_key, output_prop.val))
            import json
            logger.debug('End of scan for %s, '
                         '\n\tsending %s'
                         '\n\tin json %s', self, output_val, json.dumps(output_val))
            # TODO:  Values should be set with a 3s expiry which later can be tuned
            # as a block / scan setting.
            raik.communications.local.set_values(output_val)

    def block_scan(self):
        pass


class ExternalBlock:

    def __init__(self):
        # These are used by the program loader
        self.name = None

        # These are updated upon block connection
        self.scope = None
        self._block_key = None
        self._value_key_map = {}
        self._value_keys = None
        self._connected = False

    def connect(self):
        block_key_name = f'raik.blocks.{self.name}.*'
        for block_key in raik.communications.local.search_config_keys(block_key_name):
            self._block_key = block_key
            break

        if self._block_key:
            self._connected = True
            self.refresh()
        else:
            raise RuntimeError('Block not found or the block is private')

    def disconnect(self):
        """
        Used to signal removal of a proxy object
        :return:
        """
        if self._connected:
            self._connected = False

    def __call__(self):
        if not self._connected:
            self.connect()
        else:
            self.refresh()

    def refresh(self):
        if not self._value_key_map:
            for value_key in raik.communications.local.search_value_keys(
                f'{self._block_key}.*'
            ):
                self._value_key_map[value_key] = value_key[value_key.rfind('.') + 1:]
            self._value_keys = tuple(self._value_key_map.keys())

        values_data = raik.communications.local.get_values(self._value_keys)
        logger.debug('Reading %s:  \n\t%s', self, values_data)
        for k, v in values_data.items():
            setattr(self, self._value_key_map[k], v)
