"""
These are utilities that assist in accessing data on google services.

Useful links:

* https://help.form.io/integrations/googledrive/

"""
# noinspection PyUnresolvedReferences
from apiclient.discovery import build
from httplib2 import Http
from google.oauth2 import service_account

# Follow instructions from Google for setting up an API key

# Setup the Sheets API
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


def load_sheets_credentials(credentials_json):
    """
    UPDATE to google-auth:
    https://google-auth.readthedocs.io/en/latest/reference/google.oauth2.credentials.html?highlight=credentials
    https://google-auth.readthedocs.io/en/latest/user-guide.html

    Load a credentials file for accessing sheets.

    I don't have all the instructions on hand on how I set this up to
    begin with.  I don't remember it being straight forward, but what
    I do have is roughly captured below.

    To generate a credentials file, simply:

    * Log into the API Console:
      https://console.developers.google.com/apis/credentials

    * Create a Project which will host a service account, credentials and permissions

        * Enable the Sheets API in the project

    * If you haven't created a service account, do so:

        * Select Create Credentials -> Create Service Account

    * To generate a token (credentials_json) for the service account:

        * Service Accounts -> Select service account menu for the target account
        * Create Key -> JSON (or edit the service account and click Create Key)
        * You can have multiple private keys for an account

    * Grant Service account permission to project / sheet



    For further information, see:

    * https://cloud.google.com/iam/docs/creating-managing-service-accounts
    * https://developers.google.com/sheets/api/guides/authorizing

    :param credentials_json:
    :return:
    """
    credentials = service_account.Credentials.from_service_account_file(
        credentials_json,
    )
    scoped_credentials = credentials.with_scopes(
        SCOPES
    )

    return scoped_credentials


def _get_sheet_service(credentials_json):
    # Build a connection object
    credentials = load_sheets_credentials(credentials_json=credentials_json)
    service = build('sheets', 'v4', credentials=credentials)
    return service


def read_sheet_range(credentials_json, spreadsheet_id, key_range, heading_row=False):
    """
    Read a google sheet.  However, some data is needed in order to connect:

    * Spreadsheet ID - This can retrieved from the URL or from Drive
    * credentials_json - see load_sheets_credentials

    :param credentials_json:
    :param spreadsheet_id:
    :param key_range:
    :param heading_row:
    :return:
    """
    service = _get_sheet_service(credentials_json)

    # Call the Sheets API
    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id,
        range=key_range
    ).execute()

    data = []

    sheet_values = result.get('values', [])
    headings = None
    for v in sheet_values:
        if heading_row and not headings:
            headings = v
        else:
            data.append(v)

    if heading_row:
        ret = []
        for row in data:
            new_row = {}
            for i, h in enumerate(headings):
                new_row[h] = row[i]
            ret.append(new_row)
        return ret
    else:
        return data


# def write_sheet_range(credentials_json, spreadsheet_id, key_range, heading_row=False):
#     service = _get_sheet_service(credentials_json)
#     raise NotImplementedError()
#     # Call the Sheets API
#     # THIS_KEY = '2'
#     #
#     # this_index = keys.index(THIS_KEY)
#     # old_value = values[this_index]
#     #
#     # target_cell_range = 'Sheet1!B%i' % this_index + 1
#     #
#     # result = service.spreadsheets().values().update(
#     #     spreadsheetId=spreadsheet_id,
#     #     range=target_cell_range,
#     #     includeValuesInResponse=True,
#     #     body={
#     #         'range': target_cell_range,
#     #         'values': [[old_value + '1']]
#     #     },
#     #     valueInputOption='RAW'
#     # ).execute()
#     # sheet_values = result.get('values', [])
