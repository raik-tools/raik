# Handles random data generation
import os
import textwrap
import base64


def get_true_random(num_bytes):
    """
    This only works on GNU/Linux and is best used with a HWRNG supplying /dev/random
    :param num_bytes:
    :return:
    """
    with open('/dev/random', 'rb') as f:
        b = f.read(num_bytes)
    return b


def get_random_double(rand=os.urandom):
    """
    Gets a random double (based on 64bits) between 0 and 1
    :param rand:
    :return:
    """
    # 18446744073709551616
    b = rand(8)
    i = int.from_bytes(b, byteorder="big")
    return i / 18446744073709551616


def get_random_int(result_min, result_max, rand=os.urandom):
    """
    Gets a random integer between the requested range.
    :param result_min:
    :param result_max:
    :param rand:
    :return: int
    """
    return int((result_max - result_min) * get_random_double(rand) + result_min)


def get_random_length_bytes(result_min, result_max, rand=os.urandom):
    """
    Gets variable length random data.
    :param result_min: Min number of bytes
    :param result_max: Max number of bytes
    :param rand: A function to get random bytes from (specifying number of bytes as an argument)
    :return: bytes
    """
    l = get_random_int(result_min, result_max, rand)
    return rand(l)


def get_random_bytes(length, as_base_64=False, rand=os.urandom):
    """
    Simply Random Bytes
    :param length:
    :param as_base_64:  Set to True to return the bytes in ASCII B64
    :param rand: A function to get random bytes from (specifying number of bytes as an argument)
    :return: bytes, str
    """
    d = rand(length)
    if as_base_64:
        d = base64.b64encode(d)
    return d


def get_random_ipv6_private_address(rand=os.urandom, as_string=False):
    ipv6_private = 0xfd
    ipv6_prefix = int.from_bytes(rand(5), byteorder="big")
    ipv6_subnet = int.from_bytes(rand(2), byteorder="big")
    ipv6_host = int.from_bytes(rand(8), byteorder="big")
    full_address = ipv6_private * 0x1000000000000000000000000000000 + \
                   ipv6_prefix * 0x100000000000000000000 + \
                   ipv6_subnet * 0x10000000000000000 + \
                   ipv6_host
    if not as_string:
        return full_address
    else:
        full_address = '%X' % full_address
        return ':'.join(textwrap.wrap(full_address, 4))


def get_random_mac_address(rand=os.urandom):
    mac_bytes = rand(6)
    return '-'.join('%02x' % a for a in mac_bytes)

