import pytest

from raik.admin import init_logging
import raik.communications.local

# Set up test wide logging
init_logging()


# Set up test redis
@pytest.fixture(autouse=True)
def test_redis_db():
    import warnings

    warnings.warn(
        "Tests should not rely on Redis unless explicitly stated to do so.  This "
        "Fixture needs to be updated not autouse."
    )

    # original_config = raik.communications.local._MSG_STORE["config"]
    # original_val = raik.communications.local._MSG_STORE["val"]
    #
    # raik.communications.local._MSG_STORE["config"] = [
    #     raik.communications.local.DB_CONFIG_TEST,
    #     None,
    # ]
    # raik.communications.local._MSG_STORE["val"] = [
    #     raik.communications.local.DB_VAL_TEST,
    #     None,
    # ]
    #
    # yield
    #
    # raik.communications.local.clear_all_values()
    # raik.communications.local.clear_all_config()
    # raik.communications.local.clear_all_events()
    #
    # raik.communications.local._MSG_STORE["config"] = original_config
    # raik.communications.local._MSG_STORE["val"] = original_val
