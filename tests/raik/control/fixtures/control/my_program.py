"""
Sample program layout.  Much of the functionality will be managed by
admin commands
"""
# noinspection PyUnresolvedReferences
from project_lib.simple_dol import SimpleDOL

# noinspection PyUnresolvedReferences
from project_lib.and_ import And
from raik import ExternalBlock
from raik import PROGRAM_SCOPE

SCOPE = PROGRAM_SCOPE.private


# External Blocks
pump3: SimpleDOL = ExternalBlock()

# Internal Blocks
pump1 = SimpleDOL()
pump2 = SimpleDOL()
pumps2_3_running = And()

SCAN_ORDER = [
    pump2,
    # pump3 is async...
    pumps2_3_running,
    pump1,
]


# Program static variables
system_pumps = (pump1, pump2, pump3)


def connections():
    """
    A location where connections between blocks can be made, but not logic, this is
    one to one connections.
    """
    pumps2_3_running.in_array = pump2.running
    pumps2_3_running.in_array = pump3.running
    pump1.interlock = pumps2_3_running.out


def pre_scan():
    """
    Runs before blocks are executed, a good place to calculate un connected inputs.
    Although, blocks are a preferred mechanism, this capacity is just to
    test the ability to update inputs and read state externally
    :return:
    """

    run_states = tuple(map(lambda x: x.running, system_pumps))

    total_running = sum(run_states)
    if total_running in [1, 3]:
        first_not_running = system_pumps[run_states.index(False)]
        first_not_running.cmd_start = True


# # noinspection PyCallingNonCallable
# def scan_test():
#     pump2()
#     pump3()
#     pumps2_3_running()
#     pump1()
#
#
# def time_tests():
#     import timeit
#     connections()
#
#     # Original code
#     # It's only the scan performance that matters for different connection patterns
#     # scan_duration_a = timeit.timeit('scan_test_rev_a()', globals=globals(), number=1000000)
#     # print('Test A: %0.2fus avg per execution' % scan_duration_a)
#     # This is consistently 12 to 15us
#
#     # Updated code
#     # scan_duration_b = timeit.timeit('scan_test_rev_b()', globals=globals(), number=1000000)
#     # print('Test B: %0.2fus avg per execution' % scan_duration_b)
#     # This is 8 to 10 us
#
#     # Current Code
#     scan_duration = timeit.timeit('scan_test()', globals=globals(), number=1000000)
#     print('Test Now: %0.2fus avg per execution' % scan_duration)
#     # Down to 5us, not intentionally
#     # 11us after adding the get/set mehods for SelfProxy.  I'm sure we still had self
#     # proxy when achieving 5us.
#
#
# time_tests()

if __name__ == "__main__":
    pass
