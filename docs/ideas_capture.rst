=============
Ideas Capture
=============


Security Tools
--------------

Develop Scripts and a basic web / configuration / launch interface and progress tracking for the following:

* Create a secrets USB stick (password protected or not)
* Generate or save a secret to the stick
* Generate a CA on the stick
* Clone a stick
* Distribute a secret
* Online integration for easy "killswitch" URL
