

def ideal_gas_law(
        P1=None,
        P2=None,
        V1=None,
        V2=None,
        T1=None,
        T2=None,
        ref_pressure = None
):
    """
    Given any of the optional parameters, this equation will return what it can from the ideal gas law written as:
    P1/P2 = V1/V2 = T1/T2
    :param P1: Absolute Pressure
    :param P2: Absolute Pressure
    :param V1: Volume
    :param V2: Volume
    :param T1: Absolute Temperature
    :param T2: Absolute Temperature
    :param ref_pressure: If the given pressures are not absolute, then this is used to convert them.
    :return: All parameters known and that are able to be calculated (P1 through T2)
    """

    # Validate units
    if P1.unit.category != 'Absolute Pressure':
        assert ref_pressure, 'A reference pressure is needed'

    # This is getting too heavy, maybe we should be less magical about units.

    # and then have them cary through to the output parameters

    return {
        'P1': P1,
        'P2': P2,
        'V1': V1,
        'V2': V2,
        'T1': T1,
        'T2': T2,
    }
# EOF

# Unit testing
