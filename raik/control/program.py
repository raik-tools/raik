from typing import List, Dict, TextIO, Tuple
from types import ModuleType
import os
import psutil
import json
import importlib
import pathlib
import tempfile
import stat
import dataclasses

from .function_block import FunctionBlock, ExternalBlock
from .program_enums import PROGRAM_SCOPE


@dataclasses.dataclass
class ProgramConfig:
    name: str
    scope: str
    blocks: Dict[str, str]
    module: ModuleType
    temp_file: TextIO = None

    def save_run_file(self):
        scope = self.scope
        program_name = self.name

        if scope == PROGRAM_SCOPE.private:
            temp_file = (
                    pathlib.Path(
                        tempfile.gettempdir()) / f'RAIK_PROGRAM-{program_name}-{os.getpid()}'
            )
            with temp_file.open('w'):
                pass
            temp_file.chmod(stat.S_IRUSR | stat.S_IWUSR)

        elif scope == PROGRAM_SCOPE.user:
            temp_file = (
                    pathlib.Path(
                        tempfile.gettempdir()) / f'RAIK_PROGRAM-{program_name}-u{os.getuid()}'
            )
            with temp_file.open('w'):
                pass
            temp_file.chmod(stat.S_IRUSR | stat.S_IWUSR)
        elif scope == PROGRAM_SCOPE.host:
            temp_file = (
                    pathlib.Path(tempfile.gettempdir()) / f'RAIK_PROGRAM-{program_name}-host'
            )
            with temp_file.open('w'):
                pass
            temp_file.chmod(
                stat.S_IRUSR | stat.S_IWUSR |
                stat.S_IRGRP |
                stat.S_IROTH
            )
        else:
            raise NotImplementedError('Scope %s not implemented' % scope)

        external_data = {
            'name': program_name,
            'scope': scope,
            'blocks': self.blocks
        }

        f = temp_file.open('w+')
        f.write(json.dumps(external_data))
        f.flush()
        self.temp_file = f

    def close_run_file(self):
        if self.temp_file:
            self.temp_file.close()
            os.remove(self.temp_file.name)
            self.temp_file = None


IN_PROCESS_PROGRAMS: Dict[str, ProgramConfig] = {}
IN_PROCESS_BLOCKS: Dict[str, FunctionBlock] = {}


def load_program(module_name: str) -> None:
    """
    Loads a program by:

    * Import the module
    * Load in external block proxies
    * Generate scope based communications for the declared function blocks
      (maybe once an external is registered then update the state?)
    * Some initial auditing of block ids
    * Set up the connections between blocks
    * Save the program state to file.

    This requires that the project lib has been loaded.
    """
    # Scope checking
    if module_name in IN_PROCESS_PROGRAMS:
        raise ValueError(f'Program {module_name} already loaded')

    for _name, _conf in list_programs().items():
        if _name == module_name:
            raise ValueError(
                f'Program {module_name} loaded '
                f'in the {_conf["scope"]} space'
            )
    existing_blocks = list_blocks()

    # Module and block loading
    program_module = importlib.import_module(f'control.{module_name}')
    mod_blocks = {}

    # Retrieve required program parameters
    scope = program_module.SCOPE
    mod_data = ProgramConfig(
        name=module_name,
        blocks=mod_blocks,
        scope=scope,
        module=program_module
    )

    # Look for function blocks
    blocks = {}
    for obj_name in dir(program_module):
        if obj_name[0] == '_':
            continue
        obj = getattr(program_module, obj_name)
        if isinstance(obj, FunctionBlock):
            obj.name = obj_name
            if obj_name in existing_blocks:
                raise ValueError(
                    f'Program {module_name} loading '
                    f'duplicate block {obj_name}'
                )
            obj.scope = scope
            blocks[obj_name] = obj
            mod_blocks[obj_name] = obj.__class__.__name__
        elif isinstance(obj, ExternalBlock):
            obj.name = obj_name
            # This will likely where we will connect the proxy block
            mod_blocks[obj_name] = '__proxy__'

    # Register module and blocks
    IN_PROCESS_PROGRAMS[module_name] = mod_data
    IN_PROCESS_BLOCKS.update(blocks)

    # Store program data
    mod_data.save_run_file()


def list_programs() -> Dict[str, dict]:
    """
    Kind of the opposite of load programs.  Finds / lists active programs on
    the system through their process IDs and data files.  Once
    programs have been found, it will then inspect the programs in redis and
    if they don't exist, send a signal to refresh their details.
    :return: A dictionary of visible program names with their config as value
    """
    program_list = {}
    for p in psutil.process_iter():
        try:
            open_files = p.open_files()
        except psutil.AccessDenied:
            continue
        if len(open_files) >= 1:
            for f in open_files:
                f_path = pathlib.Path(f.path)
                if f_path.name[:12] == 'RAIK_PROGRAM':
                    prefix, mod_name, scope_data = str(f_path.name).split('-')
                    # logger.debug('Process found: %i', p.pid)
                    with f_path.open('r') as a:
                        json_text = a.read()
                    program_list[mod_name] = json.loads(json_text)
    return program_list


def list_blocks() -> Dict[str, Tuple[str, str, List[str]]]:
    """
    Lists all available blocks within the currently available scope.  This will
    call list_programs first to ensure that redis is up to date.
    :return: Block names:  (parent program, block class, list of references)
    """
    blocks_data = {}
    for program_name, program_conf in list_programs().items():
        for block_name, block_type in program_conf['blocks'].items():
            block_data = blocks_data.setdefault(block_name, [None, None, []])
            if block_type == '__proxy__':
                block_data[2].append(program_name)
            else:
                assert block_data[0] is None
                # noinspection PyTypeChecker
                block_data[0] = program_name
                block_data[1] = block_type
    return blocks_data


def del_in_process_block(block_name: str) -> None:
    """
    Used to clean up blocks
    """
    block = IN_PROCESS_BLOCKS.get(block_name)
    if block:
        if isinstance(block, FunctionBlock):
            del IN_PROCESS_BLOCKS[block_name]
        elif isinstance(block, ExternalBlock):
            block.disconnect()
            del IN_PROCESS_BLOCKS[block_name]


def del_in_process_program(module_name: str):
    """
    Cleans up a program (which also cleans up it's contained blocks.
    Note that this doesn't end the process, just stops the execution and
    unloads the data.
    """
    conf = IN_PROCESS_PROGRAMS[module_name]
    conf.close_run_file()
    del IN_PROCESS_PROGRAMS[module_name]
    for block_name in conf.blocks:
        del_in_process_block(block_name)


def execute_program(program_name: str):
    """
    Assumes the program has been loaded previously.
    :param program_name:
    :return:
    """

    if program_name not in IN_PROCESS_PROGRAMS:
        raise RuntimeError(
            f'Program {program_name} is not loaded'
        )

    program_config = IN_PROCESS_PROGRAMS[program_name]

    # Determine scan order
    scan_order = getattr(program_config.module, 'SCAN_ORDER', [])
    if not scan_order:
        for block_name in program_config.blocks:
            block = IN_PROCESS_BLOCKS[block_name]
            scan_order.append(block)
        program_config.module.SCAN_ORDER = scan_order

    # Connect blocks if required
    connections_done = getattr(program_config.module, '_connections_done', False)
    if not connections_done:
        connections = getattr(program_config.module, 'connections', None)
        if connections:
            connections()
        program_config.module._connections_done = True

    # Perform pre_scan code
    pre_scan = getattr(program_config.module, 'pre_scan', None)
    if pre_scan:
        pre_scan()

    # Execute blocks
    for block in scan_order:
        block()


def get_program_state_str(program_name: str) -> str:
    if program_name not in IN_PROCESS_PROGRAMS:
        raise RuntimeError(
            f'Program {program_name} is not loaded'
        )

    program_config = IN_PROCESS_PROGRAMS[program_name]

    # Scan order
    scan_order = getattr(program_config.module, 'SCAN_ORDER', [])

    # Blocks string
    state_lines = sorted([str(block) for block in scan_order])
    return '\n'.join(state_lines)
