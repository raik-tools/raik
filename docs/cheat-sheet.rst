=====================================================
RAID Documentation Sphinx Style guide and Cheat Sheet
=====================================================

:Field Title: This is a field list
:Location: For templates, it appears at the start of the document under the title

Welcome to the documentation style guide and cheat sheet.  Ideally try to stick to the conventions shown in this doc.

.. contents:: Table of Contents
    :depth: 1
    :local:


Useful Links
============

* http://rest-sphinx-memo.readthedocs.io/en/latest/ReST.html

Document structure
==================

Summary
-------

* ``*.rst`` Each document is a single file representing a single chapter (for the time being)
* ``=`` with over-line, for chapters
* ``=``, for sections
* ``-``, for subsections
* ``^``, for subsubsections
* ``"``, for paragraphs

.. note::

    This differs to the python convention of using the pound (``*``) for chapters.

.. note::

    We're using the python section semantics but are not defining a parts section at this stage.  Maybe in the future.

Section in a document is level 2
==================================

Keep in mind that the document title is the level 1 at this stage.

Subsection in a document is level 3
-----------------------------------

Sub-subsection is 4
^^^^^^^^^^^^^^^^^^^

Not sure if we'll use paragraph
"""""""""""""""""""""""""""""""

And this is as deep as we'll go.  Each level down time is seven times slower, so be warned.


Research
--------

Important to note terminology for document structure:
* http://docutils.sourceforge.net/docs/user/rst/quickstart.html
* http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#sections
* https://devguide.python.org/documenting/#style-guide


Sphinx doesn't provide semantics to each level in the section hierarchy.  All it does is look for a change and track
the order oft he change in under/over lining.

Python uses the following semantics:
* ``#`` with overline, for parts
* ``*`` with overline, for chapters (in practice this can also be ``=``)
* ``=``, for sections
* ``-``, for subsections
* ``^``, for subsubsections
* ``"``, for paragraphs

Paragraph Markup
================

Indentation
-----------

    4 Space are used for indentation
    which needs to be consistent for a paragraph.


Inline Conventions
------------------

*italics*, **bold**, or ``monotype``.


Admonitions
-----------

.. note::

    You'll need to add an indent paragraph under a note.

.. warning::

    Note and warning are the most popular admonition.  We're not using others at the moment.

Lists
-----

You can flow from a paragraph to a list immediately:
* And we don't need white space
* between each point

    * Unless we're going to the next level
    * with a sub list

* As well as back again.

#. The same applies for
#. Numbered items.

Code Blocks
-----------

.. code-block:: python

    x = y + 1  # This is a pointless function.



.. _cross-references:

References
==========

URLs
----

Several styles, such as plain old links: http://plantuml.com/state-diagram

Or `with a text <http://plantuml.com/state-diagram>`_

Or my favourite, using `a link`_.

.. _a link: http://plantuml.com/state-diagram

Cross-References
----------------

To permanently link to a piece of text ensure to prefix it with a label and then it's as easy as
:ref:`cross-references`.

Document references directly - :doc:`/enhancements/005-control_functions`

To reference a python module:  TODO

The function :py:func:`spam` or simply :func:`spam`

The module :module:`spam`


Importing Self-Doc
------------------

This is about referencing code and extracting it's documentation using
auto-doc.

Diagrams
========

Diagrams support using `PlantUML`_:


.. uml ::

    [*] --> Backlog : Create
    Backlog --> Closed : Close
    Backlog --> Accepted : Put on radar

    Accepted --> InProgress : Start
    Accepted --> Closed : Close
    Accepted --> Backlog : Take off radar

    InProgress --> Backlog : Stop
    InProgress --> Accepted : Hold
    InProgress --> Closed : Close


    Closed --> [*]
    Closed --> Backlog : Reopen

    note right of Closed
        On close resolution is set to default resolutions
    end note

.. _PlantUML: http://plantuml.com


Tables
======

TODO