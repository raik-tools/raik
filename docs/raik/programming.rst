===================
Programming in RAIK
===================

Background
==========

RAIK's inception comes from work in the control system industry, specifically
in heavy industry and process automation (combining PLC and DCS projects).

As such, RAIK's is conceptually inspired by `IEC61131-3`_

.. _IEC61131-3: https://en.wikipedia.org/wiki/IEC_61131-3

Organisational Units in RAIK
============================

Project
-------

A collection of configuration including programs,
  schedules (tasks) and libraries for execution.

Program
-------

Developed functionally and are the first code to be
  executed.  These are the highest level programming construct.

Function
--------

The simplest code to be executed.  The hint here is
  "Function" in that it should follow a functional programming paradigm.

Function Blocks
---------------

Think of these as stateful functions.  These
  are implemented using classes.

Connections
^^^^^^^^^^^

Function blocks offer special properties which under the bonnet create
connections between blocks.  Note that although these connections can
be interacted with somewhat transparently, the only exception is that
the python "is" operator will fail to work when testing for True or
False values directly (ie pump.run is True will fail, you need to use
just pump.run and rely on it's truth value testing).