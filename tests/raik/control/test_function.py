"""
See control/__init__.py as well

This module focuses on function execution and interfaces.  Note that
function execution offers no challenging interface, the implementation
is that of normal python functions.
"""


# noinspection PyUnresolvedReferences
def test_import_project_lib_function(tmp_project):
    """
    Tests import and function execution from top level functions in
    the project lib
    """
    from project_lib import add_two

    assert 5 == add_two(3, 2)
